#include "smt/sat/solver/run.hpp"
#include "smt/sat/solver/offline/minisat.hpp"

using namespace unsot::smt::sat::solver;

int main(int argc, const char* argv[])
{
    return offline::Minisat::Run(argc, argv).run();
}
