#include "ode/solver/run.hpp"
#include "ode/solver/euler.hpp"

using namespace unsot::ode::solver;

int main(int argc, const char* argv[])
{
    return Euler::Run<Euler>(argc, argv).run();
}
