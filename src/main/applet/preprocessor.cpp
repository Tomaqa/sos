#include "expr/preprocess/run.hpp"

using namespace unsot::expr;

int main(int argc, const char* argv[])
{
    return Preprocess::Run(argc, argv).run();
}
