#include "solver/run.hpp"
#include "smt/solver/offline/bools/run.hpp"
#include "solver/offline/reals/equal/minisat/euler.hpp"

using namespace unsot::solver;
using namespace unsot::ode::solver;
namespace solver = unsot::solver;

int main(int argc, const char* argv[])
{
    using Solver = solver::offline::reals::equal::minisat::Solver<Euler>;
    return Solver::Run(argc, argv).run();
}
