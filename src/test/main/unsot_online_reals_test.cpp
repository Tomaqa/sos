#include "solver_test.hpp"
#include "solver/online/reals/minisat/euler.hpp"

#include "solver/run.hpp"
#include "smt/solver/online/reals/run.hpp"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace unsot::solver::test;
    using namespace std;
    namespace solver = unsot::solver;

    using Solver = solver::online::reals::minisat::Solver<ode::solver::Euler>;

    ////////////////////////////////////////////////////////////////

    Solver s;
    s.parse(Ball::input);

    Sat sat = s.solve();
    cout << endl;
    s.print_vars();

    check_smt<Ball>(s, sat);

    ////////////////////////////////////////////////////////////////

    Solver s2;
    Ball_file::parse(s2);
    check_smt<Ball_file>(s2, mode_to_sat(s2.cmode()));

    ////////////////////////////////////////////////////////////////

    Solver s3;
    Prostate_file::parse(s3);
    check_smt<Prostate_file>(s3, mode_to_sat(s3.cmode()));

    ////////////////////////////////////////////////////////////////

    Solver s4;
    Glucose_file::parse(s4);
    check_smt<Glucose_file>(s4, mode_to_sat(s4.cmode()));

    ////////////////////////////////////////////////////////////////

    Solver s5;
    Fun_file::parse(s5);
    check_smt<Fun_file>(s5, mode_to_sat(s5.cmode()));

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
