#include "test.hpp"
#include "expr/var_arith.hpp"

#include "util/alg.hpp"
#include "util/numeric/alg.hpp"
#include "util/view.hpp"

#include "expr/var.tpp"

namespace unsot::test {
    using namespace expr;
    using namespace expr::var;
    using namespace util::numeric;
    using var::Ptr;
    using unsot::to_string;

    template <typename B>
    struct Test_fun_mixin : Inherit<B, Test_fun_mixin<B>> {
        using Inherit = unsot::Inherit<B, Test_fun_mixin<B>>;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        static inline Ptrs* arg_ptrs_l{};

        using Inherit::carg_ptr;
        using Inherit::arg_ptr;
        const Ptr& carg_ptr(const Id& kid) const override
        {
            return (*arg_ptrs_l)[kid];
        }
        Ptr& arg_ptr(const Id& kid) override
        {
            return (*arg_ptrs_l)[kid];
        }
    };

    template <typename B>
    struct Test_auto_mixin : Inherit<B, Test_auto_mixin<B>> {
        using Inherit = unsot::Inherit<B, Test_auto_mixin<B>>;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        static inline Ptrs* equality_ptrs_l{};

        using Inherit::cassigner_ptr;
        using Inherit::assigner_ptr;
        const Ptr& cassigner_ptr(const Id& kid) const override
        {
            return (*equality_ptrs_l)[kid];
        }
        Ptr& assigner_ptr(const Id& kid) override
        {
            return (*equality_ptrs_l)[kid];
        }
    };

    static_assert(is_base_of_v<Var<Int>, Fun_base<Int>>);
    static_assert(is_base_of_v<Fun_base<Int, Conf, Var<Int>, expr::Pred<Int>>, Pred_base<Int, Conf, Var<Int>>>);
    static_assert(is_base_of_v<Var<Int>, Assignee<Int>>);

    static_assert(is_same_v<Fun<Int>, expr::Fun<Int>>);

    template <typename Sort, typename ConfT = Conf>
    using Test_fun = Test_fun_mixin<Fun_base<Sort, ConfT>>;
    template <typename Sort, typename ConfT = Conf>
    using Test_pred = Test_fun_mixin<Pred_base<Sort, ConfT>>;
    template <typename Sort, typename ConfT = Conf>
    using Test_equality = Equality<Test_pred<Sort, ConfT>, ConfT>;
    template <typename Sort, typename ConfT = Conf>
    using Test_auto_var = Test_auto_mixin<Auto_assignee_base<Sort, Test_equality<Sort, ConfT>, ConfT>>;

    static_assert(is_base_of_v<Fun_base<Int>, Test_fun<Int>>);
    static_assert(!is_base_of_v<Test_fun<Int>, Test_pred<Int>>);
    static_assert(is_base_of_v<Pred_base<Int>, Test_pred<Int>>);
    static_assert(is_base_of_v<Test_pred<Int>, Test_equality<Int>>);
    static_assert(is_base_of_v<Assignee<Int>, Test_auto_var<Int>>);
    static_assert(is_base_of_v<Auto_assignee_base<Int, Test_equality<Int>>, Test_auto_var<Int>>);

    ////////////////////////////////////////////////////////////////

    using Vars_type = int;
    constexpr Vars_type regular = (1 << 0);
    constexpr Vars_type assignee = (1 << 1);
    constexpr Vars_type auto_assign = (1 << 2);
    constexpr Vars_type assign_2 = (1 << 3);

    String type_to_string(const Vars_type& type)
    {
        String str;
        switch (type) {
        case regular: return str+"regular";
        case assignee: return str+"assign";
        case auto_assign: return str+"auto_assign";
        case assign_2: return str+"assign (2nd)";
        default: return str+"unknown";
        }
    }

    template <typename Sort, typename ArgSort = Sort, typename ConfT = Conf>
    struct Vars_input : Object<Vars_input<Sort, ArgSort>> {
        struct Unset_var : Object<Unset_var> {
            Unset_var(Key key_) : key(move(key_)) { }

            Key key;

            String to_string() const
            {
                using unsot::to_string;
                return to_string(key);
            }
        };
        struct Set_var : Object<Set_var> {
            Set_var(Key key_, Sort val = {})
                : key(move(key_)), value(move(val)) { }

            Key key;
            Sort value;

            String to_string() const
            {
                using unsot::to_string;
                return to_string(key) + " " + to_string(value);
            }
        };
        struct Unset_fun : Object<Unset_fun> {
            Unset_fun(Key key_, List ls_ = {})
                : key(move(key_)), ls(move(ls_)) { }

            Key key;
            List ls;

            String to_string() const
            {
                using unsot::to_string;
                return to_string(key) + " " + to_string(ls);
            }
        };
        struct Set_fun : Object<Set_fun> {
            Set_fun(Key key_, Sort val = {}, List ls_ = {})
                : key(move(key_)), value(move(val)), ls(move(ls_)) { }

            Key key;
            Sort value;
            List ls;

            String to_string() const
            {
                using unsot::to_string;
                return to_string(key) + " " + to_string(value) + " " + to_string(ls);
            }
        };

        Vector<Unset_var> unset_vars;
        Vector<Set_var> set_vars;
        Vector<Unset_fun> unset_funs;
        Vector<Set_fun> set_funs;

        Vars_type type;

        Vars_input(Vars_type type_,
                   Vector<Unset_var>&& unset_vars_ = {},
                   Vector<Set_var>&& set_vars_ = {},
                   Vector<Unset_fun>&& unset_funs_ = {},
                   Vector<Set_fun>&& set_funs_ = {})
            : unset_vars(move(unset_vars_)),
              set_vars(move(set_vars_)),
              unset_funs(move(unset_funs_)),
              set_funs(move(set_funs_)),
              type(type_)
        { }
        virtual ~Vars_input() = default;

        virtual size_t size() const
        {
            return unset_vars.size()
                 + set_vars.size()
                 + unset_funs.size()
                 + set_funs.size()
                 ;
        }

        virtual String to_string() const
        {
            using unsot::to_string;
            return type_to_string(type) + " type of vars:\n"
                 + to_string(unset_vars) + "\n"
                 + to_string(set_vars) + "\n"
                 + to_string(unset_funs) + "\n"
                 + to_string(set_funs);
        }

        using Var = expr::Var<Sort, ConfT>;
        using Assign_var = Assignee<Sort, ConfT>;
        using Auto_assign_var = Test_auto_var<Sort, ConfT>;

        using Fun = Test_fun<Sort, ConfT>;
        using Pred = Test_pred<ArgSort, ConfT>;
        using Equality = Test_equality<ArgSort, ConfT>;

        using Value = typename Var::Value;
        using Values = typename Var::Values;
        using Values_ptr = typename Var::Values_ptr;
        using Arg_values = typename Pred::Arg_values;
        using Arg_values_ptr = typename Pred::Arg_values_ptr;

        mutable Ptrs ptrs{};

        mutable Keys_ptr keys_ptr{new_keys()};
        mutable Values_ptr values_ptr{new_values<Sort>()};
        mutable Ids_map_ptr imap_ptr{new_ids_map()};

        mutable Ptrs* arg_ptrs_l{};
        mutable Ptrs arg_ptrs{};

        mutable Keys_ptr arg_keys_ptr{};
        mutable Arg_values_ptr arg_values_ptr{};
        mutable Ids_map_ptr arg_imap_ptr{};

        mutable Ptrs* pred_ptrs_l{};
        mutable Ptrs pred_ptrs{};

        using Args_input = Vars_input<ArgSort, ArgSort, ConfT>;
        mutable Args_input* args_input_l{};

        virtual Ptrs& cons_ptrs() const
        {
            if (!ptrs.empty()) {
                ptrs.clear();
                keys_ptr->clear();
                values_ptr->clear();
                imap_ptr->clear();
                arg_ptrs.clear();
                arg_keys_ptr->clear();
                arg_values_ptr->clear();
                arg_imap_ptr->clear();
                pred_ptrs.clear();
            }

            if (args_input_l) {
                if constexpr (is_same_v<Sort, Bool>) {
                    args_input_l->pred_ptrs_l = &ptrs;
                    arg_ptrs_l = &args_input_l->ptrs;
                    arg_keys_ptr = args_input_l->keys_ptr;
                    arg_values_ptr = args_input_l->values_ptr;
                    arg_imap_ptr = args_input_l->imap_ptr;
                    args_input_l->cons_ptrs();
                }
            }

            Fun::arg_ptrs_l = &ptrs;
            if constexpr (is_same_v<Sort, ArgSort>) {
                Pred::arg_ptrs_l = &ptrs;
                arg_keys_ptr = keys_ptr;
                arg_values_ptr = values_ptr;
                arg_imap_ptr = imap_ptr;
            }
            else {
                Pred::arg_ptrs_l = (arg_ptrs_l) ? arg_ptrs_l : &arg_ptrs;
                if (!arg_keys_ptr) arg_keys_ptr = new_keys();
                if (!arg_values_ptr) arg_values_ptr = new_values<ArgSort>();
                if (!arg_imap_ptr) arg_imap_ptr = new_ids_map();
            }

            if constexpr (is_same_v<Sort, Bool>) {
                Auto_assign_var::equality_ptrs_l = &ptrs;
            }
            else {
                Auto_assign_var::equality_ptrs_l = (pred_ptrs_l) ? pred_ptrs_l : &pred_ptrs;
            }

            for (auto& v : unset_vars) {
                ptrs.emplace_back(new_var(v.key));
            }
            for (auto& v : set_vars) {
                ptrs.emplace_back(new_var(v.key, true, v.value));
            }
            for (auto& v : unset_funs) {
                ptrs.emplace_back(Fun::new_me(keys_ptr, values_ptr, imap_ptr,
                                              Fun::Fun::cons(v.ls, keys_ptr, values_ptr, imap_ptr),
                                              v.key));
            }
            for (auto& v : set_funs) {
                ptrs.emplace_back(Fun::new_me(keys_ptr, values_ptr, imap_ptr,
                                              Fun::Fun::cons(v.ls, keys_ptr, values_ptr, imap_ptr),
                                              v.key, v.value));
            }

            return ptrs;
        }

        virtual Ptr new_var(Key key_, bool is_set = false, Value val = {}) const
        {
            using unsot::to_string;
            switch (type) {
            case regular: return is_set ? Var::new_me(keys_ptr, values_ptr, imap_ptr, move(key_), move(val))
                                        : Var::new_me(keys_ptr, values_ptr, imap_ptr, move(key_));
            case assignee: case assign_2: return is_set ? Assign_var::new_me(keys_ptr, values_ptr, imap_ptr, move(key_), move(val))
                                                        : Assign_var::new_me(keys_ptr, values_ptr, imap_ptr, move(key_));
            case auto_assign: return is_set ? Auto_assign_var::new_me(keys_ptr, values_ptr, imap_ptr, move(key_), move(val))
                                            : Auto_assign_var::new_me(keys_ptr, values_ptr, imap_ptr, move(key_));
            default: THROW("Unknown type of vars demanded: ") + to_string(type);
            }
        }
    };

    template <typename ArgSort = Bool, typename ConfT = Conf>
    struct Bools_input : Inherit<Vars_input<Bool, ArgSort, ConfT>> {
        using Inherit = unsot::Inherit<Vars_input<Bool, ArgSort, ConfT>>;

        using typename Inherit::Unset_var;
        using typename Inherit::Set_var;
        using typename Inherit::Unset_fun;
        using typename Inherit::Set_fun;

        using Unset_pred = Unset_fun;
        using Set_pred = Set_fun;
        using Unset_equality = Unset_pred;
        using Set_equality = Set_pred;

        Vector<Unset_pred> unset_preds;
        Vector<Set_pred> set_preds;
        Vector<Unset_equality> unset_equalities;
        Vector<Set_equality> set_equalities;

        Bools_input(Vars_type type_,
                    Vector<Unset_var>&& unset_vars_ = {},
                    Vector<Set_var>&& set_vars_ = {},
                    Vector<Unset_fun>&& unset_funs_ = {},
                    Vector<Set_fun>&& set_funs_ = {},
                    Vector<Unset_pred>&& unset_preds_ = {},
                    Vector<Set_pred>&& set_preds_ = {},
                    Vector<Unset_equality>&& unset_equalities_ = {},
                    Vector<Set_equality>&& set_equalities_ = {})
            : Inherit(type_,
                      move(unset_vars_),
                      move(set_vars_),
                      move(unset_funs_),
                      move(set_funs_)),
              unset_preds(move(unset_preds_)),
              set_preds(move(set_preds_)),
              unset_equalities(move(unset_equalities_)),
              set_equalities(move(set_equalities_))
        { }
        virtual ~Bools_input() = default;

        size_t size() const override
        {
            return Inherit::size()
                 + unset_preds.size()
                 + set_preds.size()
                 + unset_equalities.size()
                 + set_equalities.size()
                 ;
        }

        String to_string() const override
        {
            using unsot::to_string;
            return Inherit::to_string() + "\n"
                 + to_string(unset_preds) + "\n"
                 + to_string(set_preds) + "\n"
                 + to_string(unset_equalities) + "\n"
                 + to_string(set_equalities);
        }

        using typename Inherit::Fun;
        using typename Inherit::Pred;
        using typename Inherit::Equality;

        Ptrs& cons_ptrs() const override
        {
            Inherit::cons_ptrs();

            for (auto& v : unset_preds) {
                this->ptrs.emplace_back(Pred::new_me(this->keys_ptr, this->values_ptr, this->imap_ptr,
                                                     Pred::Fun::cons(v.ls, this->arg_keys_ptr, this->arg_values_ptr, this->arg_imap_ptr),
                                                     v.key));
            }
            for (auto& v : set_preds) {
                this->ptrs.emplace_back(Pred::new_me(this->keys_ptr, this->values_ptr, this->imap_ptr,
                                                     Pred::Fun::cons(v.ls, this->arg_keys_ptr, this->arg_values_ptr, this->arg_imap_ptr),
                                                     v.key, v.value));
            }
            for (auto& v : unset_equalities) {
                this->ptrs.emplace_back(Equality::new_me(this->keys_ptr, this->values_ptr, this->imap_ptr,
                                                         Equality::Fun::cons(v.ls, this->arg_keys_ptr, this->arg_values_ptr, this->arg_imap_ptr),
                                                         v.key));
            }
            for (auto& v : set_equalities) {
                this->ptrs.emplace_back(Equality::new_me(this->keys_ptr, this->values_ptr, this->imap_ptr,
                                                         Equality::Fun::cons(v.ls, this->arg_keys_ptr, this->arg_values_ptr, this->arg_imap_ptr),
                                                         v.key, v.value));
            }

            return this->ptrs;
        }
    };

    template <typename Sort, typename ArgSort = Sort, typename ConfT = Conf>
        using Vars_input_ptr = Shared_ptr<Vars_input<Sort, ArgSort, ConfT>>;

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort = Sort, typename ConfT = Conf>
    struct Vars_case : Inherit<Void_case<Vars_input_ptr<Sort, ArgSort, ConfT>>> {
        using Inherit = unsot::Inherit<Void_case<Vars_input_ptr<Sort, ArgSort, ConfT>>>;

        using Inherit::Inherit;

        using typename Inherit::Params;

        using Bools_input = test::Bools_input<ArgSort, ConfT>;

        using Var = typename Params::element_type::Var;
        using Fun = typename Params::element_type::Fun;

        void do_stuff() override
        {
            Ptrs& ptrs = this->cparams()->cons_ptrs();

            auto bools_ptr = dynamic_cast<Bools_input*>(this->params().get());

            if (!this->should_throw()) {
                cout << "[ " << this->cparams()->keys_ptr << "]{ " << ptrs << "}" << endl;
            }

            const auto eit_var_unset = begin(ptrs)+this->cparams()->unset_vars.size();
            expect(std::all_of(begin(ptrs), eit_var_unset,
                               [](auto& ptr){ return ptr->is_unset(); }),
                   "Vars without values are not unset.");
            const auto eit_var_set = eit_var_unset+this->cparams()->set_vars.size();
            expect(std::all_of(eit_var_unset, eit_var_set,
                               [](auto& ptr){ return ptr->is_set(); }),
                   "Vars with values are not set.");
            const auto eit_fun_unset = eit_var_set+this->cparams()->unset_funs.size();
            expect(std::all_of(eit_var_set, eit_fun_unset,
                               [](auto& ptr){ return ptr->is_unset(); }),
                   "Funs without values are not unset.");
            const auto eit_fun_set = eit_fun_unset+this->cparams()->set_funs.size();
            expect(std::all_of(eit_fun_unset, eit_fun_set,
                               [](auto& ptr){ return ptr->is_set(); }),
                   "Funs with values are not set.");
            if (bools_ptr) {
                const auto eit_pred_unset = eit_fun_set+bools_ptr->unset_preds.size();
                expect(std::all_of(eit_fun_set, eit_pred_unset,
                                   [](auto& ptr){ return ptr->is_unset(); }),
                       "Preds without values are not unset.");
                const auto eit_pred_set = eit_pred_unset+bools_ptr->set_preds.size();
                expect(std::all_of(eit_pred_unset, eit_pred_set,
                                   [](auto& ptr){ return ptr->is_set(); }),
                       "Preds with values are not set.");
                const auto eit_equality_unset = eit_pred_set+bools_ptr->unset_equalities.size();
                expect(std::all_of(eit_pred_set, eit_equality_unset,
                                   [](auto& ptr){ return ptr->is_unset(); }),
                       "Equalities without values are not unset.");
                const auto eit_equality_set = eit_equality_unset+bools_ptr->set_equalities.size();
                expect(std::all_of(eit_equality_unset, eit_equality_set,
                                   [](auto& ptr){ return ptr->is_set(); }),
                       "Equalities with values are not set.");
            }

            const Sort val{};
            const Sort val2{val+1};
            const Sort val3{val2+1};
            for (auto& ptr : ptrs) Var::cast(ptr).set(val);
            for (auto& ptr : ptrs) {
                expect(ptr->is_set() && Var::cast(ptr).get() == val,
                       "Var not set after setting: "s + to_string(ptr));
            }
            for (auto& ptr : ptrs) ptr->unset();
            for (auto& ptr : ptrs) {
                bool throwed = false;
                try {
                   Var::cast(ptr).get();
                }
                catch (const Error&) {
                   throwed = true;
                }
                expect(ptr->is_unset() && throwed,
                       "Var not unset after unsetting or exception not thrown: "s
                       + to_string(ptr));
            }

            for (auto& ptr : ptrs) ptr->lock();
            for (auto& ptr : ptrs) Var::cast(ptr).set(val2);
            for (auto& ptr : ptrs) {
                expect(ptr->is_unset(),
                       "Var not unset after locking and setting it: "s
                       + to_string(ptr));
            }
            for (auto& ptr : ptrs) ptr->unlock();
            for (auto& ptr : ptrs) {
                expect(ptr->is_unset(),
                       "Var not unset after unlocking but not setting it: "s
                       + to_string(ptr));
            }
            for (auto& ptr : ptrs) Var::cast(ptr).set(val3);
            for (auto& ptr : ptrs) {
                expect(ptr->is_set() && Var::cast(ptr).get() == val3,
                       "Var not set after unlocking and setting: "s + to_string(ptr));
            }
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort = Sort, typename ConfT = Conf>
    struct Vars_evaluable_case : Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Values<bool>>> {
        using Inherit = unsot::Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Values<bool>>>;

        using typename Inherit::Output;
        using typename Inherit::Input;
        using typename Inherit::Cons;

        using Inherit::Inherit;

        using Var = typename Input::element_type::Var;
        using Equality = typename Input::element_type::Equality;

        Cons ccons() const override
        {
            return this->cinput()->cons_ptrs();
        }

        Output result() override
        {
            Cons ptrs(ccons());

            if (!this->should_throw()) {
                cout << "[ " << this->cinput()->keys_ptr << "] " << type_to_string(this->cinput()->type) << endl;
            }

            int i = 0;

            auto was_set_map = to<Mask>(ptrs, [](auto& ptr){
                return ptr->is_set();
            });

            if (this->cinput()->type == assign_2) {
                for (auto& ptr : ptrs) Var::cast(ptr).try_compute();
            }

            Output out = to<Output>(ptrs, [](auto& ptr){
                ptr->evaluable();
                return ptr->evaluable().is_true();
            });

            auto set_test_f = [&](Sort value, bool not_vars = false){
                for_each(ptrs, cbegin(was_set_map),
                         [value](auto& ptr, bool was_set){
                    if (ptr->is_var() == was_set) {
                        if (Equality::is_me(ptr) && !value) return;
                        Var::cast(ptr) = value;
                    }
                });
                for (auto& ptr : ptrs) {
                    if (!not_vars || !ptr->is_var()) ptr->computable();
                }
                Output out2 = to<Output>(ptrs, cbegin(was_set_map),
                                         [](auto& ptr, bool was_set){
                    if (was_set) return true;
                    return ptr->computable().is_true();
                });
                /// "after setting to 0" - except of equalities which would be falsified
                expect(out == out2,
                       "Test #"s + to_string(i) + ": results of evaluable and computable (after setting* to "s + to_string(value) + ") differ:\n"s
                       + to_string(out) + "\n!=\n" + to_string(out2));
            };

            auto unset_test_f = [&](bool computable, bool not_vars = false){
                for_each(ptrs, cbegin(was_set_map), [](auto& ptr, bool was_set){
                    if (!was_set) ptr->unset();
                });
                for (auto& ptr : ptrs) {
                    if (!not_vars || !ptr->is_var()) {
                        if (computable) ptr->computable();
                        else ptr->evaluable();
                    }
                }
                Output out2 = to<Output>(ptrs, cbegin(was_set_map),
                                         [computable](auto& ptr, bool was_set){
                    if (was_set) return true;
                    return computable ? ptr->computable().is_true()
                                      : ptr->evaluable().is_true();
                });
                expect(out == out2,
                       "Test #"s + to_string(i) + ": results of evaluable and "s + (computable ? "computable" : "evaluable") + " (after unsetting) differ:\n"s
                       + to_string(out) + "\n!=\n" + to_string(out2));
            };

            if constexpr (is_same_v<Sort, Bool>) {
                if (this->cinput()->type != assign_2) {
                    for (Bool val = 0; val <= 1; ++val) {
                        for (int not_vars = 0; not_vars <= 1; ++not_vars) {
                            ++i; set_test_f(val, not_vars);
                            ++i; unset_test_f(false, not_vars);
                            ++i; set_test_f(val, not_vars);
                            ++i; unset_test_f(true, not_vars);
                            ++i; set_test_f(val, not_vars);
                            ++i; unset_test_f(false, not_vars);
                            ++i; unset_test_f(true, not_vars);
                            ++i; set_test_f(val, not_vars);
                            ++i; unset_test_f(true, not_vars);
                        }
                    }
                }
            }

            return out;
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort = Sort, typename ConfT = Conf>
    struct Vars_assignable_case : Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Vector<Values<bool>>>> {
        using Inherit = unsot::Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Vector<Values<bool>>>>;

        using typename Inherit::Output;
        using typename Inherit::Input;
        using typename Inherit::Cons;

        using Equality = typename Input::element_type::Equality;

        using Inherit::Inherit;

        Cons ccons() const override
        {
            return this->cinput()->cons_ptrs();
        }

        bool condition(const Output& expected_, const Output& result_) const override
        {
            auto& res = result_.front();
            return any_of(expected_, [&res](auto& vals){
                return vals == res;
            });
        }

        Output result() override
        {
            Cons ptrs(ccons());

            if (!this->should_throw()) {
                cout << "[ " << this->cinput()->keys_ptr << "] " << type_to_string(this->cinput()->type) << endl;
            }

            auto out_f = [](auto& ptrs_, auto& out, int count = 1, bool pre_computable = false, bool post_computable = false){
                out.clear();
                for_each_if(ptrs_, Equality::is_me, [&](auto& ptr){
                    auto& eq = Equality::cast(ptr);
                    Flag ass;
                    for (int i = 1; i <= count; ++i) {
                        if (pre_computable) eq.computable();
                        ass = eq.any_assignable();
                        if (post_computable) eq.computable();
                    }
                    out.push_back(ass.is_true());
                });
            };

            Output out;
            out.resize(1);
            auto& out1 = out.front();
            out_f(ptrs, out1);

            typename Output::value_type out2;
            for (int cp = 0; cp <= 1; ++cp) {
                for (int i = 1; i <= 3; ++i) {
                    for (int pre = 0; pre <= 1; ++pre) {
                        for (int post = 0; post <= 1; ++post) {
                            if (cp) {
                                Cons new_ptrs(ccons());
                                out_f(new_ptrs, out2, i, pre, post);
                            }
                            else out_f(ptrs, out2, i, pre, post);
                            expect(out2 == out1,
                                   "Results differ ("s
                                   + (cp ? " new_ptrs" : "")
                                   + " i=" + to_string(i)
                                   + " pre_computable:" + to_string(pre)
                                   + " post_computable:" + to_string(post)
                                   + " ): " + to_string(out1) + " != " + to_string(out2));
                        }
                    }
                }
            }

            if (this->cinput()->type == auto_assign) return out;

            Mask indep_mask;
            View<Cons> view(ptrs);
            for_each_if(ptrs, Equality::is_me, [&indep_mask, &view](auto& ptr){
                indep_mask.push_back(ptr->independent().is_true());
                view.push_back(&ptr);
            });
            for_each_if(ptrs, mem_fn(&Base::is_var), [](auto& ptr){
                ptr->unset();
            });
            typename Output::value_type unset_out;
            out_f(ptrs, unset_out);
            assert(size(out1) == size(unset_out));
            assert(size(unset_out) == size(indep_mask));
            for (Idx i = 0; i < int(size(out1)); ++i) {
                if (masked(indep_mask, i)) continue;
                const bool is_ass = out1[i];
                if (!is_ass) continue;
                const bool is_ass_after_unset = unset_out[i];
                expect(is_ass != is_ass_after_unset,
                       "Assignability did not change after unsetting variables at "s
                       + view[i]->to_string() + ": expected: " + to_string(!is_ass_after_unset)
                       + ", got: " + to_string(is_ass_after_unset));
            }

            return out;
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort = Sort, typename ConfT = Conf, typename OutSort = Sort>
    struct Vars_eval_case : Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Vector<Values<Optional<OutSort>>>>> {
        using Inherit = unsot::Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Vector<Values<Optional<OutSort>>>>>;

        using typename Inherit::Output;
        using typename Inherit::Input;
        using typename Inherit::Cons;

        using Inherit::Inherit;

        using Args_input = typename Input::element_type::Args_input;

        using Var = typename Input::element_type::Var;
        using Equality = typename Input::element_type::Equality;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            auto& res = result_.front();
            return any_of(expected_, [&res](auto& vals){
                return apx_equal(vals, res);
            });
        }

        Cons ccons() const override
        {
            return this->cinput()->cons_ptrs();
        }

        Output result() override
        {
            static const auto any_ass_f = [](auto& ptr) -> Flag {
                if (!Equality::is_me(ptr)) return false;
                return Equality::cast(ptr).any_assignable();
            };

            auto get_f = [this](auto& ptrs) -> Output {
                auto& ptrs_ = is_same_v<Sort, OutSort> ? ptrs
                            : this->cinput()->args_input_l->ptrs;
                return {to<typename Output::value_type>(ptrs_, [](auto& ptr){
                    if constexpr (is_same_v<Sort, OutSort>)
                        return Var::cast(ptr).try_get();
                    else return Args_input::Var::cast(ptr).try_get();
                })};
            };

            auto eval_f = [this, &get_f](auto& ptrs, int out_count = 1, int in_count = 1,
                                         int pre_in_count = 1, int post_in_count = 1,
                                         int pre_unset_out_count = 0,
                                         int pre_evaluable_ocnt = 0, int pre_computable_ocnt = 0, int pre_any_ass_ocnt = 0,
                                         int pre_evaluable_icnt = 0, int post_evaluable_icnt = 0,
                                         int pre_computable_icnt = 0, int post_computable_icnt = 0,
                                         int pre_any_ass_icnt = 0, int post_any_ass_icnt = 0,
                                         bool non_vars_only = false,
                                         bool pre_pre_unset_in = false, bool post_pre_unset_in = false){
                Unique_view<Cons> unset_vw(ptrs);
                if (pre_unset_out_count > 0 || pre_pre_unset_in || post_pre_unset_in) for (auto& ptr : ptrs) {
                    if (ptr->is_set()) continue;
                    if (!Equality::is_me(ptr)) continue;
                    unset_vw.push_back(&ptr);
                }

                for (int u = 0; u <= pre_unset_out_count; ++u) {
                    if (u >= 1) for (auto& ptr : ptrs) if (unset_vw.contains(&ptr)) ptr->unset();
                    for (int i = 0; i < out_count; ++i) {
                        for (int j = 0; j < pre_evaluable_ocnt; ++j) for (auto& ptr : ptrs) ptr->evaluable();
                        for (int j = 0; j < pre_computable_ocnt; ++j) for (auto& ptr : ptrs) ptr->computable();
                        for (int j = 0; j < pre_any_ass_ocnt; ++j) for (auto& ptr : ptrs) any_ass_f(ptr);
                        for (auto& ptr : ptrs) for (int j = 0; j < in_count; ++j) {
                            if (pre_pre_unset_in && unset_vw.contains(&ptr)) ptr->unset();
                            for (int l = 0; l < pre_in_count; ++l) {
                                for (int k = 0; k < pre_evaluable_icnt; ++k) ptr->evaluable();
                                for (int k = 0; k < pre_computable_icnt; ++k) ptr->computable();
                                for (int k = 0; k < pre_any_ass_icnt; ++k) any_ass_f(ptr);
                            }
                            if (post_pre_unset_in && unset_vw.contains(&ptr)) ptr->unset();
                            if (non_vars_only) {
                                if (ptr->is_var()) continue;
                                if (this->cinput()->type == auto_assign)
                                    if (any_ass_f(ptr)) {
                                        Var::cast(ptr).try_compute();
                                        continue;
                                    }
                            }
                            Var::cast(ptr).try_eval();
                            for (int l = 0; l < post_in_count; ++l) {
                                for (int k = 0; k < post_evaluable_icnt; ++k) ptr->evaluable();
                                for (int k = 0; k < post_computable_icnt; ++k) ptr->computable();
                                for (int k = 0; k < post_any_ass_icnt; ++k) any_ass_f(ptr);
                            }
                        }
                    }
                }

                return get_f(ptrs);
            };

            Cons ptrs(ccons());
            if (!this->should_throw()) {
                cout << " [ " << this->cinput()->keys_ptr << "] " << type_to_string(this->cinput()->type) << endl;
            }
            Output out = eval_f(ptrs);

            if (this->ignored_result()) return out;
            auto& exp = this->cexpected();

            for (int out_count = 1; out_count <= 2; ++out_count)
            for (int in_count = 1; in_count <= 2; ++in_count)
            for (int pre_in_count = 1; pre_in_count <= 2; ++pre_in_count)
            for (int post_in_count = 1; post_in_count <= 1; ++post_in_count)
            for (int pre_unset_out_count = 0; pre_unset_out_count <= 1; ++pre_unset_out_count)
            for (int pre_evaluable_ocnt = 0; pre_evaluable_ocnt <= 1; ++pre_evaluable_ocnt)
            for (int pre_computable_ocnt = 0; pre_computable_ocnt <= 1; ++pre_computable_ocnt)
            for (int pre_any_ass_ocnt = 0; pre_any_ass_ocnt <= 1; ++pre_any_ass_ocnt)
            for (int pre_evaluable_icnt = 0; pre_evaluable_icnt <= 1; ++pre_evaluable_icnt)
            for (int post_evaluable_icnt = 0; post_evaluable_icnt <= 1; ++post_evaluable_icnt)
            for (int pre_computable_icnt = 0; pre_computable_icnt <= 1; ++pre_computable_icnt)
            for (int post_computable_icnt = 0; post_computable_icnt <= 1; ++post_computable_icnt)
            for (int pre_any_ass_icnt = 0; pre_any_ass_icnt <= 1; ++pre_any_ass_icnt)
            for (int post_any_ass_icnt = 0; post_any_ass_icnt <= 1; ++post_any_ass_icnt)
            for (int non_vars_only = 0; non_vars_only <= 1; ++non_vars_only)
            for (int pre_pre_unset_in = 0; pre_pre_unset_in <= 1; ++pre_pre_unset_in)
            for (int post_pre_unset_in = 0; post_pre_unset_in <= 1; ++post_pre_unset_in)
            {
                if (this->cinput()->type == auto_assign) {
                    //+ it does not work for auto_assigns with visits, but probably should?
                    if (pre_any_ass_ocnt > 0) continue;

                    //+ did not examine too much if it should or should not pass these cases
                    if (pre_pre_unset_in || post_pre_unset_in) continue;
                }

                Cons ptrs2(ccons());
                Output out2 = eval_f(ptrs2, out_count, in_count,
                                     pre_in_count, post_in_count,
                                     pre_unset_out_count,
                                     pre_evaluable_ocnt, pre_computable_ocnt, pre_any_ass_ocnt,
                                     pre_evaluable_icnt, post_evaluable_icnt,
                                     pre_computable_icnt, post_computable_icnt,
                                     pre_any_ass_icnt, post_any_ass_icnt,
                                     non_vars_only,
                                     pre_pre_unset_in, post_pre_unset_in);
                expect(condition(exp, out2),
                       "Result '"s + "out_count:" + to_string(out_count) + " in_count:" + to_string(in_count)
                       + " pre_in_count:" + to_string(pre_in_count) + " post_in_count:" + to_string(post_in_count)
                       + " pre_unset_out_count:" + to_string(pre_unset_out_count)
                       + " pre_evaluable_ocnt:" + to_string(pre_evaluable_ocnt) + " pre_computable_ocnt:" + to_string(pre_computable_ocnt) + " pre_any_ass_ocnt:" + to_string(pre_any_ass_ocnt)
                       + " pre_evaluable_icnt:" + to_string(pre_evaluable_icnt) + " post_evaluable_icnt:" + to_string(post_evaluable_icnt)
                       + " pre_computable_icnt:" + to_string(pre_computable_icnt) + " post_computable_icnt:" + to_string(post_computable_icnt)
                       + " pre_any_ass_icnt:" + to_string(pre_any_ass_icnt) + " post_any_ass_icnt:" + to_string(post_any_ass_icnt)
                       + " non_vars_only:" + to_string(non_vars_only)
                       + " pre_pre_unset_in:" + to_string(pre_pre_unset_in) + " post_pre_unset_in:" + to_string(post_pre_unset_in)
                       + "' does not fit the expected: "s + to_string(out2));
            }

            return out;
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename Sort>
    struct Vars_consistent_output {
        using first_type = Values<Flag>;
        using second_type = Values<Optional<Sort>>;
        first_type consistent{};
        second_type values{};
    };

    template <typename Sort>
    String to_string(const Vars_consistent_output<Sort>& rhs)
    {
        using unsot::to_string;
        return to_string(rhs.consistent) + "$ " + to_string(rhs.values);
    }

    template <typename Sort, typename ArgSort = Sort, typename ConfT = Conf, typename OutSort = Sort>
    struct Vars_consistent_case : Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Vars_consistent_output<OutSort>>> {
        using Inherit = unsot::Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Vars_consistent_output<OutSort>>>;

        using typename Inherit::Output;
        using typename Inherit::Input;
        using typename Inherit::Cons;

        using Inherit::Inherit;

        using Args_input = typename Input::element_type::Args_input;

        using Var = typename Input::element_type::Var;
        using Equality = typename Input::element_type::Equality;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return expected_.consistent == result_.consistent
                && apx_equal(expected_.values, result_.values);
        }

        Cons ccons() const override
        {
            return this->cinput()->cons_ptrs();
        }

        Output result() override
        {
            auto vals_f = [this](auto& ptrs){
                auto& ptrs_ = is_same_v<Sort, OutSort> ? ptrs
                            : this->cinput()->args_input_l->ptrs;
                return to<typename Output::second_type>(ptrs_, [](auto& ptr){
                    if constexpr (is_same_v<Sort, OutSort>)
                        return Var::cast(ptr).try_get();
                    else return Args_input::Var::cast(ptr).try_get();
                });
            };

            Cons ptrs(ccons());

            if (!this->should_throw()) {
                cout << "[ " << this->cinput()->keys_ptr << "] " << type_to_string(this->cinput()->type) << endl;
            }

            for (auto& ptr : ptrs) Var::cast(ptr).try_eval();
            for (auto& ptr : ptrs) ptr->consistent();
            auto consistent = to<typename Output::first_type>(ptrs, [](auto& ptr){
                return ptr->consistent();
            });
            Output out = {move(consistent), vals_f(ptrs)};
            Output out2;

            if (this->cinput()->type != auto_assign) {
                Cons ptrs1(ccons());
                auto vals = vals_f(ptrs1);
                for (auto& ptr : ptrs1) ptr->unset();
                for (auto& ptr : ptrs1) {
                    ptr->to_string();
                    if (!Equality::is_me(ptr)) continue;
                    Equality::cast(ptr).any_assignable();
                }
                for (Idx idx = 0; idx < ptrs1.size(); ++idx) {
                    if (vals[idx].invalid()) continue;
                    Var::cast(ptrs1[idx]).set(vals[idx].value());
                    ptrs1[idx].to_string();
                }
                for (auto& ptr : ptrs1) {
                    ptr->to_string();
                    if (Equality::is_me(ptr)) Equality::cast(ptr).any_assignable();
                    ptr->consistent();
                }
                for (auto& ptr : ptrs) Var::cast(ptr).try_eval();
                consistent = to<typename Output::first_type>(ptrs1, [](auto& ptr){
                    return ptr->consistent();
                });
                out2 = {move(consistent), vals_f(ptrs1)};
                expect(condition(out, out2),
                       "Result 'try_eval_all and 2x consistent_all' and 'unset_all, all(to_str, assign.), all(set, to_str), all(to_str, consist.), try_eval_all and consistent_all' differ:\n"s
                       + to_string(out) + "\n!=\n" + to_string(out2));
            }

            if (this->cinput()->type != auto_assign) return out;

            Cons ptrs2(ccons());
            for (auto& ptr : ptrs2) Var::cast(ptr).try_eval();
            consistent = to<typename Output::first_type>(ptrs2, [](auto& ptr){
                return ptr->consistent();
            });
            out2 = {move(consistent), vals_f(ptrs2)};
            expect(condition(out, out2),
                   "Result 'try_eval_all and 2x consistent_all' and 'try_eval_all, then consistent_all' differ:\n"s
                   + to_string(out) + "\n!=\n" + to_string(out2));

            Cons ptrs3(ccons());
            for (auto& ptr : ptrs3) {
                if (ptr->is_var()) continue;
                if (!Equality::is_me(ptr)) {
                    Var::cast(ptr).try_eval();
                    continue;
                }
                auto& equal = Equality::cast(ptr);
                if (!equal.any_assignable()) equal.try_eval();
                else equal.compute();
            }
            consistent = to<typename Output::first_type>(ptrs3, [](auto& ptr){
                return ptr->consistent();
            });
            out2 = {move(consistent), vals_f(ptrs3)};
            expect(condition(out, out2),
                   "Result 'try_eval_all and 2x consistent_all' and 'compute equals. and try_eval_all only non-vars' differ:\n"s
                   + to_string(out) + "\n!=\n" + to_string(out2));

            Cons ptrs4(ccons());
            consistent = to<typename Output::first_type>(ptrs4, [](auto& ptr){
                Var::cast(ptr).try_eval();
                return ptr->consistent();
            });
            out2 = {move(consistent), vals_f(ptrs4)};
            expect(condition(out, out2),
                   "Result 'try_eval_all and 2x consistent_all' and '(try_eval and consistent)-all' differ:\n"s
                   + to_string(out) + "\n!=\n" + to_string(out2));

            Values<Id> ids;
            Cons ptrs5(ccons());
            consistent = to<typename Output::first_type>(ptrs5, [&ids, cnt{-1}](auto& ptr) mutable {
                cnt++;
                if (ptr->is_var()) {
                    ids.push_back(cnt);
                    return unknown;
                }
                if (!Equality::is_me(ptr)) {
                    Var::cast(ptr).try_eval();
                    return ptr->consistent();
                }
                auto& equal = Equality::cast(ptr);
                if (!equal.any_assignable()) equal.try_eval();
                else equal.compute();
                return equal.consistent();
            });
            for (auto& id : ids) {
                consistent[id] = ptrs5[id]->consistent();
            }
            out2 = {move(consistent), vals_f(ptrs5)};
            expect(condition(out, out2),
                   "Result 'try_eval_all and 2x consistent_all' and '(compute equal. or try_eval only non-var and consistent)-all and then consistent_all vars-only' differ:\n"s
                   + to_string(out) + "\n!=\n" + to_string(out2));

            return out;
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort = Sort, typename ConfT = Conf>
    struct Vars_distance_case : Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Vector<Optional<Distance>>>> {
        using Inherit = unsot::Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort, ArgSort, ConfT>, Vector<Optional<Distance>>>>;

        using typename Inherit::Output;
        using typename Inherit::Input;
        using typename Inherit::Cons;

        using Inherit::Inherit;

        using Var = typename Input::element_type::Var;

        static_assert(conf::has_distance_v<ConfT>);

        Cons ccons() const override
        {
            return this->cinput()->cons_ptrs();
        }

        Output result() override
        {
            Cons ptrs(ccons());

            if (!this->should_throw()) {
                cout << "[ " << this->cinput()->keys_ptr << "] " << type_to_string(this->cinput()->type) << endl;
            }

            return to<Output>(ptrs, [](auto& ptr){
                Var::cast(ptr).try_eval();
                return Var::cast(ptr).eval_distance();
            });
        }
    };

    ////////////////////////////////////////////////////////////////

    //+  template <typename Fs, typename Sort = typename Fs::Value>
    //+  struct Vars_table_case : Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort>, bool>> {
    //+      using Inherit = unsot::Inherit<Cons_case<Ptrs&, Vars_input_ptr<Sort>, bool>>;

    //+      using typename Inherit::Output;
    //+      using typename Inherit::Cons;

    //+      using Inherit::Inherit;

    //+      Cons ccons() const override
    //+      {
    //+          return this->cinput()->cons_ptrs();
    //+      }

    //+      Output result() override
    //+      {
    //+          Cons ptrs(ccons());

    //+         return all_of(Fs::all_params, [&ptrs](auto&& params){
    //+             for (auto& ptr : ptrs) Fs::set(ptr, params);

    //+             return all_of(ptrs, [](auto& ptr){
    //+                 try {
    //+                     return Fs::table(Fs::cond1(ptr), Fs::cond2(ptr));
    //+                 }
    //+                 catch (Ignore) {
    //+                     return true;
    //+                 }
    //+             });
    //+         });
    //+     }
    //+ };

    //+ template <typename Sort>
    //+ struct S_cd {
    //+     using Value = Sort;
    //+     using Left = bool;
    //+     using Right = bool;
    //+     using Params = Left;
    //+     static inline const Vector<Params> all_params{false, true};
    //+     static Left cond1(const Var_ptr<Value>& ptr)
    //+     {
    //+         return ptr->is_set();
    //+     }
    //+     static Right cond2(const Var_ptr<Value>& ptr)
    //+     {
    //+         return ptr->computed();
    //+     }
    //+     static void set(Var_ptr<Value>& ptr, Params params)
    //+     {
    //+         if (params) {
    //+             Var::cast(ptr).set({});
    //+             ptr->computed() = true;
    //+         }
    //+         else ptr->computed() = false;
    //+     }
    //+     static bool table(Left s, Right cd)
    //+     {
    //+         if (!s && cd) return false;
    //+         return true;
    //+     };
    //+ };

    //+ template <typename Sort>
    //+ struct Sc_c {
    //+     using Value = Sort;
    //+     using Left = Flag;
    //+     using Right = Flag;
    //+     using Params = Left;
    //+     static inline const Vector<Params> all_params{unknown, false, true};
    //+     static Left cond1(const Var_ptr<Value>& ptr)
    //+     {
    //+         return ptr->surely_computable();
    //+     }
    //+     static Right cond2(const Var_ptr<Value>& ptr)
    //+     {
    //+         return ptr->computable();
    //+     }
    //+     static void set(Var_ptr<Value>& ptr, Params params)
    //+     {
    //+         if (params.unknown()) return;
    //+         if (params.is_true()) ptr->computable_flag() = true;
    //+         else ptr->computable_flag() = false;
    //+     }
    //+     static bool table(Left sc, Right c)
    //+     {
    //+         if (sc.unknown()) return true;
    //+         return sc == c;
    //+     };
    //+ };

    //+ template <typename Sort>
    //+ struct C_e : Inherit<Sc_c<Sort>> {
    //+     using Inherit = unsot::Inherit<Sc_c<Sort>>;
    //+     using typename Inherit::Value;
    //+     using typename Inherit::Left;
    //+     using typename Inherit::Right;
    //+     static Left cond1(const Var_ptr<Value>& ptr)
    //+     {
    //+         return ptr->computable();
    //+     }
    //+     static Right cond2(const Var_ptr<Value>& ptr)
    //+     {
    //+         return ptr->evaluable();
    //+     }
    //+     static bool table(Left c, Right e)
    //+     {
    //+         if (c.unknown()) return true;
    //+         if (e.is_true()) return true;
    //+         if (c.is_false() && e.is_false()) return true;
    //+         if (c.valid() && e.unknown()) return false;
    //+         return false;
    //+     };
    //+ };

    //+ template <typename Sort>
    //+ struct S_e : Inherit<C_e<Sort>> {
    //+     using Inherit = unsot::Inherit<C_e<Sort>>;
    //+     using typename Inherit::Value;
    //+     using typename Inherit::Right;
    //+     using Left = Optional<Value>;
    //+     using Params = Ignore;
    //+     static inline const Vector<Params> all_params;
    //+     static Left cond1(const Var_ptr<Value>& ptr)
    //+     {
    //+         return Var::cast(ptr).try_get();
    //+     }
    //+     static void set(Var_ptr<Value>& /*ptr*/, Params /*params*/)
    //+     { }
    //+     static bool table(Left s, Right e)
    //+     {
    //+         if (s.invalid()) return true;
    //+         if (e.is_true()) return true;
    //+         return false;
    //+     };
    //+ };

    //+ struct S_a : Inherit<S_e<Bool>> {
    //+     using Inherit = unsot::Inherit<S_e<Bool>>;
    //+     using typename Inherit::Value;
    //+     using typename Inherit::Left;
    //+     using Right = Flag;
    //+     static Right cond2(const Var_ptr<Value>& ptr)
    //+     {
    //+         if (!Test_equality<Bool>::is_me(ptr)) throw ignore;
    //+         return Test_equality<Bool>::cast(ptr).any_assignable();
    //+     }
    //+     static bool table(Left s, Right a)
    //+     {
    //+         if (s.invalid()) return true;
    //+         if (!s.cvalue()) return !a;
    //+         return true;
    //+     };
    //+ };

    //+ struct Cd_a : Inherit<S_a>, S_cd<Bool> {
    //+     using Inherit = unsot::Inherit<S_a>;
    //+     using Inherit2 = S_cd<Bool>;
    //+     using typename Inherit::Value;
    //+     using typename Inherit::Left;
    //+     using typename Inherit::Right;
    //+     using typename Inherit2::Params;
    //+     using Inherit2::all_params;
    //+     static Left cond1(const Var_ptr<Value>& ptr)
    //+     {
    //+         if (!ptr->is_set() || !ptr->computed()) return {};
    //+         return Var::cast(ptr).try_get();
    //+     }
    //+     using Inherit::cond2;
    //+     using Inherit2::set;
    //+     static bool table(Left cd, Right a)
    //+     {
    //+         if (cd.invalid()) return true;
    //+         return !a;
    //+     };
    //+ };

    //+ struct E_a : Inherit<Cd_a> {
    //+     using Inherit = unsot::Inherit<Cd_a>;
    //+     using typename Inherit::Value;
    //+     using Left = Flag;
    //+     using typename Inherit::Right;
    //+     static Left cond1(const Var_ptr<Value>& ptr)
    //+     {
    //+         return ptr->evaluable();
    //+     }
    //+     static bool table(Left e, Right a)
    //+     {
    //+         if (a) return e.is_true();
    //+         return true;
    //+     };
    //+ };
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;
    using unsot::ignore;

    using B = Bools_input<>;
    using Bf = Bools_input<Bool, Full_conf>;
    using R = Vars_input<Real>;
    using Rf = Vars_input<Real, Real, Full_conf>;
    using PR = Bools_input<Real>;
    using PRf = Bools_input<Real, Full_conf>;

    ////////////////////////////////////////////////////////////////

    const String vars_msg = "constructing of Var-s";
    Suite(vars_msg + "<Bool>").test<Vars_case<Bool>>({
        { new B{regular}                                                    },
        { new B{assignee}                                                   },
        { new B{auto_assign}                                                },
        { new B{regular, {}}                                                },
        { new B{regular, {}, {}}                                            },
        { new B{regular, {}, {}, {}}                                        },
        { new B{regular, {}, {}, {}, {}}                                    },
        { new B{regular, {}, {}, {}, {}, {}}                                },
        { new B{regular, {}, {}, {}, {}, {}, {}}                            },
        { new B{regular, {}, {}, {}, {}, {}, {}, {}}                        },
        { new B{regular, {}, {}, {}, {}, {}, {}, {}, {}}                    },
        { new B{regular, {{"a"}} }                                          },
        { new B{regular, {{"a"}, {"b"}} }                                   },
        { new B{regular, {{"a"}, {"b"}}, {{"x", true}} }                    },
        { new B{regular, {{"a"}, {"b"}}, {{"x", true}, {"y", false}} }      },
        { new B{regular, {{"a"}}, {}, {{"f", "- a"}} }                      },
        { new B{regular, {{"a"}}, {}, {{"f1", "log a"}, {"f2", "abs 2"}} }    },
        { new B{regular, {{"a"}}, {}, {{"f1", "log a"}, {"f2", "abs f1"}} }   },
        { new B{regular, {{"a"}}, {}, {{"f1", "+ a"}}, {{"f2", true, "not f1"}} }  },
        { new B{regular, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {{"p2", false, "and p1"}} } },
        { new B{assignee, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {{"p2", false, "and p1"}}, {{"e1", "= a 2"}} } },
        { new B{assignee, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {}, {{"e1", "= f1 2"}} } },
        { new B{assignee, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {}, {{"e1", "= f1 p1"}} } },
        { new B{assignee, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {{"p2", false, "= f1 a"}}, {{"e1", "= a p2"}}, {{"e2", true, "= e1 a"}} } },
        { new B{auto_assign, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {{"p2", false, "and p1"}}, {{"e1", "= a 2"}} } },
        { new B{auto_assign, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {{"p2", false, "= f1 a"}}, {{"e1", "= a p2"}}, {{"e2", true, "= e1 a"}} } },
        { new Vars_input<Bool>{regular, {{"a"}}, {{"x", true}}, {{"f", "+ x"}} }    },
        { new B{regular, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} } },
        { new B{assignee, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} } },
        { new B{assignee, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} } },
    });

    Suite(vars_msg + "<Bool>", true).test<Vars_case<Bool>>({
        { new B{regular, {{"a"}}, {}, {{"f"}} }                             },
        { new B{regular, {{"a"}}, {}, {{"f", "+ f"}} }                      },
        { new B{regular, {{"a"}}, {}, {{"f", "+ a f"}} }                    },
        { new B{regular, {{"a"}}, {}, {{"f", "+ a f 2"}} }                  },
        { new B{regular, {{"a"}}, {}, {{"f", "+ a c"}} }                    },
        { new B{regular, {{"a"}}, {}, {{"f", "+ a f c"}} }                  },
        { new B{regular, {{"a"}}, {}, {{"f1", "log f1"}, {"f2", "abs f2"}} }},
        { new B{regular, {{"a"}}, {}, {{"f1", "log f1"}, {"f2", "abs f1"}} }},
        { new B{regular, {{"a"}}, {}, {{"f1", "log f2"}, {"f2", "abs f1"}} }        },
        { new B{regular, {{"a"}}, {}, {{"f1", "+ f1"}}, {{"f2", true, "not f1"}} }  },
        { new B{regular, {{"a"}}, {}, {{"f1", "+ f2"}}, {{"f2", true, "not f1"}} }  },
        { new B{regular, {}, {}, {}, {}, {{"p1", "+ 0"}}, {} }                      },
        { new B{regular, {}, {}, {}, {}, {}, {{"p2", false, "- 1"}} }               },
        { new B{auto_assign, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {{"p2", false, "and p1"}}, {{"e1", "not f1"}} } },
        { new B{auto_assign, {{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {{"p2", false, "= f1 a"}}, {{"e1", "f1"}} } },
        { new Vars_input<Bool>{regular, {{"a"}}, {{"x", true}}, {{"f", "+ f"}} }    },
    });

    Suite(vars_msg + "<Bool> (visit)").test<Vars_case<Bool, Bool, Full_conf>>({
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} } },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} } },
    });

    Suite(vars_msg + "<Real>").test<Vars_case<Real>>({
        { new R{regular}                                                      },
        { new R{regular, {}}                                                  },
        { new R{regular, {}, {}}                                              },
        { new R{regular, {}, {}, {}}                                          },
        { new R{regular, {}, {}, {}, {}}                                      },
        { new R{regular, {{"a"}, {"b"}}, {{"x", 5.}, {"y", 0.}} }             },
    });

    R r1{auto_assign, {{"x"}}, {{"y", 1.}} };
    PR* pr1 = new PR{regular, {{"a"}}, {{"b", true}}, {}, {}, {{"p1", "< 2 1"}}, {}, {{"e1", "= x 2"}} };

    R r2{auto_assign, {{"x"}}, {{"y", 1.}}, {{"f1", "+ x y"}} };
    PR* pr2 = new PR{regular, {{"a"}}, {{"b", true}}, {{"f1", "+ 1"}}, {}, {{"p1", "< 2 1"}}, {}, {{"e1", "= x 1"}, {"e2", "= x y"}}, {{"e3", true, "= x f1"}, {"e4", false, "= y f1"}} };

    Rf r3{auto_assign, {{"x"}, {"y"}, {"z"}} };
    auto pr3 = new PRf{regular, {}, {}, {}, {}, {{"p1", "< x z"}, {"p2", "< y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} };

    Rf r4{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", 1.}} };
    auto pr4 = new PRf{regular, {}, {}, {}, {}, {{"p1", "< x z"}, {"p2", "< y z"}, {"p3", "< z w"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} };

    Suite(vars_msg + "<Bool, Real>").test<Vars_case<Bool, Real>>({
        { invoke([pr1, &r1]{ pr1->args_input_l = &r1; return pr1; }) },
        { invoke([pr2, &r2]{ pr2->args_input_l = &r2; return pr2; }) },
        { new PR{regular, {{"a"}}, {{"b", true}}, {{"f1", "+ a"}}, {}, {{"p1", "< 2 1"}} } },
        { new PR{regular, {{"a"}}, {{"b", true}}, {{"f1", "+ 1"}}, {}, {{"p1", "< 2 1"}}, {}, {{"p2", "= 2 1"}} } },
    });

    Suite(vars_msg + "<Bool, Real>", true).test<Vars_case<Bool, Real>>({
        { new PR{regular, {{"a"}}, {{"b", true}}, {{"f1", "+ 1"}}, {}, {{"p1", "= b 0"}} } },
        { new PR{auto_assign, {{"a"}}, {{"b", true}}, {{"f1", "+ 1"}}, {}, {{"p1", "= 2 1"}}, {}, {{"p2", "< 2 1"}} } },
        { new PR{auto_assign, {{"a"}}, {{"b", true}}, {{"f1", "+ 1"}}, {}, {{"p1", "< 2 1"}}, {}, {{"p2", "= q 1"}} } },
    });

    Suite(vars_msg + "<Bool, Real> (visit)").test<Vars_case<Bool, Real, Full_conf>>({
        { invoke([pr3, &r3]{ pr3->args_input_l = &r3; return pr3; }) },
        { invoke([pr4, &r4]{ pr4->args_input_l = &r4; return pr4; }) },
    });

    ////////////////////////////////////////////////////////////////

    const String vars_evaluable_msg = "evaluability of Var-s";
    Suite(vars_evaluable_msg + "<Bool>").test<Vars_evaluable_case<Bool>>({
        { new B{regular},                                                            {}                                            },
        { new B{regular, {{"a"}, {"b"}}, {{"x", true}, {"y", false}} },              {false, false, true, true}                    },
        { new B{regular, {{"a"}}, {{"x", false}}, {{"f1", "+ x"}}, {{"f2", true, "not f1"}} },  {false, true, true, true}          },
        { new B{regular, {}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}, {"f2", "not f1"}, {"f3", "and x f1"}} },  {true, true, true, true, true} },
        { new B{regular, {{"?"}}, {{"x", true}, {"y", false}}, {{"f", "or x y"}} },  {false, true, true, true}               },
        { new B{regular, {{"?"}}, {{"x", true}, {"y", false}}, {{"f", "or x ?"}} },  {false, true, true, false}              },
        { new B{regular, {}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", true, "= x y"}} },  {true, true, true, true, true} },
        { new B{regular, {}, {{"x", true}, {"y", false}}, {{"f", "* 0.1 10"}}, {}, {{"p", "= 5.5 5"}} },  {true, true, true, true} },
        { new B{assignee, {{"x"}}, {{"y", false}}, {}, {}, {}, {}, {{"e1", "= x 1"}} }, {false, true, true}                           },
        { new B{assignee, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} },  {false, false, false, false, false, false, false, false} },
        { new B{assignee, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {false, false, false, true, false, false, false, true, true, false, false} },
        { new B{assignee, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {}, {}, {{"e1", "= x (and y 0)"}, {"e2", "= y z"}, {"e3", "= z (not w)"}, {"e4", "= w x"}} }, {false, false, false, true, true, true, false, false} },
        { new B{assignee, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} },  {false, false, false, false, false, false, false, true} },
        { new B{auto_assign, {{"x"}}, {{"y", false}}, {}, {}, {}, {}, {{"e1", "= x 1"}} }, {true, true, true}                           },
        { new B{regular, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} },  {false, false, false, false, false, false, false, true} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {{"e", "= x 0"}} },             {false, true} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x 0"}} },       {true, true} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },   {false, true} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x 0"}} },  {false, true} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y 1"}} }, {false, false, false, true} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", true, "= y 1"}} }, {false, false, false, true} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", false, "= y 1"}} }, {false, false, false, true} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}, {"e1", "= x y"}} }, {false, false, true, false} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", true, "= x y"}} }, {false, false, true, true} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", false, "= x y"}} }, {false, false, true, true} },
        { new B{assign_2, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y 1"}} }, {false, true, true, true} },
        { new B{assign_2, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", true, "= y 1"}} }, {false, true, true, true} },
        { new B{assign_2, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", false, "= y 1"}} }, {false, false, false, true} },
        { new B{assign_2, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}, {"e1", "= x y"}} }, {true, true, true, true} },
        { new B{assign_2, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", true, "= x y"}} }, {true, true, true, true} },
        { new B{assign_2, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", false, "= x y"}} }, {false, true, true, true} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {{"e", "= x x"}} },             {false, false} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} },   {false, true} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} },  {false, true} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x x"}} },       {true, true} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} }, {true, true} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} }, {true, true} },
        { new B{assignee, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= x 5"}, {"e2", "= y (+ z x)"}, {"e3", "= z x"}} }, {false, false, false, true, false, false} },
        { new B{assignee, {{"T"}, {"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= T 1"}, {"e2", "= x (and y T)"}, {"e3", "= y (and z T)"}, {"e4", "= z 1"}} }, {false, false, false, false, true, false, false, true} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {{"e", "= x 0"}} },             {true, true} },
        { new B{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x 0"}} },       {true, true} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },   {true, true} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x 0"}} },  {false, true} },
        { new B{auto_assign, {{"T"}, {"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= T 1"}, {"e2", "= x (and y T)"}, {"e3", "= y (and z T)"}, {"e4", "= z 1"}} }, {true, true, true, true, true, true, true, true} },
    });

    Suite(vars_evaluable_msg + "<Bool> (visit)").test<Vars_evaluable_case<Bool, Bool, Full_conf>>({
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} },  {false, false, false, false, false, false, false, false} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {true, true, true, true, true, true, true, true, true, true, true} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {}, {}, {{"e1", "= x (and y 0)"}, {"e2", "= y z"}, {"e3", "= z (not w)"}, {"e4", "= w x"}} }, {true, true, true, true, true, true, true, true} },
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} },  {false, false, false, false, false, false, false, true} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y 1"}} }, {true, true, true, true} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", true, "= y 1"}} }, {true, true, true, true} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", false, "= y 1"}} }, {false, false, false, true} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}, {"e1", "= x y"}} }, {true, true, true, true} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", true, "= x y"}} }, {true, true, true, true} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", false, "= x y"}} }, {false, true, true, true} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {{"e", "= x x"}} },             {false, false} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} },   {false, true} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} },  {false, true} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x x"}} },       {true, true} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} }, {true, true} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} }, {true, true} },
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= x 5"}, {"e2", "= y (+ z x)"}, {"e3", "= z x"}} }, {true, true, true, true, true, true} },
    });

    ////////////////////////////////////////////////////////////////

    const String vars_assignable_msg = "assignability of Equality-s";
    Suite(vars_assignable_msg + "<Bool>").test<Vars_assignable_case<Bool>>({
        { new B{assignee, {{"x"}}, {{"y", false}}, {}, {}, {}, {}, {{"e1", "= x 1"}} }, {{true}} },
        { new B{assignee, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} },  {{false, false, false}} },
        { new B{assignee, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {{true, true, false, false}} },
        { new B{assignee, {{"y"}, {"z"}}, {{"x", true}}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} },  {{true, false, true}} },
        { new B{assignee, {{"y"}, {"z"}}, {{"x", true}}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", false, "= z x"}} },  {{true, false, false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {{"e", "= x 0"}} },             {{true}} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x 0"}} },       {{false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },   {{true}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x 0"}} },  {{false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y 1"}} }, {{false, true}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", true, "= y 1"}} }, {{false, true}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", false, "= y 1"}} }, {{false, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", true, "= x y"}} }, {{true, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", false, "= x y"}} }, {{true, false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {{"e", "= x x"}} },             {{false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} },   {{false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} },  {{false}} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x x"}} },       {{false}} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} }, {{false}} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} }, {{false}} },
        { new B{assignee, {{"x"}}, {{"y", true}}, {}, {}, {}, {}, {{"e", "= x y"}} }, {{true}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y x"}} }, {{false, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", true, "= y x"}} }, {{false, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", false, "= y x"}} }, {{false, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y x"}}, {{"e1", true, "= x y"}} }, {{false, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y x"}}, {{"e1", false, "= x y"}} }, {{false, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"ex", "= x 0"}, {"ey", "= y 1"}, {"exy", "= x y"}} }, {{true, true, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"ex", "= x 0"}, {"exy", "= x y"}, {"ey", "= y 1"}} }, {{true, false, true}} },
        { new B{assignee, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"exy", "= x y"}, {"ex", "= x 0"}, {"ey", "= y 1"}} }, {{false, true, true}} },
        { new B{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {}, {}, {{"e1", "= x (and y 0)"}, {"e2", "= y z"}, {"e3", "= z (not w)"}, {"e4", "= w x"}} }, {{true, true, false, true}} },
        { new B{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"ex", "= x 0"}, {"ey", "= y 1"}, {"exy", "= x y"}} }, {{true, true, false}, {false, true, true}, {true, false, true}} },
        { new B{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"ex", "= x 0"}, {"exy", "= x y"}, {"ey", "= y 1"}} }, {{true, true, false}, {false, true, true}, {true, false, true}} },
        { new B{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"exy", "= x y"}, {"ex", "= x 0"}, {"ey", "= y 1"}} }, {{true, true, false}, {false, true, true}, {true, false, true}} },
        { new B{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= x 5"}, {"e2", "= y (+ z x)"}, {"e3", "= z x"}} }, {{true, true, true}} },
        { new B{auto_assign, {{"T"}, {"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= T 1"}, {"e2", "= x (and y T)"}, {"e3", "= y (and z T)"}, {"e4", "= z 1"}} }, {{true, true, true, true}} },
    });

    Suite(vars_assignable_msg + "<Bool> (visit)").test<Vars_assignable_case<Bool, Bool, Full_conf>>({
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} },  {{false, false, false}} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {{true, true, true, false}} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {}, {}, {{"e1", "= x (and y 0)"}, {"e2", "= y z"}, {"e3", "= z (not w)"}, {"e4", "= w x"}} }, {{true, true, false, true}} },
        { new Bf{auto_assign, {{"y"}, {"z"}}, {{"x", true}}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} },  {{true, true, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} },  {{false, false, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y 1"}} }, {{true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", true, "= y 1"}} }, {{true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", false, "= y 1"}} }, {{false, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", true, "= x y"}} }, {{true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y 1"}}, {{"e1", false, "= x y"}} }, {{true, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y x"}} }, {{false, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", true, "= y x"}} }, {{false, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x y"}}, {{"e2", false, "= y x"}} }, {{false, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y x"}}, {{"e1", true, "= x y"}} }, {{false, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e2", "= y x"}}, {{"e1", false, "= x y"}} }, {{false, false}} },
    });

    ////////////////////////////////////////////////////////////////

    const String vars_eval_msg = "evaluation of Var-s";
    Suite(vars_eval_msg + "<Bool>").test<Vars_eval_case<Bool>>({
        { new B{regular},                                                            {}                                            },
        { new B{regular, {{"a"}, {"b"}}, {{"x", true}, {"y", false}} },              {{{}, {}, true, false}}           },
        { new B{regular, {{"a"}}, {{"x", false}}, {{"f1", "+ x"}}, {{"f2", true, "not f1"}} },  {{{}, false, false, true}}  },
        { new B{regular, {}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}, {"f2", "not f1"}, {"f3", "and x f1"}} },  {{false, true, true, false, false}} },
        { new B{regular, {{"?"}}, {{"x", true}, {"y", false}}, {{"f", "or x y"}} },  {{{}, true, false, true}}         },
        { new B{regular, {{"?"}}, {{"x", true}, {"y", false}}, {{"f", "or x ?"}} },  {{{}, true, false, {}}}           },
        { new B{regular, {}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", true, "= x y"}} },  {{false, true, true, false, true}} },
        { new B{regular, {}, {{"x", true}, {"y", false}}, {{"f", "* 0.1 10"}}, {}, {{"p", "= 5.5 5"}} }, {{true, false, false, true}} },
        { new B{assignee, {{"x"}}, {{"y", false}}, {}, {}, {}, {}, {{"e1", "= x 1"}} }, {{true, false, true}}          },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },  {{{}, true}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x 1"}} },  {{{}, false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {{"e1", "= x 1"}, {"e2", "= x 0"}} }, {{true, true, false}, {false, false, true}}     },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {{"e1", "= x 1"}}, {{"e2", false, "= x 0"}} }, {{true, true, false}}     },
        /// Can evaluate multiple times with `out_count > 1`
        { new B{assignee, {{"x"}, {"a"}, {"b"}, {"c"}}, {}, {{"f", "xor a b c"}}, {}, {}, {}, {{"e1", "= x f"}, {"e2", "= a b"}, {"e3", "= b c"}, {"e4", "= c 1"}, {"e5", "= a 0"}, {"e6", "= a 1"}} }, { {{}, false, {}, true, {}, {}, {}, {}, true, true, false},
                                                                                                                                                                                                          {{}, false, false, true, {}, {}, true, false, true, true, false},
                                                                                                                                                                                                          {true, false, false, true, true, true, true, false, true, true, false} }     },
        { new B{auto_assign, {{"x"}}, {{"y", false}}, {}, {}, {}, {}, {{"e1", "= x 1"}} }, {{true, false, true}}       },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },  {{false, true}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x 1"}} },  {{{}, false}} },
        { new B{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x 0"}} },  {{true, false}} },
        { new B{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },  {{true, true}}          },
        { new B{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", false, "= x 0"}} },  {{true, false}}        },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {{"e1", "= x 1"}, {"e2", "= x 0"}} }, {{true, true, false}, {false, false, true}}  },
        { new B{auto_assign, {{"T"}, {"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {{"e1", "= T 1"}, {"e2", "= x (and y T)"}, {"e3", "= y (and z T)"}, {"e4", "= z 1"}} }, {{true, true, true, true, true, true, true, true}} },
        { new B{auto_assign, {{"?"}}, {{"x", true}, {"y", false}}, {}, {}, {{"p1", "or x y"}, {"p2", "not p1"}}, {}, {{"e1", "= (not p2) ?"}} }, {{true, true, false, true, false, true}} },
        /*!
        { new B{auto_assign, {{"x"}, {"a"}, {"b"}, {"c"}}, {}, {{"f", "xor a b c"}}, {}, {}, {}, {{"e1", "= x f"}, {"e2", "= a b"}, {"e3", "= b c"}, {"e4", "= c 1"}, {"e5", "= a 0"}, {"e6", "= a 1"}} }, {{false, false, true, true, false, true, false, true, true, true, false},
                                                                                                                                                                                                            {true, true, true, true, true, true, true, true, true, false, true},
                                                                                                                                                                                                            {true, false, false, true, true, true, true, false, true, true, false},
                                                                                                                                                                                                            {false, false, false, false, false, true, true, true, false, true, false}}     },
        */
    });

    Suite(vars_eval_msg + "<Bool>", true).test<Vars_eval_case<Bool>>({
        { new B{regular, {}, {{"x", true}} },                                        {{false}}                         },
        { new B{regular, {}, {{"x", false}} },                                       {{true}}                          },
    });

    Suite(vars_eval_msg + "<Bool> (visit)").test<Vars_eval_case<Bool, Bool, Full_conf>>({
        { new Bf{auto_assign, {{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {}, {}, {{"e1", "= x f1"}}, {{"e2", false, "= x y"}} },  {{{}, true, {}, {}, false}} },
        { new Bf{auto_assign, {{"x"}}, {{"y", true}}, {{"f1", "or 0 y"}}, {}, {}, {}, {{"e1", "= x f1"}}, {{"e2", false, "= x y"}} },  {{true, true, true, true, false}} },
        { new Bf{auto_assign, {{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {}, {}, {{"e1", "= x f1"}}, {{"e2", true, "= x y"}} },  {{true, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {}, {}, {{"e1", "= x f1"}, {"e2", "= x y"}} },  {{true, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} },  {{{}, {}, {}, {}, {}, {}, {}, {}}} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {{true, true, true, true, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {{true, true, true, true, true, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {{true, true, true, true, true, true, false, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {}, {}, {{"e1", "= x (and y 0)"}, {"e2", "= y z"}, {"e3", "= z (not w)"}, {"e4", "= w x"}} }, {{false, true, false, true, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} },  {{{}, {}, {}, {}, {}, {}, {}, true}} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {{"e", "= x x"}} },             {{{}, {}}} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} },   {{{}, true}} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} },   {{{}, true}} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} },  {{{}, false}} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x x"}} },       {{true, true}} },
        { new Bf{auto_assign, {}, {{"x", false}}, {}, {}, {}, {}, {{"e", "= x x"}} },      {{false, true}} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} }, {{true, true}} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} }, {{true, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"ex", "= x 1"}, {"ey", "= y 1"}, {"exy", "= x y"}} }, {{true, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"ex", "= x 1"}, {"exy", "= x y"}, {"ey", "= y 1"}} }, {{true, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"exy", "= x y"}, {"ex", "= x 1"}, {"ey", "= y 1"}} }, {{true, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"ex", "= x 0"}, {"e!", "= x (and y)"}, {"ey", "= y 0"}, {"exy", "= x y"}} }, {{false, false, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"ex", "= x 0"}, {"e!", "= x (not y)"}, {"ey", "= y 1"}, {"exy", "= (not x) y"}} }, {{false, true, true, true, true, true}} },
    });

    Suite(vars_eval_msg + "<Real>").test<Vars_eval_case<Real>>({
        { new R{regular, {{"a"}} },                                                  {{{}}}                                 },
        { new R{regular, {}, {{"x", 3.}, {"y", -1.}}, {{"f1", "+ x y"}, {"f2", "- f1"}, {"f3", "* x f1"}} },  {{3., -1., 2., -2., 6.}} },
    });

    Suite(vars_eval_msg + "<Bool, Real>").test<Vars_eval_case<Bool, Real>>({
        { new PR{regular, {}, {{"x", true}, {"y", false}}, {{"f", "* 0.1 10"}}, {}, {{"p", "= 5.5 5"}} },  {{true, false, false, false}} },
    });

    Suite(vars_eval_msg + "<Bool, Real>", true).test<Vars_eval_case<Bool, Real>>({
        { new PR{regular, {}, {{"x", true}, {"y", false}}, {{"f", "* 0.1 10"}}, {}, {{"p", "= x 5"}} },     ignore                 },
    });

    Rf er1{auto_assign, {{"x"}, {"y"}, {"z"}} };
    auto epr1 = new PRf{regular, {}, {}, {}, {}, {{"p1", "< x z"}, {"p2", "< y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} };

    Rf er2{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", 1.}} };
    auto epr2 = new PRf{regular, {}, {}, {}, {}, {{"p1", "< x z"}, {"p2", "< y z"}, {"p3", "< z w"}}, {}, {{"e1", "= x (+ y 1)"}, {"e2", "= y z"}, {"e3", "= z (- w 1)"}, {"e4", "= w x"}} };

    Suite(vars_eval_msg + "<Bool, Real> (visit)").test<Vars_eval_case<Bool, Real, Full_conf>>({
        { invoke([epr1, &er1]{ epr1->args_input_l = &er1; return epr1; }),  {{{}, {}, {}, {}, {}}} },
        { invoke([epr2, &er2]{ epr2->args_input_l = &er2; return epr2; }),  {{false, false, true, true, true, true, true}} },
    });

    Suite(vars_eval_msg + "<Bool, Real> (visit)", true).test<Vars_eval_case<Bool, Real, Full_conf>>({
        { new PRf{regular, {}, {{"x", true}, {"y", false}}, {{"f", "* 0.1 10"}}, {}, {{"p", "= x 5"}} },     ignore                 },
    });

    R err1{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", 3.14}} };
    auto eprr1 = new PR{regular, {}, {}, {}, {}, {}, {}, {{"e1", "= x (+ y 1)"}, {"e2", "= y z"}, {"e3", "= z (- w 1)"}, {"e4", "= w x"}} };

    Suite(vars_eval_msg + "<Bool, Real> -> Real").test<Vars_eval_case<Bool, Real, Conf, Real>>({
        { invoke([eprr1, &err1]{ eprr1->args_input_l = &err1; return eprr1; }),  {{4.14, 3.14, 4.14, 3.14}} },
    });

    ////////////////////////////////////////////////////////////////

    const String vars_consistent_msg = "consistency of Var-s";
    Suite(vars_consistent_msg + "<Bool>").test<Vars_consistent_case<Bool>>({
        { new B{regular},                                                            {}                                            },
        { new B{regular, {{"a"}, {"b"}}, {{"x", true}, {"y", false}} },              {{unknown, unknown, unknown, unknown}, {{}, {}, true, false}} },
        { new B{regular, {}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}, {"f2", "not f1"}, {"f3", "and x f1"}} },  {{unknown, unknown, true, true, true}, {false, true, true, false, false}} },
        { new B{regular, {{"?"}}, {{"x", true}, {"y", false}}, {{"f", "or x ?"}} },  {{unknown, unknown, unknown, unknown}, {{}, true, false, {}}} },
        { new B{regular, {}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", true, "= x y"}} },  {{unknown, unknown, true, true, false}, {false, true, true, false, true}} },
        { new B{assignee, {}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", true, "= x y"}} },  {{unknown, unknown, true, true, false}, {false, true, true, false, true}} },
        { new B{assignee, {{"x"}}, {{"y", false}}, {}, {}, {}, {}, {{"e1", "= x 1"}} }, {{true, unknown, true}, {true, false, true}}   },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },   {{true, true}, {false, true}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x 1"}} },  {{unknown, unknown}, {{}, false}} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x 0"}} },       {{unknown, true}, {true, false}} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },  {{unknown, false}, {true, true}} },
        { new B{assignee, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", false, "= x 0"}} }, {{unknown, true}, {true, false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {{"e1", "= x 1"}}, {{"e2", false, "= x 0"}} }, {{true, true, true}, {true, true, false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", false, "= x 1"}, {"e2", true, "= x 0"}} }, {{true, true, true}, {false, false, true}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", true, "= x 1"}, {"e2", false, "= x 0"}} }, {{true, true, true}, {true, true, false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", false, "= x 1"}, {"e2", true, "= x 0"}, {"e3", true, "= x 0"}} }, {{true, true, true, true}, {false, false, true, true}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", false, "= x 1"}, {"e2", true, "= x 0"}, {"e3", false, "= x 0"}} }, {{true, true, true, false}, {false, false, true, false}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", false, "= x 1"}, {"e2", false, "= x 0"}, {"e3", true, "= x 0"}} }, {{true, true, false, true}, {false, false, false, true}} },
        { new B{assignee, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", true, "= x 1"}, {"e2", false, "= x 0"}, {"e3", false, "= x 0"}} }, {{true, true, true, true}, {true, true, false, false}} },
        { new B{assignee, {{"x"}, {"y"}}, {{"z", true}}, {}, {}, {}, {}, {}, {{"e1", false, "= z y"}, {"e2", true, "= z x"}} }, {{true, unknown, unknown, unknown, true}, {true, {}, true, false, true}} },
        { new B{assignee, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {}, {{"e1", true, "= z 1"}, {"e2", true, "= y z"}, {"e3", false, "= x y"}} }, {{unknown, true, true, true, true, unknown}, {{}, true, true, true, true, false}} },
        { new B{auto_assign, {}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", true, "= x y"}} },  {{unknown, unknown, true, true, false}, {false, true, true, false, true}} },
        { new B{auto_assign, {{"x"}}, {{"y", false}}, {}, {}, {}, {}, {{"e1", "= x 1"}} }, {{true, unknown, true}, {true, false, true}}   },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },   {{true, true}, {false, true}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x 1"}} },  {{unknown, unknown}, {{}, false}} },
        { new B{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x 0"}} },       {{unknown, true}, {true, false}} },
        { new B{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", true, "= x 0"}} },  {{unknown, false}, {true, true}} },
        { new B{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", false, "= x 0"}} }, {{unknown, true}, {true, false}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {{"e1", "= x 1"}}, {{"e2", false, "= x 0"}} }, {{true, true, true}, {true, true, false}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", false, "= x 1"}, {"e2", true, "= x 0"}} }, {{true, true, true}, {false, false, true}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", true, "= x 1"}, {"e2", false, "= x 0"}} }, {{true, true, true}, {true, true, false}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", false, "= x 1"}, {"e2", true, "= x 0"}, {"e3", true, "= x 0"}} }, {{true, true, true, true}, {false, false, true, true}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", false, "= x 1"}, {"e2", true, "= x 0"}, {"e3", false, "= x 0"}} }, {{true, true, true, false}, {false, false, true, false}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", false, "= x 1"}, {"e2", false, "= x 0"}, {"e3", true, "= x 0"}} }, {{true, true, false, true}, {false, false, false, true}} },
        { new B{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e1", true, "= x 1"}, {"e2", false, "= x 0"}, {"e3", false, "= x 0"}} }, {{true, true, true, true}, {true, true, false, false}} },
        { new B{auto_assign, {{"x"}, {"y"}}, {{"z", true}}, {}, {}, {}, {}, {}, {{"e1", false, "= z y"}, {"e2", true, "= z x"}} }, {{true, unknown, unknown, unknown, true}, {true, {}, true, false, true}} },
    });

    Suite(vars_consistent_msg + "<Bool> (visit)").test<Vars_consistent_case<Bool, Bool, Full_conf>>({
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} },  {{unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown}, {{}, {}, {}, {}, {}, {}, {}, {}}} },
        { new Bf{auto_assign, {{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {{true, true, true, unknown, true, true, true, true, true, true, true}, {true, true, true, true, true, true, false, true, true, true, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}}, {}, {{"e1", "= x y"}, {"e2", "= y z"}}, {{"e3", true, "= z x"}} },  {{unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown}, {{}, {}, {}, {}, {}, {}, {}, true}} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {{"e", "= x x"}} },             {{unknown, unknown}, {{}, {}}} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} },   {{unknown, unknown}, {{}, true}} },
        { new Bf{auto_assign, {{"x"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} },  {{unknown, unknown}, {{}, false}} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {{"e", "= x x"}} },       {{unknown, true}, {true, true}} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", true, "= x x"}} }, {{unknown, true}, {true, true}} },
        { new Bf{auto_assign, {}, {{"x", true}}, {}, {}, {}, {}, {}, {{"e", false, "= x x"}} }, {{unknown, false}, {true, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e", "= x y"}} },      {{unknown, unknown, unknown}, {{}, {}, {}}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {}, {{"e", true, "= x y"}} },  {{unknown, unknown, unknown}, {{}, {}, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {}, {{"e", false, "= x y"}} }, {{unknown, unknown, unknown}, {{}, {}, false}} },
        { new Bf{auto_assign, {{"x"}, {"y"}}, {{"z", true}}, {}, {}, {}, {}, {}, {{"e1", false, "= z y"}, {"e2", true, "= z x"}} }, {{true, unknown, unknown, unknown, true}, {true, {}, true, false, true}} },
        { new Bf{auto_assign, {{"x"}, {"y"}, {"z"}}, {}, {}, {}, {}, {}, {}, {{"e1", true, "= z 1"}, {"e2", true, "= y z"}, {"e3", false, "= x y"}} }, {{unknown, true, true, true, true, unknown}, {{}, true, true, true, true, false}} },
    });

    ////////////////////////////////////////////////////////////////

    /*!
    Suite("evaluation of distances").test<Vars_distance_case<Bool>>({
        { new B{add_property(auto_assign, distance_t), {{"x"}}, {{"y", false}} }, {~0U, ~0U} },
        { new B{add_property(auto_assign, distance_t), {{"x"}}, {{"y", false}}, {}, {}, {}, {}, {{"e1", "= x 1"}} }, {~0U, ~0U, 0} },
        { new B{add_property(auto_assign, distance_t), {{"x"}, {"y"}}, {}, {}, {}, {}, {}, {{"e1", "= x 1"}, {"e2", "= x y"}} }, {1, ~0U, 0, 1} },
    }); */

    ////////////////////////////////////////////////////////////////

    //+ const String vars_table_msg = "truth table of Var predicates ";

    //+ #define BOOL_VARS {{"?"}}, {{"x", true}, {"y", false}}
    //+ #define BOOL_FUNS {{"f1", "and ?"}, {"f2", "and x"}, {"f3", "and y"}}, {{"f1s1", true, "and f1"}, {"f1s0", false, "and f1"}, {"f2s1", true, "and f2"}, {"f2s0", false, "and f2"}, {"f3s1", true, "and f3"}, {"f3s0", false, "and f3"}}
    //+ #define BOOL_PREDS {{"p1", "and ?"}, {"p2", "and x"}, {"p3", "and y"}}, {{"p1s1", true, "and p1"}, {"p1s0", false, "and p1"}, {"p2s1", true, "and p2"}, {"p2s0", false, "and p2"}, {"p3s1", true, "and p3"}, {"p3s0", false, "and p3"}}
    //+ #define BOOL_EPREDS_U_A0 {"e?1", "= ? ?"}, {"ex1", "= x x"}, {"ey1", "= y y"}
    //+ #define BOOL_EPREDS_S_A0 {"e?1s1", true, "= ? ?"}, {"e?1s0", false, "= ? ?"}, {"ex1s1", true, "= x x"}, {"ex1s0", false, "= x x"}, {"ey1s1", true, "= y y"}, {"ey1s0", false, "= y y"}

    //+ #define BOOLS BOOL_VARS, BOOL_FUNS, BOOL_PREDS
    //+ #define EBOOLS_A0 BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0}

    //+ Suite(vars_table_msg + "S&Cd").test<Vars_table_case<S_cd<Bool>>>({
    //+     { new B{false, BOOLS },  true },
    //+     { new B{true, EBOOLS_A0 },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 y"}} },  true },
    //+ });

    //+ Suite(vars_table_msg + "SEU&EU").test<Vars_table_case<Sc_c<Bool>>>({
    //+     { new B{false, BOOLS },  true },
    //+     { new B{true, EBOOLS_A0 },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 y"}} },  true },
    //+ });

    //+ Suite(vars_table_msg + "EU&E").test<Vars_table_case<C_e<Bool>>>({
    //+     { new B{false, BOOLS },  true },
    //+     { new B{true, EBOOLS_A0 },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 y"}} },  true },
    //+ });

    //+ Suite(vars_table_msg + "S&E").test<Vars_table_case<S_e<Bool>>>({
    //+     { new B{false, BOOLS },  true },
    //+     { new B{true, EBOOLS_A0 },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 y"}} },  true },
    //+ });

    //+ Suite(vars_table_msg + "S&A").test<Vars_table_case<S_a>>({
    //+     { new B{true, EBOOLS_A0 },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 y"}} },  true },
    //+ });

    //+ Suite(vars_table_msg + "Cd&A").test<Vars_table_case<Cd_a>>({
    //+     { new B{true, EBOOLS_A0 },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 y"}} },  true },
    //+ });

    //+ Suite(vars_table_msg + "E&A").test<Vars_table_case<E_a>>({
    //+     { new B{true, EBOOLS_A0 },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= ? 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 ?"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= x 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 x"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 0"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= y 1"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 0 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0, {"ea", "= 1 y"}}, {BOOL_EPREDS_S_A0} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= ? 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 ?"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= x 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 x"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 0"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= y 1"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 0 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", true, "= 1 y"}} },  true },
    //+     { new B{true, BOOLS, {BOOL_EPREDS_U_A0}, {BOOL_EPREDS_S_A0, {"ea", false, "= 1 y"}} },  true },
    //+ });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
