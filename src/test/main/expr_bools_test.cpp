#include "test.hpp"
#include "expr/bools.hpp"

namespace unsot::test {
    using namespace expr;
    using namespace expr::bools;

    ////////////////////////////////////////////////////////////////

    /// `Ptr` used instead of `List` to avoid deep-copy (default copy always takes place in initializer list)
    struct Cnf_case : Inherit<Cons_case<Cnf, Ptr>> {
        using Inherit::Inherit;

        Cons cons() override
        {
            /// To also check move assignment
            Ptr ptr = move(input());
            auto& ls = List::cast(ptr);
            Cons tmp(move(ls), 1);
            Cons cnf;
            cnf = move(tmp);
            return cnf;
        }
    };

    struct To_formula_case : Inherit<Case<Cnf, Formula>> {
        using Inherit::Inherit;

        Output result() override
        {
            return cinput().to_formula();
        }
    };

    struct To_conflict_case : Inherit<Case<Clause>> {
        using Inherit::Inherit;

        Output result() override
        {
            return to_conflict(cinput());
        }
    };

    struct To_pair_conflict_case : Inherit<Case<Clause, Cnf>> {
        using Inherit::Inherit;

        Output result() override
        {
            return to_pair_conflict(cinput());
        }
    };
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;
    using unsot::ignore;

    ////////////////////////////////////////////////////////////////

    const String cnf_msg = "conversion to CNF";
    Suite(cnf_msg).test<Cnf_case>({
        { List::new_me("and a"),                               {{"a"}},                          },
        { List::new_me("or a"),                                {{"a"}},                          },
        { List::new_me("not a"),                               {{"~a"}},                         },
        { List::new_me("~a"),                                  {{"~a"}},                         },
        { List::new_me("not ~a"),                              {{"a"}},                          },
        { List::new_me("a"),                                   {{"a"}},                          },
        { List::new_me("0"),                                   {{"0"}},                          },
        { List::new_me("1"),                                   {{"1"}},                          },
        { List::new_me("not 0"),                               {{"1"}},                          },
        { List::new_me("not 1"),                               {{"0"}},                          },
        { List::new_me("~0"),                                  {{"~0"}},                         },
        { List::new_me("~1"),                                  {{"~1"}},                         },
        { List::new_me("not ~0"),                              {{"0"}},                          },
        { List::new_me("not ~1"),                              {{"1"}},                          },
        { List::new_me("a b"),                                 {{"a"}, {"b"}},                   },
        { List::new_me("a b c"),                               {{"a"}, {"b"}, {"c"}},            },
        { List::new_me("foo a"),                               {{"foo"}, {"a"}},                 },
        { List::new_me("and (and a)"),                         {{"a"}},                          },
        { List::new_me("or (or a)"),                           {{"a"}},                          },
        { List::new_me("and (or a)"),                          {{"a"}},                          },
        { List::new_me("or (and a)"),                          {{"a"}},                          },
        { List::new_me("not (not a)"),                         {{"a"}},                          },
        { List::new_me("not (and a)"),                         {{"~a"}},                         },
        { List::new_me("not (or a)"),                          {{"~a"}},                         },
        { List::new_me("and (not a)"),                         {{"~a"}},                         },
        { List::new_me("or (not a)"),                          {{"~a"}},                         },
        { List::new_me("and a b"),                             {{"a"}, {"b"}},                   },
        { List::new_me("and a 1"),                             {{"a"}, {"1"}},                   },
        { List::new_me("or a b"),                              {{"a", "b"}},                     },
        { List::new_me("and (and a b)"),                       {{"a"}, {"b"}},                   },
        { List::new_me("or (and a b)"),                        {{"a"}, {"b"}},                   },
        { List::new_me("or a b"),                              {{"a", "b"}},                     },
        { List::new_me("or (or a b)"),                         {{"a", "b"}},                     },
        { List::new_me("and (or a b)"),                        {{"a", "b"}},                     },
        { List::new_me("and a b c"),                           {{"a"}, {"b"}, {"c"}},            },
        { List::new_me("or a b c"),                            {{"a", "b", "c"}},                },
        { List::new_me("and a (not b)"),                       {{"a"}, {"~b"}},                  },
        { List::new_me("or a (not b)"),                        {{"a", "~b"}},                    },
        { List::new_me("and (or a (not b)) (or (not c) d)"),   {{"a", "~b"}, {"~c", "d"}},       },
        { List::new_me("and (or a (not b)) (not c)"),          {{"a", "~b"}, {"~c"}},            },
        { List::new_me("and a (or (not c) d)"),                {{"a"}, {"~c", "d"}},             },
        { List::new_me("or (and (not a) b) (and c (not d))"),  {{"~_x1", "~a"}, {"~_x1", "b"}, {"_x1", "a", "~b"}, {"~_x2", "c"}, {"~_x2", "~d"}, {"_x2", "~c", "d"}, {"_x1", "_x2"}},         },
        //+ { "not (and a b)",                          {{"~a", "~b"}},                   },
        //+ { "not (or a b)",                          {{"~a"}, {"~b"}},                  },
        //+ { "=> a b",                                 {{"~a", "b"},                     },
        //+ { "not (=> a b)",                           {{"a"}, {"~b"}},                  },
        { List::new_me("not (and a b)"),                         {{"~_x1", "a"}, {"~_x1", "b"}, {"_x1", "~a", "~b"}, {"~_x1"}}, },
        { List::new_me("not (or a b)"),                          {{"_x1", "~a"}, {"_x1", "~b"}, {"~_x1", "a", "b"}, {"~_x1"}},  },
        { List::new_me("=> a b"),                                {{"_x1", "a"}, {"_x1", "~b"}, {"~_x1", "~a", "b"}, {"_x1"}},   },
        { List::new_me("not (=> a b)"),                          {{"_x1", "a"}, {"_x1", "~b"}, {"~_x1", "~a", "b"}, {"~_x1"}},  },
        { List::new_me("=> (and (or p q) r) (not s)"),           {{"_x1", "~p"}, {"_x1", "~q"}, {"~_x1", "p", "q"}, {"~_x2", "_x1"}, {"~_x2", "r"}, {"_x2", "~_x1", "~r"}, {"_x3", "_x2"}, {"_x3", "s"}, {"~_x3", "~_x2", "~s"}, {"_x3"}},  },
        { List::new_me("and (or (and (not a) b) (and c (not d))) (or a c)"),  {{"~_x1", "~a"}, {"~_x1", "b"}, {"_x1", "a", "~b"}, {"~_x2", "c"}, {"~_x2", "~d"}, {"_x2", "~c", "d"}, {"_x1", "_x2"}, {"a", "c"}},         },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); List ls("and"); ls.push_back(ptr); ls.push_back(ptr); return move(ls).to_ptr(); }),              {{"looooooooooong"}, {"looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); List ls("or"); ls.push_back(ptr); ls.push_back(ptr); return move(ls).to_ptr(); }),               {{"looooooooooong", "looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(ptr); ls.push_back(nptr); return move(ls).to_ptr(); }),   {{"looooooooooong"}, {"~looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(ptr); return move(ls).to_ptr(); }),   {{"~looooooooooong"}, {"looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(nptr); return move(ls).to_ptr(); }),  {{"~looooooooooong"}, {"~looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(ptr); ls.push_back(nptr); return move(ls).to_ptr(); }),    {{"looooooooooong", "~looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(ptr); return move(ls).to_ptr(); }),    {{"~looooooooooong", "looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(nptr); return move(ls).to_ptr(); }),   {{"~looooooooooong", "~looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); List ls("and"); ls.push_back(ptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),              {{"~_x1", "looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "~looooooooooong", "~looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); List ls("or"); ls.push_back(ptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),               {{"_x1", "~looooooooooong"}, {"_x1", "~looooooooooong"}, {"~_x1", "looooooooooong", "looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(ptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "~looooooooooong", "looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~_x1", "~looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "looooooooooong", "~looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),  {{"~_x1", "~looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "looooooooooong", "looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(ptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),    {{"_x1", "~looooooooooong"}, {"_x1", "looooooooooong"}, {"~_x1", "looooooooooong", "~looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),    {{"_x1", "looooooooooong"}, {"_x1", "~looooooooooong"}, {"~_x1", "~looooooooooong", "looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"_x1", "looooooooooong"}, {"_x1", "looooooooooong"}, {"~_x1", "~looooooooooong", "~looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); List ls("or"); ls.push_back(ptr); ls.push_back(ptr); List ls2("and"); ls2.push_back(ptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"looooooooooong"}, {"looooooooooong", "looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(ptr); ls.push_back(nptr); List ls2("and"); ls2.push_back(ptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"looooooooooong"}, {"looooooooooong", "~looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(ptr); List ls2("and"); ls2.push_back(ptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"looooooooooong"}, {"~looooooooooong", "looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(nptr); List ls2("and"); ls2.push_back(ptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),  {{"looooooooooong"}, {"~looooooooooong", "~looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(ptr); ls.push_back(ptr); List ls2("and"); ls2.push_back(nptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~looooooooooong"}, {"looooooooooong", "looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(ptr); ls.push_back(nptr); List ls2("and"); ls2.push_back(nptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),  {{"~looooooooooong"}, {"looooooooooong", "~looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(ptr); List ls2("and"); ls2.push_back(nptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),  {{"~looooooooooong"}, {"~looooooooooong", "looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(nptr); List ls2("and"); ls2.push_back(nptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }), {{"~looooooooooong"}, {"~looooooooooong", "~looooooooooong"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); List ls("and"); ls.push_back(ptr); ls.push_back(ptr); List ls2("or"); ls2.push_back(ptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "~looooooooooong", "~looooooooooong"}, {"looooooooooong", "_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(ptr); ls.push_back(nptr); List ls2("or"); ls2.push_back(ptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "~looooooooooong", "looooooooooong"}, {"looooooooooong", "_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(ptr); List ls2("or"); ls2.push_back(ptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~_x1", "~looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "looooooooooong", "~looooooooooong"}, {"looooooooooong", "_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(nptr); List ls2("or"); ls2.push_back(ptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),  {{"~_x1", "~looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "looooooooooong", "looooooooooong"}, {"looooooooooong", "_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(ptr); ls.push_back(ptr); List ls2("or"); ls2.push_back(nptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "~looooooooooong", "~looooooooooong"}, {"~looooooooooong", "_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(ptr); ls.push_back(nptr); List ls2("or"); ls2.push_back(nptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "~looooooooooong", "looooooooooong"}, {"~looooooooooong", "_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(ptr); List ls2("or"); ls2.push_back(nptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),   {{"~_x1", "~looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "looooooooooong", "~looooooooooong"}, {"~looooooooooong", "_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(nptr); List ls2("or"); ls2.push_back(nptr); ls2.push_back(move(ls).to_ptr()); return move(ls2).to_ptr(); }),  {{"~_x1", "~looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "looooooooooong", "looooooooooong"}, {"~looooooooooong", "_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); List ls("or"); ls.push_back(ptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("and"); ls3.push_back(ptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"looooooooooong"}, {"_x1", "~looooooooooong"}, {"_x1", "~looooooooooong"}, {"~_x1", "looooooooooong", "looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(ptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("and"); ls3.push_back(ptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"looooooooooong"}, {"_x1", "~looooooooooong"}, {"_x1", "looooooooooong"}, {"~_x1", "looooooooooong", "~looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("and"); ls3.push_back(ptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"looooooooooong"}, {"_x1", "looooooooooong"}, {"_x1", "~looooooooooong"}, {"~_x1", "~looooooooooong", "looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("and"); ls3.push_back(ptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),  {{"looooooooooong"}, {"_x1", "looooooooooong"}, {"_x1", "looooooooooong"}, {"~_x1", "~looooooooooong", "~looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(ptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("and"); ls3.push_back(nptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),  {{"~looooooooooong"}, {"_x1", "~looooooooooong"}, {"_x1", "~looooooooooong"}, {"~_x1", "looooooooooong", "looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(ptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("and"); ls3.push_back(nptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),  {{"~looooooooooong"}, {"_x1", "~looooooooooong"}, {"_x1", "looooooooooong"}, {"~_x1", "looooooooooong", "~looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("and"); ls3.push_back(nptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),  {{"~looooooooooong"}, {"_x1", "looooooooooong"}, {"_x1", "~looooooooooong"}, {"~_x1", "~looooooooooong", "looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("or"); ls.push_back(nptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("and"); ls3.push_back(nptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }), {{"~looooooooooong"}, {"_x1", "looooooooooong"}, {"_x1", "looooooooooong"}, {"~_x1", "~looooooooooong", "~looooooooooong"}, {"~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); List ls("and"); ls.push_back(ptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("or"); ls3.push_back(ptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "~looooooooooong", "~looooooooooong"}, {"looooooooooong", "~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(ptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("or"); ls3.push_back(ptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "~looooooooooong", "looooooooooong"}, {"looooooooooong", "~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("or"); ls3.push_back(ptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"~_x1", "~looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "looooooooooong", "~looooooooooong"}, {"looooooooooong", "~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("or"); ls3.push_back(ptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),  {{"~_x1", "~looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "looooooooooong", "looooooooooong"}, {"looooooooooong", "~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(ptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("or"); ls3.push_back(nptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "~looooooooooong", "~looooooooooong"}, {"~looooooooooong", "~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(ptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("or"); ls3.push_back(nptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"~_x1", "looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "~looooooooooong", "looooooooooong"}, {"~looooooooooong", "~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(ptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("or"); ls3.push_back(nptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),   {{"~_x1", "~looooooooooong"}, {"~_x1", "looooooooooong"}, {"_x1", "looooooooooong", "~looooooooooong"}, {"~looooooooooong", "~_x1"}}, },
        { invoke([]{ Ptr ptr = new_key("looooooooooong"); Ptr nptr = List::new_me("not"); List::cast(nptr).push_back(ptr); List ls("and"); ls.push_back(nptr); ls.push_back(nptr); List ls2("not"); ls2.push_back(move(ls).to_ptr()); List ls3("or"); ls3.push_back(nptr); ls3.push_back(move(ls2).to_ptr()); return move(ls3).to_ptr(); }),  {{"~_x1", "~looooooooooong"}, {"~_x1", "~looooooooooong"}, {"_x1", "looooooooooong", "looooooooooong"}, {"~looooooooooong", "~_x1"}}, },
    });

    Suite(cnf_msg, true).test<Cnf_case>({
        { List::new_me("log a"),                                  ignore                  },
        { List::new_me("(foo a)"),                                ignore,                 },
        { List::new_me("((foo a))"),                              ignore                  },
        { List::new_me("and (a)"),                                ignore                  },
        { List::new_me("and (a b)"),                              ignore                  },
        { List::new_me("and (foo a)"),                            ignore                  },
        { List::new_me("and (foo a b)"),                          ignore                  },
        { List::new_me("+ a b"),                                  ignore                  },
        { List::new_me("* a b"),                                  ignore                  },
        { List::new_me("and (- a) b"),                            ignore                  },
        { List::new_me("and (~ a) b"),                            ignore                  },
    });

    const String to_phi_msg = "conversion from CNF to formula";
    Suite(to_phi_msg).test<To_formula_case>({
        { {{"a"}},                                  "and a",                },
        { {{"~a"}},                                 "and (not a)",          },
        { {{"a"}, {"b"}},                           "and a b",              },
        { {{"a", "b"}},                             "and (or a b)",         },
        { {{"a", "~b"}, {"c", "d"}},                "and (or a (not b)) (or c d)",},
        { {{"a"}, {"~c", "d"}},                     "and a (or (not c) d)", },
        { {{"~a", "b"}, {"~c"}},                    "and (or (not a) b) (not c)",},
    });

    const String conflict_msg = "conversion to conflict clause";
    Suite(conflict_msg).test<To_conflict_case>({
        { {},                                       {},                     },
        { {"a"},                                    {"~a"},                 },
        { {"a", "b"},                               {"~a", "~b"},           },
        { {"a", "b", "c"},                          {"~a", "~b", "~c"},     },
        { {"a", "~b"},                              {"~a", "b"},            },
    });

    const String pair_conflict_msg = "conversion to pairwise conflict CNF";
    Suite(pair_conflict_msg).test<To_pair_conflict_case>({
        { {},                                       {},                     },
        { {"a"},                                    {},                     },
        { {"a", "b"},                               {{"~a", "~b"}},         },
        { {"a", "b", "c"},                          {{"~a", "~b"}, {"~a", "~c"}, {"~b", "~c"}}, },
        { {"a", "~b"},                              {{"~a", "b"}},          },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
