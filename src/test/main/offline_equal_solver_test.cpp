#include "smt/solver_test.hpp"
#include "smt/solver/offline/reals/equal/minisat.hpp"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace unsot::smt::solver::test;
    using namespace std;
    using offline::reals::equal::minisat::Solver;

    ////////////////////////////////////////////////////////////////

    Solver s;
    s.parse(Bounce::input);

    cout << s.csat_solver().cvar_ids_bimap() << endl;
    cout << s.csat_solver().to_cnf() << endl;
    cout << s.csat_solver().to_ecnf() << endl;

    cout << endl;
    Sat sat = s.solve();
    cout << endl;

    if (sat) {
        cout << "SAT model: " << s.csat_solver().cmodel() << endl;
    }
    else {
        cout << "SAT conflict: " << s.csat_solver().cconflict() << endl;
    }

    cout << endl;
    cout << s.cbools() << endl;
    cout << s.creals() << endl;

    cout << endl;
    cout << "Conflicts: " << s.ccheck_sat_ref().ct_inconsistent_count() << endl;

    check_smt(s, sat);

    ////////////////////////////////////////////////////////////////

    cout << endl << endl;
    //! move assignment would corrupt the underlying views
    Solver s2;
    s2.parse(Branch::input);
    sat = s2.solve();
    check_smt<Branch>(s2, sat);

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
