#include "test.hpp"
#include "unsot.hpp"

namespace unsot::test {
    using unsot::to_string;

    ////////////////////////////////////////////////////////////////

    template <bool withEq = false>
    struct Base : Dynamic<Base<withEq>> {
        using Dynamic = unsot::Dynamic<Base<withEq>>;
        using typename Dynamic::This;
        virtual ~Base() = default;
        String to_string() const& override
        {
            return "Base";
        }
    };
    template <>
    struct Base<true> : Dynamic<Base<true>> {
        using Dynamic = unsot::Dynamic<Base<true>>;
        using typename Dynamic::This;
        virtual ~Base() = default;
        String to_string() const& override
        {
            return "Base";
        }
        bool lequals(const This&) const
        {
            return true;
        }
    };
    template <bool withEq>
    struct Derived : Inherit<Base<withEq>, Derived<withEq>> {
        using Inherit = unsot::Inherit<Base<withEq>, Derived<withEq>>;
        using typename Inherit::This;
        virtual ~Derived() = default;
        String to_string() const& override
        {
            return "Derived";
        }
    };
    template <bool withEq>
    struct Simply_derived : Inherit<Base<withEq>> {
        using Inherit = unsot::Inherit<Base<withEq>>;
        using typename Inherit::This;
        virtual ~Simply_derived() = default;
        String to_string() const& override
        {
            return "Simply_derived";
        }
    };
    template <bool withEq>
    struct Derived_w_eq : Inherit<Base<withEq>, Derived_w_eq<withEq>> {
        using Inherit = unsot::Inherit<Base<withEq>, Derived_w_eq<withEq>>;
        using typename Inherit::This;
        virtual ~Derived_w_eq() = default;
        String to_string() const& override
        {
            return "Derived_w_eq";
        }
        bool lequals(const This&) const
        {
            return true;
        }
    };
    template <bool withEq>
    struct Derived_from_derived_w_eq : Inherit<Derived_w_eq<withEq>, Derived_from_derived_w_eq<withEq>> {
        using Inherit = unsot::Inherit<Derived_w_eq<withEq>, Derived_from_derived_w_eq<withEq>>;
        using typename Inherit::This;
        virtual ~Derived_from_derived_w_eq() = default;
        String to_string() const& override
        {
            return "Derived_from_derived_w_eq";
        }
    };
    template <bool withEq>
    struct Derived_w_eq_from_derived_w_eq : Inherit<Derived_w_eq<withEq>, Derived_w_eq_from_derived_w_eq<withEq>> {
        using Inherit = unsot::Inherit<Derived_w_eq<withEq>, Derived_w_eq_from_derived_w_eq<withEq>>;
        using typename Inherit::This;
        virtual ~Derived_w_eq_from_derived_w_eq() = default;
        String to_string() const& override
        {
            return "Derived_w_eq_from_derived_w_eq";
        }
        bool lequals(const This&) const
        {
            return true;
        }
    };

    template <bool withEq>
    String to_string(const Base<withEq>* rhs)
    {
        return rhs->to_string();
    }

    template <bool withEq>
    struct Dynamic_case : Inherit<Case<Base<withEq>*>> {
        using Inherit = unsot::Inherit<Case<Base<withEq>*>>;

        using Inherit::Inherit;

        using typename Inherit::Input;
        using typename Inherit::Output;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return *result_ == *expected_;
        }

        Output result() override
        {
            auto& in = this->cinput();
            cout << in << " " << (this->should_throw() ? "!=" : "==") << " " << this->cexpected() << endl;
            return in;
        }
    };

    template <bool withEq>
    struct Dynamic_test {
        using Base = test::Base<withEq>;
        using Derived = test::Derived<withEq>;
        using Simply_derived = test::Simply_derived<withEq>;
        using Derived_w_eq = test::Derived_w_eq<withEq>;
        using Derived_from_derived_w_eq = test::Derived_from_derived_w_eq<withEq>;
        using Derived_w_eq_from_derived_w_eq = test::Derived_w_eq_from_derived_w_eq<withEq>;

        static_assert(is_base_of_v<Base, Derived>);
        static_assert(is_base_of_v<Base, Simply_derived>);
        static_assert(is_base_of_v<Base, Derived_w_eq>);
        static_assert(is_base_of_v<Base, Derived_from_derived_w_eq>);
        static_assert(is_base_of_v<Base, Derived_w_eq_from_derived_w_eq>);
        static_assert(!is_base_of_v<Derived, Simply_derived>
                      && !is_base_of_v<Simply_derived, Derived>);
        static_assert(!is_base_of_v<Derived, Derived_w_eq>
                      && !is_base_of_v<Derived_w_eq, Derived>);
        static_assert(is_base_of_v<Derived_w_eq, Derived_from_derived_w_eq>);
        static_assert(is_base_of_v<Derived_w_eq, Derived_w_eq_from_derived_w_eq>);
        static_assert(!is_base_of_v<Derived_from_derived_w_eq, Derived_w_eq_from_derived_w_eq>
                      && !is_base_of_v<Derived_w_eq_from_derived_w_eq, Derived_from_derived_w_eq>);

        static void perform()
        {
            Base* base_l = new Base;
            Base* der_l = new Derived;
            Base* sder_l = new Simply_derived;
            Base* der_eq_l = new Derived_w_eq;
            Base* der_der_eq_l = new Derived_from_derived_w_eq;
            Base* der_eq_der_eq_l = new Derived_w_eq_from_derived_w_eq;

            const String dyn_msg = "dynamic skelet classes"s + (withEq ? " [with equals in Base]" : "");
            Suite(dyn_msg).test<Dynamic_case<withEq>>({
                {base_l,                                    base_l,                 },
                {base_l,                                    sder_l,                 },
                {base_l,                                    der_l,                  },
                {base_l,                                    der_eq_l,               },
                {base_l,                                    der_der_eq_l,           },
                {base_l,                                    der_eq_der_eq_l,        },
                {der_l,                                     der_l,                  },
                {sder_l,                                    base_l,                 },
                {sder_l,                                    der_l,                  },
                {sder_l,                                    sder_l,                 },
                {sder_l,                                    der_eq_l,               },
                {sder_l,                                    der_der_eq_l,           },
                {sder_l,                                    der_eq_der_eq_l,        },
                {der_eq_l,                                  der_eq_l,               },
                {der_eq_l,                                  der_der_eq_l,           },
                {der_eq_l,                                  der_eq_der_eq_l,        },
                {der_der_eq_l,                              der_der_eq_l,           },
                {der_eq_der_eq_l,                           der_eq_der_eq_l,        },
            });

            Suite(dyn_msg, true).test<Dynamic_case<withEq>>({
                {der_l,                                     base_l,                 },
                {der_l,                                     sder_l,                 },
                {der_l,                                     der_eq_l,               },
                {der_l,                                     der_der_eq_l,           },
                {der_l,                                     der_eq_der_eq_l,        },
                {der_eq_l,                                  base_l,                 },
                {der_eq_l,                                  der_l,                  },
                {der_eq_l,                                  sder_l,                 },
                {der_der_eq_l,                              base_l,                 },
                {der_der_eq_l,                              sder_l,                 },
                {der_der_eq_l,                              der_l,                  },
                {der_der_eq_l,                              der_eq_l,               },
                {der_der_eq_l,                              der_eq_der_eq_l,        },
                {der_eq_der_eq_l,                           base_l,                 },
                {der_eq_der_eq_l,                           sder_l,                 },
                {der_eq_der_eq_l,                           der_l,                  },
                {der_eq_der_eq_l,                           der_eq_l,               },
                {der_eq_der_eq_l,                           der_der_eq_l,           },
            });

            delete base_l;
            delete der_l;
            delete sder_l;
            delete der_eq_l;
            delete der_der_eq_l;
            delete der_eq_der_eq_l;
        }
    };

    ////////////////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;

    ////////////////////////////////////////////////////////////////

    Dynamic_test<false>::perform();
    Dynamic_test<true>::perform();

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
