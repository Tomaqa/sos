#include "test.hpp"
#include "smt/sat/solver/online/minisat.hpp"

#include "util/scope_guard.hpp"

#include "smt/sat/solver.tpp"
#include "smt/sat/solver/minisat.tpp"
#include "smt/sat/solver/online/minisat.tpp"

#include <deque>

namespace unsot::test {
    using namespace smt;
    using namespace smt::sat::solver;
    using namespace smt::sat::solver::online;
    using namespace smt::sat::solver::minisat;
    using std::deque;

    template <typename T>
    ostream& operator <<(ostream& os, const deque<T>& q)
    {
        for (auto& e : q) {
            os << to_string(e) << " ";
        }
        return os;
    }

    using Minisat = sat::solver::online::Minisat<>;
    using Clause = Minisat::Clause;

    struct Test_smt_solver {
        struct Check_sat : sat::solver::online::Smt_check_sat_base<typename Minisat::Online_conf> {
            Minisat* minisat_l{};

            deque<Var_id> ids{};
            Vector<Mask> masks{};
            Mask values{};

            int _decision_level{0};

            void init(Minisat& s)
            {
                minisat_l = &s;
                s.connect(*this);
                const int n_vars = s.vars_size();
                masks.reserve(n_vars);
                masks.push_back(Mask(n_vars));
                values.resize(n_vars);
            }

            Mask& mask()
            {
                return masks[decision_level()];
            }

            String values_to_string(int level = -1)
            {
                if (level < 0) level = decision_level();
                auto& m = masks[level];
                String str;
                for (size_t i = 0; i < m.size(); ++i) {
                    if (m[i]) str += to_string(values[i]);
                    else str += "X";
                    str += " ";
                }
                return str;
            }

            int decision_level() const noexcept override
            {
                return _decision_level;
            }

            void notice_new_decision_level() override
            {
                cout << "New decision level: " << decision_level() << " -> " << decision_level()+1 << endl;
                masks.push_back(mask());
                ++_decision_level;
                assert(int(masks.size()) == decision_level()+1);
            }

            void notice(Lit p) override
            {
                notice(move(p), true);
            }

            void notice(Lit p, bool print)
            {
                auto id = p.var_id();
                assert(minisat_l->contains(id));
                if (print) cout << "Propagate: " << p << endl;
                ids.push_back(id);
                util::mask(mask(), id);
                values[id] = p.sign();
            }

            Online_conf::T_propagate_result t_propagate_exec() override
            {
                assert(minisat_l && minisat_l->vars_size());

                if (ids.empty()) return true;
                cout << "Perform T-propagation: " << ids << endl;
                cout << values_to_string() << endl;

                auto sg = Scope_guard([this]{ ids.clear(); });

                /// At most one even variable can be negative
                /// Any odd positive variable implies next odd to be positive
                Clause prop;
                const int n_vars = minisat_l->vars_size();
                while (!ids.empty()) {
                    auto i = ids.front();
                    ids.pop_front();
                    assert(masked(mask(), i));
                    if (i&1) {
                        auto j = i+2;
                        if (!values[i] || j >= n_vars) continue;
                        if (!masked(mask(), j)) {
                            Lit p(j, true);
                            prop.push_back(p);
                            notice(p, false);
                            minisat_l->sat_propagate(p);
                            continue;
                        }

                        if (values[j]) continue;
                        Clause confl{Lit(i, false), Lit(j, true)};
                        cout << "T-inconsistency(odds): " << confl << endl;
                        /// `sat_backtrack_to` is missing,
                        /// but it is luckily not necessary for this instance
                        minisat_l->sat_learn(move(confl));
                        return false;
                    }

                    if (values[i]) continue;
                    for (int j = 0; j < n_vars; ++j) {
                        if (j&1 || i == j || !masked(mask(), j) || values[j]) continue;
                        Clause confl{Lit(i, true), Lit(j, true)};
                        cout << "T-inconsistency(evens): " << confl << endl;
                        minisat_l->sat_learn(move(confl));
                        return false;
                    }
                }

                if (prop.empty()) return true;
                cout << "T-propagated clause: " << prop << endl;
                return unknown;
            }

            void notice_backtrack() override
            {
                notice_backtrack(true);
            }

            void notice_backtrack(bool print)
            {
                if (print) cout << "Backtrack: " << values_to_string();
                masks.pop_back();
                --_decision_level;
                if (print) cout << "-> " << values_to_string() << endl << endl;
            }

            void notice_backtrack_to(int level) override
            {
                assert(level < decision_level());
                cout << "Backtrack " << decision_level() << " -> " << level << endl;
                cout << values_to_string();
                while (level != decision_level()) notice_backtrack(false);
                cout << "-> " << values_to_string() << endl << endl;
            }
        };

        Unique_ptr<Check_sat> check_sat_ptr{};

        int cverbosity() const noexcept
        {
            return 4;
        }

        void init(Minisat& s)
        {
            check_sat_ptr = make_unique<Check_sat>();
            check_sat_ptr->init(s);
        }
    };

    ////////////////////////////////////////////////////////////////

}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;
    using test::Minisat;

    ////////////////////////////////////////////////////////////////

    Test_smt_solver smt;
    Minisat s = Minisat::cons();

    auto x0 = s.new_var();
    Lit x0t(x0, true);
    Lit x0f(x0, false);
    auto x1 = s.new_var();
    Lit x1t(x1, true);
    Lit x1f(x1, false);
    auto x2 = s.new_var();
    Lit x2t(x2, true);
    Lit x2f(x2, false);
    auto x3 = s.new_var();
    Lit x3t(x3, true);
    Lit x3f(x3, false);
    auto x4 = s.new_var();
    Lit x4t(x4, true);
    Lit x4f(x4, false);
    auto x5 = s.new_var();
    Lit x5t(x5, true);
    Lit x5f(x5, false);

    smt.init(s);

    s.add_clause(x0f, x2f, x4f);
    s.add_clause(x2t, x3f);
    s.add_clause(x0f, x5f);
    s.add_clause(x1t, x2f);
    s.add_clause(x3f, x4f);

    cout << endl << "CNF:" << endl << s.to_cnf() << endl;

    const bool sat = s.solve();

    cout << endl << "Sat: " << sat << endl;
    expect(sat, "Expected 'sat'!");

    cout << "Model: " << s.cmodel() << endl;

    expect(s.model_value(x0) ==  true, "Expected  x0!");
    expect(s.model_value(x1) == false, "Expected ~x1!");
    expect(s.model_value(x2) == false, "Expected ~x2!");
    expect(s.model_value(x3) == false, "Expected ~x3!");
    expect(s.model_value(x4) ==  true, "Expected  x4!");
    expect(s.model_value(x5) == false, "Expected ~x5!");
    decltype(s.cmodel()) exp = {1, 0, 0, 0, 1, 0};
    expect(s.cmodel() == exp, "Expected model: "s + to_string(exp));

    s.add_clause({x0f, x1t, x2t, x3t, x4f, x5t});
    expect(!s.solve(), "Expected 'unsat' after forbidding the final assignment!");

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
