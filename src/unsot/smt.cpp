#include "smt.hpp"

namespace unsot::smt {
    String Sat::to_string() const& noexcept
    {
        switch (int val(*this); val) {
        case sat: return "sat";
        case unsat: return "unsat";
        default: return "unknown";
        }
    }
}
