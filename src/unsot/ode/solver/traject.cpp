#include "ode/solver.hpp"
#include "ode/solver/traject.hpp"

namespace unsot::ode::solver {
    using Traject = Base::Traject;

    Traject::Traject(const flow::Ptr& flow_ptr)
        : _flow_ptr(flow_ptr)
    { }

    void Traject::reset(size_t size_)
    {
        const bool do_reserve = (size_ > size());
        clear();
        if (do_reserve) reserve(size_);
    }

    void Traject::add_step(State state)
    {
        push_back(move(state));
    }

    bool Traject::is_set_ostream() const noexcept
    {
        return cos_ptr();
    }

    void Traject::set_ostream(Ostream& os) noexcept
    {
        os_ptr() = &os;
    }

    bool Traject::write_block(int id) const
    {
        if (!is_set_ostream()) return false;
        os() << "## " << id << std::endl << *this << std::endl;
        return true;
    }

    String Traject::to_string() const&
    {
        using unsot::to_string;
        String str(to_string(ckeys().ct_ode_keys_slice()) + "\n");
        for (auto& state : *this) {
            str += to_string(state.ct_ode_values_slice()) + "\n";
        }
        str += "\n";
        return str;
    }
}
