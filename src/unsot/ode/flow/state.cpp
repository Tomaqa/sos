#include "ode/flow.hpp"
#include "ode/flow/state.hpp"

#include "util/alg.hpp"
#include "expr/pos.hpp"

namespace unsot::ode::flow {
    using namespace expr;

    State& State::operator =(State rhs)
    {
        swap(rhs);
        return *this;
    }

    void State::swap(State& rhs)
    {
        Inherit::swap(rhs);
        std::swap(_ode_size, rhs._ode_size);
        std::swap(_arg_size, rhs._arg_size);
    }

    State::State(Real t_, Reals ode_vals, Reals arg_vals, Real tau_)
        : Inherit(move(ode_vals))
    {
        const size_t ode_size_ = size();
        reserve(ode_size_ + std::size(arg_vals) + special_args_size);
        insert(cend(), make_move_iterator(std::begin(arg_vals)),
                       make_move_iterator(std::end(arg_vals)));
        init(t_, tau_, ode_size_);
    }

    State::State(Real t_, Reals values, size_t ode_size_, Real tau_)
        : Inherit(move(values))
    {
        init(t_, tau_, ode_size_);
    }

    void State::init(Real t_, Real tau_, size_t ode_size_)
    {
        using unsot::to_string;
        expect(tau_ <= t_,
               "Value of tau cannot be greater than t: "s
               + to_string(tau_) + " > " + to_string(t_));
        set_subsizes(ode_size_);
        check();
        push_back(t_);
        push_back(tau_);
    }

    void State::set_subsizes(size_t ode_size_)
    {
        using unsot::to_string;
        expect(ode_size_ <= size(),
               "Passed size of ODE values is greater than total size: "s
               + to_string(ode_size_) + " > " + to_string(size()));
        _ode_size = ode_size_;
        _arg_size = size() - ode_size();
    }

    bool State::valid() const noexcept
    {
        return valid_subsizes();
    }

    void State::check() const
    {
        check_subsizes();
    }

    bool State::valid_subsizes() const noexcept
    {
        return ode_size() > 0
            && ode_size() + arg_size() == size();
    }

    void State::check_subsizes() const
    {
        using unsot::to_string;
        expect(valid_subsizes(),
               "Invalid argument values (sub)sizes: "s
               + to_string(ode_size()) + "," + to_string(arg_size())
               + "; total: " + to_string(size()));
    }

    State State::parse(List ls)
    try {
        State rhs;
        rhs.parse_impl(move(ls));
        return rhs;
    }
    catch (const Error& err) {
        THROW("Invalid format of reals state '") + ls.to_string() + "':\n" + err;
    }

    void State::parse_impl(List&& ls)
    {
        Const_pos pos(ls);
        expect(ls.size() >= 2 && ls.size() <= 4 && is_elem_at(pos)
               && is_flat_list_at(pos.next())
               && (ls.size() == 2
                   || is_flat_list_at(pos.next()) || is_elem_at(pos))
               && (ls.size() < 4 || is_elem_at(pos.next())),
               "Expected value and one or two lists of values "s
               + "and optionally value at the end: "s
               + "initial t and ODE (and optionally constant)"
               + " flow arguments (and opt. initial tau).");

        Real t_ = to_real_check(move(ls.front()));
        ls.pop_front();
        Real tau_ = 0;
        if (Elem_base::is_me(ls.cback())) {
            tau_ = to_real_check(move(ls.back()));
            ls.pop_back();
        }

        const size_t ode_size_ = List::cast(ls.cfront()).size();
        Reals vals = to_reals_check(ls.flatten());

        *this = State(t_, move(vals), ode_size_, tau_);
    }

    Reals State::ct_ode_values_slice() const
    {
        Reals vals;
        vals.reserve(t_ode_size());
        vals.push_back(ct());
        copy(code_begin(), code_end(), back_inserter(vals));
        return vals;
    }

    Reals State::t_ode_values_slice()
    {
        Reals vals;
        vals.reserve(t_ode_size());
        vals.push_back(ct());
        move(ode_begin(), ode_end(), back_inserter(vals));
        return vals;
    }

    String State::to_string() const&
    {
        using unsot::to_string;
        String str(to_string(ct())
                   + " (" + to_string(ctau()) + ") :\t");
        str += to_string(code_values_slice());
        if (arg_size() > 0) {
            str += "| " + to_string(carg_values_slice());
        }
        return str;
    }
}
