#include "ode/flow.hpp"
#include "ode/flow/invariants.hpp"

#include "util/alg.hpp"
#include "expr/alg.hpp"

namespace unsot::ode::flow::aux {
    using namespace expr;
    using Invariants = Flow::Invariants;
    using Local_invariants = Flow::Local_invariants;

    Invariants::Invariants()
        : _empty(true)
    { }

    Invariants::Invariants(const Keys& keys_)
        : Inherit(keys_), _empty(true)
    { }

    Invariants::Invariants(const Keys& keys_, Formula phi)
        : Inherit(keys_, move(phi)), _empty(false)
    { }

    Invariants::Invariants(const Keys& keys_, Formulas phis, Formula def_phi)
        : Inherit(keys_, move(phis), move(def_phi)),
          _empty(std::empty(cfuns()))
    { }

    void Invariants::init()
    {
        if (empty()) return;
        if (std::empty(cfuns()) || all_of(cfuns(), mem_fn(&Fun::empty))) {
            _empty = true;
            return;
        }
        _empty = false;
        Inherit::init();
    }

    void Invariants::init_funs()
    {
        mask().reserve(size());
        Inherit::init_funs();
    }

    void Invariants::init_fun(Fun& fun_)
    {
        const bool mask1 = fun_.empty();
        mask().push_back(mask1);
        if (!mask1) Inherit::init_fun(fun_);
    }

    void Invariants::check_funs() const
    try {
        Inherit::check_funs();
    }
    catch (const Error& err) {
        using unsot::to_string;
        THROW("Invalid invariant formulas '") + to_string(cfuns()) + "'\n: " + err;
    }

    bool Invariants::valid_formula_head_impl(const Formula& phi) const
    {
        if (std::empty(phi.coper()) && phi.empty()) throw ignore;
        if (!Inherit::valid_formula_head_impl(phi)) return false;

        if (!contains_any_valid_fun_key(phi)) return false;

        if (phi.coper() == "=" && !is_atom_equality(phi)) return false;

        return true;
    }

    bool Invariants::valid_formula_body_impl(const Formula& phi) const
    {
        assert(!phi.empty() || std::empty(phi.coper()));
        if (phi.empty()) throw ignore;
        if (!Inherit::valid_formula_body_impl(phi)) return false;

        if (is_atom_formula(phi)) return true;

        assert(Fun::Opers::valid_bool(phi));
        for_each(phi, as_cformulas_check([this](auto& subphi){
            if (!valid_formula(subphi)) THROW(error);
        }));
        return true;
    }

    bool Invariants::is_atom_formula(const Formula& phi) const noexcept
    {
        assert(phi.initialized());
        if (phi.coper() != "=") return valid_atom_oper(phi);

        return is_atom_equality(phi);
    }

    bool Invariants::is_atom_equality(const Formula& phi) const noexcept
    {
        assert(phi.coper() == "=");

        //+ check this in tests
        return all_of(phi, [this](auto& eptr){
            if (Elem_base::is_me(eptr)) return true;
            return !is_atom_formula(Formula::cast(eptr));
        });
    }

    bool Invariants::valid_atom_oper(const Formula& phi)
    {
        return Fun::Opers::valid_pred(phi);
    }

    bool Invariants::valid_fun_key(const Key& key_) const noexcept
    {
        return is_global_key(key_) || Inherit::valid_fun_key(key_);
    }

    void Invariants::check_fun_key(const Key& key_) const
    {
        using unsot::to_string;
        expect(valid_fun_key(key_),
               "Invalid Invariants key: "s + to_string(key_));
    }

    bool Invariants::valid_id(const Id& id) const noexcept
    {
        return empty() || Inherit::valid_id(id);
    }

    bool Invariants::operator ()(const Reals& state, const Id& id) const
    {
        if (empty() || masked(id)) return true;
        return Inherit::operator ()(state, id);
    }

    String Invariants::body_to_string() const
    {
        if (empty()) return "\n  true\n";
        return Inherit::body_to_string();
    }

    String Invariants::fun_to_string(const Id& id_) const
    {
        if (initialized() && masked(id_)) return "true";
        return Inherit::fun_to_string(id_);
    }

    bool Invariants::equals_this(const This& rhs) const noexcept
    {
        if (empty()) return rhs.empty();
        if (rhs.empty()) return false;
        return Inherit::equals_this(rhs);
    }

    bool Invariants::lequals(const This& rhs) const noexcept
    {
        return cmask() == rhs.cmask();
    }

    ////////////////////////////////////////////////////////////////

    Local_invariants::Local_invariants(const Keys& keys_, Key key_,
                                       Formulas phis, Formula def_phi)
        : Inherit(keys_, move(key_), move(phis), move(def_phi))
    { }

    bool Local_invariants::valid_formula_head_impl(const Formula& phi) const
    {
        if (!Inherit::valid_formula_head_impl(phi)) return false;

        /// `Invariants::valid_formula_body' checks recursively all subphis
        /// s.t. they are equal
        assert(valid_fun_key(ckey()));
        return get_fun_key(phi) == ckey();
    }
}
