#include "ode/flow.hpp"

#include "util/alg.hpp"
#include "expr/pos.hpp"
#include "ode/flow/parse.hpp"

namespace unsot::ode::flow::aux {
    using namespace expr;

    Flow::Flow(Keys keys_)
    {
        set_keys(move(keys_));
    }

    Flow::Flow(Keys keys_, All_formulas all_odes_phis,
               All_formulas_map all_invs_phis_map)
        : Flow(move(keys_))
    {
        init(move(all_odes_phis), move(all_invs_phis_map));
    }

    Flow::Flow(Keys keys_, Formulas odes_phis,
               All_formulas_map all_invs_phis_map)
        : Flow(move(keys_), All_formulas{move(odes_phis)},
               move(all_invs_phis_map))
    { }

    Flow::Flow(Keys keys_, All_formulas all_odes_phis, Formulas invs_phis)
        : Flow(move(keys_))
    {
        auto map = Parse(ckeys()).divide_invs_formulas(move(invs_phis));
        init(move(all_odes_phis), move(map));
    }

    Flow::Flow(Keys keys_, Formulas odes_phis, Formulas invs_phis)
        : Flow(move(keys_), All_formulas{move(odes_phis)}, move(invs_phis))
    { }

    void Flow::init(All_formulas all_odes_phis,
                    All_formulas_map all_invs_phis_map)
    {
        set_all_odes(move(all_odes_phis));
        set_all_invs(move(all_invs_phis_map));
        init();
    }

    void Flow::set_all_odes(All_formulas&& all_odes_phis)
    {
        using unsot::to_string;

        const size_t count = std::size(all_odes_phis);
        const size_t size_ = size();
        expect(count == size_,
               "Size of ODEs formulas differs from the Flow size ("s
               + to_string(count) + " != " + to_string(size_)
               + "):\n" + to_string(all_odes_phis));

        auto& keys_ = ckeys();
        to(all_odes(), move(all_odes_phis), ckeys().code_begin(),
            [&keys_](auto&& odes_phis, auto& key_){
                return Odes::cons(keys_, key_, move(odes_phis));
        });
    }

    void Flow::set_all_invs(All_formulas_map&& all_invs_phis_map)
    {
        using unsot::to_string;

        set_ginvs(all_invs_phis_map);
        set_timer(all_invs_phis_map);
        set_all_linvs(all_invs_phis_map);

        expect(std::empty(all_invs_phis_map),
               "Additional content in the all-invariants formulas map: "s
               + to_string(all_invs_phis_map));
    }

    void Flow::set_ginvs(All_formulas_map& all_invs_phis_map)
    {
        auto ginvs_phis = all_invs_phis_map.extract_value(global_key).value();
        set_ginvs(move(ginvs_phis));
    }

    void Flow::set_timer(All_formulas_map& all_invs_phis_map)
    {
        auto tau_phis = all_invs_phis_map.extract_value(tau_key).value();
        auto t_phis = all_invs_phis_map.extract_value(t_key).value();
        set_timer(move(tau_phis), move(t_phis));
    }

    void Flow::set_ginvs(Formulas&& phis)
    {
        set_ginvs({ckeys(), move(phis)});
    }

    void Flow::set_timer(Formulas&& tau_phis, Formulas&& t_phis)
    {
        set_timer({ckeys(), move(tau_phis), move(t_phis)});
    }

    void Flow::set_keys(Keys&& keys_)
    {
        keys_ptr() = new_keys(move(keys_));
    }

    void Flow::set_ginvs(Invariants&& ginvs_)
    {
        ginvs_ptr() = new_invs(move(ginvs_));
    }

    void Flow::set_timer(Timer&& timer_)
    {
        timer_ptr() = new_timer(move(timer_));
    }

    void Flow::init_all_linvs()
    {
        auto& keys_ = ckeys();
        to(all_linvs(), keys_.code_begin(), keys_.code_end(),
           keys_.ode_size(), [&keys_](auto& key){
            return Local_invariants::cons(keys_, key);
        });
    }

    void Flow::set_all_linvs(All_formulas_map& all_invs_phis_map)
    {
        auto& keys_ = ckeys();
        to(all_linvs(), keys_.code_begin(), keys_.code_end(),
           keys_.ode_size(), [&keys_, &all_invs_phis_map](auto& key){
            auto phis = all_invs_phis_map.extract_value(key).value();
            return Local_invariants::cons(keys_, key, move(phis));
        });
    }

    void Flow::init()
    {
        check();
    }

    bool Flow::valid() const noexcept
    {
        return valid_keys();
    }

    void Flow::check() const
    {
        check_keys();
    }

    bool Flow::valid_keys() const noexcept
    {
        return valid_keys_tp(call_odes())
            && valid_keys_tp(call_linvs());
    }

    void Flow::check_keys() const
    {
        check_keys_tp(call_odes());
        check_keys_tp(call_linvs());
    }

    Flow Flow::parse(List ls)
    {
        return Parse(move(ls)).perform();
    }

    void Flow::check_input(const State& state, const Config& config) const
    {
        check_state(state);
        check_config(config);
    }

    bool Flow::valid_state(const State& state) const noexcept
    {
        return ckeys().valid_state(state);
    }

    void Flow::check_state(const State& state) const
    {
        ckeys().check_state(state);
    }

    bool Flow::valid_config(const Config& config) const noexcept
    {
        return valid_ids(call_odes(), config.odes_ids)
            && ctimer().valid_ids(config.tau_id, config.t_id)
            && valid_ids(call_linvs(), config.linvs_ids)
            && cginvs().valid_id(config.ginv_id);
    }

    void Flow::check_config(const Config& config) const
    {
        check_ids(call_odes(), config.odes_ids);
        ctimer().check_ids(config.tau_id, config.t_id);
        check_ids(call_linvs(), config.linvs_ids);
        cginvs().check_id(config.ginv_id);
    }

    void Flow::eval_step(const Reals& in, Reals& out,
                         const Config& config) const
    {
        const auto& ids = config.odes_ids;
        if (std::empty(ids)) {
            transform(call_odes(), begin(out),
                      [&in](auto& odes){ return odes(in); });
            return;
        }
        transform(call_odes(), cbegin(ids), begin(out),
                  [&in](auto& odes, auto& id_){
            return odes(in, id_);
        });
    }

    bool Flow::timer(const Reals& state, const Config& config) const
    {
        return invoke(ctimer(), state, config.tau_id, config.t_id);
    }

    bool Flow::local_invariants(const Reals& state,
                                const Config& config) const
    {
        const auto& ids = config.linvs_ids;
        if (std::empty(ids)) {
            return all_of(call_linvs(), [&state](auto& linvs){
                return linvs(state);
            });
        }
        return all_of(call_linvs(), cbegin(ids),
                      [&state](auto& linvs, auto& id_){
            return linvs(state, id_);
        });
    }

    bool Flow::global_invariant(const Reals& state,
                                const Config& config) const
    {
        const auto& id_ = config.ginv_id;
        return invoke(cginvs(), state, id_);
    }

    bool Flow::invariant(const Reals& state, const Config& config) const
    {
        return timer(state, config)
            && local_invariants(state, config)
            && global_invariant(state, config);
    }

    String Flow::to_string() const&
    {
        using unsot::to_string;
        return ckeys().to_string() + "\n"
             + to_string(call_odes()) + "\n"
             + to_string(ctimer()) + "\n"
             + to_string(call_linvs()) + "\n"
             + to_string(cginvs());
    }

    bool Flow::equals(const This& rhs) const noexcept
    {
        return ckeys() == rhs.ckeys()
            && call_odes() == rhs.call_odes()
            && ctimer() == rhs.ctimer()
            && call_linvs() == rhs.call_linvs()
            && cginvs() == rhs.cginvs();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::ode::flow {
    using namespace expr;

    Config::Config(Ids odes_ids_, Ids timer_ids, Ids linvs_ids_, Id ginv_id_)
        : Config(move(odes_ids_), def_id, def_id,
                 move(linvs_ids_), move(ginv_id_))
    {
        set_timer_ids(timer_ids);
    }

    Config::Config(Ids odes_ids_, Ids timer_ids, Ids invs_ids)
        : Config(move(odes_ids_))
    {
        set_timer_ids(timer_ids);
        set_linvs_ids_and_ginv_id(move(invs_ids));
    }

    Config::Config(Ids odes_ids_, Ids invs_ids)
        : Config(move(odes_ids_))
    {
        set_timer_ids(invs_ids);
        set_linvs_ids_and_ginv_id(move(invs_ids));
    }

    void Config::set_timer_ids(Ids& invs_ids) noexcept
    {
        if (std::empty(invs_ids)) return;

        const auto bit = begin(invs_ids);
        const auto eit = end(invs_ids);
        auto it = bit;
        if (it != eit) {
            tau_id = move(*it++);
            if (it != eit) t_id = move(*it++);
        }

        if (it == eit) invs_ids.clear();
        else invs_ids.erase(bit, it);
    }

    void Config::set_linvs_ids_and_ginv_id(Ids&& invs_ids)
    {
        if (std::empty(invs_ids)) return;

        using unsot::to_string;
        expect(!std::empty(odes_ids),
               "Cannot deduce the number of inv. ids with empty ODE ids: "
               + to_string(invs_ids));
        expect(std::size(invs_ids) >= std::size(odes_ids)
               && std::size(invs_ids) <= std::size(odes_ids)+1,
               "Expected local invariants ids and a global one, "s
               + "or only the local ones; got: "
               + to_string(invs_ids)
               + " (with these ODE ids: " + to_string(odes_ids) + ")");

        if (std::size(invs_ids) > std::size(odes_ids)) {
            ginv_id = move(invs_ids.back());
            invs_ids.pop_back();
        }
        linvs_ids = move(invs_ids);
    }

    Config Config::parse(List ls)
    {
        using unsot::to_string;

        if (ls.emerge().empty()) return {};
        if (is_flat(ls)) return {to_values_check<Id>(move(ls))};

        expect(ls.size() >= 1 && ls.size() <= 5
               && List::is_me(ls.cfront()),
               "Expected ids of chosen variants"s
               + "(<odes_ids> [<timer ids>] [<linvs_ids>] [<ginv_id>])"
               + " or (<ode_ids> [<timer ids>] [<invs_ids>])"
               + " or (<ode_ids> [<invs_ids>])"
               + ", got: " + ls.to_string());

        Pos pos(ls);
        Ids odes_ids_ = to_values_check<Id>(extract_list(pos));

        const int max_invs_size = std::size(odes_ids_) + 3;
        Ids invs_ids;
        if (pos) {
            invs_ids.reserve(max_invs_size);
            if (is_elem_at(pos)) {
                for (int i = 0; i < 2 && pos && is_elem_at(pos); ++i) {
                    invs_ids.emplace_back(cast_int_check(move(pos.get())));
                }
            }
            else {
                Ids invs_ids_ = to_values_check<Id>(extract_list_check(pos));
                const int invs_size_ = std::size(invs_ids_);
                expect(invs_size_ <= max_invs_size,
                       "Too many invs. ids: "s + to_string(invs_ids_));
                expect(invs_size_ <= 2 || !pos,
                       "Too many timer. ids: "s + to_string(invs_ids_));
                append(invs_ids, move(invs_ids_));
                if (std::size(invs_ids) < 2) {
                    invs_ids.insert(end(invs_ids), 2-std::size(invs_ids),
                                    def_id);
                }
            }
        }

        expect(!pos || (is_list_at(pos)
                        && int(std::size(invs_ids)) <= max_invs_size),
               "Too many arguments: "s + to_string(invs_ids)
               + "... " + pos.to_string());
        if (pos) {
            Ids linvs_ids_ = to_values_check<Id>(extract_list(pos));
            expect(int(std::size(linvs_ids_)) < max_invs_size,
                   "Too many local invs. ids: "s + to_string(linvs_ids_));
            append(invs_ids, move(linvs_ids_));

            if (pos) {
                expect(pos.at_back() && is_elem_at(pos),
                       "Expected single global inv. id, got: "s
                       + pos.to_string())
                invs_ids.emplace_back(cast_int_check(move(pos.peek())));
            }
        }

        return {move(odes_ids_), move(invs_ids)};
    }

    String Config::to_string() const&
    {
        using unsot::to_string;
        return "( "s + to_string(odes_ids) + ") { "
             + to_string(tau_id) + " " + to_string(t_id) + " } [ "
             + to_string(linvs_ids) + "] < " + to_string(ginv_id) + " >";
    }

    bool Config::equals(const This& rhs) const noexcept
    {
        return odes_ids == rhs.odes_ids
            && tau_id == rhs.tau_id && t_id == rhs.t_id
            && linvs_ids == rhs.linvs_ids && ginv_id == rhs.ginv_id;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::ode::flow {
    void check_is_special_key(const Key& key)
    {
        expect(is_special_key(key),
               "Expected special key, got: "s + to_string(key));
    }

    void check_is_global_key(const Key& key)
    {
        expect(is_global_key(key),
               "Expected global key, got: "s + to_string(key));
    }
}
