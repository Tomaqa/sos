#pragma once

#include "smt/solver.hpp"

#include "util/alg.hpp"
#include "util/string/alg.hpp"
#include "expr/bools.hpp"
#include "expr/preprocess.hpp"
#include "smt/solver/run.hpp"

#include <iostream>
#include <sstream>

namespace unsot::smt::solver {
    template <typename S>
    Crtp<S>::Crtp()
    {
        option(":produce-models") = "1";
        option(":diagnostic-output-channel") = "stderr";
        option(":regular-output-channel") = "stdout";
        option(":verbosity") = "0";
        option(":t-strategy");
    }

    template <typename S>
    List Crtp<S>::preprocess(String input)
    {
        istringstream iss(move(input));
        return preprocess(iss);
    }

    template <typename S>
    List Crtp<S>::preprocess(istream& is)
    {
        return make_parser().preprocess(is);
    }

    template <typename S>
    void Crtp<S>::parse(String input)
    {
        List ls = preprocess(move(input));
        parse_only(move(ls));
    }

    template <typename S>
    void Crtp<S>::parse(istream& is)
    {
        List ls = preprocess(is);
        parse_only(move(ls));
    }

    template <typename S>
    Preprocess Crtp<S>::preprocess_lines_only(String input)
    {
        istringstream iss(move(input));
        return preprocess_lines_only(iss);
    }

    template <typename S>
    Preprocess Crtp<S>::preprocess_lines_only(istream& is)
    {
        return make_parser().preprocess_lines(is);
    }

    template <typename S>
    List Crtp<S>::preprocess_list_only(List ls)
    {
        make_parser().preprocess_list(ls);
        return ls;
    }

    template <typename S>
    void Crtp<S>::parse_only(List ls)
    {
        make_parser().parse(ls);
    }

    template <typename S>
    void Crtp<S>::be_verbose() noexcept
    {
        if (cverbosity() <= 0) verbosity() = 1;
        else ++verbosity();
    }

    template <typename S>
    void Crtp<S>::be_quiet() noexcept
    {
        if (cverbosity() >= 0) verbosity() = -1;
        else --verbosity();
    }

    template <typename S>
    Istream& Crtp<S>::is() const noexcept
    {
        if (auto run_l_ = run_l()) return run_l_->is();
        return std::cin;
    }

    template <typename S>
    Ostream& Crtp<S>::os() const noexcept
    {
        if (auto run_l_ = run_l()) return run_l_->os();
        return std::cout;
    }

    template <typename S>
    bool Crtp<S>::valid_option(const Option& opt, const Option_value& val) const noexcept
    try {
        return valid_option(opt) && valid_option_value(opt, val);
    }
    catch (Dummy) {
        return false;
    }

    template <typename S>
    void Crtp<S>::check_option(const Option& opt, const Option_value& val) const
    {
        check_option(opt);
        check_option_value(opt, val);
    }

    template <typename S>
    bool Crtp<S>::valid_option(const Option& opt) const noexcept
    {
        return coptions_map().contains(opt);
    }

    template <typename S>
    void Crtp<S>::check_option(const Option& opt) const
    {
        expect(valid_option(opt), "Unrecognized option: "s + to_string(opt));
    }

    template <typename S>
    bool Crtp<S>::valid_option_value(const Option& opt, const Option_value& val) const
    {
        if (util::contains({":produce-models", ":verbosity"}, opt)) {
            return is_value<int>(val);
        }

        if (util::contains({":diagnostic-output-channel", ":regular-output-channel"}, opt)) {
            return util::contains({"stdout", "stderr"}, val);
        }

        if (opt == ":t-strategy") return true;

        throw dummy;
    }

    template <typename S>
    void Crtp<S>::check_option_value(const Option& opt, const Option_value& val) const
    try {
        expect(valid_option_value(opt, val),
               "Option '"s + to_string(opt) + "': invalid value '"s + to_string(val) + "'");
    }
    catch (Dummy) {
        THROW("Checking option '") + to_string(opt) + "', value '" + to_string(val) + "': "
            + "`valid_option_value` not implemented!";
    }

    template <typename S>
    void Crtp<S>::set_option(const Option& opt, Option_value val)
    try {
        check_option(opt, val);
        option(opt) = val;
        apply_option(opt);
    }
    catch (Dummy) {
        THROW("Setting option '") + to_string(opt) + "', value '" + to_string(val) + "': "
            + "`apply_option` not implemented!";
    }

    template <typename S>
    Option_value Crtp<S>::get_option(const Option& opt) const
    {
        check_option(opt);
        return coption(opt);
    }

    template <typename S>
    void Crtp<S>::apply_option(const Option& opt)
    {
        auto& val = coption(opt);
        if (opt == ":produce-models") {
            producing_models() = to_value<bool>(val).value();
            return;
        }
        if (opt == ":verbosity") {
            verbosity() = to_value<Verbosity>(val).value();
            return;
        }
        //+
        if (opt == ":diagnostic-output-channel") THROW("Not implemented yet!");
        if (opt == ":regular-output-channel") THROW("Not implemented yet!");
        if (opt == ":t-strategy") return set_check_sat_strategy(val);

        throw dummy;
    }

    template <typename S>
    void Crtp<S>::set_all_strategies_impl(const String& name, bool force)
    try {
        set_check_sat_strategy(name, force);
    }
    //+ only the "unknown strategy error", not any error ... ideally just catch dummy
    catch (const Error&) {
        throw dummy;
    }

    template <typename S>
    void Crtp<S>::set_check_sat_strategy_impl(const String& /*name*/, bool /*force*/)
    {
        throw dummy;
    }

    template <typename S>
    bool Crtp<S>::contains(const Key& key_) const noexcept
    {
        return contains_var(key_);
    }

    template <typename S>
    void Crtp<S>::check_contains(const Key& key_) const
    {
        expect(contains(key_), "Undeclared/undefined variable/function: "s + to_string(key_));
    }

    template <typename S>
    void Crtp<S>::check_not_contains(const Key& key_) const
    {
        expect(!contains(key_), "Variable/function already declared/defined: "s + to_string(key_));
    }

    template <typename S>
    void Crtp<S>::check_contains_var(const Key& key_) const
    {
        expect(contains_var(key_), "Undeclared/undefined variable: "s + to_string(key_));
    }

    template <typename S>
    void Crtp<S>::check_not_contains_var(const Key& key_) const
    {
        expect(!contains_var(key_), "Variable already declared/defined: "s + to_string(key_));
    }

    template <typename S>
    Key Crtp<S>::new_var_key(Key key_)
    {
        return move(key_) + "_" + to_string(++auto_var_counter());
    }

    template <typename S>
    bool Crtp<S>::is_aux_var(const Key& key_) const noexcept
    {
        return starts_with(key_, aux_var_key_prefix);
    }

    template <typename S>
    void Crtp<S>::assert_expr(expr::Ptr eptr)
    {
        if (List::is_me(eptr)) assert_expr(List::cast(move(eptr)));
        else assert_elem(Elem::cast(move(eptr)));
    }

    template <typename S>
    Sat Crtp<S>::solve()
    {
        if (is_dry_run()) return unknown;

        init_solve();
        const Sat sat = check_sat_ref().perform();
        finish_solve(sat);

        return sat;
    }

    template <typename S>
    void Crtp<S>::init_solve()
    {
        /// Can be used for e.g. preprocessing of the input formula
        //+ not profiled
        assert(cmode() == Mode::assert);
        mode() = Mode::check;
        expect(ccheck_sat_ptr(), "No check-sat algorithm is set!");
    }

    template <typename S>
    void Crtp<S>::finish_solve(const Sat& sat)
    {
        assert(cmode() != Mode::assert);
        if (sat) mode() = Mode::sat;
        else if (sat == Sat::unsat) mode() = Mode::unsat;

        if (cverbosity() >= -1) os() << sat << std::endl;
    }

    template <typename S>
    String Crtp<S>::var_to_string(const Key& key_) const
    {
        check_contains_var(key_);
        return var_to_string_impl(key_);
    }

    template <typename S>
    void Crtp<S>::print_var(const Key& key_) const
    {
        if (is_dry_run() || cverbosity() <= -1) return;
        check_contains_var(key_);
        print_var_impl(key_);
    }

    template <typename S>
    void Crtp<S>::print_vars() const
    {
        if (is_dry_run() || cverbosity() <= -1) return;
        print_vars_impl();
    }

    template <typename S>
    void Crtp<S>::print_var_impl(const Key& key_) const
    {
        if (is_aux_var(key_)) return;
        os() << var_to_string_impl(key_) << std::endl;
    }

    template <typename S>
    bool Crtp<S>::allowed_get_value() const noexcept
    {
        if (cproducing_models())
            return util::contains({Mode::check, Mode::sat}, cmode());
        if (cverbosity() >= -1)
            std::cerr << "Producing models is disabled." << std::endl;
        return false;
    }

    template <typename S>
    String Crtp<S>::stats_to_string() const
    {
        return var_stats_to_string();
    }
}

#include "smt/solver/strategy.tpp"
#include "smt/solver/check_sat.tpp"
#include "smt/solver/parser.tpp"
