#pragma once

#include "smt/solver/reals/check_sat.hpp"

#include "expr/var/vars/alg.hpp"

namespace unsot::smt::solver::reals {
    template <typename B, var::Type typeV>
    typename Mixin<B, typeV>::Sat_solver::Clause
    Mixin<B, typeV>::Check_sat::
        make_depend_real_preds_clause_of(const var::Id& vid, var::Distance max_len)
    {
        assert(max_len > 1);
        auto clause = make_depend_real_preds_clause_impl_of(vid, max_len);
        assert(empty(clause) || size(clause) > 1);
        assert(empty(clause) || vid == this->csolver().cbool_id(cbegin(clause)->var_id()));
        return clause;
    }

    template <typename B, var::Type typeV>
    typename Mixin<B, typeV>::Sat_solver::Clause
    Mixin<B, typeV>::Check_sat::
        make_depend_real_preds_conflict_of(const var::Id& vid, var::Distance max_len)
    {
        auto clause = make_depend_real_preds_clause_of(vid, max_len);
        this->csolver().csat_solver().neg(clause);
        return clause;
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::Check_sat::all_sat() const noexcept
    {
        return Inherit::all_sat() && lall_sat();
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::Check_sat::lall_sat() const noexcept
    {
        const bool all = var::all_surely_consistent(this->csolver().creal_preds());
        assert(!all || var::all_set(this->csolver().creals()));
        return all;
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::Check_sat::restart()
    {
        Inherit::restart();
        assert(var::all_unset_or_locked(this->csolver().creals()));
    }
}
