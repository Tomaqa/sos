#pragma once

#include "smt/solver/strategy.hpp"

#include <iostream>
#include <omp.h>
#include <sstream>

namespace unsot::smt::solver {
    template <typename S>
    template <typename ConfT, typename... Args>
    typename Crtp<S>::template Strategy<ConfT, Args...>::Strategy_conf::Ret
    Crtp<S>::Strategy<ConfT, Args...>::perform(Args... args)
    {
        SMT_CVERB3LN(full_name() << "::perform ...");

        const bool prof = cprofiling();
        double start;
        if (prof) start = omp_get_wtime();

        if constexpr (is_ret_void_v) perform_profiled(FORWARD(args)...);
        else this->ret() = perform_profiled(FORWARD(args)...);

        if (prof) {
            const double finish = omp_get_wtime();
            const Duration dur = finish - start;
            duration() += dur;
            ++count();
        }

        if constexpr (!is_ret_void_v) return move(this->ret());
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    typename Crtp<S>::template Strategy<ConfT, Args...>::Strategy_conf::Ret
    Crtp<S>::Strategy<ConfT, Args...>::perform_profiled(Args... args)
    {
        if constexpr (print_perform_parts_v) SMT_CVERB2LN(full_name() << " init ...\n");
        perform_init(args...);

        if constexpr (print_perform_parts_v) SMT_CVERB2LN(full_name() << " body ...\n");
        if constexpr (is_ret_void_v) perform_body(FORWARD(args)...);
        else this->ret() = perform_body(FORWARD(args)...);

        if constexpr (print_perform_parts_v) SMT_CVERB2LN(full_name() << " finish ...\n");
        perform_finish();

        if constexpr (!is_ret_void_v) return move(this->ret());
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    String Crtp<S>::Strategy<ConfT, Args...>::profiling_to_string(Duration total) const
    {
        assert(cprofiling());

        const Duration dur = cduration();
        const auto cnt = ccount();
        ostringstream oss;
        oss << name() << " time: " << dur << " s (" << (dur/total)*100 << " % of " << profiling_total_name() << ")\n"
            << name() << " count: " << cnt << " x (avg: " << (dur/cnt) << ")\n"
            ;
        return oss.str();
    }
}
