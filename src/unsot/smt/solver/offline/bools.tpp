#pragma once

#include "smt/solver/offline/bools.hpp"

namespace unsot::smt::solver::offline::bools {}

#include "smt/solver/offline/bools/check_sat.tpp"
