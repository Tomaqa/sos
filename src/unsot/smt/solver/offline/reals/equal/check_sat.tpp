#pragma once

#include "smt/solver/offline/reals/equal/check_sat.hpp"

#include "util/alg.hpp"
#include "util/flag/alg.hpp"
#include "expr/var/vars/alg.hpp"

namespace unsot::smt::solver::offline::reals::equal {
    template <typename B>
    void Mixin<B>::Check_sat::try_eval_reals()
    {
        if (var::try_eval_all<Reals>(this->solver().reals_view())) this->progress() = true;
    }

    template <typename B>
    Flag Mixin<B>::Check_sat::update_reals()
    {
        sort_real_preds_view();
        return all_true(this->csolver().creal_preds_view(), [this](auto& vptr){
            assert(vptr->is_set());
            this->set_last_depend_id(vptr->cid());

            const bool was_computed = vptr->computed();
            const Flag consistent = vptr->consistent();
            if (!was_computed && vptr->computed()) this->progress() = true;
            return consistent;
        });
    }

    template <typename B>
    void Mixin<B>::Check_sat::sort_real_preds_view()
    {
        /*
        static constexpr auto sort_f = [](auto& ptr1, auto& ptr2){
            const auto dist1 = vptr1->eval_distance();
            const auto dist2 = vptr2->eval_distance();
            if (dist1 < dist2) return true;
            if (dist1 > dist2) return false;
            if (!Real_equality::is_me(vptr1)
                || !Real_equality::is_me(vptr2)
            ) return false;
            auto& epred1 = Real_equality::ccast(vptr1);
            auto& epred2 = Real_equality::ccast(vptr2);
            return epred1.order() > epred2.order();
        };
        */

        if constexpr (var::conf::has_distance_v<Preds::Types::Conf>) {
            static constexpr auto conv_f = [](auto& ptr){
                const auto& vptr = *ptr;
                return vptr->eval_distance();
            };
            auto& view = this->solver().real_preds_view().accessors();
            if (!is_sorted(view, conv_f)) sort(view, conv_f);
        }
    }
}
