#pragma once

#include "smt/solver/offline/reals/check_sat.hpp"

#include "util/alg.hpp"
#include "expr/var/vars/alg.hpp"
#include "smt/solver/reals/alg.hpp"

#include <iostream>
#include <sstream>

#include <omp.h>

namespace unsot::smt::solver::offline::reals {
    template <typename B>
    void Mixin<B>::Check_sat::set_last_depend_id(var::Id vid, bool is_bool) noexcept
    {
        _last_depend_id = move(vid);
        _is_last_depend_bool = is_bool;
    }

    template <typename B>
    Sat Mixin<B>::Check_sat::perform_body()
    {
        bool was_unknown = false;
        while (true) {
            if (!Inherit::perform_body()) break;

            const Sat sat = eval_reals();
            if (sat) return true;

            if (sat == unknown) was_unknown = true;

            if (was_unknown && SMT_VERBOSITY2) {
                _SMT_CVERBLN("backtrack(unknown):\n" << this->csolver().creals());
                SMT_CVERB3LN(this->csolver().creal_preds());
                _SMT_CVERB(std::endl);
            }

            append_real_preds_conflict();
            if (!assert_conflicts()) break;
        }

        return was_unknown ? Sat::unknown : Sat::unsat;
    }

    template <typename B>
    Sat Mixin<B>::Check_sat::eval_reals()
    {
        /// Quadratic in worst case
        do {
            double start;
            progress() = false;
            if (this->cprofiling()) start = omp_get_wtime();
            try_eval_reals();
            if (this->cprofiling()) {
                const double finish = omp_get_wtime();
                const Duration duration = finish - start;
                try_eval_reals_duration() += duration;
            }

            if (this->cprofiling()) start = omp_get_wtime();
            const Flag upd_flag = update_reals();
            if (this->cprofiling()) {
                const double finish = omp_get_wtime();
                const Duration duration = finish - start;
                update_reals_duration() += duration;
                ++eval_reals_count();
            }
            if (upd_flag.is_false()) return false;
            if (upd_flag.is_true()) break;
        } while (cprogress());

        if (Solver::Reals_t::Check_sat::lall_sat()) return true;
        return unknown;
    }

    /// Basic implementation
    template <typename B>
    void Mixin<B>::Check_sat::try_eval_reals()
    {
        if (var::try_eval_all(this->solver().reals())) progress() = true;
    }

    template <typename B>
    typename Mixin<B>::Sat_solver::Clause Mixin<B>::Check_sat::make_depend_real_preds_clause()
    {
        auto& vid = clast_depend_id();
        if (is_last_depend_bool())
            return this->make_depend_real_preds_clause_of(vid);
        else return typename Inherit::template Make_depend_preds_clause<Real>(this->solver()).perform(vid);
    }

    template <typename B>
    typename Mixin<B>::Sat_solver::Clause Mixin<B>::Check_sat::make_depend_real_preds_conflict()
    {
        auto clause = make_depend_real_preds_clause();
        this->csolver().csat_solver().neg(clause);
        return clause;
    }

    template <typename B>
    bool Mixin<B>::Check_sat::assert_conflicts()
    {
        if (empty(cconflicts())) return false;

        if (SMT_VERBOSITY3) {
            _SMT_CVERBLN("assert_conflicts:");
            _SMT_CVERBLN(cconflicts());
            static const String msg = "conflict";
            for (auto& confl : cconflicts())
                this->print_sat_clause_impl(confl, msg);
        }

        if (any_of(cconflicts(), [](auto& c){ return empty(c); }))
            return false;

        auto& ssolver = this->solver().sat_solver();
        ssolver.add_cnf(move(conflicts()));
        ++this->t_inconsistent_count();

        if (!ssolver.okay()) return false;
        restart();
        return true;
    }

    template <typename B>
    void Mixin<B>::Check_sat::restart()
    {
        Inherit::restart();
        conflicts().clear();
        last_conflict_l() = nullptr;
    }

    template <typename B>
    void Mixin<B>::Check_sat::add_conflict(typename Sat_solver::Clause conflict)
    {
        conflicts().push_back(move(conflict));
        last_conflict_l() = &conflicts().back();
    }

    template <typename B>
    void Mixin<B>::Check_sat::append_conflict(typename Sat_solver::Clause conflict)
    {
        if (!clast_conflict_l()) add_conflict({});

        append(last_conflict(), move(conflict));
    }

    template <typename B>
    void Mixin<B>::Check_sat::add_real_preds_conflict()
    {
        add_conflict(make_depend_real_preds_conflict());
    }

    template <typename B>
    void Mixin<B>::Check_sat::append_real_preds_conflict()
    {
        append_conflict(make_depend_real_preds_conflict());
    }

    template <typename B>
    String Mixin<B>::Check_sat::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total);

        const Duration sd = this->cduration();
        const Duration rd = ctry_eval_reals_duration();
        const Duration rpd = cupdate_reals_duration();
        const auto rc = ceval_reals_count();
        oss << "Try-eval reals time: " << rd << " s (" << (rd/sd)*100 << " % of solve)\n"
            << "Update reals time: " << rpd << " s (" << (rpd/sd)*100 << " % of solve)\n"
            << "Eval reals: " << rc << " x (avg: " << ((rd+rpd)/rc) << ")\n"
            ;
        return oss.str();
    }
}
