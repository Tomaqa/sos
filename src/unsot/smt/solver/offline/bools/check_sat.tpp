#pragma once

#include "smt/solver/offline/bools/check_sat.hpp"

#include <sstream>
#include <omp.h>

namespace unsot::smt::solver::offline::bools {
    template <typename B>
    Sat Mixin<B>::Check_sat::perform_body()
    {
        return eval_bools();
    }

    template <typename B>
    bool Mixin<B>::Check_sat::eval_bools()
    {
        double start;
        if (this->cprofiling()) start = omp_get_wtime();
        auto& ssolver = this->solver().sat_solver();
        auto assumps = this->make_sat_clause(this->csolver().cbools());
        const bool sat = ssolver.solve(move(assumps));
        if (this->cprofiling()) {
            const double finish = omp_get_wtime();
            const Duration duration = finish - start;
            eval_bools_duration() += duration;
            ++eval_bools_count();
        }

        if (!sat) return false;

        return update_bools();
    }

    template <typename B>
    bool Mixin<B>::Check_sat::update_bools()
    {
        auto& s = this->solver();
        auto& ssolver = s.csat_solver();
        auto& bools = s.bools();
        auto& sat_ids_map = s.csat_var_ids_map();
        const auto size_ = bools.size();
        assert(size_ == size(sat_ids_map));
        for (var::Id vid = 0; vid < size_; ++vid) {
            auto& sat_vid = sat_ids_map[vid];
            const sat::Value val = ssolver.model_value(sat_vid);
            /// When does `model_value` return DCs ?
            assert(val != unknown);

            auto& vptr = bools.ptr(vid);
            Var::cast(vptr) = Bool(val);
        }

        assert(Solver::Bools_t::Check_sat::lall_sat());
        return true;
    }

    template <typename B>
    String Mixin<B>::Check_sat::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total);

        const Duration sd = this->cduration();
        const Duration bd = ceval_bools_duration();
        const auto bc = ceval_bools_count();
        oss << "Eval bools time: " << bd << " s (" << (bd/sd)*100 << " % of solve)\n"
            << "Eval bools: " << bc << " x (avg: " << (bd/bc) << ")\n"
            ;
        return oss.str();
    }
}
