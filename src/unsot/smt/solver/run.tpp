#pragma once

#include "smt/solver/run.hpp"

#include <omp.h>

namespace unsot::smt::solver {
    template <typename S>
    String Crtp<S>::Run::usage() const
    {
        return Inherit::usage() + lusage();
    }

    template <typename S>
    String Crtp<S>::Run::lusage() const
    {
        return usage_row('v', "Be verbose, or more verbose if it already is"s
                              + " [or set concrete level]")
             + usage_row('q', "Be quiet, or more quiet if it already is")
             + usage_row('E', "Preprocess only (before|after parsing)")
             + usage_row('B', "Only output the resulting abstracted Boolean formula")
             + usage_row('d', "Only output the resulting formula in DIMACS format")
             + usage_row('I', "Print the number of T-inconsistencies")
             + usage_row('P', "Turn on profiling");
    }

    template <typename S>
    List Crtp<S>::Run::preprocess()
    {
        const bool prof = csolver().ccheck_sat_ref().cprofiling();
        double start;
        if (prof) start = omp_get_wtime();

        //+ incompatible with `Preprocess::Run'
        //+ -> it must accept existing `Preprocess' object
        Preprocess preprocess_obj(solver().preprocess_lines_only(is()));
        //+
        preprocess_obj.set_run(*this);
        for (auto& [key, val] : _macros_map) {
            preprocess_obj.add_macro(key, {{}, val});
        }
        List ls = solver().preprocess_list_only(move(preprocess_obj).perform());

        if (prof) {
            const double finish = omp_get_wtime();
            preprocess_duration += finish - start;
        }

        return ls;
    }

    template <typename S>
    void Crtp<S>::Run::parse()
    try {
        solver().parse_only(preprocess());
    }
    catch (Ignore) {
        THROW("Unexpected ignore thrown.");
    }

    template <typename S>
    void Crtp<S>::Run::init()
    {
        Inherit::init();
        linit();
    }

    template <typename S>
    void Crtp<S>::Run::linit()
    {
        solver().set_run(this->that());
    }

    template <typename S>
    void Crtp<S>::Run::do_stuff()
    {
        auto& s = solver();

        if (_preprocess_only || _bool_formula_only || _dimacs_only) {
            if (_preprocess_only == 1) {
                Inherit::do_stuff();
                throw ignore;
            }
            s.set_dry_run();
            parse();
            if (_preprocess_only) os() << to_lines(s.cparsed_list());
            else if (_bool_formula_only) os() << to_lines(s.cbool_formula());
            else s.csat_solver().to_dimacs(os());
            throw ignore;
        }

        Duration start{0};
        if (s.ccheck_sat_ref().cprofiling()) start = omp_get_wtime();
        /// `Check_sat` can change here
        parse();
        auto& check_sat = s.ccheck_sat_ref();
        if (check_sat.cprofiling()) {
            const Duration finish = omp_get_wtime();
            const Duration total = finish - start;
            print_profiling(total);
        }

        if (_print_t_inconsistent_count) {
            os() << endl << "# T-inconsistencies: " << check_sat.ct_inconsistent_count() << endl;
        }
    }

    template <typename S>
    String Crtp<S>::Run::getopt_str() const noexcept
    {
        return Inherit::getopt_str() + lgetopt_str();
    }

    template <typename S>
    String Crtp<S>::Run::lgetopt_str() const noexcept
    {
        return "v::qEBdIP";
    }

    template <typename S>
    bool Crtp<S>::Run::process_opt(char c)
    {
        if (Inherit::process_opt(c)) return true;
        return lprocess_opt(c);
    }

    template <typename S>
    bool Crtp<S>::Run::lprocess_opt(char c)
    {
        switch (c) {
        case 'v': {
            Verbosity verb;
            if (optarg && get_value<Verbosity>(verb, optarg)) {
                solver().verbosity() = move(verb);
                return true;
            }

            solver().be_verbose();
            return process_optarg_opts();
        }
        case 'q':
            solver().be_quiet();
            return true;
        case 'E':
            ++_preprocess_only;
            return true;
        case 'B':
            _bool_formula_only = true;
            return true;
        case 'd':
            _dimacs_only = true;
            return true;
        case 'I':
            _print_t_inconsistent_count = true;
            return true;
        case 'P':
            solver().check_sat_ref().profiling() = true;
            return true;
        }

        return false;
    }

    template <typename S>
    void Crtp<S>::Run::print_profiling(Duration total) const
    {
        os() << endl << csolver().stats_to_string()
             << endl << "Total time: " << total << " s" << endl
             << "Preprocess time: " << preprocess_duration << " s ("
             << (preprocess_duration/total)*100 << " % of total)" << endl
             << csolver().ccheck_sat_ref().profiling_to_string(total)
             ;
    }
}
