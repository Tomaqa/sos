#pragma once

#include "smt/solver/online/reals.hpp"

namespace unsot::smt::solver::online::reals {
    template <typename B>
    void Mixin<B>::set_check_sat_strategy_impl(const String& name, bool force)
    try {
        Inherit::set_check_sat_strategy_impl(name, force);
    }
    catch (Dummy) {
        using Initial_ = typename Check_sat::With_init_map::T_suggest_decision::Initial;
        using Car_ = typename Check_sat::Analyze_t_inconsistency::Car;

        if (name == Car_::svariant_name()) {
            this->set_t_suggest_decision_strategy(Initial_::svariant_name(), force);
            this->set_t_learn_strategy(name, force);
            return;
        }

        throw;
    }

    template <typename B>
    void Mixin<B>::set_t_suggest_decision_strategy_impl(const String& name, bool force)
    try {
        Inherit::set_t_suggest_decision_strategy_impl(name, force);
    }
    catch (Dummy) {
        using Ch_init = typename Check_sat::With_init_map;
        using T_sugg = typename Check_sat::T_suggest_decision;
        using T_sugg_init = typename Ch_init::T_suggest_decision;
        using Initial = typename T_sugg_init::Initial;
        using Bmc = typename T_sugg::Bmc::Sorted;
        using Bmc_init = typename T_sugg_init::Sorted_bmc;

        if (name == Initial::svariant_name()) {
            this->template set_check_sat_strategy_tp<Ch_init>(force);
            return this->check_sat_ref().template set_t_suggest_decision_strategy_tp<Initial>(force);
        }
        if (name == Bmc::svariant_name()) {
            return this->check_sat_ref().template set_t_suggest_decision_strategy_tp<Bmc>(force);
        }
        if (name == Bmc_init::svariant_name()) {
            this->template set_check_sat_strategy_tp<Ch_init>(force);
            return this->check_sat_ref().template set_t_suggest_decision_strategy_tp<Bmc_init>(force);
        }

        throw;
    }

    template <typename B>
    void Mixin<B>::set_analyze_t_inconsistency_strategy_impl(const String& name, bool force)
    try {
        Inherit::set_analyze_t_inconsistency_strategy_impl(name, force);
    }
    catch (Dummy) {
        using Car = typename Check_sat::Analyze_t_inconsistency::Car;

        if (name == Car::svariant_name()) {
            return this->check_sat_ref().template set_analyze_t_inconsistency_strategy_tp<Car>(force);
        }

        throw;
    }
}

#include "smt/solver/online/reals/check_sat.tpp"
