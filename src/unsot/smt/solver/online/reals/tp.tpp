#include "smt/solver/online/reals.tpp"
#include "smt/solver/online/reals/run.tpp"

#include "smt/solver/strategy.tpp"
#include "smt/solver/online.tpp"
#include "smt/solver/online/bools.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace TP_INST_SOLVER_NAMESPACE {
    template class aux::_S::Online_reals_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
