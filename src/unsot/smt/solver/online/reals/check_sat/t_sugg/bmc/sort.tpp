#pragma once

#include "smt/solver/online/reals/check_sat/t_sugg/bmc/sort.hpp"

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    template <typename B3>
    Flag Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::Sort_mixin<B3>::
        sort_bool_view_compare_rest(const var::Ptr& vptr1, const var::Ptr& vptr2) const
    {
        if (auto flag = Inherit::sort_bool_view_compare_rest(vptr1, vptr2); flag.valid()) return flag;

        /// (Initial) assigners and predicates go first
        if (vptr1->is_var()) return false;
        assert(Pred::is_me(vptr1));
        if (vptr2->is_var()) return true;
        assert(Pred::is_me(vptr2));

        using Ass = Preds::Types::Assigner;
        if (!Ass::is_me(vptr1)) return false;
        if (!Ass::is_me(vptr2)) return true;

        auto& ass1 = Ass::cast(vptr1);
        if (!maybe_initial(ass1)) return false;
        auto& ass2 = Ass::cast(vptr2);
        if (!maybe_initial(ass2)) return true;

        return sort_bool_view_compare_initial(ass1, ass2);
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    Flag Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::Sort_mixin<B3>::
        sort_bool_view_compare_initial([[maybe_unused]] const Preds::Types::Assigner& ass1,
                                       [[maybe_unused]] const Preds::Types::Assigner& ass2) const
    {
        assert(maybe_initial(ass1));
        assert(maybe_initial(ass2));

        return unknown;
    }
}
