#include "smt/solver/online/reals/rail.tpp"

#include "smt/solver/online/reals.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace TP_INST_SOLVER_NAMESPACE {
    template class aux::_S::Rail_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
