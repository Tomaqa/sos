#pragma once

#include "smt/solver/online/bools/check_sat.hpp"

#include "util/alg.hpp"

#include <sstream>

namespace unsot::smt::solver::online::bools {
    template <typename B>
    Mixin<B>::Check_sat::Check_sat(Solver& solver_)
        : Inherit(solver_)
    {
        This::notice_new_decision_level();
    }

    template <typename B>
    bool Mixin<B>::Check_sat::tracked(const var::Id& vid) const noexcept
    {
        return tracked(this->csolver().cbools().cptr(vid));
    }

    template <typename B>
    bool Mixin<B>::Check_sat::tracked(const var::Ptr& vptr) const noexcept
    {
        return vptr->is_set();
    }

    template <typename B>
    bool Mixin<B>::Check_sat::decided(const var::Id& vid) const noexcept
    {
        assert(tracked(vid));
        assert(util::contains(ctracked_bools_view(cdecision_level_of(vid)).caccessors(), vid));
        //+ an auxiliary SAT variable could have been decided?, which is not tracked
        return decided_at(vid, cdecision_level_of(vid));
    }

    template <typename B>
    bool Mixin<B>::Check_sat::decided(const var::Ptr& vptr) const noexcept
    {
        return decided(vptr->cid());
    }

    template <typename B>
    bool Mixin<B>::Check_sat::decided_at(const var::Id& vid, int level) const noexcept
    {
        return decision_bool_id_at(level) == vid;
    }

    template <typename B>
    bool Mixin<B>::Check_sat::decided_at(const var::Ptr& vptr, int level) const noexcept
    {
        return decided_at(vptr->cid(), level);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::tracked_before(const var::Id& vid, const var::Id& vid2) const
    {
        assert(tracked(vid));
        assert(tracked(vid2));
        assert(vid != vid2);
        const int l1 = cdecision_level_of(vid);
        const int l2 = cdecision_level_of(vid2);
        if (l1 != l2) return l1 < l2;

        return tracked_at_same_level_before(vid, l1, vid2);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::tracked_at_same_level_before(const var::Id& vid, int level, const var::Id& vid2) const
    {
        assert(tracked(vid));
        assert(tracked(vid2));
        assert(vid != vid2);
        assert(level == cdecision_level_of(vid));
        assert(level == cdecision_level_of(vid2));
        auto& vw = ctracked_bools_view(level);
        assert(util::contains(vw.caccessors(), vid));
        assert(util::contains(vw.caccessors(), vid2));
        //+ potentionally inefficient
        for (auto& id_ : vw.caccessors()) {
            if (id_ == vid) return true;
            if (id_ == vid2) return false;
        }
        assert(false);
        THROW(error);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::tracked_at_same_level_before(const var::Id& vid, const var::Id& vid2) const
    {
        return tracked_at_same_level_before(vid, cdecision_level_of(vid), vid2);
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice(typename Sat_solver::Lit p)
    {
        auto vptr_l = this->solver().find_bool_ptr(p);
        if (!vptr_l) return;
        notice(*vptr_l, Bool(p.sign()));
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice(var::Ptr& vptr, Bool val)
    {
        assert(!tracked(vptr));
        this->t_propagate_ref().pre_check_sat_notice(vptr);
        this->t_suggest_decision_ref().pre_check_sat_notice(vptr);
        t_explain_ref().pre_check_sat_notice(vptr);
        analyze_t_inconsistency_ref().pre_check_sat_notice(vptr);
        assert(!tracked(vptr));

        notice_impl(vptr, move(val));

        assert(tracked(vptr));
        this->t_propagate_ref().post_check_sat_notice(vptr);
        this->t_suggest_decision_ref().post_check_sat_notice(vptr);
        t_explain_ref().post_check_sat_notice(vptr);
        analyze_t_inconsistency_ref().post_check_sat_notice(vptr);
        assert(tracked(vptr));
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice_impl(var::Ptr& vptr, Bool val)
    {
        Var::cast(vptr) = move(val);
        SMT_CVERB3LN(Inherit::name() << "::notice: " << vptr);
        track(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice_new_decision_level()
    {
        Inherit::notice_new_decision_level();
        tracked_bool_views().push_back(Tracked_bools_view(this->solver().bools()));
        SMT_CVERB3LN(Inherit::name() << "::new_decision_level -> " << decision_level());
    }

    template <typename B>
    void Mixin<B>::Check_sat::track(const var::Ptr& vptr)
    {
        track_at(vptr, decision_level());
    }

    template <typename B>
    void Mixin<B>::Check_sat::track_at(const var::Ptr& vptr, int level)
    {
        auto& vid = vptr->cid();
        assert(!util::contains(ctracked_bools_view(level).caccessors(), vid));
        tracked_bools_view(level).push_back(vid);

        decision_level_of(vid) = level;
    }

    template <typename B>
    void Mixin<B>::Check_sat::perform_init_profiled()
    {
        t_explain_ref().pre_check_sat_init();
        analyze_t_inconsistency_ref().pre_check_sat_init();
        Inherit::perform_init_profiled();
        t_explain_ref().post_check_sat_init();
        analyze_t_inconsistency_ref().post_check_sat_init();
    }

    template <typename B>
    void Mixin<B>::Check_sat::perform_init_impl()
    {
        Inherit::perform_init_impl();
        decision_levels_map().reserve(this->csolver().cbools().size());
    }

    template <typename B>
    Sat Mixin<B>::Check_sat::perform_body()
    {
        if (!this->solver().sat_solver().solve()) return false;
        if (this->all_sat()) return true;
        assert(Solver::Bools_t::Check_sat::lall_sat());

        return Inherit::perform_body();
    }

    template <typename B>
    int Mixin<B>::Check_sat::max_decision_level_of(const typename Sat_solver::Clause& clause) const
    {
        return max_decision_level_of(cbegin(clause), cend(clause));
    }

    template <typename B>
    void Mixin<B>::Check_sat::t_backtrack_to(int level)
    {
        assert(level <= decision_level());
        if (level == decision_level()) return;
        this->notice_backtrack_to(level);
        SMT_CVERB4LN("sat_backtrack: " << decision_level() << " -> " << level);
        this->solver().sat_solver().sat_backtrack_to(level);
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice_backtrack()
    {
        Inherit::notice_backtrack();
        auto& curr_vw = current_tracked_bools_view();
        const auto bit = curr_vw.begin();
        const auto eit = curr_vw.end();
        for (auto it = eit; it != bit;) {
            auto& vptr = *--it;
            assert(tracked(vptr));
            t_backtrack_bool(vptr);
            assert(!tracked(vptr));
        }
        tracked_bool_views().pop_back();
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice_restart()
    {
        t_explain_ref().pre_check_sat_restart();
        analyze_t_inconsistency_ref().pre_check_sat_restart();
        Inherit::notice_restart();
        t_explain_ref().post_check_sat_restart();
        analyze_t_inconsistency_ref().post_check_sat_restart();
    }

    template <typename B>
    void Mixin<B>::Check_sat::t_backtrack_bool(var::Ptr& vptr)
    {
        this->t_propagate_ref().pre_check_sat_t_backtrack_bool(vptr);
        this->t_suggest_decision_ref().pre_check_sat_t_backtrack_bool(vptr);
        t_explain_ref().pre_check_sat_t_backtrack_bool(vptr);
        analyze_t_inconsistency_ref().pre_check_sat_t_backtrack_bool(vptr);

        t_backtrack_bool_impl(vptr);

        this->t_propagate_ref().post_check_sat_t_backtrack_bool(vptr);
        this->t_suggest_decision_ref().post_check_sat_t_backtrack_bool(vptr);
        t_explain_ref().post_check_sat_t_backtrack_bool(vptr);
        analyze_t_inconsistency_ref().post_check_sat_t_backtrack_bool(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::t_backtrack_bool_impl(var::Ptr& vptr)
    {
        vptr->unset();
    }

    template <typename B>
    String Mixin<B>::Check_sat::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total);

        const Duration sd = this->cduration();
        const Duration td = this->ct_propagate_ref().cduration();
        oss << ct_explain_ref().profiling_to_string(sd)
            << canalyze_t_inconsistency_ref().profiling_to_string(td)
            ;
        return oss.str();
    }
}

#include "smt/solver/online/bools/check_sat/t_prop.tpp"
#include "smt/solver/online/bools/check_sat/t_sugg.tpp"
#include "smt/solver/online/bools/check_sat/t_learn.tpp"
