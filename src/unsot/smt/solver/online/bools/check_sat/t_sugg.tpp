#pragma once

#include "smt/solver/online/bools/check_sat/t_sugg.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    bool Mixin<B>::Check_sat::T_suggest_decision::invalid_current_bool() const noexcept
    {
        return this->invalid_current_state();
    }

    template <typename B>
    typename Mixin<B>::Check_sat::T_suggest_decision::Ret
    Mixin<B>::Check_sat::T_suggest_decision::perform_with(const var::Ptr& vptr)
    {
        Flag val = perform_value_with(vptr);
        perform_print_with(vptr, val);
        SMT_CVERB3(endl);
        return {perform_var_id_with(vptr), move(val)};
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::perform_print_with(const var::Ptr& vptr, const Flag& val) const
    {
        SMT_CVERB3(Inherit::name() << " -> " << vptr);
        if (val.valid()) SMT_CVERB3(" <- " << val);
    }

    template <typename B>
    typename Mixin<B>::Sat_solver::Var_id
    Mixin<B>::Check_sat::T_suggest_decision::perform_var_id_with(const var::Ptr& vptr) const
    {
        return this->csolver().csat_var_id(vptr->cid());
    }

    template <typename B>
    Flag Mixin<B>::Check_sat::T_suggest_decision::perform_value_with(const var::Ptr& vptr) const
    {
        if (!vptr->is_var()) return true;

        //+ allow to set a global default value (including unknown)
        return unknown;
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::pre_check_sat_t_backtrack_bool(var::Ptr& vptr)
    {
        maybe_backtrack_from_bool(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::backtrack()
    {
        [[maybe_unused]] auto& prev_vptr = ccurrent_bool_ptr();
        if constexpr (_debug_) {
            SMT_CVERB4LN(Inherit::name() << "::backtrack: " << prev_vptr << " ...");
        }

        backtrack_impl();

        if constexpr (_debug_) {
            SMT_CVERB4(Inherit::name() << "::backtrack: " << prev_vptr << " -> ");
            if (invalid_current_bool()) SMT_CVERB4LN("<none>")
            else SMT_CVERB4LN(ccurrent_bool_ptr());
        }
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::backtrack_impl()
    {
        Inherit::backtrack();
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::maybe_backtrack_from_bool(const var::Ptr& vptr)
    {
        if (!maybe_backtrack_from_bool_head(vptr)) return;
        if (!maybe_backtrack_from_bool_body(vptr)) return;
        backtrack_from_bool_finish();
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_suggest_decision::
        maybe_backtrack_from_bool_head([[maybe_unused]] const var::Ptr& vptr) const
    {
        auto& ch = this->ccheck_sat_ref();
        assert(ch.cdecision_level_of(vptr->cid()) == ch.decision_level());
        if (!this->cenabled() || ch.crestarting()) return false;

        return true;
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_suggest_decision::
        maybe_backtrack_from_bool_body(const var::Ptr& vptr)
    {
        return vptr->cid() == ccurrent_bool_id();
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::backtrack_from_bool_finish()
    {
        backtrack();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2>::perform_print_with(const var::Ptr& vptr, const Flag& val) const
    {
        Inherit::perform_print_with(vptr, val);
        /// `add_state` must be called before this (i.e. usually before `perform_with`)
        SMT_CVERB3("  " << this->state_to_string(this->ccurrent_state()));
    }
}

#include "smt/solver/online/bools/check_sat/t_sugg/order.tpp"
