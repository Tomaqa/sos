#pragma once

#include "smt/solver/online/bools/check_sat/t_learn.hpp"

#include "util/alg.hpp"
#include "util/random.hpp"

#include <sstream>

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <typename ConfT>
    void Mixin<B>::Check_sat::T_learn<ConfT>::perform_body(var::Ptr& vptr)
    {
        auto clause = explain(vptr);
        perform_body_head(vptr, clause);
        perform_body_impl(vptr, move(clause));
        perform_body_tail(vptr);
    }

    template <typename B>
    template <typename ConfT>
    void Mixin<B>::Check_sat::T_learn<ConfT>::perform_body_head(var::Ptr& vptr, typename Sat_solver::Clause& clause)
    {
        assert(!empty(clause));
        [[maybe_unused]] auto first = begin(clause);
        order_clause(vptr, clause);
        // The first element of the clause must be untouched
        assert(first == begin(clause));
    }

    template <typename B>
    template <typename ConfT>
    void Mixin<B>::Check_sat::T_learn<ConfT>::perform_body_impl(var::Ptr&, typename Sat_solver::Clause&& clause)
    {
        perform_with(move(clause));
    }

    template <typename B>
    template <typename ConfT>
    void Mixin<B>::Check_sat::T_learn<ConfT>::perform_body_tail(var::Ptr&)
    {
        this->progressed() = true;
    }

    template <typename B>
    template <typename ConfT>
    void Mixin<B>::Check_sat::T_learn<ConfT>::perform_finish()
    {
        Inherit::perform_finish();

        assert(this->cprogressed());
        if (this->cprofiling()) {
            this->total_literals_count() += this->cliterals_count();
        }
    }

    template <typename B>
    template <typename ConfT>
    bool Mixin<B>::Check_sat::T_learn<ConfT>::perform_limited(var::Ptr& vptr, var::Distance max_len)
    {
        //+ no printing nor profiling
        this->perform_init(vptr);
        const bool success = perform_body_limited(vptr, max_len);
        this->perform_finish();

        return success;
    }

    template <typename B>
    template <typename ConfT>
    bool Mixin<B>::Check_sat::T_learn<ConfT>::perform_body_limited(var::Ptr& vptr, var::Distance max_len)
    {
        auto clause = explain(vptr, max_len);
        if (empty(clause)) return false;
        perform_body_head(vptr, clause);
        perform_body_impl(vptr, move(clause));
        perform_body_tail(vptr);
        return true;
    }

    template <typename B>
    template <typename ConfT>
    typename Mixin<B>::Sat_solver::Clause
    Mixin<B>::Check_sat::T_learn<ConfT>::explain(const var::Ptr& vptr, var::Distance max_len)
    {
        /// The `from` var is not included here - not necessary
        /// .. also not necessarily included in the resulting clause
        assert(!vptr->is_var());
        auto clause = explain_impl(vptr, max_len);
        assert(max_len != var::inf_dist || !empty(clause));
        return clause;
    }

    template <typename B>
    template <typename ConfT>
    void Mixin<B>::Check_sat::T_learn<ConfT>::order_clause(var::Ptr&, typename Sat_solver::Clause& clause)
    {
        using Base_ = T_learn_base;

        if constexpr (Base_::order_decision_levels_v == Base_::Order_decision_levels::no) return;

        if constexpr (Base_::order_decision_levels_v == Base_::Order_decision_levels::random) {
            shuffle(begin(clause)+1, end(clause));
            return;
        }

        if constexpr (Base_::sorting_decision_levels()) {
            auto& ssolver = this->csolver().csat_solver();
            std::stable_sort(begin(clause)+1, end(clause), [&ssolver](auto& l1, auto& l2){
                const auto lhs = ssolver.decision_level_of(l1.var_id());
                const auto rhs = ssolver.decision_level_of(l2.var_id());
                if constexpr (Base_::order_decision_levels_v == Base_::Order_decision_levels::asc)
                    return lhs < rhs;
                else return lhs > rhs;
            });
            return;
        }
    }

    template <typename B>
    template <typename ConfT>
    void Mixin<B>::Check_sat::T_learn<ConfT>::perform_with(typename Sat_solver::Clause clause)
    {
        auto& s = this->solver();
        auto& ch = s.check_sat_ref();
        auto& ssolver = s.sat_solver();

        if constexpr (_debug_) {
            assert(all_of(clause, [&](auto& lit){
                auto lptr_l = s.cfind_bool_ptr(lit);
                assert(lptr_l);
                return ch.tracked(*lptr_l);
            }));
            assert(ch.max_decision_level_of(clause) == ch.decision_level());
        }

        this->literals_count() = size(clause);

        ch.print_sat_clause(clause, Inherit::name());
        ssolver.sat_learn(move(clause));
    }

    template <typename B>
    template <typename ConfT>
    String Mixin<B>::Check_sat::T_learn<ConfT>::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total);

        const auto tc = this->cprogressed_count();
        const auto lc = this->ctotal_literals_count();
        const double avgc = lc/double(tc);
        oss << Inherit::name() << " avg. clause length: ";
        if (tc) oss << avgc;
        else oss << "x";
        oss << "\n";
        return oss.str();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    void Mixin<B>::Check_sat::Analyze_t_inconsistency::
        perform_body_head(var::Ptr& vptr, typename Sat_solver::Clause& confl)
    {
        Inherit::perform_body_head(vptr, confl);
        backtrack(vptr, confl);
    }

    template <typename B>
    void Mixin<B>::Check_sat::Analyze_t_inconsistency::
        backtrack(var::Ptr&, typename Sat_solver::Clause& confl)
    {
        auto& ch = this->check_sat_ref();
        ch.t_backtrack_to(ch.max_decision_level_of(confl));
    }
}
