#pragma once

#include "smt/solver/reals.hpp"

#include "util/string/alg.hpp"
#include "expr/var/vars/alg.hpp"

#include <sstream>

namespace unsot::smt::solver::reals {
    template <typename B, var::Type typeV>
    Mixin<B, typeV>::Mixin()
    {
        static_init();
    }

    template <typename B, var::Type typeV>
    Mixin<B, typeV>::Mixin(Mixin&& rhs)
        : Inherit(move(rhs)), _reals(move(rhs._reals))
    {
        static_init();
    }

    template <typename B, var::Type typeV>
    Mixin<B, typeV>& Mixin<B, typeV>::operator =(Mixin&& rhs)
    {
        if (this == &rhs) return *this;

        Inherit::operator =(move(rhs));

        _reals = move(rhs._reals);
        _real_preds = {this->bools(), reals()};

        static_init();

        return *this;
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::static_init()
    {
        if constexpr (Reals::Types::var_type == var::Type::auto_assignee)
            reals().connect_preds(real_preds());
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::valid_sort(const Key& sort) const noexcept
    {
        return Inherit::valid_sort(sort) || sort == sort_key;
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::is_t_key(const Key& key_) const noexcept
    {
        //+ BMC steps can be marked differently than with "<>"
        return starts_with(key_, "_t<");
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::contains(const Key& key_) const noexcept
    {
        return Inherit::contains(key_) || contains_real(key_);
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::contains_var(const Key& key_) const noexcept
    {
        return Inherit::contains_var(key_) || contains_real_var(key_);
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::contains_real(const Key& key_) const noexcept
    {
        return creals().contains(key_);
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::check_contains_real(const Key& key_) const
    {
        expect(contains_real(key_),
               "Undeclared/undefined real variable: "s + to_string(key_));
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::contains_bool_var(const Key& key_) const noexcept
    {
        return Inherit::contains_bool_var(key_)
            && !Pred::is_me(this->cbools().cptr(key_));
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::contains_real_var(const Key& key_) const noexcept
    {
        //+ two lookups
        return contains_real(key_) && Real_var::is_me(creals().cptr(key_));
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::contains_real_fun(const Key& key_) const noexcept
    {
        //+ two lookups
        return contains_real(key_) && Real_fun::is_me(creals().cptr(key_));
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::contains_real_pred(const Key& key_) const noexcept
    {
        assert(!creal_preds().contains(key_) || this->contains_bool(key_));
        assert(!creal_preds().contains(key_) || Pred::is_me(creal_preds().cptr(key_)));
        //+ two lookups
        return creal_preds().contains(key_);
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::
        contains_def_real_fun(const Key& key_) const noexcept
    {
        //+ returns true also for non-real fun_defs
        return this->contains_fun_def(key_);
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::
        contains_def_real_pred(const Key& key_) const noexcept
    {
        //+ returns true also for fun_defs with non-real args
        return this->contains_def_bool_fun(key_);
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::reserve_reals(size_t size_)
    {
        reals().reserve(size_);
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::declare_var_impl(Key& key_, const Key& sort)
    try {
        Inherit::declare_var_impl(key_, sort);
    }
    catch (Dummy) {
        if (sort != sort_key) throw;

        declare_real_var(move(key_));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::define_var_impl(Key& key_, const Key& sort, Elem& elem)
    try {
        Inherit::define_var_impl(key_, sort, elem);
    }
    catch (Dummy) {
        if (sort != sort_key) throw;

        define_real_var(move(key_), move(elem).template to_value_check<Real>());
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::declare_real_var(Key key_)
    {
        define_real_var_tp(move(key_));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::define_real_var(Key key_, Real val)
    {
        define_real_var_tp(move(key_), move(val));
    }

    template <typename B, var::Type typeV>
    template <typename ValT>
    void Mixin<B, typeV>::define_real_var_tp(Key&& key_, ValT&& val)
    {
        pre_add_real(key_);
        this->template define_var_tp<Real>(move(key_), move(val));
    }

    template <typename B, var::Type typeV>
    Key Mixin<B, typeV>::instantiate_expr_impl(Formula& phi)
    try {
        return Inherit::instantiate_expr_impl(phi);
    }
    catch (Dummy) {
        return instantiate_real(move(phi));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::assert_expr_impl(Formula& phi)
    try {
        Inherit::assert_expr_impl(phi);
    }
    catch (Dummy) {
        assert_real(move(phi));
    }

    template <typename B, var::Type typeV>
    bool Mixin<B, typeV>::try_conv_to_bool_impl(expr::Ptr& eptr)
    {
        if (Inherit::try_conv_to_bool_impl(eptr)) return true;

        auto& phi = Formula::cast(eptr);
        if (!Pred::Fun::Opers::valid_pred(phi)) return false;

        eptr = new_key(instantiate_real_pred(move(phi)));
        return true;
    }

    template <typename B, var::Type typeV>
    Key Mixin<B, typeV>::instantiate_real(Formula phi)
    {
        phi.maybe_virtual_init();
        if (Real_fun::Fun::Opers::valid_f(phi))
            return instantiate_real_fun(move(phi));
        else return instantiate_real_pred(move(phi));
    }

    template <typename B, var::Type typeV>
    Key Mixin<B, typeV>::instantiate_real_fun(Formula phi)
    {
        return instantiate_real_fun_inst(make_real_auto_fun_inst(move(phi)));
    }

    template <typename B, var::Type typeV>
    Key Mixin<B, typeV>::instantiate_real_pred(Formula phi)
    {
        return instantiate_real_pred_inst(
            make_real_auto_pred_inst(move(phi))
        );
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::assert_real(Formula phi)
    {
        phi.maybe_virtual_init();
        assert_real_pred(move(phi));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::assert_real_pred(Formula phi)
    {
        /// Bypass defining the function as it is unnecessary
        assert_real_pred_inst(make_real_auto_pred_inst(move(phi)));
    }

    template <typename B, var::Type typeV>
    typename Mixin<B, typeV>::Fun_inst
    Mixin<B, typeV>::make_real_auto_fun_inst(Formula phi)
    {
        return this->make_auto_fun_inst(move(phi), aux_real_fun_key_prefix);
    }

    template <typename B, var::Type typeV>
    typename Mixin<B, typeV>::Fun_inst
    Mixin<B, typeV>::make_real_auto_pred_inst(Formula phi)
    {
        return this->make_auto_fun_inst(move(phi), aux_real_pred_key_prefix);
    }

    template <typename B, var::Type typeV>
    Key Mixin<B, typeV>::instantiate_real_fun_inst(Fun_inst fun)
    {
        return this->template instantiate_fun_inst_tp<Real>(move(fun),
            [](This* this_, auto&& fun_){
                auto&& [k, phi] = move(fun_);
                this_->pre_add_real(k);
                this_->reals().add_fun(move(phi), move(k));
        });
    }

    template <typename B, var::Type typeV>
    Key Mixin<B, typeV>::instantiate_real_pred_inst(Fun_inst fun)
    {
        return this->template instantiate_fun_inst_tp<Real>(move(fun),
            [](This* this_, auto&& fun_){
                auto&& [k, phi] = move(fun_);
                auto sat_vid = this_->pre_add_bool(k);
                this_->real_preds().add_pred(move(phi), move(k));
                this_->post_add_bool(move(sat_vid));
        });
    }

    template <typename B, var::Type typeV>
    Formula Mixin<B, typeV>::expand_real_fun_inst(Fun_inst fun)
    {
        return this->expand_fun_inst(move(fun));
    }

    template <typename B, var::Type typeV>
    Formula Mixin<B, typeV>::expand_real_pred_inst(Fun_inst fun)
    {
        return expand_real_fun_inst(move(fun));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::assert_real_pred_inst(Fun_inst fun)
    {
        Key k = instantiate_real_pred_inst(move(fun));
        this->assert_elem(move(k));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::define_fun_impl(Key& key_, const Key& sort, Formula& phi,
                                          Keys& arg_keys, const Key& args_sort)
    try {
        Inherit::define_fun_impl(key_, sort, phi, arg_keys, args_sort);
    }
    catch (Dummy) {
        const bool is_real_sort = (sort == sort_key);
        if (!is_real_sort && sort != bools::sort_key) throw;
        if (!empty(arg_keys) && args_sort != sort_key) throw;

        if (is_real_sort)
            define_real_fun(move(key_), move(phi), move(arg_keys));
        else define_real_pred(move(key_), move(phi), move(arg_keys));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::define_real_fun(Key key_, Formula phi, Keys arg_keys)
    {
        this->template define_fun_tp<Real>(move(key_), move(phi), move(arg_keys));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::define_real_pred(Key key_, Formula phi, Keys arg_keys)
    {
        this->template define_fun_tp<Bool, Real>(move(key_), move(phi), move(arg_keys));
    }

    template <typename B, var::Type typeV>
    Key Mixin<B, typeV>::instantiate_def_real_fun(const Key& key_, const Keys& arg_keys)
    {
        return this->instantiate_def_fun_tp(key_, arg_keys, &This::instantiate_real_fun_inst);
    }

    template <typename B, var::Type typeV>
    Key Mixin<B, typeV>::instantiate_def_real_pred(const Key& key_, const Keys& arg_keys)
    {
        return this->instantiate_def_fun_tp(key_, arg_keys, &This::instantiate_real_pred_inst);
    }

    template <typename B, var::Type typeV>
    Formula Mixin<B, typeV>::expand_def_real_fun(const Key& key_, const Keys& arg_keys)
    {
        return expand_real_fun_inst(this->make_fun_inst(key_, arg_keys));
    }

    template <typename B, var::Type typeV>
    Formula Mixin<B, typeV>::expand_def_real_pred(const Key& key_, const Keys& arg_keys)
    {
        return expand_real_pred_inst(this->make_fun_inst(key_, arg_keys));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::assert_def_real_pred(const Key& key_, const Keys& arg_keys)
    {
        assert_real_pred_inst(this->make_fun_inst(key_, arg_keys));
    }

    template <typename B, var::Type typeV>
    Idx Mixin<B, typeV>::compute_bmc_step_of(const Key& key) const
    {
        //+ it must be more general - detect the currently used format from the input
        String str = extract_first(key, '<', '>');
        if (auto opt_idx = to_value<Idx>(move(str))) return opt_idx.value();
        return invalid_idx;
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::replace_bmc_step_of(Key& key, Idx idx) const
    {
        replace_first(key, '<', '>', to_string(idx));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::finish_solve(const Sat& sat)
    {
        Inherit::finish_solve(sat);
        if (sat != unknown) return;

        if (SMT_VERBOSITY2) {
            static const auto print_f = [](auto& vars, auto&& view, const String& msg){
                for_each_if(vars, mem_fn(&var::Base::is_unset), [&view](auto& vptr){
                    view.push_back(vptr->cid());
                });
                _SMT_CVERB(msg << ": ");
                if (view.empty()) _SMT_CVERB("<none>");
                else _SMT_CVERB(view);
                _SMT_CVERB(std::endl);
            };

            print_f(creal_preds(), Preds_view(this->bools()), "unknown-preds");
            print_f(creals(), Reals_view(reals()), "unknown-reals");
        }
        assert(any_of(creal_preds(), not_fn(mem_fn(&var::Base::compute_computability))));
    }

    template <typename B, var::Type typeV>
    String Mixin<B, typeV>::var_to_string_impl(const Key& key_) const
    try {
        return Inherit::var_to_string_impl(key_);
    }
    catch (Dummy) {
        if (!contains_real(key_)) throw;
        return var_to_string(*creals().cptr(key_));
    }

    template <typename B, var::Type typeV>
    void Mixin<B, typeV>::print_vars_impl() const noexcept
    {
        Inherit::print_vars_impl();
        for (auto& vptr : creals()) {
            this->print_var_impl(vptr->ckey());
        }
    }

    template <typename B, var::Type typeV>
    String Mixin<B, typeV>::var_stats_to_string() const
    {
        ostringstream oss;
        oss << Inherit::var_stats_to_string();

        const auto rp = creal_preds().size();
        const auto b = this->cbools().size();
        oss << "# real preds: " << rp << " (" << (double(rp)/b)*100 << " % of bools)\n"
            << "# reals: " << creals().size() << "\n"
            ;
        return oss.str();
    }
}

#include "smt/solver/reals/check_sat.tpp"
