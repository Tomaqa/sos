#pragma once

#include "smt/solver/bools/check_sat.hpp"

#include "expr/var/vars/alg.hpp"

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    typename Mixin<B, SatSolver>::Sat_solver::Lit
    Mixin<B, SatSolver>::Check_sat::make_sat_lit(const Var& var) const
    {
        auto& sat_vid = this->csolver().csat_var_id(var.cid());
        const bool val = var.get();
        return {sat_vid, val};
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Check_sat::add_sat_lit(typename Sat_solver::Clause& clause, const Var& var) const
    {
        add_sat_lit_impl(clause, var);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Check_sat::maybe_add_sat_lit(typename Sat_solver::Clause& clause, const Var& var) const
    {
        add_sat_lit_impl<true>(clause, var);
    }

    template <typename B, typename SatSolver>
    template <bool maybeV>
    void Mixin<B, SatSolver>::Check_sat::add_sat_lit_impl(typename Sat_solver::Clause& clause, const Var& var) const
    {
        if constexpr (maybeV) if (var.is_unset()) return;
        clause.emplace_back(make_sat_lit(var));
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::Check_sat::all_sat() const noexcept
    {
        return lall_sat();
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::Check_sat::lall_sat() const noexcept
    {
        return var::all_set(this->csolver().cbools());
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Check_sat::restart()
    {
        var::unset_all(this->solver().bools());
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Check_sat::print_sat_clause(const typename Sat_solver::Clause& clause, const String& msg) const
    {
        if (!SMT_VERBOSITY3) return;
        print_sat_clause_impl(clause, msg);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Check_sat::print_sat_clause_impl(const typename Sat_solver::Clause& clause, const String& msg) const
    {
        _SMT_CVERBLN(msg << ": " << clause);
        for (auto& lit : clause) {
            auto lptr_l = this->csolver().cfind_bool_ptr(lit);
            assert(lptr_l);
            _SMT_CVERBLN(*lptr_l);
        }
        _SMT_CVERB(std::endl);
    }
}
