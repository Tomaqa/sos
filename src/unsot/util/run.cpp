#include "util/run.hpp"

#include "util/alg.hpp"
#include "util/string/alg.hpp"
#include "util/run/alg.hpp"

extern int errno;

namespace unsot::util {
    bool Path::has_dirname() const
    {
        return has_parent_path();
    }

    Path Path::dirname() const
    {
        return parent_path();
    }

    ////////////////////////////////////////////////////////////////

    Run::Run(int argc, const char* argv[])
        : _argc(argc), _argv(const_cast<Argv>(argv))
    { }

    int Run::run()
    try {
        init();
        do_stuff();
        finish();

        return 0;
    }
    catch (const Error& err) {
        cerr << err << endl;
        return 1;
    }
    catch (Ignore) {
        return 0;
    }
    catch (Dummy) {
        cout << usage() << endl;
        return 2;
    }

    void Run::init()
    try {
        if (_argv && _argc > 0) {
            getopts();
            process_additional_args();
        }
        set_istream_ptr();
        set_ostream_ptr();
    }
    catch (const Error& err) {
        throw err + "\n" + usage();
    }

    void Run::do_stuff()
    {
        throw dummy;
    }

    String Run::usage_row(char opt, String desc)
    {
        return "\n    -"s + opt
               + "    " + move(desc);
    }

    String Run::usage() const
    {
        return "USAGE: "s + _argv[0] + " [options] [<input_file>]\n"
               + "Options:"
               + usage_row('h', "Displays this message and exits")
               + usage_row('i', "Sets input file name")
               + usage_row('o', "Sets output file name");
    }

    void Run::getopts()
    {
        if (require_args() && _argc == 1) throw dummy;
        const String& optstring = getopt_str();
        for (int c; (c = getopt(_argc, _argv, optstring.c_str())) != EOF;) {
            process_opt(c);
        }
    }

    String Run::getopt_str() const noexcept
    {
        return ":hi:o:";
    }

    bool Run::process_opt(char c)
    {
        switch (char coptopt = optopt; c) {
        case 'h':
            cout << usage() << endl;
            throw ignore;
        case 'i':
            ipath() = optarg;
            return true;
        case 'o':
            opath() = optarg;
            return true;
        case ':':
            THROW("Option -"s + coptopt + " requires operand\n");
        case '?':
            THROW("Unrecognized option: -"s + coptopt + "\n");
        }

        return false;
    }

    bool Run::process_optarg_opts()
    {
        if (!optarg) return true;
        return all_of(String(exchange(optarg, nullptr)),
                      [this](char c){ return process_opt(c); });
    }

    void Run::process_additional_args()
    {
        if (_argc - optind >= 1) {
            ipath() = _argv[optind++];
        }
        expect(_argc - optind == 0, "Additional arguments: "s + _argv[optind] + "\n");
    }

    void Run::set_istream_ptr()
    {
        set_stream_ptr(_is_ptr, _ifs, cipath(), &cin, "Input stream error.");
    }

    void Run::set_ostream_ptr()
    {
        set_stream_ptr(_os_ptr, _ofs, copath(), &cout, "Output stream error.");
    }

    Path Run::project_dirname()
    {
        Path path = root();
        for (auto& fn : pwd()) {
            path /= fn;
            if (starts_with(fn.string(), project_name)) return path;
        }

        THROW("Project root directory not found.");
    }

    Path Run::common_dirname() const
    {
        if (empty(cipath())) return pwd();
        return util::common_dirname(cipath());
    }

    ifstream Run::open_ifstream(const Path& path) const
    try {
        //+ why it is not recognized automatically?
        if (ifstream ifs(path.cparent()); ifs.good()) return ifs;
        if (path.is_absolute()) throw dummy;

        const Path dir = common_dirname();
        if (ifstream ifs(dir/path); ifs.good()) return ifs;
        throw dummy;
    }
    catch (Dummy) {
        THROW("Failed to load the file: " + to_string(path));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::util {
    Path pwd()
    {
        return filesystem::current_path();
    }

    Path root()
    {
        return pwd().root_path();
    }
}
