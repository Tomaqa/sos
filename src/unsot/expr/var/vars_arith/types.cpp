#include "expr/var/vars_arith.hpp"

#include "expr/var/vars.tpp"
#include "expr/var/vars/types.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr::var::vars {
    template class aux::_Types<Int>::Fun::Vars_fun_t::Fun_mixin;
    template class aux::_Types<Int>::Pred::Vars_fun_t::Fun_mixin;
    template class aux::_Types<Real>::Fun::Vars_fun_t::Fun_mixin;
    template class aux::_Types<Int, Real>::Pred::Vars_fun_t::Fun_mixin;
    template class aux::_Ftypes<Int, Real>::Pred::Vars_fun_t::Fun_mixin;
    template class aux::_Atypes<Int>::Fun::Vars_fun_t::Fun_mixin;
    template class aux::_Atypes<Int>::Pred::Vars_fun_t::Fun_mixin;
    template class aux::_Atypes<Real>::Fun::Vars_fun_t::Fun_mixin;

    template class aux::_Atypes<Int>::Var::Vars_auto_assignee_t::Auto_mixin;
    template class aux::_Atypes<Real>::Var::Vars_auto_assignee_t::Auto_mixin;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr::var {
    template class vars::aux::_Types<Int>::Equality::Assigner_t::Assigner_mixin;
    template class vars::aux::_Types<Int>::Equality::Equality_t::Equality_mixin;
    template class vars::aux::_Types<Int, Real>::Equality::Assigner_t::Assigner_mixin;
    template class vars::aux::_Types<Int, Real>::Equality::Equality_t::Equality_mixin;
    template class vars::aux::_Ftypes<Int, Real>::Equality::Assigner_t::Assigner_mixin;
    template class vars::aux::_Ftypes<Int, Real>::Equality::Equality_t::Equality_mixin;
    template class vars::aux::_Ftypes<Int, Real>::Equality::Assigner_distance_mixin;
    template class vars::aux::_Atypes<Int>::Equality::Assigner_t::Assigner_mixin;
    template class vars::aux::_Atypes<Int>::Equality::Equality_t::Equality_mixin;
    template class vars::aux::_Atypes<Int>::Equality::Assigner_distance_mixin;

    template class vars::aux::_Atypes<Int>::Var::Auto_assignee_t::Auto_mixin;
    template class vars::aux::_Atypes<Int>::Var::Auto_visit_mixin;
    template class vars::aux::_Atypes<Real>::Var::Auto_assignee_t::Auto_mixin;
    template class vars::aux::_Atypes<Real>::Var::Auto_visit_mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
