#pragma once

#include "expr/var/assign.hpp"

#include "util/alg.hpp"
#include "util/flag/alg.hpp"

namespace unsot::expr::var::assign {
    template <typename B>
    void Mixin<B>::reset_impl() noexcept
    {
        Inherit::reset_impl();
        assigned() = false;
        depend_ids().clear();
    }

    template <typename B>
    bool Mixin<B>::maybe_assignable() const noexcept
    {
        return this->is_unset();
    }

    template <typename B>
    void Mixin<B>::prepare_to_assign_by([[maybe_unused]] Id aid) const noexcept
    {
        EVAR_CVERB3LN("prepare_to_assign_by: [" << aid << "]");
    }

    template <typename B>
    void Mixin<B>::forget_to_assign_by([[maybe_unused]] const Id& aid) const noexcept
    {
        EVAR_CVERB3LN("forget_to_assign_by: [" << aid << "]");
    }

    template <typename B>
    bool Mixin<B>::still_assignable() const noexcept
    {
        return This::maybe_assignable();
    }

    template <typename B>
    void Mixin<B>::assign_me(Value val) noexcept
    {
        EVAR_CVERB3LN("assign_me: " << val);
        this->set_value_only_impl(move(val));
        this->set_only_impl();
        assigned() = true;
        this->rcomputed() = true;
        this->computable_flag() = false;
    }

    template <typename B>
    void Mixin<B>::add_depend_id(Id aid) const noexcept
    {
        EVAR_CVERB3LN("add_depend_id: [" << aid << "]");
        depend_ids().insert(move(aid));
    }

    template <typename B>
    void Mixin<B>::rm_depend_id(const Id& aid) const noexcept
    {
        EVAR_CVERB3LN("rm_depend_id: [" << aid << "]");
        depend_ids().erase(aid);
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, typename AssigneeT>
    void Assigner_mixin<B, AssigneeT>::linit()
    {
        add_assignees();
    }

    template <typename B, typename AssigneeT>
    Id Assigner_mixin<B, AssigneeT>::assignee_id(const expr::Ptr& eptr) const noexcept
    {
        if (!is_key(eptr)) return invalid_id;
        const Key& key_ = cast_key(eptr);
        const Ptr* ptr_l;
        try { ptr_l = &this->carg_ptr(key_); }
        catch (const Error&) { return invalid_id; }
        auto& ptr = *ptr_l;
        if (ptr->locked() || !Assignee::is_me(ptr)
            || !Assignee::cast(ptr).allowed_assigner(this->cid())
        ) return invalid_id;
        return ptr->cid();
    }

    template <typename B, typename AssigneeT>
    Id Assigner_mixin<B, AssigneeT>::add_assignee(const Key& key_)
    {
        if (auto& aid = this->cfun().ckey_id(key_); add_assignee(aid))
            return aid;
        return invalid_id;
    }

    template <typename B, typename AssigneeT>
    bool Assigner_mixin<B, AssigneeT>::add_assignee(Id aid)
    {
        /// the assignee do not have to be a dependant (and vice versa)
        if (auto [_, inserted] = assignee_ids().insert(aid); !inserted)
            return false;

        assignee(aid).connect_assigner(this->cid());
        return true;
    }

    template <typename B, typename AssigneeT>
    Id Assigner_mixin<B, AssigneeT>::try_add_assignee(const expr::Ptr& eptr)
    {
        Id aid = assignee_id(eptr);
        if (aid != invalid_id) add_assignee(aid);
        return aid;
    }

    template <typename B, typename AssigneeT>
    Idx Assigner_mixin<B, AssigneeT>::assignee_idx(const Id& aid) const
    {
        return find_key_id(this->cfun(), aid);
    }

    template <typename B, typename AssigneeT>
    bool Assigner_mixin<B, AssigneeT>::nothing_to_assign() const noexcept
    {
        return cid_to_assign() == invalid_id;
    }

    template <typename B, typename AssigneeT>
    bool Assigner_mixin<B, AssigneeT>::has_assigned() const noexcept
    {
        return !empty(cassigned_ids());
    }

    template <typename B, typename AssigneeT>
    void Assigner_mixin<B, AssigneeT>::reset_impl() noexcept
    {
        Inherit::reset_impl();

        [[maybe_unused]] bool removed_depend_id = false;
        if (!nothing_to_assign()) {
            auto& ass = assignee(cid_to_assign());
            [[maybe_unused]] bool id_was_depend;
            if constexpr (_debug_) {
                id_was_depend = contains(ass.cdepend_ids(), this->cid());
            }
            /// Redundant only if also in `assigned_ids`, but does not have to be there
            ass.forget_to_assign_by(this->cid());
            if constexpr (_debug_) {
                if (id_was_depend) removed_depend_id = !contains(ass.cdepend_ids(), this->cid());
            }
        }

        if (has_assigned()) {
            assert(!nothing_to_assign());
            assert(contains(cassigned_ids(), cid_to_assign()));
            for (auto& aid : cassigned_ids()) {
                auto& ass = assignee(aid);
                EVAR_CVERB3LN("unset: " << ass.ckey() << "[" << aid << "]");
                if constexpr (_debug_) {
                    assert(ass.cassigned());
                    /// Could be already removed in the `!nothing_to_assign()` branch
                    if (!removed_depend_id || aid != cid_to_assign())
                        assert(contains(ass.cdepend_ids(), this->cid()));
                }
                ass.unset();
            }
        }

        id_to_assign() = invalid_id;
        assigned_ids().clear();
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::computable_body() const noexcept
    {
        if (any_assignable()) return true;
        return computable_body_not_assign();
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::any_assignable() const noexcept
    {
        //+ maybe use `assignable_flag' to remember last value
        //+ maybe use sth. like `Property' class (also for `evaluable' etc.)
        if (auto flag = surely_any_assignable(); flag.valid()) {
            EVAR_CVERB3LN("surely_any_assignable -> " << flag);
            return flag;
        }

        auto flag = any_assignable_body();
        EVAR_CVERB3LN("any_assignable_body -> " << flag);
        return flag;
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::surely_any_assignable() const noexcept
    {
        if (empty(cassignee_ids())) return false;
        if (this->falsified() || this->computed()) return false;
        if (nothing_to_assign()) return unknown;
        if (has_assigned()) return false;
        return surely_still_assignable(cassignee(cid_to_assign()));
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::any_assignable_body() const noexcept
    {
        return any_true(cassignee_ids(), [this](auto& aid){
            return try_prepare_to_assign(cassignee(aid));
        });
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::assignable(const Assignee& ass) const noexcept
    {
        return ass.maybe_assignable();
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::try_prepare_to_assign(const Assignee& ass) const noexcept
    {
        if (auto flag = assignable(ass); !flag) return flag;
        prepare_to_assign(ass);
        return true;
    }

    template <typename B, typename AssigneeT>
    void Assigner_mixin<B, AssigneeT>::prepare_to_assign(const Assignee& ass) const noexcept
    {
        assert(nothing_to_assign());
        assert(contains(cassignee_ids(), ass.cid()));
        ass.prepare_to_assign_by(this->cid());
        id_to_assign() = ass.cid();
    }

    template <typename B, typename AssigneeT>
    void Assigner_mixin<B, AssigneeT>::forget_to_assign(const Assignee& ass) const noexcept
    {
        assert(cid_to_assign() == ass.cid());
        ass.forget_to_assign_by(this->cid());
        id_to_assign() = invalid_id;
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::
        surely_still_assignable(const Assignee& ass) const noexcept
    {
        assert(ass.cid() == cid_to_assign());
        if (surely_still_assignable_impl(ass)) return true;
        forget_to_assign(ass);
        return unknown;
    }

    template <typename B, typename AssigneeT>
    bool Assigner_mixin<B, AssigneeT>::
        surely_still_assignable_impl(const Assignee& ass) const noexcept
    {
        return ass.still_assignable();
    }

    template <typename B, typename AssigneeT>
    void Assigner_mixin<B, AssigneeT>::assign(Arg_value val, Assignee& ass) noexcept
    {
        EVAR_CVERB3LN("assign: " << ass.ckey() << "[" << ass.cid() << "]");
        assert(ass.still_assignable());
        assert(contains(cassignee_ids(), ass.cid()));
        assert(contains(this->cdepend_ids(), ass.cid()));
        assert(!contains(cassigned_ids(), ass.cid()));
        /// `prepare_to_assign` is not necessary to be called
        ass.add_depend_id(this->cid());
        ass.assign_me(move(val));
        assigned_ids().push_back(ass.cid());
        assert(contains(ass.cdepend_ids(), this->cid()));
        assert(ass.cassigned());
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::computable_body_not_assign() const noexcept
    {
        return Inherit::computable_body();
    }

    template <typename B, typename AssigneeT>
    bool Assigner_mixin<B, AssigneeT>::compute_body_prepare()
    {
        assert(this->computable());
        if (!any_assignable()) {
            const bool success = compute_body_prepare_not_assign();
            EVAR_CVERB3LN("compute_prepare_not_assign -> " << (success ? "" : "!") << "success");
            return success;
        }

        /// Do not use `Fun_mixin's function (which evals *all* args)
        if (!Inherit::Fun_t::Inherit::compute_body_prepare()) return false;

        assert(computable_body());
        assert(!empty(cassignee_ids()) && !nothing_to_assign());
        const bool success = compute_body_prepare_assign();
        EVAR_CVERB3LN("compute_prepare_assign -> " << (success ? "" : "!") << "success");
        assert(!nothing_to_assign());
        assert(!success || has_assigned());
        return success;
    }

    template <typename B, typename AssigneeT>
    bool Assigner_mixin<B, AssigneeT>::compute_body_prepare_not_assign()
    {
        /// Do use (possibly) `Fun_mixin's function (which evals *all* args)
        return Inherit::compute_body_prepare();
    }

    template <typename B, typename AssigneeT>
    typename Assigner_mixin<B, AssigneeT>::Value
    Assigner_mixin<B, AssigneeT>::compute_body_value(bool success)
    {
        if (nothing_to_assign()) {
            assert(!has_assigned());
            assert(!surely_any_assignable());
            auto val = compute_body_value_not_assign(success);
            EVAR_CVERB3LN("compute_value_not_assign -> " << val);
            return val;
        }

        assert(!success || has_assigned());
        assert(!this->falsified() && !this->computed());
        auto val = compute_body_value_assign(success);
        EVAR_CVERB3LN("compute_value_assign -> " << val);
        return val;
    }

    template <typename B, typename AssigneeT>
    typename Assigner_mixin<B, AssigneeT>::Value
    Assigner_mixin<B, AssigneeT>::compute_body_value_not_assign(bool success)
    {
        return Inherit::compute_body_value(success);
    }

    template <typename B, typename AssigneeT>
    typename Assigner_mixin<B, AssigneeT>::Value
    Assigner_mixin<B, AssigneeT>::compute_body_value_assign(bool success)
    {
        return success;
    }

    template <typename B, typename AssigneeT>
    Flag Assigner_mixin<B, AssigneeT>::independent_arg(const Id& aid) const noexcept
    {
        if (Inherit::independent_arg(aid)) return true;

        return cid_to_assign() == aid;
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    bool Equality_mixin<B>::lvalid_pre_init() const noexcept
    {
        const auto& fun_ = this->cfun();
        return fun_.coper() == "=" && fun_.size() == 2;
    }

    template <typename B>
    void Equality_mixin<B>::lcheck_pre_init() const
    {
        expect(lvalid_pre_init(), "Formula is not an equality: "s + this->cfun().to_string());
    }

    template <typename B>
    void Equality_mixin<B>::add_assignees()
    {
        for_each(this->cformula(), [this](auto& eptr){
            [[maybe_unused]] const Id aid = this->try_add_assignee(eptr);
            if constexpr (!_debug_) return;
            if (aid == invalid_id) return;
            assert(contains(this->cdepend_ids(), aid));
            assert(contains(this->cassignee_ids(), aid));
        });
    }

    template <typename B>
    Idx Equality_mixin<B>::assignee_idx(const Id& aid) const
    {
        const Idx idx = Inherit::assignee_idx(aid);
        if (idx == invalid_idx || size(this->cfun().cpart_key_ids(idx)) != 1)
            return invalid_idx;

        return idx;
    }

    template <typename B>
    Flag Equality_mixin<B>::assignable(const Assignee& ass) const noexcept
    {
        if (!Inherit::assignable(ass)) return false;

        const Idx ass_idx = assignee_idx(ass.cid());
        if (ass_idx == invalid_idx) return false;
        assert(ass_idx <= 1);

        return this->evaluable_part(1 - ass_idx);
    }

    template <typename B>
    bool Equality_mixin<B>::surely_still_assignable_impl(const Assignee& ass) const noexcept
    {
        if (!Inherit::surely_still_assignable_impl(ass)) return false;
        const Idx ass_idx = assignee_idx(ass.cid());
        assert(ass_idx != invalid_idx && ass_idx <= 1);
        return this->surely_evaluable_part(1 - ass_idx);
    }

    template <typename B>
    bool Equality_mixin<B>::compute_body_prepare_assign()
    {
        auto& aid = this->cid_to_assign();
        const Idx ass_idx = assignee_idx(aid);
        assert(ass_idx != invalid_idx && ass_idx <= 1);

        Arg_value val = this->eval_part(1 - ass_idx);
        this->assign(move(val), this->assignee(aid));
        return true;
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, typename AssignerT>
    void Auto_mixin<B, AssignerT>::connect_assigner(Id aid)
    {
        Inherit::connect_assigner(aid);
        add_assigner_id(move(aid));
    }

    template <typename B, typename AssignerT>
    void Auto_mixin<B, AssignerT>::add_assigner_id(Id aid)
    {
        assigner_ids().insert(move(aid));
        this->depend_ids().reserve(size(cassigner_ids())*4);
    }

    template <typename B, typename AssignerT>
    bool Auto_mixin<B, AssignerT>::maybe_assignable() const noexcept
    {
        if (!Inherit::maybe_assignable()
            || this->surely_computable().is_false()) return false;

        return empty(this->cdepend_ids());
    }

    template <typename B, typename AssignerT>
    void Auto_mixin<B, AssignerT>::prepare_to_assign_by(Id aid) const noexcept
    {
        Inherit::prepare_to_assign_by(aid);
        this->add_depend_id(move(aid));
        this->computable_flag() = true;
    }

    template <typename B, typename AssignerT>
    void Auto_mixin<B, AssignerT>::forget_to_assign_by(const Id& aid) const noexcept
    {
        Inherit::forget_to_assign_by(aid);
        this->rm_depend_id(aid);
        this->computable_flag().forget();
    }

    template <typename B, typename AssignerT>
    Flag Auto_mixin<B, AssignerT>::computable_body() const noexcept
    {
        if (Inherit::computable_body()) return true;
        if (this->is_set()) return false;

        return computable_body_impl();
    }

    template <typename B, typename AssignerT>
    Flag Auto_mixin<B, AssignerT>::computable_body_impl() const noexcept
    {
        return any_true(cassigner_ids(), [this](auto& aid){
            return cassigner(aid).any_assignable();
        });
    }

    template <typename B, typename AssignerT>
    bool Auto_mixin<B, AssignerT>::compute_body_prepare()
    {
        if (empty(this->cdepend_ids()))
            return compute_body_prepare_impl([this]{
                return any_of(cassigner_ids(), [this](auto& aid) -> bool {
                    auto& ass = assigner(aid);
                    if (!ass.any_assignable()) return false;
                    return ass.compute();
                });
        });

        if (!Inherit::compute_body_prepare()) return false;

        return compute_body_prepare_impl([this]{
            return all_of(this->cdepend_ids(), [this](auto& aid) -> bool {
                return assigner(aid).compute();
            });
        });
    }

    template <typename B, typename AssignerT>
    template <typename BodyPred>
    bool Auto_mixin<B, AssignerT>::compute_body_prepare_impl(BodyPred pred) const
    {
        if (!pred()) return false;

        return this->cassigned();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    void Distance_mixin<B>::assign_me_distance(Distance assigner_dist) noexcept
    {
        this->distance() = assigner_dist+1;
    }
}
