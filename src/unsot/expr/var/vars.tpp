#pragma once

#include "expr/var/vars.hpp"

#include "util/alg.hpp"

#include "expr/var.tpp"

namespace unsot::expr::var::vars {
    namespace aux {
        template <typename Sort, typename ArgSort>
        Base<Sort, ArgSort>::Base()
            : _ptrs_ptr(new_ptrs()), _ptrs_view(ptrs()),
              _keys_ptr(new_keys()), _values_ptr(new_values<Value>()),
              _ids_map_ptr(new_ids_map())
        { }

        template <typename Sort, typename ArgSort>
        bool Base<Sort, ArgSort>::
            global_contains(const Key& key_) const noexcept
        {
            return cids_map().contains(key_);
        }

        template <typename Sort, typename ArgSort>
        void Base<Sort, ArgSort>::
            check_global_contains(const Key& key_) const
        {
            expect(global_contains(key_), "Key not contained yet: "s + key_);
        }

        template <typename Sort, typename ArgSort>
        void Base<Sort, ArgSort>::
            check_not_global_contains(const Key& key_) const
        {
            expect(!global_contains(key_), "Key already contained: "s + key_);
        }

        template <typename Sort, typename ArgSort>
        bool Base<Sort, ArgSort>::
            local_contains(const Key& key_) const noexcept
        {
            auto& imap = cids_map();
            auto it = imap.find(key_);
            return it != std::end(imap) && local_contains(it->second);
        }

        template <typename Sort, typename ArgSort>
        void Base<Sort, ArgSort>::
            check_local_contains(const Key& key_) const
        {
            expect(local_contains(key_),
                   "Key not contained in the local view yet: "s + key_);
        }

        template <typename Sort, typename ArgSort>
        void Base<Sort, ArgSort>::
            check_not_local_contains(const Key& key_) const
        {
            expect(!local_contains(key_),
                   "Key already contained in the local view: "s + key_);
        }

        template <typename Sort, typename ArgSort>
        bool Base<Sort, ArgSort>::
            local_contains(const Key_id& kid) const noexcept
        {
            return cptrs_view().contains(kid);
        }

        template <typename Sort, typename ArgSort>
        Ptr* Base<Sort, ArgSort>::global_find(const Key& key_) noexcept
        {
            auto& imap = ids_map();
            auto it = imap.find(key_);
            if (it == std::end(imap)) return nullptr;
            return &ptr(it->second);
        }

        template <typename Sort, typename ArgSort>
        Ptr* Base<Sort, ArgSort>::local_find(const Key& key_) noexcept
        {
            auto& imap = ids_map();
            auto it = imap.find(key_);
            if (it == std::end(imap)) return nullptr;
            return local_find(it->second);
        }

        template <typename Sort, typename ArgSort>
        Ptr* Base<Sort, ArgSort>::local_find(const Key_id& kid) noexcept
        {
            if (!local_contains(kid)) return nullptr;
            return &ptr(kid);
        }

        template <typename Sort, typename ArgSort>
        void Base<Sort, ArgSort>::reserve(size_t size_)
        {
            ptrs().reserve(size_);
            ptrs_view().reserve(size_);
            keys().reserve(size_);
            values().reserve(size_);
        }

        template <typename Sort, typename ArgSort>
        void Base<Sort, ArgSort>::push_back(Ptr ptr)
        {
            Key_id kid = ptr->cid();
            ptrs().push_back(move(ptr));
            ptrs_view().push_back(move(kid));
        }
    }

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort>
    Base<Sort, ArgSort>::Base()
        : _args_base_l_ptr(new_args_base_l())
    { }

    template <typename Sort, typename ArgSort>
    Base<Sort, ArgSort>::Base(Base&& rhs)
    {
        swap(rhs);
    }

    template <typename Sort, typename ArgSort>
    Base<Sort, ArgSort>& Base<Sort, ArgSort>::operator =(Base&& rhs)
    {
        swap(rhs);
        return *this;
    }

    template <typename Sort, typename ArgSort>
    void Base<Sort, ArgSort>::swap(Base& rhs) noexcept
    {
        using std::swap;
        swap(static_cast<Inherit&>(*this), static_cast<Inherit&>(rhs));
        swap(_args_base_l_ptr, rhs._args_base_l_ptr);

        if constexpr (is_global_v) connect_args(*this);
    }

    template <typename Sort, typename ArgSort>
    void Base<Sort, ArgSort>::connect_args(Args_base& args)
    {
        if constexpr (is_global_v) args_base_l() = &args;
        else args_base_l_ptr() = args.cargs_base_l_ptr();
    }

    template <typename Sort, typename ArgSort>
    void Base<Sort, ArgSort>::check_contains(const Key& key_) const
    {
        expect(contains(key_), "Var not contained yet: "s + key_);
    }

    template <typename Sort, typename ArgSort>
    void Base<Sort, ArgSort>::check_not_contains(const Key& key_) const
    {
        expect(!contains(key_), "Var already contained: "s + key_);
    }

    template <typename Sort, typename ArgSort>
    String Base<Sort, ArgSort>::to_string() const&
    {
        using unsot::to_string;
        if constexpr (is_global_v) return to_string(this->cptrs());
        else return this->cptrs_view().to_string();
    }

    template <typename Sort, typename ArgSort>
    String Base<Sort, ArgSort>::to_string()&&
    {
        using unsot::to_string;
        if constexpr (is_global_v) return to_string(move(this->ptrs()));
        else return move(this->ptrs_view()).to_string();
    }

    template <typename Sort, typename ArgSort>
    bool Base<Sort, ArgSort>::equals(const This& rhs) const
    {
        if (size() != rhs.size()) return false;
        if (!all_of(*this, rhs.cbegin(), [](auto& ptr, auto& ptr2){
            return ptr->ckey() == ptr2->ckey() && *ptr == *ptr2;
        })) return false;

        if constexpr (is_global_v) return true;
        else return cargs_base() == rhs.cargs_base();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    void Auto_assign_mixin<B>::swap(Auto_assign_mixin& rhs) noexcept
    {
        Inherit::swap(rhs);
        std::swap(_preds_base_l_ptr, rhs._preds_base_l_ptr);
    }

    template <typename B>
    void Auto_assign_mixin<B>::connect_preds(Preds_base& preds_base)
    {
        preds_base_l() = &preds_base;
    }
}
