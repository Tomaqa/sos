#include "expr/preprocess.hpp"

#include "util/hash.hpp"

namespace unsot::expr {
    namespace {
        const Hash<Key>& reserved_keys_map() noexcept
        {
            static const Hash<Key> map{
                {Macro::include_key},
                {Macro::def_key},
                {Macro::undef_key},
                {Macro::let_key},
                {Macro::set_key},
                {Macro::if_key},
                {Macro::ifdef_key},
                {Macro::ifndef_key},
                {Macro::isdef_key},
                {Macro::isndef_key},
                {Macro::for_key},
                {Macro::while_key},
                {Macro::null_key},
                {Macro::listp_key},
                {Macro::numberp_key},
                {Macro::refp_key},
                {Macro::equal_key},
                {Macro::eq_key},
                {Macro::len_key},
                {Macro::print_key},
                {Macro::assert_key},
                {Macro::error_key},
                {Macro::car_key},
                {Macro::cdr_key},
                {Macro::nth_key},
                {Macro::nthcdr_key},
                {Macro::ref_name_key},
            };

            return map;
        }

        inline Key to_end_key(Key key_) noexcept
        {
            return Macro::end_key + move(key_);
        }

        inline const Hash<Key, Key>& reserved_end_keys_map() noexcept
        {
            static const Hash<Key, Key> map{
                {Macro::def_key,   to_end_key(Macro::def_key)},
                {Macro::let_key,   to_end_key(Macro::let_key)},
                {Macro::if_key,    to_end_key(Macro::if_key)},
                {Macro::for_key,   to_end_key(Macro::for_key)},
                {Macro::while_key, to_end_key(Macro::while_key)},
            };

            return map;
        }
    }

    bool Macro::is_end_key(String_view view) noexcept
    {
        return starts_with(view, end_key);
    }

    bool Macro::is_macro_of(String_view view, const Key& key_) noexcept
    {
        return is_macro(view)
            && view.size() == size(key_)+1
            && ends_with(view, key_);
    }

    bool Macro::is_end_key_of(String_view view, const Key& key_) noexcept
    {
        return is_end_key(view)
            && view.size() == size(end_key)+size(key_)
            && ends_with(view, key_);
    }

    bool Macro::is_reserved_key(const Key& key_) noexcept
    {
        if (empty(key_)) return true;
        if (starts_with(key_, char_str)) {
            if (size(key_) > 2) return false;
            return size(key_) == 1 || is_escape_char(key_.back());
        }
        return reserved_keys_map().contains(key_)
            || is_reserved_end_key(key_);
    }

    bool Macro::is_reserved_end_key(const Key& key_) noexcept
    {
        return reserved_end_keys_map().contains(key_);
    }

    void Macro::check_is_not_reserved_key(const Key& key_)
    {
        expect(!is_reserved_key(key_),
               "Unexpected reserved macro key: "s + to_string(key_));
    }

    const Key& Macro::end_key_of(String_view view)
    {
        return reserved_end_keys_map()[view];
    }
}
