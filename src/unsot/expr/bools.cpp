#include "expr/bools.hpp"

#include "util/alg.hpp"
#include "util/string/alg.hpp"
#include "expr/opers_arith.hpp"

namespace unsot::expr::bools {
    Lit::Lit(Var_id var_id_, int sign_)
        : Inherit(sign_ ? move(var_id_) : bools::neg(move(var_id_)))
    { }

    bool Lit::is_neg() const noexcept
    {
        return bools::is_neg(*this);
    }

    bool Lit::sign() const noexcept
    {
        return !is_neg();
    }

    Lit& Lit::neg()
    {
        *this = move(*this).to_neg();
        return *this;
    }

    Lit Lit::to_neg() const&
    {
        return bools::neg(static_cast<const Key&>(*this));
    }

    Lit Lit::to_neg()&&
    {
        return bools::neg(static_cast<Key&&>(move(*this)));
    }

    Var_id Lit::cvar_id() const&
    {
        return is_neg() ? to_neg() : *this;
    }

    Var_id Lit::var_id()&&
    {
        return is_neg() ? move(*this).to_neg() : move(*this);
    }

    Ptr Lit::to_ptr() const&
    {
        return Lit(*this).to_ptr();
    }

    Ptr Lit::to_ptr()&&
    {
        if (!is_neg()) return new_key(move(*this));
        return Formula::new_me("not", List(new_key(move(*this).to_neg())));
    }

    ////////////////////////////////////////////////////////////////

    template <typename K, Req<is_keys_v<K>>>
    Clause::Clause(K&& keys)
        : Clause(to<Clause>(FORWARD(keys)))
    { }

    template Clause::Clause(const Keys&);
    template Clause::Clause(Keys&&);

    Clause& Clause::neg()
    {
        for (auto& lit : *this) lit.neg();
        return *this;
    }

    Clause Clause::to_neg() const&
    {
        return Clause(*this).to_neg();
    }

    Clause&& Clause::to_neg()&&
    {
        return move(neg());
    }

    Ptr Clause::to_ptr() const&
    {
        return Clause(*this).to_ptr();
    }

    Ptr Clause::to_ptr()&&
    {
        if (empty()) return {};
        if (size() == 1) return move(front()).to_ptr();
        return Formula::new_me("or", to<List>(move(*this), [](auto&& lit){
            return move(lit).to_ptr();
        }));
    }

    String Clause::to_string() const&
    {
        return "( "s + Inherit::to_string() + ")";
    }

    String Clause::to_string()&&
    {
        return "( "s + move(*this).Inherit::to_string() + ")";
    }

    ////////////////////////////////////////////////////////////////

    Idx Cnf::last_idx() const noexcept
    {
        return counter()-1;
    }

    size_t Cnf::increased_size() const noexcept
    {
        return last_idx() - init_idx() + 1;
    }

    Var_id Cnf::new_var()
    {
        const auto cnt = counter()++;
        gcounter++;
        return var_id(cx(), cnt);
    }

    void Cnf::add_not(Lit p, Formula& not_phi)
    {
        /// (p = ~a) <=> (~p | ~a) & (p | a)
        Lit a = Elem::cast(move(not_phi).front());

        push_back({neg(p), neg(a)});
        push_back({move(p), move(a)});
    }

    void Cnf::add_and(Lit p, Formula& and_phi)
    {
        /// (p = &{a_i}) <=> &{~p | a_i} & (p | |{~a_i})
        const size_t size_ = and_phi.size();
        Clause pair_clause;
        Clause n_clause;
        pair_clause.reserve(2);
        n_clause.reserve(size_+1);
        reserve(size() + size_+1);
        pair_clause.emplace_back(neg(p));
        n_clause.emplace_back(move(p));

        for (auto&& ptr : move(and_phi)) {
            Lit lit = Elem::cast(move(ptr));
            pair_clause.push_back(lit);
            n_clause.emplace_back(neg(move(lit)));
            emplace_back(pair_clause);
            pair_clause.pop_back();
        }
        emplace_back(move(n_clause));
    }

    void Cnf::add_or(Lit p, Formula& or_phi)
    {
        /// (p = |{a_i}) <=> &{p | ~a_i} & (~p | |{a_i})
        const size_t size_ = or_phi.size();
        Clause pair_clause;
        Clause n_clause;
        pair_clause.reserve(2);
        n_clause.reserve(size_+1);
        reserve(size() + size_+1);
        pair_clause.emplace_back(p);
        n_clause.emplace_back(neg(move(p)));

        for (auto&& ptr : move(or_phi)) {
            Lit lit = Elem::cast(move(ptr));
            pair_clause.emplace_back(neg(lit));
            n_clause.emplace_back(move(lit));
            emplace_back(pair_clause);
            pair_clause.pop_back();
        }
        emplace_back(move(n_clause));
    }

    void Cnf::add_bin_and(Lit p, Formula& and_phi)
    {
        /// (p = a & b) <=> (~p | a) & (~p | b) & (p | ~a | ~b)
        Lit not_p = neg(p);
        Lit a = Elem::cast(move(and_phi).front());
        Lit b = Elem::cast(move(and_phi).back());

        push_back({not_p, a});
        push_back({move(not_p), b});
        push_back({move(p), neg(move(a)), neg(move(b))});
    }

    void Cnf::add_bin_or(Lit p, Formula& or_phi)
    {
        /// (p = a | b) <=> (p | ~a) & (p | ~b) & (~p | a | b)
        Lit a = Elem::cast(move(or_phi).front());
        Lit b = Elem::cast(move(or_phi).back());

        push_back({p, neg(a)});
        push_back({p, neg(b)});
        push_back({neg(move(p)), move(a), move(b)});
    }

    void Cnf::add_bin_xor(Lit p, Formula& xor_phi)
    {
        /// (p = a ^ b) <=> (~p | a | b) & (~p | ~a | ~b)
        ///               & (p | ~a | b) & (p | a | ~b)
        Lit not_p = neg(p);
        Lit a = Elem::cast(move(xor_phi).front());
        Lit b = Elem::cast(move(xor_phi).back());
        Lit not_a = neg(a);
        Lit not_b = neg(b);

        push_back({not_p, a, b});
        push_back({move(not_p), not_a, not_b});
        push_back({p, move(not_a), move(b)});
        push_back({move(p), move(a), move(not_b)});
    }

    void Cnf::add_bin_iff(Lit p, Formula& iff_phi)
    {
        /// (p = a <=> b) <=> (~p | a | ~b) & (~p | ~a | b)
        ///                 & (p | a | b) & (p | ~a | ~b)
        Lit not_p = neg(p);
        Lit a = Elem::cast(move(iff_phi).front());
        Lit b = Elem::cast(move(iff_phi).back());
        Lit not_a = neg(a);
        Lit not_b = neg(b);

        push_back({not_p, a, not_b});
        push_back({move(not_p), not_a, b});
        push_back({p, move(a), move(b)});
        push_back({move(p), move(not_a), move(not_b)});
    }

    void Cnf::add_bin_implies(Lit p, Formula& impl_phi)
    {
        /// (p = a => b) <=> (p | a) & (p | ~b) & (~p | ~a | b)
        Lit a = Elem::cast(move(impl_phi).front());
        Lit b = Elem::cast(move(impl_phi).back());

        push_back({p, a});
        push_back({p, neg(b)});
        push_back({neg(move(p)), neg(move(a)), move(b)});
    }

    template <typename L, Req<is_list_v<L>>>
    void Cnf::parse_top(L&& ls)
    {
        reserve(ls.size());
        Ptr ptr;
        if constexpr (is_formula_v<L>) {
            ptr = FORWARD(ls).to_ptr();
            assert(ptr->initialized());
        }
        else {
            auto& front = ls.cfront();
            ptr = is_key(front) && fun::Opers<Bool>::valid(cast_key(front))
                ? Formula::new_me(FORWARD(ls))
                : Formula::new_me("and", FORWARD(ls));
        }

        parse(ptr, true);
    }

    template void Cnf::parse_top(const List&);
    template void Cnf::parse_top(List&&);
    template void Cnf::parse_top(const Formula&);
    template void Cnf::parse_top(Formula&&);

    void Cnf::parse(Ptr& ptr, bool is_top, bool neg_)
    try {
        maybe_deep_copy(ptr);
        if (Elem::is_me(ptr)) return parse_elem(Elem::cast(ptr), is_top, neg_);

        Formula& phi = Formula::cast(ptr);
        auto& f_key = phi.coper();

        const bool is_and = (f_key == "and");
        const bool is_or = (f_key == "or");
        const bool is_not = (f_key == "not");

        const bool is_unary = (phi.size() == 1);

        if (is_unary) {
            expect(is_and || is_or || is_not, "Unexpected unary function.");
            ptr = move(phi.front());
            return parse(ptr, is_top, neg_ ^ is_not);
        }
        expect(!is_not, "Negation must be unary.");

        const bool keep_top = is_and && is_top && !neg_;
        for (auto& ptr_ : phi) parse(ptr_, keep_top);
        if (keep_top) return;

        if (is_or && is_top && !neg_) {
            emplace_back(to<Clause>(cast_to_elems(move(phi))));
            return;
        }

        Elem elem = new_var();
        Var_id& x = elem.get_key();
        parse_elems(phi, x, is_and, is_or);
        parse_elem(elem, is_top, neg_);
        if (!is_top) ptr = move(elem).to_ptr();
    }
    catch (const Error& err) {
        using unsot::to_string;
        throw "At "s + to_string(ptr) + ":\n" + err;
    }

    void Cnf::parse_elem(Elem& elem, bool is_top, bool neg_)
    {
        if (neg_) elem = neg(move(elem));
        if (is_top) push_back({move(elem)});
    }

    void Cnf::parse_elems(Formula& phi, const Var_id& x, bool is_and, bool is_or)
    {
        const bool is_binary = (phi.size() == 2);

        if (is_and) return is_binary ? add_bin_and(x, phi) : add_and(x, phi);
        if (is_or) return is_binary ? add_bin_or(x, phi) : add_or(x, phi);

        expect(is_binary, "Expected binary formula.");

        const auto& f_key = phi.coper();
        if (f_key == "xor") return add_bin_xor(x, phi);
        if (f_key == "=") return add_bin_iff(x, phi);
        if (f_key == "=>") return add_bin_implies(x, phi);

        THROW("Unknown boolean function: " + f_key);
    }

    Formula Cnf::to_formula() const&
    {
        return Cnf(*this).to_formula();
    }

    Formula Cnf::to_formula()&&
    {
        if (empty()) return {};
        return Formula::cons("and", to<List>(move(*this), [](auto&& clause){
            return move(clause).to_ptr();
        }));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr::bools {
    Var_id var_id(Var_id x, Idx idx)
    {
        return move(x) + to_string(idx);
    }


    bool is_neg(const Key& key_) noexcept
    {
        return starts_with(key_, neg_prefix);
    }

    Key neg(const Key& key_)
    {
        if (is_neg(key_)) return key_.substr(1);
        return neg_prefix+key_;
    }

    Key neg(Key&& key_)
    {
        if (is_neg(key_)) return move(key_).erase(0, 1);
        return neg_prefix+move(key_);
    }

    template <typename C, Req<is_clause_v<C>>>
    Clause to_conflict(C&& clause)
    {
        return FORWARD(clause).to_neg();
    }

    template <typename C, Req<is_clause_v<C>>>
    Cnf to_pair_conflict(C&& clause)
    {
        /// (!p1 | !p2) & (!p1 | !p3) & ... & (!p2 | !p3) & ...
        /// -> size: (n)(2) = O(n^2)
        if (empty(clause)) return {};
        Cnf cnf;
        cnf.reserve(size(clause));
        for (auto bit = clause.begin(), eit2 = clause.end(), eit = eit2-1,
             it = bit; it != eit; ++it) {
            for (auto it2 = it+1; it2 != eit2; ++it2) {
                cnf.push_back({neg(*it), neg(*it2)});
            }
        }

        return cnf;
    }

    template Clause to_conflict(const Clause&);
    template Clause to_conflict(Clause&&);
    template Cnf to_pair_conflict(const Clause&);
    template Cnf to_pair_conflict(Clause&&);
}
