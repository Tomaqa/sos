#include "expr.hpp"

#include "util/alg.hpp"

#include "expr/list.tpp"

namespace unsot::expr {
    //+ possibly use shallow copy, but it may be tricky (e.g. `Formula`s are incompatible with it)
    List::List(const List& rhs)
        : List(rhs, deep_copy_tag)
    { }

    List::List(const List& rhs, Shallow_copy_tag)
        : List(rhs.cbegin(), rhs.cend(), shallow_copy_tag)
    { }

    List::List(const List& rhs, Deep_copy_tag)
        : List(rhs.cbegin(), rhs.cend(), deep_copy_tag)
    { }

    List& List::operator =(const List& rhs)
    {
        List tmp(rhs);
        swap(tmp);
        return *this;
    }

    List::List(initializer_list<Ptr> ils)
        : List(ils.begin(), ils.end())
    { }

    List::List(Ptr ptr)
    {
        emplace_back(move(ptr));
    }

    template List::List(String, Tag<Elem>);
    template List::List(istream&, Tag<Elem>);
    template List& List::parse<Elem>(istream&, streampos&, int depth);

    List List::shallow_copy() const
    {
        return shallow_copy(cbegin(), cend());
    }

    List List::deep_copy() const
    {
        return deep_copy(cbegin(), cend());
    }

    void List::check_is_me(const Ptr& ptr)
    {
        check_is_list(ptr);
    }

    List& List::nest()&
    {
        *this = move(*this).to_nested();
        return *this;
    }

    List List::to_nested() const&
    {
        return {to_ptr()};
    }

    List List::to_nested()&&
    {
        return {move(*this).to_ptr()};
    }

    List& List::emerge() noexcept
    {
        if (size() != 1 || !is_me(cfront())) return *this;

        auto& ptr = front();
        maybe_deep_copy(ptr);
        List ls = move(cast(ptr));
        if (ls.empty()) {
            clear();
            return *this;
        }

        Container::operator =(move(ls));
        return *this;
    }

    List& List::emerge_rec() noexcept
    {
        if (empty()) return *this;
        for_each_if(*this, is_me, [](auto& ptr){
            maybe_deep_copy(ptr);
            List& ls = cast(ptr);
            if (ls.emerge_rec().size() == 1) {
                ptr = move(ls.front());
            }
        });
        return emerge();
    }

    List& List::flatten()&
    {
        if (is_flat(*this)) return *this;

        Container cont;
        for (auto& ptr : *this) {
            if (!is_me(ptr)) {
                cont.emplace_back(move(ptr));
                continue;
            }
            maybe_deep_copy(ptr);
            auto subls = move(cast(ptr)).to_flat();
            move(subls, back_inserter(cont));
        }

        Container::operator =(move(cont));
        return *this;
    }

    List List::to_flat() const&
    {
        return List(*this).to_flat();
    }

    List List::to_flat()&&
    {
        return move(flatten());
    }

    String List::to_string() const&
    {
        return make_to_string(*this);
    }

    String List::to_string()&&
    {
        return make_to_string(move(*this));
    }

    bool List::lequals(const This& rhs) const noexcept
    {
        return util::equal(*this, rhs, [](auto& ptr1, auto& ptr2){
            return ptr1->equals(*ptr2);
        });
    }

    //+ size_t List::hash() const
    //+ {
    //+     return std::hash<String>()(to_string());
    //+ }

    ////////////////////////////////////////////////////////////////

    namespace aux {
        template struct List_foo<Elem>;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template class List::To_string_crtp<List::To_string<const List>, const List>;
    template class List::To_string_crtp<List::To_string<List>, List>;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    bool is_flat(const List& ls) noexcept
    {
        return all_of(ls, Elem_base::is_me);
    }

    bool is_deep(const List& ls) noexcept
    {
        return all_of(ls, List::is_me);
    }

    void check_is_flat(const List& ls)
    try {
        for_each(ls, Elem_base::check_is_me);
    }
    catch (const Error& err) {
        THROW("Expected expr list with elements only, got: ") + ls.to_string() + "\n" + err;
    }

    void check_is_deep(const List& ls)
    try {
        for_each(ls, List::check_is_me);
    }
    catch (const Error& err) {
        THROW("Expected expr list with nested lists only, got: ") + ls.to_string() + "\n" + err;
    }

    bool is_as_elem_base(const List& ls) noexcept
    {
        return is_as_elem<Elem_base>(ls);
    }

    void check_is_as_elem_base(const List& ls)
    {
        check_is_as_elem<Elem_base>(ls);
    }

    bool is_as_key(const List& ls) noexcept
    {
        return is_as_elem_base(ls) && is_key(ls.cfront());
    }

    void check_is_as_key(const List& ls)
    {
        check_is_as_elem_base(ls);
        check_is_key(ls.cfront());
    }

    bool has_keys_only(const List& ls) noexcept
    {
        return all_of(ls, is_key);
    }

    void check_has_keys_only(const List& ls)
    try {
        for_each(ls, check_is_key);
    }
    catch (const Error& err) {
        try { check_is_flat(ls); }
        catch (const Error& err2) {
            THROW(err2) + "\n" + err;
        }
        THROW(err);
    }
}
