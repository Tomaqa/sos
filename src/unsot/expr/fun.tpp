#pragma once

#include "expr/fun.hpp"

#include "util/union.hpp"
#include "util/alg.hpp"

namespace unsot::expr::fun {
    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::linit()
    {
        simplify();
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::init_oper()
    {
        Inherit::init_oper();
        auto& op = this->coper();
        size_t size_ = this->size();
        try {
            switch (size_) {
            case 1:
                if constexpr (is_fun_v) f_holder() = Opers::unary_f(op);
                else f_holder() = Opers::unary_pred(op);
                return;
            case 2:
                if constexpr (is_fun_v) f_holder() = Opers::binary_f(op);
                else f_holder() = Opers::binary_pred(op);
                return;
            case 3:
                if constexpr (is_fun_v) f_holder() = Opers::ternary_f(op);
                else f_holder() = Opers::ternary_pred(op);
                return;
            default:
                if constexpr (is_fun_v)
                    f_holder() = Cont_f(Opers::binary_f(op));
                else f_holder() = Cont_pred(Opers::binary_pred(op));
                return;
            }
        }
        catch (const Error&) {
            if constexpr (is_fun_v && is_pred_v) {
                if (Opers::valid_pred(*this)) switch (size_) {
                case 1:
                    f_holder() = Opers::unary_pred(op);
                    return;
                case 2:
                    f_holder() = Opers::binary_pred(op);
                    return;
                case 3:
                    f_holder() = Opers::ternary_pred(op);
                    return;
                default:
                    f_holder() = Cont_pred(Opers::binary_pred(op));
                    return;
                }
            }
            if constexpr (is_pred_v || is_bool_v) {
                if (Opers::valid_bool(*this)) switch (size_) {
                case 1:
                    f_holder() = Opers::unary_bool(op);
                    return;
                case 2:
                    f_holder() = Opers::binary_bool(op);
                    return;
                case 3:
                    f_holder() = Opers::ternary_bool(op);
                    return;
                default:
                    f_holder() = Cont_bool(Opers::binary_bool(op));
                    return;
                }
            }

            if constexpr (is_fun_v) Opers::check_f(*this);
            else if constexpr (is_pred_v) Opers::check_pred(*this);
            else if constexpr (is_bool_v) Opers::check_bool(*this);
            assert(false);
        }
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::init_arg_list(Ptr& ptr)
    {
        Inherit::init_arg_list(ptr);
        lazy_values().emplace_back(make_lazy_fun(ptr));
    }

    template <typename Arg, typename ConfT>
    Ptr Fun_tp<Arg, ConfT>::init_arg_list_new(Ptr&& ptr)
    {
        //+ this all is certainly not ideal
        if (This::is_me(ptr)) return Inherit::init_arg_list_new(move(ptr));
        if (Fun_base::is_me(ptr)) {
            return new_fun<Arg>(Fun_base::cast(move(ptr)),
                this->ckeys_ptr(), this->cvalues_ptr(), this->cids_map_ptr()
            );
        }
        if (Formula::is_me(ptr))
            return new_fun<Arg>(Formula::cast(move(ptr)),
                this->ckeys_ptr(), this->cvalues_ptr(), this->cids_map_ptr()
            );
        return new_fun<Arg>(List::cast(move(ptr)), this->ckeys_ptr(),
                            this->cvalues_ptr(), this->cids_map_ptr());
    }

    template <typename Arg, typename ConfT>
    Pair<bool, Key_id> Fun_tp<Arg, ConfT>::init_arg_key_add(Ptr& ptr)
    {
        auto [added, kid] = Inherit::init_arg_key_add(ptr);
        lazy_values().emplace_back(make_lazy_key(kid));
        return {added, move(kid)};
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::init_arg_value(Ptr& ptr)
    {
        Inherit::init_arg_value(ptr);
        auto val = to_value_check<Value>(ptr);
        lazy_values().emplace_back(make_lazy_value(move(val)));
    }

    template <typename Arg, typename ConfT>
    typename Fun_tp<Arg, ConfT>::Lazy_value
    Fun_tp<Arg, ConfT>::make_lazy_fun(const Ptr& ptr) const noexcept
    {
        if (Fun<Arg>::is_me(ptr))
            return [fptr{ptr}]{ return Fun<Arg>::cast(fptr).eval().value(); };
        return [pptr{ptr}]{ return Pred<Arg>::cast(pptr).eval().value(); };
    }

    template <typename Arg, typename ConfT>
    typename Fun_tp<Arg, ConfT>::Lazy_value
    Fun_tp<Arg, ConfT>::make_lazy_key(const Key_id& kid) const noexcept
    {
        /// clang is not satisfied with plain `kid`
        return [vals_l{&this->cvalues()}, kid{kid}]{
            return value<Value>(*vals_l, kid);
        };
    }

    template <typename Arg, typename ConfT>
    typename Fun_tp<Arg, ConfT>::Lazy_value
    Fun_tp<Arg, ConfT>::make_lazy_value(Value val) const noexcept
    {
        return [CAPTURE_RVAL(val)]{ return val; };
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::simplify()
    {
        if (this->ckey_ids().empty()) return simplify_to_lazy_value();
        if (this->size() == 1) {
            auto& op = this->coper();
            if (contains({"+", "and", "or"}, op))
                return simplify_unary();
        }
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::simplify_to_lazy_value()
    {
        Value val = ret_to_value(eval_check());
        f_holder() = make_lazy_value(move(val));
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::simplify_unary()
    {
        auto& ptr = this->cfront();
        assert(List::is_me(ptr) || is_key(ptr));
        if (is_key(ptr))
            f_holder() = make_lazy_key(this->ckey_id(cast_key(ptr)));
        else f_holder() = make_lazy_fun(ptr);
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::reserve(size_t size_)
    {
        Inherit::reserve(size_);
        lazy_values().reserve(size_);
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::resize(size_t size_)
    {
        Inherit::resize(size_);
        lazy_values().resize(size_);
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::reserve_keys(size_t size_)
    {
        Inherit::reserve_keys(size_);
        this->values().reserve(size_);
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::resize_keys(size_t size_)
    {
        Inherit::resize_keys(size_);
        this->values().resize(size_);
    }

    template <typename Arg, typename ConfT>
    Key_id Fun_tp<Arg, ConfT>::add_key_impl(Key key_)
    {
        return expr::add_key_value<Value>(this->keys(), this->values(),
                                          this->ids_map(), move(key_));
    }

    template <typename Arg, typename ConfT>
    Optional<typename Fun_tp<Arg, ConfT>::Ret>
    Fun_tp<Arg, ConfT>::eval(initializer_list<Value> ils) const noexcept
    {
        this->values() = move(ils);
        return eval();
    }

    template <typename Arg, typename ConfT>
    Optional<typename Fun_tp<Arg, ConfT>::Ret>
    Fun_tp<Arg, ConfT>::eval() const noexcept
    {
        //+ optional used, but cannot fail?
        return cf_holder().cvisit([this](auto& f) -> Ret {
            using F = DECAY(f);
            if constexpr (is_same_v<F, Lazy_value>) return invoke(f);
            else if constexpr (is_unary_v<F, Opers>)
                return invoke(f, eval_part(0));
            else if constexpr (is_binary_v<F, Opers>)
                return invoke(f, eval_part(0), eval_part(1));
            else if constexpr (is_ternary_v<F, Opers>)
                return invoke(f, eval_part(0), eval_part(1), eval_part(2));
            else if constexpr (is_same_v<F, Cont_f>
                               || is_same_v<F, Cont_pred>
                               || is_same_v<F, Cont_bool>)
                return invoke(f, clazy_values(), lazy);
            else static_assert(false_tp<F>);
        });
    }

    template <typename Arg, typename ConfT>
    typename Fun_tp<Arg, ConfT>::Ret
    Fun_tp<Arg, ConfT>::eval_check(initializer_list<Value> ils) const
    {
        this->values() = move(ils);
        return eval_check();
    }

    template <typename Arg, typename ConfT>
    typename Fun_tp<Arg, ConfT>::Ret Fun_tp<Arg, ConfT>::eval_check() const
    try {
        check_values<Arg>(this->cvalues(), this->ckeys());
        auto opt = eval();
        expect(opt.valid(), "The result value is not valid.");
        return move(opt).value();
    }
    catch (const Error& err) {
        THROW("Evaluation of expr::Fun failed: ") + to_string() + "\n" + err;
    }

    template <typename Arg, typename ConfT>
    typename Fun_tp<Arg, ConfT>::Value
    Fun_tp<Arg, ConfT>::eval_part(Idx idx) const noexcept
    {
        return invoke(clazy_value(idx));
    }

    template <typename Arg, typename ConfT>
    String Fun_tp<Arg, ConfT>::to_string() const&
    {
        return to_string_impl(*this);
    }

    template <typename Arg, typename ConfT>
    String Fun_tp<Arg, ConfT>::to_string()&&
    {
        return to_string_impl(move(*this));
    }

    template <typename Arg, typename ConfT>
    template <typename F>
    String Fun_tp<Arg, ConfT>::to_string_impl(F&& fun)
    {
        using unsot::to_string;

        String str(FORWARD(fun).Inherit::to_string());
        if (fun.cvalues_ptr() && !std::empty(fun.cvalues())) {
            str += " <-- [ "s + to_string(FORWARD(fun).values()) + "]";
        }
        return str;
    }
}
