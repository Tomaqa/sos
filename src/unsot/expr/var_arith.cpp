#include "expr/var_arith.hpp"

#include "expr/var.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr {
    template class Var<Int>::Mixin;
    template class Var<Real>::Mixin;
    template class Var<Int, var::Full_conf>::Visit_mixin;
    template class Var<Real, var::Full_conf>::Visit_mixin;
    template class Var<Int, var::Full_conf>::Distance_mixin;
    template class Var<Real, var::Full_conf>::Distance_mixin;
}

namespace unsot::expr::var {
    template class aux::_Fbase<Int>::Fun_t::Mixin;
    template class aux::_Fbase<Real>::Fun_t::Mixin;
    template class aux::_Ffbase<Int>::Fun_t::Mixin;
    template class aux::_Ffbase<Real>::Fun_t::Mixin;
    template class aux::_Ffbase<Int>::Fun_visit_t::Visit_mixin;
    template class aux::_Ffbase<Real>::Fun_visit_t::Visit_mixin;
    template class aux::_Ffbase<Int>::Fun_distance_t::Distance_mixin;
    template class aux::_Ffbase<Real>::Fun_distance_t::Distance_mixin;
    template class aux::_Pbase<Int>::Fun_t::Mixin;
    template class aux::_Pbase<Real>::Fun_t::Mixin;
    template class aux::_Pbase<Int>::Pred_mixin;
    template class aux::_Pbase<Real>::Pred_mixin;
    template class aux::_Fpbase<Int>::Fun_t::Mixin;
    template class aux::_Fpbase<Real>::Fun_t::Mixin;
    template class aux::_Fpbase<Int>::Pred_mixin;
    template class aux::_Fpbase<Real>::Pred_mixin;
    template class aux::_Fpbase<Int>::Fun_visit_t::Visit_mixin;
    template class aux::_Fpbase<Real>::Fun_visit_t::Visit_mixin;
    template class aux::_Fpbase<Int>::Fun_distance_t::Distance_mixin;
    template class aux::_Fpbase<Real>::Fun_distance_t::Distance_mixin;

    template class aux::_Ass<Int>::Assignee_t::Mixin;
    template class aux::_Ass<Real>::Assignee_t::Mixin;
    template class aux::_Fass<Int>::Assignee_t::Mixin;
    template class aux::_Fass<Real>::Assignee_t::Mixin;
    template class aux::_Fass<Int>::Assignee_distance_t::Distance_mixin;
    template class aux::_Fass<Real>::Assignee_distance_t::Distance_mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
