#pragma once

#include "expr/var.hpp"

namespace unsot::expr::var {
    template <typename B, typename Sort>
    Mixin<B, Sort>::Mixin(Keys_ptr kptr, Values_ptr vptr,
                          Ids_map_ptr imap_ptr, Id id_, bool is_set_)
        : Inherit(move(kptr), move(imap_ptr), move(id_), is_set_),
          _values_ptr(move(vptr))
    { }

    template <typename B, typename Sort>
    Mixin<B, Sort>& Mixin<B, Sort>::operator =(Value val)
    {
        return set(move(val));
    }

    template <typename B, typename Sort>
    Mixin<B, Sort>& Mixin<B, Sort>::set(Value val) noexcept
    {
        if (this->unlocked()) set_value_impl(move(val));
        return *this;
    }

    template <typename B, typename Sort>
    void Mixin<B, Sort>::set_value_impl(Value val) noexcept
    {
        set_value_only_impl(move(val));
        this->set_impl();
    }

    template <typename B, typename Sort>
    void Mixin<B, Sort>::set_value_only_impl(Value val) noexcept
    {
        EVAR_CVERB3LN("set_value: " << val);
        value() = move(val);
    }

    template <typename B, typename Sort>
    typename Mixin<B, Sort>::Value Mixin<B, Sort>::get() const
    {
        this->check_is_set();
        return cvalue();
    }

    template <typename B, typename Sort>
    typename Mixin<B, Sort>::Value Mixin<B, Sort>::eval()
    {
        if (this->is_set()) return cvalue();
        return compute();
    }

    template <typename B, typename Sort>
    typename Mixin<B, Sort>::Value Mixin<B, Sort>::compute()
    {
        assert(this->computable());
        set_value_only_impl(compute_body());
        this->set_only_impl();
        assert(this->consistent());
        this->rcomputed() = true;
        return cvalue();
    }

    template <typename B, typename Sort>
    typename Mixin<B, Sort>::Value Mixin<B, Sort>::compute_body()
    {
        const bool success = compute_body_prepare();
        const Value val = compute_body_value(success);
        compute_body_finish(success);
        EVAR_CVERB3LN("compute_body -> " << val << " (" << (success ? "" : "!") << "success)");
        return val;
    }

    template <typename B, typename Sort>
    bool Mixin<B, Sort>::compute_body_prepare()
    {
        assert(this->computable());
        /// To avoid dependency cycles
        this->computable_flag() = false;
        return true;
    }

    template <typename B, typename Sort>
    typename Mixin<B, Sort>::Value
    Mixin<B, Sort>::compute_body_value(bool success)
    {
        if (!success) return Value(~0ULL);
        /// For pure `Var`, the value must be set to compute it
        return get();
    }

    template <typename B, typename Sort>
    void Mixin<B, Sort>::compute_body_finish(bool success)
    {
        if (success) this->computable_flag() = true;
        else this->consistent_flag() = false;
    }

    template <typename B, typename Sort>
    Optional<typename Mixin<B, Sort>::Value>
    Mixin<B, Sort>::try_get() const noexcept
    {
        if (this->is_unset()) return {};
        return cvalue();
    }

    template <typename B, typename Sort>
    Pair<Optional<typename Mixin<B, Sort>::Value>, bool>
    Mixin<B, Sort>::try_eval() noexcept
    {
        if (this->is_set()) return {cvalue(), false};
        auto opt = try_compute();
        const bool computed_ = opt.valid();
        return {move(opt), computed_};
    }

    template <typename B, typename Sort>
    Optional<typename Mixin<B, Sort>::Value>
    Mixin<B, Sort>::try_compute() noexcept(!_debug_)
    {
        if (!this->computable()) return {};
        return compute();
    }

    template <typename B, typename Sort>
    Flag Mixin<B, Sort>::consistent_body()
    {
        const Value set_val = cvalue();
        const Value comp_val = compute_body();
        const bool equal = (set_val == comp_val);
        if (equal) this->rcomputed() = true;
        return equal;
    }

    template <typename B, typename Sort>
    String Mixin<B, Sort>::value_to_string_body() const
    {
        using unsot::to_string;
        return to_string(cvalue());
    }

    template <typename B, typename Sort>
    bool Mixin<B, Sort>::lequals(const This& rhs) const noexcept
    {
        return cvalue() == rhs.cvalue();
    }

    template <typename B, typename Sort>
    bool Mixin<B, Sort>::equals(const Value& val) const noexcept
    {
        if (this->is_unset()) return false;
        return cvalue() == val;
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    template <typename Arg, typename FirstPred, typename VisitedPred>
    Flag Visit_mixin<B>::visit_id(const Id& id_, const Arg& arg,
                                  FirstPred first_pred, VisitedPred visited_pred) noexcept
    {
        if (auto [_, inserted] = visited_ids.insert(id_); inserted)
            return invoke(move(first_pred), arg);
        return invoke(move(visited_pred), arg);
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    void Distance_mixin<B>::reset_impl() noexcept
    {
        Inherit::reset_impl();
        distance().forget();
    }

    template <typename B>
    Distance Distance_mixin<B>::eval_distance() noexcept
    {
        if (!cdistance().valid()) {
            //+ does not distinguish 'unknown' ..
            distance() = this->independent() ? 0U : compute_distance();
        }

        return cdistance().value();
    }
}

#include "expr/var/fun.tpp"
#include "expr/var/assign.tpp"
