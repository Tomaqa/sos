#include "expr/formula.hpp"

#include "expr/alg.hpp"

#include "expr/list.tpp"

namespace unsot::expr {
    bool Formula::lvalid_pre_init() const noexcept
    {
        return has_valid_oper() && has_valid_args();
    }

    void Formula::lcheck_pre_init() const
    {
        check_has_valid_oper();
        check_has_valid_args();
    }

    void Formula::linit()
    {
        try { init_oper(); }
        catch (const Error& err) {
            THROW("Initialization of the function operation failed: ") + err;
        }
        try { init_args(); }
        catch (const Error& err) {
            THROW("Initialization of the function arguments failed: ") + err;
        }
    }

    bool Formula::has_valid_oper() const noexcept
    {
        return lhas_valid_oper();
    }

    void Formula::check_has_valid_oper() const
    {
        lcheck_has_valid_oper();
    }

    bool Formula::lhas_valid_oper() const noexcept
    {
        if (!std::empty(coper())) return true;
        return !empty() && is_key(cfront());
    }

    void Formula::lcheck_has_valid_oper() const
    {
        expect(lhas_valid_oper(), "Expected an operation key.");
    }

    void Formula::init_oper()
    {
        if (!std::empty(coper())) return;
        auto& ptr = front();
        maybe_deep_copy(ptr);
        oper() = cast_key(move(ptr));
        erase(begin());
    }

    bool Formula::has_valid_args() const noexcept
    {
        return lhas_valid_args();
    }

    void Formula::check_has_valid_args() const
    {
        lcheck_has_valid_args();
    }

    bool Formula::lhas_valid_args() const noexcept
    {
        if (!std::empty(coper())) return !empty();
        return size() > 1;
    }

    void Formula::lcheck_has_valid_args() const
    {
        expect(lhas_valid_args(), "Expected at least one argument.");
    }

    void Formula::init_args()
    {
        reserve(size());
        for (auto& ptr : *this) init_arg(ptr);
    }

    void Formula::init_arg(Ptr& ptr)
    {
        if (List::is_me(ptr)) return init_arg_list(ptr);
        if (is_key(ptr)) init_arg_key(ptr);
        else init_arg_value(ptr);
    }

    void Formula::init_arg_list(Ptr& ptr)
    {
        ptr = init_arg_list_new(move(ptr));
        assert(is_me(ptr));
    }

    Ptr Formula::init_arg_list_new(Ptr&& ptr)
    {
        /*! there is some hidden issue, it crashes when value is used instead of rval-reference
        (the move constructor call is victim?) */
        if (is_me(ptr)) {
            ptr->maybe_virtual_init();
            return move(ptr);
        }
        maybe_deep_copy(ptr);
        return new_me(List::cast(move(ptr)));
    }

    /// `if constexpr` works only in templated functions
    void Formula::reserve(size_t size_)
    {
        reserve_impl(*this, size_);
    }

    void Formula::resize(size_t size_)
    {
        resize_impl(*this, size_);
    }

    template <typename F>
    void Formula::reserve_impl(F& phi, size_t size_)
    {
        if constexpr (enabled_reserve_v<Inherit>) phi.Inherit::reserve(size_);
    }

    template <typename F>
    void Formula::resize_impl(F& phi, size_t size_)
    {
        if constexpr (enabled_resize_v<Inherit>) phi.Inherit::resize(size_);
    }

    String Formula::to_string() const&
    {
        return make_to_string(*this);
    }

    String Formula::to_string()&&
    {
        return make_to_string(move(*this));
    }

    bool Formula::lequals(const This& rhs) const noexcept
    {
        return coper() == rhs.coper();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template class List::To_string_crtp<Formula::To_string<const Formula>,
                                        const Formula>;
    template class
        List::To_string_crtp<Formula::To_string<Formula>, Formula>;
}
