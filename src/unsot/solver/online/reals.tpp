#pragma once

#include "solver/online/reals.hpp"

namespace unsot::solver::online::reals {}

#include "solver/online/reals/check_sat.tpp"
