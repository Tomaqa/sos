#pragma once

#include "solver/offline/reals/equal.hpp"

namespace unsot::solver::offline::reals::equal {}

#include "solver/offline/reals/equal/check_sat.tpp"
