#include "solver/offline/reals/equal/minisat/euler.hpp"

#include "solver/run.hpp"
#include "smt/solver/offline/bools/run.hpp"

#include "smt/solver.tpp"
#include "smt/solver/bools.tpp"
#include "smt/solver/reals.tpp"

#include "smt/solver/run.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot {
    template class aux::_S::Base::Crtp;
    template class aux::_S::Bools_t::Mixin;
    template class aux::_S::Reals_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
