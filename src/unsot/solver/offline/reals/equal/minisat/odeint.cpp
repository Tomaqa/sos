#include "solver/offline/reals/equal/minisat/odeint.hpp"

#include "smt/solver/offline/bools/run.hpp"

#include "solver.tpp"
#include "solver/offline/reals/equal.tpp"

#include "solver/run.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot {
    template class aux::_S::Unsot_t::Mixin;
    template class aux::_S::Unsot_offline_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
