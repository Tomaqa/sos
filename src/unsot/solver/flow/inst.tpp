#pragma once

#include "solver/flow/inst.hpp"

#include "util/alg.hpp"
#include "util/flag/alg.hpp"
#include "solver/flow/alg.hpp"

namespace unsot::solver::flow {
    using namespace expr;

    template <typename S>
    Inst<S>::Inst(Solver* solver_l_, flow::Id fid, Inst_id id_, const expr::Keys& init_vals,
                  const expr::Keys& real_arg_vals, const expr::Keys& bool_arg_vals)
        : _solver_l(solver_l_), _flow_id(move(fid)), _id(move(id_)),
          _init_values(init_vals), _real_arg_values(real_arg_vals),
          _bool_arg_values(bool_arg_vals)
    {
        auto& bools = solver().bools();
        current_odes_view().connect(bools);
        current_invariants_view().connect(bools);
    }

    template <typename S>
    void Inst<S>::notice_set_variant(const var::Id&) noexcept
    {
        //+ add it into the config right away
        ++n_set_variants();
    }

    template <typename S>
    void Inst<S>::notice_unset_variant(const var::Id&) noexcept
    {
        --n_set_variants();
    }

    template <typename S>
    bool Inst<S>::all_variants_set() const noexcept
    {
        assert(size_t(cn_set_variants()) <= max_set_variants());
        return size_t(cn_set_variants()) == max_set_variants();
    }

    template <typename S>
    Flag Inst<S>::surely_evaluable() const noexcept
    {
        if (!all_variants_set()) return false;
        if (ccomputed()) return true;
        return unknown;
    }

    template <typename S>
    bool Inst<S>::eval(const var::Id& trigger_ode_id_)
    {
        if (!ccomputed()) {
            auto& ch = solver().check_sat_ref();
            trigger_ode_id() = trigger_ode_id_;
            ch.pre_compute_of(*this);
            value() = compute();
            computed() = true;
            ch.post_compute_of(*this);
        }
        return cvalue();
    }

    template <typename S>
    bool Inst<S>::compute()
    {
        using Ode = typename Solver::Ode;

        auto state = make_init_state();
        const auto config = make_config();

        SMT_CVERB3LN("flow::Inst::compute:"
            << "\nkey: " << cdef().ckey()
            << "\nkeys: " << cflow().ckeys()
            << "\ninit state: " << state
            << "\nconfig: " << config
            << "\n"
        );

        auto& trigger = trigger_ode();
        auto& trigger_id = trigger.cid();
        auto& trigger_final_var = trigger.final_var();
        auto& tode_ptr = t_ode_ptr();
        auto& odes_vw = current_odes_view();
        auto& invs_vw = current_invariants_view();
        /// Add variants as dep. ids - any could affect the result
        auto dep_f = [this, &trigger_final_var](auto& vid){
            trigger_final_var.add_depend_id(vid);
        };
        dep_f(tode_ptr->cid());
        for (auto& vid : invs_vw.caccessors()) dep_f(vid);
        //+ it would be better if variants would depend on each other
        //+ (or better only on those that they really depend on)
        //+ and the variables only on their own variants
        for (auto& vid : odes_vw.caccessors()) dep_f(vid);

        auto& s = solver();
        auto& osolver = s.code_solver();
        auto& flow_id_ = cflow_id();
        if (s.required_flow_progress()) {
            if (osolver.is_almost_end(flow_id_, state, config)) {
                return false;
            }
        }

        osolver.solve(flow_id_, state, config);
        osolver.ctraject(flow_id_).write_block(cid());

        SMT_CVERB3LN("final state: " << state << "\n");

        static const auto compute_finish_f = [](auto& vptr){
            vptr->set_computed();
            vptr->computable_flag() = true;
        };

        auto ass_f = [&trigger_id](auto& vptr, auto&& val){
            auto& ode = Ode::cast(vptr);
            auto& fvar = ode.final_var();
            assert(fvar.maybe_assignable());
            const bool is_trigger = (ode.cid() == trigger_id);

            if (!is_trigger) ode.prepare_to_assign(fvar);
            ode.assign(move(val), fvar);
            if (!is_trigger) compute_finish_f(vptr);
        };
        ass_f(tode_ptr, state.ct());
        assert(state.ode_size() == odes_vw.size());
        for_each(odes_vw, move(state).ode_begin(), move(ass_f));

        for (auto& vptr : invs_vw) compute_finish_f(vptr);

        return true;
    }

    template <typename S>
    State Inst<S>::make_init_state()
    {
        auto eval_f = [this](const auto& key){
            return solver().reals().var(key).eval();
        };

        auto& init_vals = cinit_values();
        const size_t ode_size = cflow().ckeys().ode_size();
        Real t = eval_f(init_vals[0]);
        auto ode_reals = to<expr::Reals>(++cbegin(init_vals), cend(init_vals),
                                         ode_size, eval_f);
        auto arg_reals = to<expr::Reals>(cflow_arg_values(), move(eval_f));

        return {t, move(ode_reals), move(arg_reals)};
    }

    template <typename S>
    Config Inst<S>::make_config()
    {
        const size_t ode_size = cflow().ckeys().ode_size();
        Ids odes_ids;
        Ids invs_ids;
        odes_ids.reserve(ode_size);
        invs_ids.reserve(ode_size+3);
        auto& oview = current_odes_view();
        auto& iview = current_invariants_view();
        oview.clear();
        iview.clear();
        oview.reserve(odes_ids.capacity());
        iview.reserve(invs_ids.capacity());
        assert(all_variants_set());
        auto vrt_f = [this](const auto& variant, auto& ids, auto& view){
            auto& inst_vkey = cinst_variant_key_of(variant.key);
            auto& var = csolver().cbools().cvar(inst_vkey);
            if (!var.get()) return false;
            ids.push_back(variant.id);
            view.push_back(var.cid());
            return true;
        };
        auto f = [this, &vrt_f](auto& ids, auto& view){
            return [this, &vrt_f, &ids, &view]
                   (const auto& variants, auto& /*okey*/){
                if (none_of(variants, bind(vrt_f, _1, ref(ids), ref(view))))
                    throw dummy;
            };
        };
        auto ode_def_f = [](auto& /*okey*/){ assert(false); };
        auto inv_def_f = [](auto& ids){
            return [&ids](auto& /*okey*/){ ids.push_back(def_id); };
        };
        auto& fdef = cdef();
        auto& keys = cflow().ckeys();
        for_each_ode(fdef, keys, f(odes_ids, oview), move(ode_def_f));
        for_each_invariant(fdef, keys, f(invs_ids, iview),
                           inv_def_f(invs_ids));
        assert(size(odes_ids) == size(cfinal_keys())-1);
        assert(oview.size() == size(odes_ids));

        return {move(odes_ids), move(invs_ids)};
    }

    template <typename S>
    void Inst<S>::reset()
    {
        if (!ccomputed()) return;
        computed() = false;
        solver().check_sat_ref().reset_of(*this);
        trigger_ode_id() = var::invalid_id;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::solver::flow {
    template <typename S>
    Inst<S>::Instantiate::Instantiate(Solver& solver_, const flow::Id& fid,
                                      const expr::Keys& init_vals,
                                      const expr::Keys& real_arg_vals,
                                      const expr::Keys& bool_arg_vals)
        : _solver(solver_), _def(csolver().cflow_def(fid))
    {
        set_inst(init_vals, real_arg_vals, bool_arg_vals);
        set_final_keys();
        instantiate_variants();
        set_inst_arg_values();
    }

    template <typename S>
    typename Inst<S>::Instantiate& Inst<S>::Instantiate::perform()
    {
        auto& finst = inst();
        SMT_CVERB2LN("flow::Inst::Instantiate:"
            << "\nkey: " << finst.cdef().ckey()
            << "\nkeys: " << finst.cflow().ckeys()
            << "\nflow_arg_values: " << finst.cflow_arg_values()
            << "\ninit_values: " << finst.cinit_values()
            << "\nreal_arg_values: " << finst.creal_arg_values()
            << "\nbool_arg_values: " << finst.cbool_arg_values()
            << "\nfinal_keys: " << finst.cfinal_keys()
            << "\n"
        );

        return *this;
    }

    template <typename S>
    Formula Inst<S>::Instantiate::expand()
    {
        return solver().expand_def_fun(cdef().ckey(), _defun_arg_values);
    }

    template <typename S>
    Key Inst<S>::Instantiate::to_inst_key(Key key_) noexcept
    {
        return Solver::aux_var_key_prefix + solver().new_var_key(move(key_));
    }

    template <typename S>
    Inst<S> Inst<S>::Instantiate::make_inst(const expr::Keys& init_vals,
                                            const expr::Keys& real_arg_vals,
                                            const expr::Keys& bool_arg_vals)
    {
        auto& fdef = cdef();
        auto& flow = fdef.cflow();
        auto& keys = flow.ckeys();
        expect(size(init_vals) == keys.t_ode_size(),
               "Size of input values does not correspond to defined flow: "s
               + to_string(init_vals) + " !~ "
               + to_string(keys.ct_ode_keys_slice()));
        expect(size(real_arg_vals) == size(fdef.creal_arg_keys()),
               "Size of real arg. values does not correspond "s
               + "to defined flow: " + to_string(real_arg_vals) + " !~ "
               + to_string(fdef.creal_arg_keys()));
        expect(size(bool_arg_vals) == size(fdef.cbool_arg_keys()),
               "Size of bool arg. values does not correspond "s
               + "to defined flow: " + to_string(bool_arg_vals) + " !~ "
               + to_string(fdef.cbool_arg_keys()));

        auto check_f = [this](auto check_cont_f){
            return [this, CAPTURE_RVAL(check_cont_f)](const auto& val){
                (csolver().*check_cont_f)(val);
        };};
        for_each(init_vals, check_f(&Solver::check_contains_real));
        for_each(real_arg_vals, check_f(&Solver::check_contains_real));
        for_each(bool_arg_vals, check_f(&Solver::check_contains_bool));

        Inst_id fid = size(csolver().cflow_insts());
        return {&solver(), fdef.cid(), fid, init_vals, real_arg_vals, bool_arg_vals};
    }

    template <typename S>
    void Inst<S>::Instantiate::set_inst(const expr::Keys& init_vals,
                                        const expr::Keys& real_arg_vals,
                                        const expr::Keys& bool_arg_vals)
    {
        auto finst = make_inst(init_vals, real_arg_vals, bool_arg_vals);
        id() = finst.cid();
        solver().flow_insts().push_back(move(finst));
    }

    template <typename S>
    void Inst<S>::Instantiate::set_final_keys()
    {
        auto& fdef = cdef();
        auto& finst = inst();
        to(finst.final_keys(), fdef.cfinal_keys(), [this, &finst](auto& fkey){
            Key inst_key = to_inst_key(fkey);
            solver().declare_final_var(inst_key);
            auto& vptr = csolver().creals().cback();
            finst.final_var_ids().insert(vptr->cid());
            return inst_key;
        });

        auto add_ids_f = [this](const auto& vals, auto& ids){
            for (auto& val : vals) {
                //+ it should support not only keys
                auto& vptr = csolver().creals().cptr(val);
                ids.insert(vptr->cid());
            }
        };

        auto vrt_arg_vals = make_masked_arg_keys_keep_init(
            finst.cinit_values(), finst.creal_arg_values(),
            fdef.carg_keys_mask()
        );
        add_ids_f(vrt_arg_vals, finst.variant_arg_ids());
        finst.variant_arg_values() = vrt_arg_vals;

        auto vrt_dep_vals = cat(finst.cfinal_keys(), move(vrt_arg_vals));
        add_ids_f(vrt_dep_vals, finst.variant_depend_ids());
        finst.variant_depend_values() = move(vrt_dep_vals);
    }

    template <typename S>
    void Inst<S>::Instantiate::instantiate_variants()
    {
        auto variant_f = [this](auto inst_f, auto& var_ids,
                                auto& okey, auto& def_vkey){
            Key inst_vkey = invoke(move(inst_f), okey, def_vkey);
            var_ids.insert(csolver().cbools().cptr(inst_vkey)->cid());
            inst().inst_variant_key_of(def_vkey) = move(inst_vkey);
        };
        auto f = [this, &variant_f](auto inst_f, auto& var_ids){
            return [this, &variant_f, CAPTURE_RVAL(inst_f), &var_ids]
                   (const auto& variants, auto& okey) mutable {
                for (auto& variant : variants) {
                    invoke(variant_f, move(inst_f), var_ids, okey, variant.key);
                }
            };
        };
        auto ode_inst_f = [this](auto& okey, auto& def_vkey){
            return solver().instantiate_def_ode(def_vkey, okey, cinst());
        };
        auto inv_inst_f = [this](auto& okey, auto& def_vkey){
            Key inst_vkey = to_inst_key(def_vkey);
            solver().declare_invariant(inst_vkey, okey, cinst());
            return inst_vkey;
        };
        auto t_inst_f = [this, &ode_inst_f](auto& okey, auto& def_vkey){
            auto inst_vkey = ode_inst_f(okey, def_vkey);
            solver().assert_elem(inst_vkey);
            return inst_vkey;
        };

        auto& fdef = cdef();
        auto& keys = fdef.cflow().ckeys();
        auto& finst = inst();
        auto& ode_var_ids = finst.ode_var_ids();
        assert(empty(ode_var_ids));
        variant_f(move(t_inst_f), ode_var_ids, t_key, fdef.ct_ode_key());
        assert(size(ode_var_ids) == 1);
        finst.t_ode_id() = *cbegin(ode_var_ids);
        for_each_ode(fdef, keys, f(move(ode_inst_f), ode_var_ids));
        for_each_invariant(fdef, keys, f(move(inv_inst_f), finst.invariant_var_ids()));
    }

    template <typename S>
    void Inst<S>::Instantiate::set_inst_arg_values()
    {
        auto& fdef = cdef();
        auto& finst = inst();
        auto& defun = csolver().cfun_def(fdef.ckey());
        auto& defun_arg_keys = defun.carg_keys();

        auto& init_vals = finst.cinit_values();
        _defun_arg_values.reserve(size(defun_arg_keys));
        copy(init_vals, back_inserter(_defun_arg_values));

        auto& real_arg_vals = finst.creal_arg_values();
        copy(real_arg_vals, back_inserter(_defun_arg_values));
        copy(finst.cbool_arg_values(), back_inserter(_defun_arg_values));

        copy(finst.cfinal_keys(), back_inserter(_defun_arg_values));

        finst.flow_arg_values() = make_masked_arg_keys(
            init_vals, real_arg_vals, fdef.carg_keys_mask()
        );

        for_each_variant(fdef, fdef.cflow().ckeys(), [this, &finst]
                         (const auto& variants, auto& /*okey*/){
            to(_defun_arg_values, variants, [&finst](auto& vrt){
                return finst.cinst_variant_key_of(vrt.key);
            });
        });
    }
}
