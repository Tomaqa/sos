#pragma once

#include "solver/flow/def.hpp"

#include "util/alg.hpp"
#include "expr/bools.hpp"
#include "expr/alg.hpp"
#include "solver/flow/alg.hpp"

namespace unsot::solver::flow {
    using namespace expr;

    template <typename S>
    Def::Define<S>::Define(Solver& solver_, Key dkey, Formula phi, expr::Keys ode_keys_,
                           expr::Keys real_arg_keys_, expr::Keys bool_arg_keys_)
        : _solver(solver_),
          _def_ptr(new_def({move(dkey), move(real_arg_keys_), move(bool_arg_keys_)})),
          _keys(move(ode_keys_))
    {
        set_init_and_final_keys();
        set_fun();
        set_flow(move(phi));
    }

    template <typename S>
    Def::Define<S>& Def::Define<S>::perform()
    {
        Id fid = solver().add_flow_def(move(def()), move(_flow));
        auto& fdef = solver().flow_def(fid);
        fdef.id() = fid;
        fdef.flow_ptr() = csolver().code_solver().cflow_ptr(fid);
        fdef.flow_parse() = move(_flow_parse);

        auto& dkey = fdef.ckey();
        solver().define_real_pred(dkey, move(_fun_formula), move(_fun_arg_keys));

        define_variants(fdef);

        SMT_CVERB1LN("flow::Def::Define:"
            << "\nkey: " << dkey
            << "\nflow: " << fdef.cflow()
            << "\ninit_keys: " << fdef.cinit_keys()
            << "\nreal_arg_keys: " << fdef.creal_arg_keys()
            << "\nbool_arg_keys: " << fdef.cbool_arg_keys()
            << "\nfinal_keys: " << fdef.cfinal_keys()
            << "\n"
        );

        return *this;
    }

    template <typename S>
    void Def::Define<S>::set_init_and_final_keys()
    {
        auto t_ode_keys = _keys.ct_ode_keys_slice();
        auto& fdef = def();
        to(fdef.init_keys(), t_ode_keys, to_init_key);
        to(fdef.final_keys(), move(t_ode_keys), to_final_key);
    }

    template <typename S>
    void Def::Define<S>::set_fun()
    {
        set_fun_arg_keys();
    }

    template <typename S>
    void Def::Define<S>::set_fun_arg_keys()
    {
        /// Reserve also for ODE and invariant variants
        auto& fdef = cdef();
        _fun_arg_keys.reserve(
            size(fdef.cinit_keys())
            + size(fdef.creal_arg_keys()) + size(fdef.cbool_arg_keys())
            + size(fdef.cfinal_keys())
            + _keys.t_ode_size()*2
        );
        copy(fdef.cinit_keys(), back_inserter(_fun_arg_keys));
        copy(fdef.creal_arg_keys(), back_inserter(_fun_arg_keys));
        copy(fdef.cbool_arg_keys(), back_inserter(_fun_arg_keys));
        copy(fdef.cfinal_keys(), back_inserter(_fun_arg_keys));
    }

    template <typename S>
    void Def::Define<S>::set_flow(Formula phi)
    {
        _keys.set_arg_keys(make_arg_keys(cdef()));
        Parse_formula(*this, move(phi)).perform();

        /// `_parse_list' is needed here to reduce `arg_keys'
        set_keys();

        _flow_parse = {move(_keys), move(_parse_list)};
        _flow = as_const(_flow_parse).perform();
    }

    template <typename S>
    void Def::Define<S>::set_keys()
    {
        set_arg_keys_mask();
        auto& fdef = def();
        _keys.set_arg_keys(make_masked_arg_keys(_keys.arg_keys_slice(), fdef.carg_keys_mask()));
        //+ possibly add only args that it really depends on accord. to form.
        fdef.variant_arg_keys() = make_masked_arg_keys_keep_init(fdef);
        fdef.variant_depend_keys() = cat(fdef.cfinal_keys(), fdef.cvariant_arg_keys());
    }

    template <typename S>
    void Def::Define<S>::set_arg_keys_mask()
    {
        auto& fdef = def();
        auto& mask = fdef.arg_keys_mask();
        mask.reserve(size(fdef.cinit_keys()) + size(fdef.creal_arg_keys()));
        auto f = [this](const auto& akey){
            return none_of(_parse_list, as_clists([&akey](auto& ls){ try {
                for_each_if_key_rec(ls, as_ckeys([&akey](auto& k){
                    if (k == akey) throw ignore;
                }));
                return false;
            }
            catch (Ignore) {
                return true;
            }}));
        };
        transform(fdef.cinit_keys(), back_inserter(mask), f);
        transform(fdef.creal_arg_keys(), back_inserter(mask), move(f));
    }

    template <typename S>
    void Def::Define<S>::define_variants(Def& solver_fdef)
    {
        auto& fid = solver_fdef.cid();
        auto& flow_keys = solver_fdef.cflow().ckeys();
        for_each_ode(solver_fdef, flow_keys,
                     [this, &fid](const auto& variants, auto& okey){
            for (auto& variant : variants) {
                solver().define_ode(okey, variant.key, fid);
            }
        });

        solver_fdef.t_ode_key() = to_ode_variant_key(t_key);
        solver().define_ode(t_key, solver_fdef.ct_ode_key(), fid);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::solver::flow {
    template <typename S>
    Def::Define<S>::Parse_formula::Parse_formula(Define& def, Formula phi)
        : _define(def), _eptr(move(phi).to_ptr()),
          _flow_parse(cdefine()._keys)
    {
        eptr()->maybe_virtual_init();
    }

    template <typename S>
    typename Def::Define<S>::Parse_formula&
    Def::Define<S>::Parse_formula::perform()
    {
        preprocess();
        parse();
        add_to_fun_formula();
        return *this;
    }

    template <typename S>
    bool Def::Define<S>::Parse_formula::valid_init_key(const Key& key_) const noexcept
    {
        return is_init_key(key_)
            && cflow_parse().valid_ode_key(init_to_ode_key(key_));
    }

    template <typename S>
    bool Def::Define<S>::Parse_formula::valid_final_key(const Key& key_) const noexcept
    {
        return is_final_key(key_)
            && cflow_parse().valid_ode_key(final_to_ode_key(key_));
    }

    template <typename S>
    void Def::Define<S>::Parse_formula::parse()
    {
        parse_subformula(eptr());
        if (!Formula::is_me(eptr())) {
            assert(!List::is_me(eptr()));
            return;
        }

        for_each_if_list_rec_pre(Formula::cast(eptr()), [this](auto& subeptr){
            parse_subformula(subeptr);
        });
    }

    template <typename S>
    void Def::Define<S>::Parse_formula::parse_subformula(expr::Ptr& eptr_)
    {
        Formula& phi = Formula::cast(eptr_);
        /// `valid_ode_head' is perfectly sufficient here
        const bool is_ode = cflow_parse().valid_ode_head(phi);
        /// `valid_inv_head' is insufficient here
        /// as it would accept too many expressions
        if (!is_ode && !cflow_parse().valid_inv(phi)) return;

        Key key_ = is_ode ? cflow_parse().get_ode_key(phi)
                          : cflow_parse().get_inv_key(phi);
        define()._parse_list.push_back(move(phi).to_ptr());

        auto& fdef = define().def();
        auto& variants = is_ode ? fdef.odes(key_) : fdef.invariants(key_);
        auto vrt_key = is_ode ? to_ode_variant_key(move(key_))
                              : to_inv_variant_key(move(key_));
        Id vrt_id = empty(variants) ? first_id : Id(variants.back().id+1);
        variants.push_back({vrt_key, move(vrt_id)});

        eptr_ = new_key(move(vrt_key));
    }

    template <typename S>
    void Def::Define<S>::Parse_formula::add_to_fun_formula()
    {
        auto& arg_keys = define()._fun_arg_keys;
        auto& phi = define()._fun_formula;
        phi.push_back(move(eptr()));

        auto f = [&arg_keys](const auto& variants, auto& /*okey*/){
            to(arg_keys, variants, [](auto& vrt){ return vrt.key; });
        };
        auto& fdef = define().def();
        auto& keys = cdefine()._keys;
        for_each_variant(fdef, keys, move(f));

        auto ode_f = [&phi](const auto& variants, auto& /*okey*/){
            auto clause = to<expr::bools::Clause>(variants, [](auto& vrt){ return vrt.key; });
            auto conflict_phi = to_pair_conflict(move(clause)).to_formula();
            if (conflict_phi.empty()) return;
            phi.push_back(move(conflict_phi).to_ptr());
        };
        for_each_ode(fdef, keys, ode_f);
        //+ in future, invariants should not be always exclusive
        for_each_invariant(fdef, keys, move(ode_f));
    }
}
