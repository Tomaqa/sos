#pragma once

#include "solver/parser.hpp"

#include "util/scope_guard.hpp"

namespace unsot::solver {
    using namespace expr;

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Parser::
        post_preprocess_cmd(expr::Ptr& eptr, Key& cmd, Pos& pos)
    {
        Inherit::post_preprocess_cmd(eptr, cmd, pos);
        if (cmd == "init") {
            return post_preprocess_cmd_init(eptr, cmd, pos);
        }
        if (cmd == "final") {
            return post_preprocess_cmd_final(eptr, cmd, pos);
        }
        if (cmd == "diff") {
            return post_preprocess_cmd_diff(eptr, cmd, pos);
        }
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Parser::
        post_preprocess_cmd_init(expr::Ptr& eptr, Key& cmd, Pos& pos)
    {
        post_preprocess_flow_key_cmd(eptr, cmd, pos, flow::to_init_key);
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Parser::
        post_preprocess_cmd_final(expr::Ptr& eptr, Key& cmd, Pos& pos)
    {
        post_preprocess_flow_key_cmd(eptr, cmd, pos, flow::to_final_key);
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Parser::
        post_preprocess_cmd_diff(expr::Ptr& eptr, Key& cmd, Pos& pos)
    {
        post_preprocess_flow_key_cmd(eptr, cmd, pos, flow::to_derived_key);
    }

    template <typename B, typename OdeSolver>
    template <typename ConvF>
    void Mixin<B, OdeSolver>::Parser::
        post_preprocess_flow_key_cmd(expr::Ptr& eptr, Key& cmd, Pos& pos, ConvF conv)
    {
        auto& ls = pos.list();
        expect(ls.size() == 2 && is_flat(ls),
               "Additional arguments to '"s + to_string(cmd) + "' function: " + pos.to_string());
        const Key& fkey = get_key(pos);
        eptr = new_key(conv(fkey));
        throw ignore;
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Parser::
        parse_expand_cmd(expr::Ptr& eptr, Key& cmd, Pos& pos)
    {
        Inherit::parse_expand_cmd(eptr, cmd, pos);
        if (cmd == "flow") {
            return parse_expand_cmd_flow(eptr, cmd, pos);
        }
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Parser::
        parse_expand_cmd_flow(expr::Ptr& eptr, Key& /*cmd*/, Pos& pos)
    {
        auto& ls = pos.list();
        expect(ls.size() == 4 && is_key_at(pos)
               && is_flat_list_at(pos.next()) && peek_list(pos).size() >= 2
               && is_flat_list_at(pos.next()),
               "Invalid format: expected (flow <flow_key> (<init_val>++) (<arg_val>*))"s
               + ", got: " + pos.to_string());
        pos.set_it().next();

        const Key& fkey = get_key(pos);
        auto init_vals = cast_to_keys_check(get_list(pos));
        auto arg_vals = cast_to_keys_check(get_list(pos));

        auto& arg_sorts_mask = cflow_def_arg_sorts_mask(fkey);
        expect(size(arg_vals) == size(arg_sorts_mask),
               "Expected "s + to_string(size(arg_sorts_mask)) + " arg. values"
               + ", got " + to_string(size(arg_vals)) + " ( " + to_string(arg_vals) + ")");

        Keys real_arg_vals;
        Keys bool_arg_vals;
        real_arg_vals.reserve(size(arg_vals)/2);
        bool_arg_vals.reserve(size(arg_vals)/2);
        for_each(move(arg_vals), cbegin(arg_sorts_mask),
                 [&real_arg_vals, &bool_arg_vals](auto&& aval, bool is_bool){
            auto& vals = is_bool ? bool_arg_vals : real_arg_vals;
            vals.push_back(move(aval));
        });

        eptr =
            this->solver().expand_def_flow(fkey, init_vals, real_arg_vals, bool_arg_vals).to_ptr();
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Parser::parse_cmd(const Key& cmd, Pos& pos)
    try {
        Inherit::parse_cmd(cmd, pos);
    }
    catch (Dummy) {
        if (cmd == "define-flow") {
            return parse_cmd_define_flow(move(pos));
        }

        throw;
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Parser::parse_cmd_define_flow(Pos&& pos)
    {
        auto& ls = pos.list();
        expect(ls.size() == 5 && is_key_at(pos)
               && is_flat_list_at(pos.next()) && !peek_list(pos).empty()
               && is_deep_list_at(pos.next()) && is_list_at(pos.next()),
               "Invalid format: expected "s
               + "(define-flow <flow_key> (<ode_id>+) ((<arg> <arg_sort>)*) <expr>)"
               + ", got: " + pos.to_string());
        pos.set_it().next();

        const Key& fkey = get_key(pos);
        auto ode_keys = cast_to_keys_check(get_list(pos));
        auto arg_pairs = cast_to_lists(get_list(pos));
        auto phi = Formula::cons(peek_list(pos));

        check_arg_pairs(arg_pairs);
        auto& arg_sorts_mask = flow_def_arg_sorts_mask(fkey);
        Keys real_arg_keys;
        Keys bool_arg_keys;
        real_arg_keys.reserve(size(arg_pairs)/2);
        bool_arg_keys.reserve(size(arg_pairs)/2);
        arg_sorts_mask.reserve(size(arg_pairs));
        for (auto&& l : move(arg_pairs)) {
            Pos p(l);
            const auto& akey = get_key_check(p);
            auto& sort = peek_key_check(p);
            if (sort == bools::sort_key) {
                bool_arg_keys.push_back(akey);
                arg_sorts_mask.push_back(true);
                continue;
            }
            if (sort == reals::sort_key) {
                real_arg_keys.push_back(akey);
                arg_sorts_mask.push_back(false);
                continue;
            }
            this->csolver().check_sort(sort);
            THROW("Unknown sort in 'define-flow': "s + sort + " (" + akey + ")");
        }

        this->solver().define_flow(fkey, move(phi), move(ode_keys),
                                   move(real_arg_keys), move(bool_arg_keys));
    }
}
