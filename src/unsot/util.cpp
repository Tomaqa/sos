#include "util.hpp"

namespace unsot::util {
    void mask(Mask& mask_, Idx idx)
    {
        mask_[idx] = true;
    }

    void unmask(Mask& mask_, Idx idx)
    {
        mask_[idx] = false;
    }
}
