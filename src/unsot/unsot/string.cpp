#include "unsot.hpp"
#include "unsot/string.hpp"

#include <istream>
#include <iomanip>

namespace unsot {
    //+ String to_string(float arg)
    //+ {
    //+     ostringstream oss;
    //+     oss << std::setprecision(float_precision) << std::fixed << arg;
    //+     return oss.str();
    //+ }

    //+ String to_string(double arg)
    //+ {
    //+     ostringstream oss;
    //+     oss << std::setprecision(double_precision) << std::fixed << arg;
    //+     return oss.str();
    //+ }

    String to_string(char arg)
    {
        return String(1, arg);
    }

    //++ This is clumsy and not properly tested
    String to_string(istream& rhs)
    {
        String str;
        int size_ = istream_remain_size(rhs);
        if (size_ <= 0) size_ = 32;
        str.reserve(size_);

        str.assign(std::istreambuf_iterator<char>(rhs),
                   std::istreambuf_iterator<char>());
        return str;
    }

    String to_string(const Ignore&)
    {
        return "<ignore>";
    }

    int istream_remain_size(istream& is)
    {
        streampos pos = is.tellg();
        if (pos < 0) return -1;
        is.seekg(0, std::ios::end);
        auto size_ = is.tellg() - pos;
        is.seekg(pos);
        return size_;
    }
}
