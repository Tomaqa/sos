\documentclass[a4paper,11pt]{article}

\usepackage[utf8]{inputenc} % LaTeX source encoded as UTF-8

\usepackage[margin=1in]{geometry}

\usepackage{lang}

% \Finaltrue
% \Blindtrue

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\author{\open{Kolárik Tomáš}}
\title{\open{UN/SOT }Preprocessing Language}
\date{\open{\today}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\open{%
\begin{abstract}
This paper fully specifies preprocessing language
that was introduced along with UN/SOT solver.
It is designed to be usable with SMT-LIB languages.
\end{abstract}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}\label{s:intro}
Many SMT formulas have certain structure with similar parts and code repetition,
and they depend on a~number of parameters.
For example, formulas that are unrolled in a~similar way
as in bounded model checking.
A~way is to generate such formulas ad-hoc with an~external tool,
for example, a~scripting language, like Python.

We provide a~preprocessing language,
that provides such functionality, but is a~natural part of an~SMT-LIB formula.
It is inspired by C preprocessor and partly by Lisp language,
but it differs in ways.
For example, the macros defined by a~user must be expanded explicitly in our case,
unlike in C.
So, it is not possible to change meaning of parts of a~formula
that are not syntactically related to the preprocessing language.
Also, our language is much more powerful and expressive
than the C preprocessor,
since we support loops, recursions and more.
However, the key purpose remains the same---to~provide
a~way how to generate a~code before it gets semantically analyzed by a~main procedure
(i.e., a~C compiler, or in our case, an~SMT solver).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Specification}\label{s:spec}
The preprocessing language extends the \open{UN/SOT }core input language,
which is very similar to languages that are conform with the SMT-LIB standard.
The preprocessing language is actually also appliable
for more general fully parenthesized prefix notations.

The principle is that the preprocessor substitutes (expands) parts of code
that are related to a~preprocessor command.
That is, it leaves the parts of formula that are not related to such a~command unchanged.
A~preprocessor command is a~token that begins with a~reserved character,
which is either \id{\#} or \id{\$}.
Commands that begin with \id{\#} are \emph{macros} (of several kinds),
and commands that begin with \id{\$} are \emph{arithmetic expansions}.

To~avoid treating a~reserved character as a~preprocessor command,
it has to be escaped (like \id{\textbackslash\#} or \id{\textbackslash\$}).
The same applies for backslashes themselves.
Escaping has to be done in the case of SMT-LIB formulas
that naturally contain the reserved characters
(e.g. hexadecimal literals).

\paragraph{Notation.}
Angle characters \id{<>}~are not a~part of the syntax,
they stand for a~placeholder of an~argument,
or they border or group arguments.
When brackets of a~list~\id{()} are used,
it means that the list is required without the need of an~expansion.
Square brackets~\id{[]} enclose an~optional part of the syntax.
Postfix~\id{*}, that is, after an~argument, denotes that
it can appear arbitrarily many times including zero times,
separated by a~white character;
and similarly, \id{+}~is like~\id{*}, but requires at~least one occurance%
% ($n$ \id{+}s require at~least $n$ occurances)
.
If arguments that are enclosed within \id{<>}~angle brackets
are separated by~\id{|}, it means that the arguments
are multiple possibilities in the place;
the~same applies for~\id{-}, which stands for~ranges.

\Ext{%
We denote parts that are \emph{extensions} of the preprocessing language
(and should be ignored by beginners) like this.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Syntax}\label{ss:spec:syntax}
The preprocessing language is, similarly to SMT-LIB, case sensitive,
and distinguishes tokens and lists,
white characters, and comments.

\paragraph{Token.}
Recall that a~token
is a~string of non-white characters.
A~token is either an~\emph{identifier} or a~\emph{literal}.
Empty token is token with no characters (so it is not a~literal).
By default, every token that is a~preprocessor command is expanded
(including arguments of the commands).

\paragraph{List.}
Recall that a~list is a~tuple
of elements enclosed within parenthesis,
that is, characters \id{(} and \id{)},
but with no explicit separator.
Empty list has no elements.
Arithmetic list follow the prefix notation,
begins with an~arithmetic operation (e.g. \id{+}, \id{-}, \id{*})
and has at least one more element.

\paragraph{Arithmetic expression}
is a~literal or an~arithmetic list, after expansion.
That is, before expansion, it can also be arithmetic expansion or a~macro.
Preprocessor commands nested in arithmetic lists are being expanded.
\Ext{%
Here, there is no difference whether a~variable is invoked by value or by reference
(see the section with macros for more details on variables and references).%
}

\paragraph{Sequence}
is a~list or an~identifier.
The length (size) of a~sequence is the number or its elements
(which correspond to characters in the case of an~identifier).

\paragraph{White characters}
serve to separate tokens.
It does not matter which white characters are used,
and also whether there is a~white character between a~token and a~list (or vice versa).
The only exception is \emph{white-sensitive} (reserved) macros,
where these rules do not necessarily hold.
The end of line is denoted by the symbol~$\neg$.

\paragraph{Comment.}
Any part of a~line that starts with \id{;}
is treated as a~comment until the end of the line.
The preprocessor simply discards the contents of a~comment.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Macro}\label{ss:spec:macro}
Macros are preprocessor commands that begin with the reserved character \id{\#}.
There are three types of such macros, in various forms:
\emph{reserved} macros, \emph{user} macros, and \emph{variables}.
\Ext{%
Speaking of variables, there are also \emph{references} to variables.
}

%%%%%%%%%%%%%%

\subsubsection{Reserved Macro}\label{sss:spec:macro:res}
Reserved macros are built-in macros that cannot be undefined nor changed,
and no variables with the same name also cannot be defined.
They for example introduce user macros or variables,
and provide for-loops, conditional substitution,
or accessing a~given part of a~list.

A~couple of such macros are white-sensitive,
where line breaks are treated differently than other white characters,
and where a~space before or after a~list can have a~special meaning.

\emph{Block} macro \id{\#<macro>} can contain a~body of arbitrary length,
terminated by token \id{\#end<macro>}.
\emph{Predicate} macro translates to literal 1
if the condition of the macro is evaluated to true,
and to literal 0 otherwise.

\paragraph{\id{\#}}
is \emph{empty} token.
It is white-sensitive---it must not be directly followed by a~list.

\paragraph{\id{\#\#}}
is \emph{concatenation} operator.
It is white-sensitive---it must not be directly followed by a~list.
Concatenation is described in a~separate section.

\paragraph{\id{\#(\dots)}\Ext{, \id{\#\#(\dots)}}}
is \emph{macro expansion} \Ext{and \emph{reference expansion}, resp.},
allowing indirect expansions.
It is white-sensitive---there must not be a~white-character character before the list.
The contents of the list is not restricted except that it must expand
into a~valid identifier.
The identifier is then further expanded as if it was prefixed with \id{\#}
\Ext{or \id{\#\#}, resp}.

\Ext{%
Macro expansions, in addition, accept a~reference to a~preprocessor variable as well,
in which case it yields the value of the variable.
For example, \id{\#(\#\#x)} is equivalent to \id{\#x}.
}

Macro expansion of white-sensitive macros has undefined behavior.

\paragraph{\id{\#include}}
copies contents of another file into the place,
along with preprocessing it the same way as if the contents were present there directly.
This allows organization of specific parts of the model into~separate libraries.
The form is
\Spec{\#include <path>}
where \id{<path>} is a~token,
which represents a~filesystem path to another text file.
\cmt{Paths with spaces should be supported too!}
The path can be either absolute or relative.
If relative, the directory corresponds to a~common path
of PWD and the path of the main input file.
\cmt{What about the path of the current file?}

\paragraph{\id{\#define}, \id{\#def}}
introduce a~user macro at global scope, possibly with parameters.
See part with user macros for more details.

The macro \id{\#define} is white-sensitive,
and is just a~shortcut to \id{\#def}.
It is in the form
\Spec{\#define <id>[(<arg>*)] [<body>]$\neg$}
and it directly translates to \id{\#def <id> (<arg>*) <body> \#enddef}.
The list of arguments is optional here, but it must not be separated
from \id{<id>} by a~white character,
otherwise it is treated as a~part of \id{<body>}.

The block macro \id{\#def} is \emph{not} white-sensitive.
It is in the form
\Spec{\#def <id> <args> [<body>] \#enddef}
where \id{<id>} stands for an~identifier of the introduced user macro.
The list of arguments \id{<args>} is mandatory,
and must be in the form \id{(<arg>*)} after expansion,
where each \id{<arg>} is an~identifier.
The contents of \id{<body>} are not limited,
and are \emph{not} preprocessed by the time of the definition.

\paragraph{\id{\#undef}}
forgets an~already existing user macro with identifier \id{<id>}:
\Spec{\#undef <id>}

\paragraph{\id{\#let}}
introduces one or more preprocessor variables at local scope.
For more details on variables, see the corresponding part below.
There are two forms of the command:
\begin{enumerate}
\item To introduce a~single variable:
    \Spec{\#let <id> <value>}
    where \id{<id>} stands for an~identifier.
    There is no limination on \id{<value>}.
\item To define multiple variables at once as a~\emph{group}:
    \Spec{\#let (<var>+)}
    where each \id{<var>} is either \id{<id>} or \id{(<id> <value>)} after expansion,
    while \id{<value>} \emph{can} depend on a~previous varible \id{<id>}
    from the list \id{(<var>+)}.
    Each variabmle that is specified in the form without \id{<value>}
    is defined as empty token.
\end{enumerate}

\paragraph{\id{\#set}}
sets an~already existing preprocessor variable at the current local scope to a~new value:
\Spec{\#set <id> <value>}

\paragraph{\id{\#endlet}}
ends the scope of one or more already existing preprocessor variables.
There are multiple forms,
but in any way,
it must be consistent with the way
how the variable(s) was (were) introduced with the corresponding \id{\#let}---%
either as a~single variable, or as a~group:
\begin{enumerate}
\item To end the scope of a~single variable with identifier \id{<id>}:
    \Spec{\#endlet <id>}
    In this case, the target variable is specified explicitly,
    so no limitation on the location of this command is necessary.
    That is, other \id{\#endlet}s can appear between this one
    and the corresponding definition \id{\#let <id>}
    (the scopes can interleave).
    As a~shortcut, it is also possible to end the scope of multiple such variables, by
    \Spec{\#endlet (<id>+)}
\item To end the scope of the preceding \emph{group} of variables:
    \Spec{\#endlet ()}
    That is, it is not possible to interleave scopes of multiple groups of variables.
    Also, it is not possible to end the scope of separate variables of a~group.
    There is also a~white-sensitive abbreviation of this form:
    \Spec{\#endlet $\neg$}
    that is, in cases where only white characters follow the command at the line,
    the empty list is not necessary.
\end{enumerate}

\paragraph{\id{\#if}, \id{\#ifdef}, \id{\#ifndef}}
are block macros that serve for conditional processing.
The base forms the macro \id{\#if}:
\Spec{\#if <cond> [<body>] <\#elif <econd> [<ebody>]>* [\#else [<xbody>]] \#endif}
where \id{<cond>} and all \id{<econd>} are arithmetic expressions,
which are consecutively evaluated following short-circuit evaluation,
while an~expression holds if the result of it is non-zero.
Then, the macro translates to \id{<body>} if \id{<cond>} holds,
to \id{<ebody>} if \id{<econd>} holds, etc.,
and to \id{<xbody>} if the \id{\#else} clause is included and none of the conditions hold.
The selected body is preprocessed, while the other ones are just discarded.
The contents of the bodies are not limited.

The beginning of the macro \id{\#ifdef} and \id{\#ifndef}
is just a~shortcut for \id{\#if \#isdef} and \id{\#if \#isndef}, resp.

\paragraph{\id{\#for}}
is block macro which provides for loops in various forms,
usually used for the purposes of parametrized copying of a~code.
In all forms, there is at least one loop variable with an~identifier \id{<id>},
which can be accessed inside the loop body the same way
as if it was a~preprocessor variable, that is, \id{\#<id>}.
The contents of the loop body are not limited.

The available forms are:
\begin{enumerate}
\item Simple integer for loop:
    \Spec{\#for (<id> <init> <end>) [<body>] \#endfor}
    where \id{<init>} and \id{<end>} are literals with initial and ending value of the loop variable.
    The loop iterates the same way as C for loop \id{for(; <id> <= <end>; ++<id>)}.
\item Floating-point for loop:
    \Spec{\#for (<id> <init> (<cond>) (<step>)) [<body>] \#endfor}
    where \id{<init>} is a~literal
    and \id{(<cond>)} and \id{(<step>)} are arithmetic lists,
    which can contain the identifier \id{<id>}
    (not to be confused with \id{\#<id>}).
    The variable is initialized to \id{<init>}
    and the loop continues as long as \id{(<cond>)} holds, with step function \id{(<step>)}.
    The loop iterates the same way as C for loop \id{for(; <cond*>; <id> = <step*>)},
    where \id{<cond*>} and \id{<step*>} is infix representation
    of \id{(<cond>)} and \id{(<step>)}, resp., in C.
\item For-each loop:
    \Spec{\#for (<id> <args>) [<body>] \#endfor}
    where \id{<args>} is a~sequence after expansion.
    Here, the loop variable just iterates over the elements of the sequence \id{<args>}.
    Each element is allowed
    to be of a~different type (like identifier, literal, etc.).
\Ext{%
\item An~optional extension is a~generalized version of the second form:
    \Spec{\#for ( ((<id> <init>)+) (<cond>) ((<step>)+) ) [<body>] \#endfor}
    where the variables are initialized in a~similar way
    as in the case of introducing a~group of variables via \id{\#let} macro.
    The lists \id{(<cond>)} and all \id{(<step>)} can contain all the identifiers \id{<id>},
    and the number of \id{(<step>)+} lists must match the number of variables \id{<id>}.
    The loop iterates the same way as C for loop
    \id{for(; <cond*>; <id\_1> = <step\_1*>, <id\_2> = <step\_2*>, \dots)}.
\item Another extension is a~generalized version of the third form:
    \Spec{\#for ( (<id>+) <args> ) [<body>] \#endfor}
    where, after expansion, \id{<args>} is a~list of sequences with $n$ elements each,
    where $n$ is the number of loop variables \id{<id>+}.
    Then, each variable \id{<id>} is assigned to the corresponding element of the nested sequence
    during each iteration.
}
\end{enumerate}

% \paragraph{\id{\#while}}
% is block macro which provides a~while loop:
% \Spec{\#while ( (<id>+) (<cond>) ) [<body>] \#endwhile}

\paragraph{\id{\#isdef}, \id{\#isndef}}
are predicate macros testing existence of a~macro:
\Spec{\#isdef <arg>}
There are two forms, depending whether \id{<arg>} is a~list or not, after expansion:
\begin{enumerate}
\item In the case other than with a~list,
    the predicate holds if the argument yields
    \Ext{a~reference or}
    an~identifier of a~reserved macro or
    of a~currently defined user macro or variable.
\item With a~list,
    the predicate holds iff \emph{all} elements of the list satisfy the same condition
    as in the first form,
    while in addition, nested lists have no special meaning any more
    and they just do not satisfy the required condition.
\end{enumerate}
In the first form, \id{\#isndef} is just negation of \id{\#isdef}.
However, in the second form, it is \emph{not} negation,
but instead, it requires that \emph{none} of the arguments satisfy the condition
(i.e. all vs. none).

\paragraph{\id{\#null}}
is a~predicate macro that tests if an~object is ``empty'':
\Spec{\#null (<arg>)}
The behavior depends on the type of \id{<arg>}.
If it is a~list or a~token, then the condition just holds iff it is empty.
\Ext{%
If it is a~reference, the condition holds iff the reference does not point to a~valid object.
}

\paragraph{\id{\#listp}, \id{\#numberp}\Ext{, \id{\#refp}}}
are predicate macros that test the type of an~object:
\Spec{\#listp (<arg>)}
\Spec{\#numberp (<arg>)}
\Ext{\Spec{\#refp (<arg>)}}
The condition holds iff \id{<arg>} is a~list, a~literal, \Ext{and a~reference,} resp.

\paragraph{\id{\#equal}, \id{\#eq}}
are binary predicate macros
that test if two objects are the same in a~certain way.

To determine if two objects have the same type and value, we use
\Spec{\#equal (<arg\_1> <arg\_2>)}
\Ext{%
Specifically, references are also equal to objects
that are not references, but that are equal to the object that the reference points to.
}

To determine whether two objects point to the exactly same object, we use
\Spec{\#eq (<arg\_1> <arg\_2>)}
This can happen only in cases when both arguments are macros
that either point to the same variable\Ext{,
or to the same reference%
}.

\paragraph{\id{\#len}}
translates to integer literal that corresponds to the length of a~sequence \id{<arg>}:
\Spec{\#len (<arg>)}
\Ext{%
In addition, if \id{<arg>} yields a~reference,
the length of its underlying object is analyzed.
}

\paragraph{\id{\#car}, \id{\#cdr}, \id{\#nth}, \id{\#nthcdr}}
are so-called accessors, or access macros,
which operate upon a~sequence.
\Ext{%
References pointing to a~sequence are accepted as well, with the same behavior.
}

To access the first element of a~sequence, we use
\Spec{\#car (<arg>)}
The macro fails if the sequence is empty.

To produce a~sequence corresponding to the argument without the first element, we use
\Spec{\#cdr (<arg>)}
The macro does not fail even if the sequence is already empty,
but yields an~empty sequence instead.

To access element at position \id{<idx>} of a~sequence, we use
\Spec{\#nth (<idx> <arg>)}
where \id{<idx>} is integer literal, while zero corresponds to the first element.
If the position is out of bounds,
that is, outside of $\{ 0, \dots, \id{\#len(<arg>)} -1 \}$,
the macro fails.
It is a~generalized form of \id{\#car(<arg>)},
which is equivalent to \id{\#nth(0 <arg>)}

To produce a~sequence corresponding to the original one,
but without first \id{<idx>} elements, we use
\Spec{\#nthcdr (<idx> <arg>)}
If the position is out of bounds,
an~empty sequence is produced.
It is a~generalized form of \id{\#cdr(<arg>)},
which is equivalent to \id{\#nthcdr(1 <arg>)}.

\paragraph{\id{\#print}}
immediately outputs an~object and end of line to the standard error output:
\Spec{\#print <arg>}
Note that character strings and the like are not supported,
so a~list must be used instead.
However, it is not possible to control white characters.

\paragraph{\id{\#assert}}
terminates the preprocessing gracefully if a~given condition is violated:
\Spec{\#assert <cond>}
where \id{<cond>} is an~arithmetic expression.

\paragraph{\id{\#error}}
is like \id{\#print}, but in addition terminates the preprocessing gracefully.

\Ext{%
\paragraph{\id{\#ref-name}}
translates to the identifier of the variable that a~reference \id{<ref>} points to:
\Spec{\#ref-name (<ref>)}
For example, \id{\#ref-name (\#\#x)} yields \id{x}.
}

%%%%%%%%%%%%%%

\subsubsection{User Macro}\label{sss:spec:macro:user}
User macros are used to possibly parametrized expansions to given contents.
At most one user macro with the same name can be defined at the same time.
Not to be confused with preprocessor variables though---%
they always take precedence over user macros,
including definition and invocation.

Parameters of a~defined macro behaves the same
as if they were introduced as a~group by the macro \id{\#let}
right before an~invocation of the user macro,
and cancelled by \id{\#endlet} at the end of the macro body.
There is no limination on the type of particular parameters.

When the user macro is invoked afterwards,
the contents of its body is \emph{copied} to the place.
The body is \emph{not} preprocessed
at the time of the definition, but at the time of the invocation,
so the behavior of the body depends on surrounding context.
Also, the user macro can possibly cause various side effects at global scope,
like defining other user macros.

User macros support recursive calls.

A~user macro with identifier \id{<id>}, but with no defined parameters,
can be invoked as follows:
\Spec{\#<id>}
that is, no list, even if it is empty, is treated as a~part of the invocation.
If the macro was defined with $n$~parameters, \(n>0\), then the form is
\Spec{\#<id> <args>}
where \id{<args>} is a~list after expansion with at most $n$ elements.
If the number of elements is less than $n$,
then every missing parameter from the end of the parameter list is assigned to empty token.


%%%%%%%%%%%%%%

\subsubsection{Variable}\label{sss:spec:macro:var}
Preprocessor variables are introduced by the command \id{\#let}
or as parameters of a~user macro.
New variables are always created, instead of replacing the existing ones,
and only the most recent one is used when the identifier of the variable is invoked.
Scope of the~current variable corresponding to an~identifier
ends either with the end of the user macro where it represents its parameter,
or with the command \id{\#endlet}.

A~variable has a~\emph{value}, which is defined by the time of the variable definition
(if no value is specified, then it is empty token),
and can be modified by the command \id{\#set}.
\Ext{%
Note that the value is also allowed to contain a~reference to an~existing variable.
}

Invocation of a~variable with identifier \id{<id>}
can be done by value\Ext{ or by reference}.
Invocation by value is consistent to user macros with no parameters:
\Spec{\#<id>}
Invoked variable is then replaced by its value.
\Ext{%
Invocation by reference is in the form
\Spec{\#\#<id>}
which places reference to the variable, not the value itself.
See the part with references for more details.
}

%%%%%%%%%%%%%%

\subsubsection{Concatenation}\label{sss:spec:macro:conc}
Concatenation consumes two arguments and produces one output element
(i.e. it does not modify any of the arguments).
There two kinds of concatenation, with two forms each.

The first kind of concatenation is \emph{external}:
\begin{enumerate}
\item To concatenate \emph{after} thoroughly preprocessing both arguments:
    \Spec{<arg\_1> \#\# <arg\_2>}
\item To concatenate \emph{within} the preprocessing of the first argument
    (i.e. as a~part of it):
    \Spec{<arg\_1>\#\# <arg\_2>}
    where \id{<arg\_1>} must be a~token.
\end{enumerate}
The difference can be demonstrated in cases
when there is a~preceding macro that is about to consume \id{<arg\_1>} as an~argument:
the macro ignores \id{<arg\_2>} in the first form,
but in the second form, it uses the result of the concatenation.

The second kind of concatenation is \emph{internal},
where both arguments are tokens that are a~part of a~single identifier (before expansion):
\begin{enumerate}
\item The natural way of doing so is
    \Spec{<arg\_1><arg\_2>}
\item An~alternative way is
    \Spec{<arg\_1>\#\#<arg\_2>}
    which is necessary in cases when \id{<arg\_1>} is a~macro
    but \id{<arg\_2>} is not,
    for example, at \id{\#x\#\#\_suffix}, where \id{\_suffix} is not part of the macro identifier.
    Three \id{\#} in a~row (i.e. \id{\#\#\#}) are \emph{not allowed}---%
    in such cases, it is always sufficient to use the first form instead of this one.
\end{enumerate}

The behavior and the resulting type differs based on types (after expansion)
of the first and the second argument of the concatenation:
\begin{itemize}
\item \(I \times T \rightarrow I\):
    the identifier is suffixed by the token,
\item \(I \times L \rightarrow L\):
    each element of the list is concatenated with the identifier,
    where the identifier is the first argument of each concatenation,
\item \(L \times T \rightarrow L\):
    each element of the list is concatenated with the token,
    where the token is the second argument of each concatenation,
\item \(L \times L \rightarrow L\):
    the elements of the second list are appended to the first list,
\end{itemize}
where $I$ stands for identifier, $T$ for token, and $L$ for list.
Literals are not allowed as the first argument,
because such concatenations may result in invalid tokens
(i.e. neither a~literal nor an~identifier).

\Ext{%
References to variables are accepted as well, without a~special meaning---%
the effect is the same as if the variables were invoked by value.
The exception is that references cannot appear within internal concatenation
(without a~loss of expressivity),
because the identifier \id{\#\#} is always interpreted as concatenation there.
}

%%%%%%%%%%%%%%

\Ext{%
\subsubsection{References}\label{sss:spec:macro:ref}
A~reference is a~pointer to a~variable.
The purpose is for example to allow sharing a~value with multiple places.
By default, to access a~value of a~referenced variable,
it is necessary to explicitly dereference (expand) the reference.
Otherwise, the reference itself is placed.
For example, \id{\#let y \#\#x} introduces variable \id{y} pointing to variable \id{x},
while \id{\#let y \#x} (which is equivalent to \id{\#let y \#(\#\#x)})
will point to the value of variable \id{x}, which can change in the future.

It is also possible that there are multiple references in a~row
pointing to a~variable with an~actual value.
In such cases, it can be necessary to explicitly dereference each such a~reference.
For example, with \id{\#let x 5}, \id{\#let y \#\#x} and \id{\#let z \#\#y},
\id{\#(\#(\#z))} yields \id{5}.
Here, we call such a~value, that is not a~reference any more, \emph{final value}.
One can define an~auxiliary macro \id{\#\^{}}
which yields the final value.
For example, \id{\#\^{}(z)} yields \id{5} from the example above.
Beware of circular references, though.

The resulting preprocessed code may contain unexpanded references,
which are in the end replaced by the corresponding final values.
For example, code \id{\#let (x) \#\#x \#set x 5} will result in \id{5}
(which would not be the case if the variable was invoked by value).

One can be confused why references are allowed in multiple reserved macros as arguments
even in cases when an~immediate evaluation is performed
(like concatenation, arithmetic expansion, accessors, etc.)---%
the reason is just to not require the user to check and eventually expand references there,
so to make it easier to use.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Arithmetic Expansion}\label{ss:spec:arith}
They are commands followed by an~arithmetic expression, in the form
\Spec{\$[<spec>] <expr>}
It is substituted by a~literal
which is the result of immediate (meaning not a~lazy) arithmetic evaluation
of the expression \id{<expr>}.

The implicit data type of all underlying arguments of an~arithmetic expansion
(and of the result)
is float.
To~require certain data type explicitly,
\id{<spec>} must be provided, in the form
\Spec{<type>[<width>]}
with \id{<type>} being
\begin{itemize}
\item \id{<d | i>} for integer type,
\item \id{f} for float type,
\end{itemize}
and with optional bit width \id{<width>}.
If \id{<width>} is not specified, the bit width is implementation specific.

The only reasonable usage of cases where \id{<expr>} is a~literal after expansion
(e.g. in the case of \id{\$d 4.5})
is to control the data type of \id{<expr>}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
