# Experiments documentation

Our experiments are divided into subdirectories `<version>`
which is the name of a version (and tag) of UN/SOT.
A PDF report with experiments' specification and results is available there.

## Output data

All measured data is accessible online in the form of Google Spreadsheets,
separated into particular versions:
* [v0.7](https://docs.google.com/spreadsheets/d/1_LTY7xz6AW5GaD98bUVJumMuYWMIQ5ww4p5QpTHuWTU/edit?usp=sharing)
* [v0.8](https://docs.google.com/spreadsheets/d/1hv-n7oNDL3vHOmf39HkgLWS5ZE32E4tUUryUyjZT8MI/edit?usp=sharing)

Plots (not for all input combinations) are placed at `../img`,
and also directly at `<version>/img` via symlink.

## Input data

Input data is located at `/data/smto`.
External input data (like dReal's) is placed at `/data/external`.

However, the run script can alone meet one's needs entirely (see below).

## Run

To reproduce all reported experiments of given version,
run the symlinked helper script `<version>/run.sh`
(which runs `/tools/experiments/run.sh <version>`).
The script will run all input files and all parameters automatically.

The script `/tools/experiments/run.sh` accepts a few command line arguments,
run it without arguments to show a help message.
To select only a subset of all experiments,
one shall use the provided arguments,
or manually edit the `EXPERIMENTS` array in the input file
`/tools/experiments/in/<version>` Bash file (e.g. comment out some items).
You can also pass a timeout value as a command line argument,
or alter the `TIMEOUT` variable in the input file.

Note that the particular scripts relate to a demanded version of UN/SOT
only wrt. input files and parameters,
but they actually do not alter the version of the executed software itself.
To reproduce experiments related to a certain version precisely,
make sure you use the corresponding version of UN/SOT
(i.e. that you either downloaded only the demanded version,
or, if you cloned the repository, that you switched to it;
see the README in the root directory for more information).

The output is multiple datasets in CSV format printed to `stdout`
and also in a more stacked form into output file `/local/<version>`,
which can be easily pasted into one's favourite spreadsheet processor,
for example into (a copy of) the presented spreadsheet with output data,
into the `_data` sheet.
Reference output data is placed at `/tools/experiments/out/<version>`.
To compare the reference data with the data at `/local/<version>`,
run `/tools/experiments/diff.sh <version>`.
