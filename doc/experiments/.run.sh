#!/bin/bash

DIR=$(dirname "`realpath -s "$0"`")
"$DIR"/../../../tools/experiments/run.sh `basename "$DIR"` "$@"
