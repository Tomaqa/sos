set terminal svg fixed noenhanced size 600,320 font 'Arial,14'
set output "img/costs.svg"

set xlabel "Demands"
set ylabel "Costs"

unset xtics
unset ytics

set title "Comparison of different fixed and variable costs"

plot [0:5][0:] exp(x) title "High variable costs", 50+exp(x*0.66) title "High fixed costs"
