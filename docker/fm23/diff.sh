#!/bin/bash

if [[ -z $1 ]]; then
  ID=fm23
else
  ID=$1
fi

[[ -r local/$ID ]] || {
  printf -- "Experiments were not executed yet, see 'README'\n" >&2
  exit 1
}

exec tools/experiments/diff.sh $ID
