#!/bin/bash

ROOT=$(realpath "`dirname "$0"`")
while [[ ! `basename "$ROOT"` =~ ^unsot ]]; do
  ROOT=`dirname "$ROOT"`
done


function usage {
  printf "Usage: %s <tag> [<name>] [<filter>] [<timeout>]\n" "$0" >&2
  printf "\t<tag>         the version of UN/SOT (e.g. v0.7), which experiments' set are to be executed.\n" >&2
  printf "\t<name>        relates to experiments which only are desired to be executed.\n" >&2
  printf "\t<filter>      a regular expression which only applies to parameters of an experiment.\n" >&2
  printf "\t<timeout>     a number with time unit suffix s|m|h|d (default s).\n" >&2
  exit 1
}

[[ -z $1 ]] && usage
TAG=$1
shift

IN_FILE="$ROOT/tools/experiments/in/$TAG"
[[ ! -r $IN_FILE ]] && {
  printf "The provided version tag '%s' is not valid (file '%s' is not readable).\n" "$TAG" "$IN_FILE" >&2
  usage
}

source "$IN_FILE"

set -o pipefail

unset NAME FILTER
NAME_SET=0
for arg in "$@"; do
  if [[ $arg =~ ^[0-9] ]]; then
    TIMEOUT=$arg
  elif (( ! $NAME_SET )); then
    NAME="$arg"
    NAME_SET=1
  else
    FILTER="$arg"
  fi
done

[[ -z $TIMEOUT ]] && TIMEOUT=10h
export TIMEOUT

[[ -z $MAX_PROCESSES ]] && {
  if command -v nproc &>/dev/null; then
    N_CPU=$(nproc --all)
  elif [[ -r /proc/cpuinfo ]]; then
    N_CPU=$(grep processor </proc/cpuinfo | wc -l)
  elif [[ -d /sys/class/cpuid && -r /sys/class/cpuid ]]; then
    N_CPU=$(ls /sys/class/cpuid | wc -l)
  else
    N_CPU=1
  fi
  if (( $N_CPU <= 3 )); then
    MAX_PROCESSES=1
  else
    MAX_PROCESSES=$(( $N_CPU/2 ))
    [[ -r /proc/meminfo ]] && {
      MEM_SIZE_kB=$(sed -rn 's/^MemTotal: +([0-9]+) .+$/\1/p' </proc/meminfo)
      if command -v awk &>/dev/null; then
        MAX_PROCESSES=$(awk -v n=$MAX_PROCESSES -v mem=$MEM_SIZE_kB -v cpu=$N_CPU 'BEGIN { printf "%d", n*((mem/10^6)/cpu) }' </dev/null)
      elif command -v bc &>/dev/null; then
        MAX_PROCESSES=$(bc -l <<<"$MAX_PROCESSES*( ($MEM_SIZE_kB/10^6)/$N_CPU )")
        MAX_PROCESSES=${MAX_PROCESSES%.*}
      else
        MAX_PROCESSES=$(( $MAX_PROCESSES*( ($MEM_SIZE_kB/10**6)/$N_CPU ) ))
      fi
      if (( $MAX_PROCESSES < 1 )); then
        MAX_PROCESSES=1
      elif (( $MAX_PROCESSES > $N_CPU )); then
        MAX_PROCESSES=$N_CPU
      fi
    }
  fi
}
(( $MAX_PROCESSES > 1 )) && printf "I will use %d parallel processes.\n\n" $MAX_PROCESSES

OUT_FILE="$ROOT/local/$TAG"
>"$OUT_FILE" || {
  printf "The output file '%s' is not writeable file.\n" "$OUT_FILE" >&2
  exit 2
}

PROCESSES=0
FIRST_IDX=0
LAST_IDX=0
declare -a PIDS
declare -a {STD,}OUT_FILES

function failure {
  trap " " TERM
  kill 0
  wait

  for i in ${!PIDS[@]}; do
    rm -f "${STDOUT_FILES[$i]}"
    rm -f "${OUT_FILES[$i]}"
  done

  exit $1
}

function wait_f {
  local ofile="${STDOUT_FILES[$FIRST_IDX]}"
  unset STDOUT_FILES[$FIRST_IDX]
  head -n 1 "$ofile" || return $?

  local pid=${PIDS[$FIRST_IDX]}
  unset PIDS[$FIRST_IDX]
  wait $pid || {
    local res=$?
    printf "\nError while waiting on child process %s.\n" $pid >&2
    printf "Parameters:\n" >&2
    head -n 1 "$ofile" >&2
    tail -n 1 "$ofile" >&2
    printf "\n" >&2
    return $res
  }
  (( --PROCESSES ))

  tail -n +2 "$ofile" || return $?
  printf "\n"
  rm -f "$ofile"

  ofile="${OUT_FILES[$FIRST_IDX]}"
  unset OUT_FILES[$FIRST_IDX]
  cat "$ofile" >>"$OUT_FILE" || return $?
  rm -f "$ofile"

  (( ++FIRST_IDX == $MAX_PROCESSES )) && FIRST_IDX=0

  return 0
}

function measure_f {
  local sofile=`mktemp`
  local ofile=`mktemp`

  printf "'%s' : %s\n" $id "${params[*]}" >>"$sofile"
  (( $SAME )) || printf "\n'%s' : %s\n" $id "${param_ids[*]}" >>"$ofile"
  SAME=$SAME "$ROOT/tools/measure.sh" "$f" ${params[@]} | tee -a "$ofile" "$sofile" >/dev/null &

  local pid=$!
  PIDS[$LAST_IDX]=$pid
  STDOUT_FILES[$LAST_IDX]="$sofile"
  OUT_FILES[$LAST_IDX]="$ofile"
  (( ++LAST_IDX == $MAX_PROCESSES )) && LAST_IDX=0
  (( ++PROCESSES < $MAX_PROCESSES )) && return 0

  wait_f
}

trap "failure" INT TERM

unset prev_id
unset prev_param_ids
for e in "${EXPERIMENTS[@]}"; do
  arr=($e)
  id=${arr[0]}
  [[ -n $NAME && $id != $NAME ]] && continue
  params=(${arr[@]:1})
  [[ -n $FILTER && ! ${params[*]} =~ $FILTER ]] && continue
  param_ids=(${params[@]%:*})
  if [[ $id == $prev_id && ${param_ids[@]} == ${prev_param_ids[@]} ]]; then
    SAME=1
  else
    SAME=0
  fi
  prev_id=$id
  prev_param_ids=(${param_ids[@]})

  f="$ROOT/data/smto/$id/${id}.smto"
  [[ -r $f ]] || f="$ROOT/data/smt2/${id}.smt2"
  [[ -r $f ]] || {
    printf "No readable file with id '%d' found.\n" "$id" >&2
    failure 2
  }

  measure_f || failure $?
done

while (( $PROCESSES > 0 )); do
  wait_f || failure $?
done

exit 0
