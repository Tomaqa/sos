#!/bin/bash

ROOT=$(realpath "`dirname "$0"`")
while [[ ! `basename "$ROOT"` =~ ^unsot ]]; do
  ROOT=`dirname "$ROOT"`
done


function usage {
  printf -- "Usage: %s <tag>\n" "$0" >&2
  printf -- "Tag is the version of UN/SOT (e.g. v0.7), which experiments are to be compared with the reference results.\n" >&2
  exit 1
}

[[ -z $1 ]] && usage

OUT_REF_FILE="$ROOT/tools/experiments/out/$1"
[[ ! -r $OUT_REF_FILE ]] && {
  printf -- "Reference results are not available for the provided version tag '%s' (file '%s' is not readable).\n" "$1" "$OUT_REF_FILE" >&2
  usage
}

OUT_LOCAL_FILE="$ROOT/local/$1"
[[ ! -r $OUT_LOCAL_FILE ]] && {
  printf -- "Local results are not available for the provided version tag '%s' (file '%s' is not readable).\n" "$1" "$OUT_LOCAL_FILE" >&2
  usage
}

SCRIPT=(awk -F';'
  'NF==0 { print; getline; print; getline; for(i=1; i<=NF; ++i) { if ($i != "result") printf $i";"; else { limit=i; print "\t"$i; next } } exit 1; }
  { for(i=1; i<limit; ++i) printf $i";"; print "\t"$limit }'
)
REF_DATA=`"${SCRIPT[@]}" <"$OUT_REF_FILE"`
LOCAL_DATA=`"${SCRIPT[@]}" <"$OUT_LOCAL_FILE"`

DATA=`diff -wyW180 --suppress-common-lines <(cat <<<"$REF_DATA") <(cat <<<"$LOCAL_DATA")`
# DATA=`diff -wyW180 <(cat <<<"$REF_DATA") <(cat <<<"$LOCAL_DATA")`

[[ -n $DATA ]] && {
  printf "%s\n" "$DATA"
  printf -- "-%.0s" {1..50}
  printf "\n"
}

#+ take only the result columns
if [[ -z $DATA ]] || ! grep -v "?" <<<"$DATA"; then
  printf "OK.\n"
  exit 0
fi

printf -- "\nFailure!\n" >&2
exit 1
