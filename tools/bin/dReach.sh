#!/bin/bash

function usage {
  printf -- "%s <drh_file> <k> [...]\n" "$0"
  exit 0
}

function shutdown {
  [[ -n $PID ]] && kill $PID
  rm -f $out_f
  exit 0
}

unset PID
trap "shutdown" SIGINT SIGTERM

###### !! Set this properly !! ######
DREAL_DIR=~/Data/Software/dreal3
#####################################

[[ -d $DREAL_DIR && -r $DREAL_DIR ]] || {
  printf -- "'%s' is not a readable directory.\n" "$DREAL_DIR" >&2
  printf -- "Set the variable \$DREAL_DIR to proper path to dReal.\n" >&2
  exit 1
}

DREACH_CMD=("$DREAL_DIR/bin/dReach")
[[ -x ${DREACH_CMD[0]} ]] || {
  printf -- "'%s' is not executable.\n" "${DREACH_CMD[0]}" >&2
  exit 1
}

DREAL_CMD=("$DREAL_DIR/bin/dReal")
[[ -x ${DREAL_CMD[0]} ]] || {
  printf -- "'%s' is not executable.\n" "${DREAL_CMD[0]}" >&2
  exit 1
}

[[ -z $1 ]] && usage
FILE="$1"
BASE="${FILE%.drh}"
shift

[[ -z $1 ]] && usage
K=$1
shift

"${DREACH_CMD[@]}" -k$K -l$K "$FILE" --version

PATHS_FILE="${BASE}.paths"
[[ -z `cat "$PATHS_FILE"` ]] && exit 0
N=`wc -l <"$PATHS_FILE"`

GOAL=(`sed -rn '/goal:/,$s/^[^@/]*@([0-9]+).*$/\1/p' <"$FILE"`)

out_f=`mktemp`
for (( i=0; $i<$N; i++ )); do
  f="${BASE}_${K}_${i}.smt2"

  [[ -n $GOAL ]] && {
    if (( ${#GOAL[@]} > 1 )); then
      for g in ${GOAL[@]}; do
        sed -ri "/^\(assert/s/(\(= mode_${K} )[0-9]+/\1#/" "$f"
      done
      for g in ${GOAL[@]}; do
        sed -ri "s/(\(= mode_${K} )#/\1${g}/" "$f"
      done
    else
      sed -ri "/^\(assert/s/(^.*\(= mode_${K} )([0-9]+)(.*$)/\1${GOAL}\3/" "$f"
    fi
  }

  printf -- "Solving '%s' ..." "$f"
  "${DREAL_CMD[@]}" "$f" "$@" >$out_f &
  PID=$!
  wait $PID || break
  unset PID

  out=`<$out_f`
  printf -- " %s\n" "$out"
  [[ $out == unsat ]] && continue

  TRACE_FILE="${f}.json"
  [[ $TRACE_FILE -nt $f ]] && {
    cp -v "${TRACE_FILE}" "$DREAL_DIR/tools/ODE_visualization/data.json"
    (( $? )) && exit $?
  }
  break
done

shutdown
