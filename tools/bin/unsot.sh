#!/bin/bash

DIR=`dirname "$0"`
ROOT=`realpath "$DIR"`
while [[ ! `basename "$ROOT"` =~ ^unsot ]]; do
  ROOT=`dirname "$ROOT"`
done
ROOT=`realpath --relative-to="$PWD" "$ROOT"`

source "$ROOT/tools/lib"

function usage {
  "$CMD" "${ARGS[@]}" -h |& sed 's/unsot\(_[^ ]*\)/unsot/'
  printf -- '    -X    Turn on the "extended" mode (additional features provided by the Bash script)\n'
}

function shutdown {
  [[ -n $PID ]] && kill $PID
  rm -f $out_f
  exit 0
}

unset PID
trap "shutdown" SIGINT SIGTERM

USAGE=0
EXTENDED=0
OPTIONS=1
ARGS=()
for arg in "$@"; do
  (( $OPTIONS )) &&\
  [[ "$arg" =~ -[ioDtTd] ]] ||\
  case $arg in
    -*h*) USAGE=1;;&
    -*X*) EXTENDED=1
          arg=${arg//X/}
          [[ $arg == - ]] && continue
          ;;&
    --) OPTIONS=0;;
  esac

  ARGS+=("$arg")
done

(( $USAGE )) && {
  usage
  exit 0
}
(( $EXTENDED )) || exec "$CMD" "${ARGS[@]}"

out_f=`mktemp`
"$CMD" -t "$ROOT/local/out" "${ARGS[@]}" >$out_f &
PID=$!
wait $PID || exit $?
unset PID

OUT=`<$out_f`
printf -- "%s\n" "$OUT"

split_string "$OUT" LINES $'\n'
[[ ${LINES[0]} != sat ]] && exit 0

for l in "${LINES[@]}"; do
  [[ $l =~ ^Generating\ plot ]] || continue
  split_string "$l" a \'
  display "${a[1]}"
done

exit 0
