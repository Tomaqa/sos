set terminal svg size 480,480 noenhance

set output ofname

set style data lines

stats ifname using 1 nooutput
columns = STATS_columns
blocks = STATS_blocks-1

unset key

width = 2
length = 10
in_left = width
in_right = width+length
out_right = 2*width+length
init_y = width+length*0.5

set style rect back fs empty border lc black lw 1
set object 1 rect from in_left,in_left to in_right,in_right
set arrow from 0,init_y to in_left,init_y nohead lc black lw 1 dashtype 2

set xrange [0:out_right]
set yrange [0:out_right]

plot ifname using 2:3
