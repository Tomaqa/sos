#!/bin/bash

ROOT=src
[[ -n $1 ]] && ROOT="$1"

DIRS=(`find "$ROOT" -type d`)

if [[ -z $2 ]]; then
    out=("${DIRS[@]}")
else
    out=("${DIRS[@]/#$ROOT/$2}")
fi

printf -- "%s\n" "${out[@]/%//}"
