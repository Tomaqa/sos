#!/bin/sh
find src/ -type f -name '*.cpp' -exec g++ -c -std=c++17 -isystem include -I include/unsot -I src/unsot -I include/test/unsot -MM {} -MT {} \; | sed 's|^[^/]*/\([^.]*\)\.[^:]*:|build/\1.o:|' >.depend
