# UN/SOT: UN/SAT modulo ODES Not SOT

This project is the result of a PhD. research at FIT-CTU in Prague,
which is currently under active development.

The tool decides satisfiability of formulas
which can contain Boolean and real variables,
and additionally ordinary differential equations (ODEs).

## Announcement

This software is still beta version so far,
do not expect user-friendly interface and bug-free usage.
Users are welcome to use this software,
and asked to report discovered bugs to GitLab.

## Download

[GitLab](https://gitlab.com/Tomaqa/unsot) repository.

The preferred way is to *clone* the repository using Git.
You could also directly download a zipped archive with the source codes,
but it is not documented nor recommended.

To clone, run
```
git clone --recurse-submodules https://gitlab.com/Tomaqa/unsot.git
```

Note the `--recurse-submodules` option.
If it was omitted,
one has to clone/download all nested submodules manually.

## Installation

Installation itself is not implemented yet, only local build.

But first, one must take care of dependencies.

### Dependencies

The software is developed primarily for Linux.
Other operating systems were not tested at all.
In particular, we require POSIX-compliant system
(which most of Linux distributions are).

The following software is required to be installed system wide
*before* building the project:
* C++17 compiler along with std. C++ lib. (verified with GCC>=10 and Clang>=11), C POSIX library
* GNU Make
* Bash (Unix shell)
* Boost (C++ libraries)

C++ compiler defaults to GCC.
Boost is used only due to dependencies related to
[Odeint](https://www.boost.org/doc/libs/release/libs/numeric/odeint/)
(ODE solver).

There are also some dependencies that matter only during runtime:
* Gnuplot, ImageMagick [optional]

Gnuplot and ImageMagick are only used when plotting and displaying
resulting trajectories.

Some third-party libraries were *already retrieved* automatically
when the repository was cloned:
* [Minisat2](https://gitlab.com/Tomaqa/minisat) (SAT solver)

### Build

First of all, change the working directory to the UN/SOT directory:
```
cd unsot
```
Using command `ls`, verify that you see this file `README.md`
and directories such as `include` and `src`.

To build, simply run
```
make
```

If you prefer Clang compiler, run
```
make CPP=clang++
```

## Usage

Use the wrapper script `bin/unsot`.
For more information, run the script with `-h` option.

If you are just interested in reproducing published **experiments**,
follow the dedicated section below.

### Examples

```
bin/unsot data/smto/ball/ball.smto
```
This will search for an assignment of variables of a bouncing ball model
while meeting certain constraints on the final height of the ball.
Since such an assignment exists, the satisfiability problem is satisfiable,
thus the result is `sat`.
Also, the resulting values of variables are printed out.

To include plotting and displaying the dynamics of the ball, run
```
bin/unsot data/smto/ball/ball.smto -X
```

Use option `-q` to supress the values of variables.
It will just print `sat` (whether the problem is satisfiable).
To include particular run-times and other profiling information,
use option `-P`. You can run
```
bin/unsot data/smto/ball/ball.smto -Pq
```

We use a preprocessing language within our models (see documentation below),
which allows to parametrize them.
It is based on user macros, which can also be passed via command line
with the `-D` option.
For example, to run with a different bounds of the initial height, you can use
```
bin/unsot data/smto/ball/ball.smto -Xq -DX_MIN=1 -DX_MAX=3 -DX_STEP=0.5
```
which will try initial heights from the set {1, 1.5, 2, 2.5, 3}.
One can also verify that e.g. `-DX_MIN=5 -DX_MAX=10`
yields `unsat` result.
Note that the user macros (e.g. `X_MIN`)
are specific to the bouncing ball model,
and are probably not usable in other models,
which use their own specific macros.

One can also be interested in generating a formula
that contains no directives of the preprocessing language,
that is, to just preprocess a formula.
To do so, use the `-E` option. For example:
```
bin/unsot data/smto/ball/ball.smto -E -DX_MIN=2 -DX_MAX=3 -o out
```
will store the preprocessed formula into `out` file.
(To print the formula to standard output, omit the `-o out` option.)
Such formulas are, of course, still viable as a valid input to the solver,
for example:
```
bin/unsot out -X
```

All formulas of our models are preprocessed
in a similar way to Bounded Model Checking unrolling.
That is, there is always a fixed number of discrete steps,
which can be adjusted via `STEPS`, or equivalently `N`, macros.
For example:
```
bin/unsot data/smto/ball/ball.smto -X -DN=12
```
However, while it is often an important parameter in other models,
it is not interesting here in the case of the ball model.

You can experiment with other models,
for example `prostate`, `glucose`, `rail`, or `fun`,
while the file of each model is located at
`data/smto/<model>/<model>.smto`.
However, the user macros that are specific to particular models
are not documented yet.

## Documentation

Documentation content is placed inside `doc` directory
(see more details there),
including experiments and specification of the input language,
and of the preprocessing language.

### Experiments

Experiments-related information is located within the documentation,
including scripts that directly reproduce
whole series of experiments related to certain version of UN/SOT.
More information follows there.

## Contributing

Bug reports and merge requests are welcome on GitLab at
https://gitlab.com/Tomaqa/unsot.

### Development

We use GCC compiler.

Source codes are written in C++17 in Stroustrup code style,
roughly following [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines).

When the structure of source files changes,
or dependencies on header files,
one shall run `tools/dev/remake.sh` instead of plain `make`.

## License

The tool is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
