;;;; 'Linear' bouncing - simple example without ODEs
;; `x' denotes vertical position
;; `up' is discrete state

#include data/expr/stdlib.expr

;;;; Main parameters

#ifdef ONLINE
(set-option :t-suggest-strategy init)
#endif

#ifndef STEPS
#define STEPS 10
#endif

#ifndef INT
#define INT 0
#endif

#if #INT
#ifndef X_STEP
#define X_STEP 0.1
#endif
#endif

#ifndef CONSTRAINT
#define CONSTRAINT #INT
#endif

;;;; Bools definition

#set Main_bools (up)
#DECL_BOOL_CONSTS

;;;; Reals definition

#set Main_odes (x x*)
#DECL_REAL_CONSTS

#define TAU 5
#define FTAU $(* #TAU 0.35)
(define-fun tau  () Real #TAU)
(define-fun ftau () Real #FTAU)

;;; Initial conditions

(assert (and #FIRST(up)
             (= #FIRST_T 0)
             #if #INT
             #INTERVAL_STEP(#FIRST(x) 0 $(/ (+ #TAU #FTAU) 2) #X_STEP)
             #else
             (= #FIRST(x) 0)
             #endif
))

;;; Constraints

(assert (and #for (i 0 #STEPS)
             #let j $d(+ #i 1)
                 #if (< #i #STEPS)
                 (= #T_AT(#j) (+ #T_AT(#i) tau))
                 #endif
                 (ite #AT(up #i) (= #AT(x* #i) (+ #AT(x #i) ftau))
                                 (= #AT(x* #i) (- #AT(x #i) ftau))
                 )
                 (ite #AT(up #i) (> #AT(x* #i) 0)
                                 (< #AT(x* #i) tau)
                 )
             #endlet j
             #endfor
))

;;; Jump conditions

(assert (and #for (i 0 #STEPS-1)
             #let j $d(+ #i 1)
                 (ite #AT(up #i) (ite (< #AT(x* #i) tau) (and      #AT(up #j)  (= #AT(x #j) #AT(x* #i)) )
                                                         (and (not #AT(up #j)) (= #AT(x #j) tau)  ))
                                 (ite (> #AT(x* #i) 0)   (and (not #AT(up #j)) (= #AT(x #j) #AT(x* #i)) )
                                                         (and      #AT(up #j)  (= #AT(x #j) 0)    ))
                 )
             #endlet j
             #endfor
))

;;;; Final constraints

#if #CONSTRAINT
(assert (and  (> #LAST(x) 0.1) (< #LAST(x) 1.6)
))
#endif

;;; Evaluation

(check-sat)

#GET_VALUES
