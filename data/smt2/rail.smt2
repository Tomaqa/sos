;;;; Nonlinear railway example
;;;; Inspired from Rolling & Transpotting [Luteberget]
;;;; (Without ODEs)

;;; An event is if any train:
;;; * reaches max. velocity
;;; * arrives at a new node | enters new velocity restriction
;;;   -> either front or back enters new segment
;;; * velocity curve intersects braking curve

#include data/expr/rail/trains/common.expr
#include data/expr/rail/infrastructures/simple.expr
; #include data/expr/rail/infrastructures/twotrack.expr
#include data/expr/rail/schedules/simple.expr
; #include data/expr/rail/schedules/overtake.expr
#include data/expr/rail/rail.expr

(set-option :verbosity 1)

;;;; Main parameters

#ifndef STEPS
#define STEPS 1
#endif
#define STEPS-1 $d(- #STEPS 1)

;;;; Bools definition

#define MODES() (idle steady acc brake finished)

#def AUX_BOOLS()
#for (idx 0 #LAST_SCHEDULE_IDX)
#for (mode #MODES)
    #CONST(maybe_#mode #idx)
#endfor
    #CONST(will_need_brake #idx)
#endfor
#enddef
#define BOOLS #ALL_CONSTS(#MODES) #ALL_SEGMENT_BOOLS
#DECL_BOOL_CONSTS
#DECL_CONSTS(\#AUX_BOOLS Bool)

#define LAST(var idx) #CONST_AT(#var #idx #STEPS)

;;;; Reals definition

#define ODES
#define AUX_REALS() #ALL_CONSTS((x_max bdx_max fdx_max dx_max bv_max fv_max v_max next_v_max brake_dt))
#define REALS() #ALL_CONSTS((t dt x dx boffset foffset v a))
#DECL_REAL_CONSTS
#DECL_CONSTS(\#AUX_REALS Real)

#define IDLE_DT() 60

;;; Initial conditions

(assert (and #for (idx 0 #LAST_SCHEDULE_IDX)
#let sched( #nth(#idx #SCHEDULES) )
#let train( #SCHEDULE-TRAIN(#sched) )
#let start_b( #SCHEDULE-START_BOUND(#sched) )
#define const(var) #CONST_AT(#var #idx 0)
    (not #const(finished))
    (not #SEGMENT-BOOL_AT(#idx start b 0)) #SEGMENT-BOOL_AT(#idx start f 0)
    #if (not #NULL_BOUND(\#start_b))
    #let lo( #LOWER_BOUND(#start_b) )
    #let hi( #UPPER_BOUND(#start_b) )
        #if #CLOSED_BOUND(#start_b)
            #INTERVAL_STEP(#const(t) #lo #hi #IDLE_DT)
        #elif #null(#lo) #INTERVAL_STEP(#const(t) 0 #hi #IDLE_DT)
        #else #INTERVAL_STEP_COUNT(#const(t) #lo #IDLE_DT #length(#SCHEDULES))
        #endif
    #endlet hi
    #endlet lo
    #else #INTERVAL_STEP_COUNT(#const(t) 0 #IDLE_DT #length(#SCHEDULES))
    #endif
    (= #const(boffset) (- #TRAIN.LEN(#train))) (= #const(foffset) 0)
    (= #const(x) #const(foffset))
    (= #const(v) 0)
#undef const
#endlet start_b
#endlet train
#endlet sched
#endfor))

;;; Schedule constraints

(assert (and 1 #for (i 0 #STEPS) #for (idx 0 #LAST_SCHEDULE_IDX)
#let sched( #nth(#idx #SCHEDULES) )
#let train( #SCHEDULE-TRAIN(#sched) )
#let route_id( #SCHEDULE.ROUTE-ID(#sched) )
#let route( #ROUTE(#route_id) )
#define const(var) #CONST_AT(#var #idx #i)
#define consts(vars) #CONSTS_AT(#vars #idx #i)
#define seg-bool(id pos) #SEGMENT-BOOL_AT(#idx #id #pos #i)
#define seg-bools(pos) #SEGMENT-BOOLS_AT(#idx #pos #i)
    ;; The train is in exactly one mode
    #let modes( #consts(#MODES) )
    (or #modes)
    #PAIR_CONFLICT((#modes))
    #endlet modes
    ;; The train is always in some place until it finished
    (= (not #const(finished)) (or #seg-bools(b) #seg-bools(f)))
    ;; If the front is already finished, falsify the 'next' bools too
    (=> (not (or #seg-bools(f))) (not (or #seg-bools(next))) )
#for (pos #POSITIONS)
    ;; The train cannot be in more places at once
    #PAIR_CONFLICT((#seg-bools(#pos)))
    #for (seg #SEGMENTS)
    #let id #SEGMENT.ID(#seg)
    #if (not #equal(#pos next))
        ;; Next segment must differ from the current one
        #PAIR_CONFLICT(( #seg-bool(#id #pos) #seg-bool(#id next) ))
    #else
    #let adjs( #ADJ_SEGMENTS_ON_ROUTE(#id #route_id) )
        ;; Choose one of next segments
        #if (not #null(#adjs))
        (=> #seg-bool(#id f) (or
            #for (seg2 #adjs)
                #seg-bool(#SEGMENT.ID(#seg2) #pos)
            #endfor
        ))
        #endif
        ;; All segment bools must follow the order of POSITIONS
        #for (next_pos #nextcdr(#pos #POSITIONS))
        #for (seg2 #adjs)
            #PAIR_CONFLICT(( #seg-bool(#id #next_pos) #seg-bool(#SEGMENT.ID(#seg2) #pos) ))
        #endfor
        #endfor
    #endlet adjs
    #endif
    #endlet id
    #endfor
#endfor

    ;; Real boundaries
    (>= #const(t) 0)
    (>= #const(x) 0) #<=(#const(x) #MAX_X)
    #>=(#const(f##offset) 0)
    #let id #ROUTE-START_SEGMENT-ID(#route)
    (or (and (not #seg-bool(#id b)) #seg-bool(#id f))
        #>=(#const(b##offset) 0)
    )
    #endlet id
    #>=(#const(v) 0) #<=(#const(v) #MAX_V)
    #if (< #i #STEPS)
        (= (not (or #const(idle) #const(finished)))
           (and #>(#const(dt) 0) #>(#const(dx) 0)
        ))
    #else
        (= #const(dt) -1) (= #const(dx) -1) (= #const(brake_dt) -1)
    #endif
    (= (or #const(idle) #const(steady) #const(finished)) (= #const(a) 0))
    (= #const(acc) (= #const(a) #TRAIN.A(#train)))
    (= #const(brake) (= #const(a) (- #TRAIN.B(#train))))
#for (seg #SEGMENTS)
#let id #SEGMENT.ID(#seg)
#let len #SEGMENT.LEN(#seg)
#define v_max(seg) $(min #SEGMENT.V_MAX(#seg) #TRAIN.V_MAX(#train))
    ;; Segment limits
    #for (pos (b f))
        (=> #seg-bool(#id #pos)
            (and (= #const(#pos##v_max) #v_max(#seg))
                 #<=(#const(#pos##offset) #len)
                 (= #const(#pos##dx_max) (- #len #const(#pos##offset)))
        ))
    #endfor
    ;; Require scheduled times
    #if (and (> #i 0) (not #equal(#id #ROUTE-START_SEGMENT-ID(#route))))
    #let sch_bound( #SCHEDULE-BOUND(#sched #id) )
        #if (not #NULL_BOUND(\#sch_bound))
        #let lo( #LOWER_BOUND(#sch_bound) )
        #let hi( #UPPER_BOUND(#sch_bound) )
            (=> (and (not #SEGMENT-BOOL_AT(#idx #id b $d(- #i 1))) #seg-bool(#id b))
                (and #if (not #null(#lo)) #>=(#const(t) #lo) #endif
                     #if (not #null(#hi)) #<=(#const(t) #hi) #endif
            ))
        #endlet hi
        #endlet lo
        #endif
    #endlet sch_bound
    #endif
    ;; The case of initial moving the back inside the model
    #if #equal(#id #ROUTE-START_SEGMENT-ID(#route))
    (=> (and (not #seg-bool(#id b)) #seg-bool(#id f))
        (and (= #const(bv_max) #const(fv_max))
             #<(#const(boffset) 0)
             (= #const(bdx_max) (- #const(boffset)))
    ))
    #endif
    #let adjs( #ADJ_SEGMENTS_ON_ROUTE(#id #route_id) )
    #if (not #null(#adjs))
    #for (seg2 #adjs)
    #let id2 #SEGMENT.ID(#seg2)
        (=> (and #seg-bool(#id f) #seg-bool(#id2 next))
            (= #const(next_v_max) #v_max(#seg2))
        )
    #endlet id2
    #endfor
    #else
    ;; Leave the final max. velocity limit unchanged
    (=> #seg-bool(#id f) (= #const(next_v_max) #const(fv_max)) )
    ;; The case of the front finally leaving the model
    (=> (and #seg-bool(#id b) (not #seg-bool(#id f)))
        (and (= #const(fv_max) #const(bv_max))
             (= #const(fdx_max) #const(bdx_max))
             (= #const(next_v_max) #const(fv_max))
    ))
    #endif
    #endlet adjs
#undef v_max
#endlet len
#endlet id
#endfor
#for (pos (b f))
    (=> #const(finished)
        (and (= #const(#pos##v_max) 0) (= #const(next_v_max) 0)
             (= #const(#pos##dx_max) 0)
    ))
#endfor

    ;; Boundaries of auxiliary variables
    (= #const(x_max) (+ #const(x) #const(dx_max)))
    (= #const(dx_max) (min #const(bdx_max) #const(fdx_max)))
    (= #const(v_max) (min #const(bv_max) #const(fv_max)))
    (=> (not #const(finished)) (and
        #<=(#const(dx) #const(dx_max))
        #<=(#const(v) #const(v_max))
    ))

    ;; Closer definition of modes
    (= #const(maybe_idle) (= #const(v) 0))
    (= #const(maybe_steady) (= #const(v) #const(v_max)))
    (= #const(maybe_acc) #<(#const(v) #const(v_max)))
    (= #const(maybe_brake) #>(#const(v) 0))
    #PAIR_CONFLICT((#const(maybe_idle) #const(maybe_steady)))
    #PAIR_CONFLICT((#const(maybe_steady) #const(maybe_acc)))
    (xor #const(maybe_idle) #const(maybe_brake))
    (xor #const(finished) (or #const(maybe_idle) #const(maybe_steady) #const(maybe_acc)))
    (=> (not #const(maybe_idle)) (not #const(idle)))
    (=> (not #const(maybe_steady)) (not #const(steady)))
    (=> (not #const(maybe_acc)) (not #const(acc)))
    (=> (not #const(maybe_brake)) (not #const(brake)))
    ;+ Slows down ...
    ;+ (=> #const(maybe_idle) (or #const(idle) #const(acc)))
    ;+ (=> #const(maybe_steady) (or #const(steady) #const(brake)))
    ;+ (=> #const(maybe_acc) (not #const(steady)))
    ;+ (=> #const(maybe_brake) (not #const(idle)))

    (= #const(will_need_brake) #>(#const(v) #const(next_v_max)))
    #PAIR_CONFLICT((#const(will_need_brake) #const(maybe_idle)))
#undef seg-bools
#undef seg-bool
#undef consts
#undef const
#endlet route
#endlet route_id
#endlet train
#endlet sched
#endfor
#endfor))

;;; Segment constraints

(assert (and 1 #for (seg #SEGMENTS)
#let id #SEGMENT.ID(#seg)
#for (idx1 0 #LAST_SCHEDULE_IDX)
#for (i1 0 #STEPS-1)
#let j1 $d(+ #i1 1)
#let int1( (#CONST_AT(t #idx1 #i1) #CONST_AT(t #idx1 #j1)) )
#for (idx2 $d(+ #idx1 1) #LAST_SCHEDULE_IDX)
#for (i2 0 #STEPS-1)
#let j2 $d(+ #i2 1)
#let int2( (#CONST_AT(t #idx2 #i2) #CONST_AT(t #idx2 #j2)) )
#let overlap( #INTERVAL_OVERLAP(#int1 #int2) )
#for (pos1 #POSITIONS) #for (pos2 #POSITIONS)
    (=> #overlap
        #PAIR_CONFLICT((#SEGMENT-BOOL_AT(#idx1 #id #pos1 #i1) #SEGMENT-BOOL_AT(#idx2 #id #pos2 #i2)))
    )
#endfor #endfor
#endlet overlap
#endlet int2
#endlet j2
#endfor
#endfor
#endlet int1
#endlet j1
#endfor
#endfor
#endlet id
#endfor))


;; v = v0 - bt
#define eval_brake-dt(v) (/ (- #v0 #v) #b)
;; x = x0 + v0t - 0.5bt^2 // x <- x_max
#define brake_x_bound-dt() (/ (- #v0 (sqrt (max 0 (- (^ #v0 2) (* 2 #b #dx_max))))) #b)

;; x = x0 + vt // x <- x_max, v const.
#define steady_x_bound-dt() (/ #dx_max #v0)
;; x = x0 + vt & x2 = x + vt2 - 0.5bt2^2 // t2 <- eval_brake-dt, x2 <- fdx_max
#define steady_brake_bound-dt() (/ (- #fdx_max (* #eval_brake-dt(#next_v_max) (- #v0 (* 0.5 #b #eval_brake-dt(#next_v_max))))) #v0)

;; v = v0 + at
#define acc_v_bound-dt(v) (/ (- #v #v0) #a)
;; x = x0 + v0t + 0.5at^2 // x <- x_max
#define acc_x_bound-dt() (/ (- (sqrt (+ (^ #v0 2) (* 2 #a #dx_max))) #v0) #a)
;; x = x0 + v0t + 0.5at^2 & x2 = x + vt2 - 0.5bt2^2 & v = v0 + at & v2 = v - bt2 // x2 <- fdx_max, v2 <- next_v_max
#def acc_brake_bound-dt()
#let down $(* #b (+ #a #b))
#let urhs( (* #next_v_max $(+ #a #b)) )
#let ulhs( (sqrt (max 0 (+ (^ #urhs 2) (* #down (- (* 2 #a #fdx_max) (- (^ #next_v_max 2) (^ #v0 2))))))) )
#let up( (- #ulhs #urhs) )
#let dtb( (/ #up #down) )
    (+ #acc_v_bound-dt(#next_v_max) (* $(/ #b #a) #dtb))
#endlet dtb
#endlet up
#endlet ulhs
#endlet urhs
#endlet down
#enddef


;;; Jump conditions

(assert (and 1 #for (i 0 #STEPS-1)
#let j $d(+ #i 1)
#for (idx 0 #LAST_SCHEDULE_IDX)
#let sched( #nth(#idx #SCHEDULES) )
#let train( #SCHEDULE-TRAIN(#sched) )
#let route_id( #SCHEDULE.ROUTE-ID(#sched) )
#let route( #ROUTE(#route_id) )
#define const_at(var i) #CONST_AT(#var #idx #i)
#define seg-bool_at(id pos i) #SEGMENT-BOOL_AT(#idx #id #pos #i)
    #for (seg #SEGMENTS)
    #let id #SEGMENT.ID(#seg)
        #for (pos (b f))
        #let next_pos #next(#pos #POSITIONS)
            ;; Must have either stayed or transfered to next segment
            (=> #seg-bool_at(#id #pos #j) (or #seg-bool_at(#id #pos #i) #seg-bool_at(#id #next_pos #i)))
            ;; Must either stay or transfer to next segment
            (=> #seg-bool_at(#id #next_pos #i) (or #seg-bool_at(#id #pos #j) #seg-bool_at(#id #next_pos #j)))
        #endlet next_pos
        #endfor
    #endlet id
    #endfor

    #for (var (t x))
        (= #const_at(#var #j) (+ #const_at(#var #i) #const_at(d#var #i)))
    #endfor

;; Set deltas
#let a #TRAIN.A(#train)
#let b #TRAIN.B(#train)
#let v0 #const_at(v #i)
#let next_v_max #const_at(next_v_max #i)
#let dx_max #const_at(dx_max #i)
#let fdx_max #const_at(fdx_max #i)
#let dt #const_at(dt #i)
#let brake_dt #const_at(brake_dt #i)
    ;; Restrict mode jumps
    (=> #const_at(idle #i) (or #const_at(idle #j) #const_at(acc #j)))
    (=> #const_at(steady #i) (not #const_at(idle #j)))
    (=> #const_at(acc #i) (not #const_at(idle #j)))
    (=> #const_at(brake #i) (not #const_at(finished #j)))
    (=> #const_at(finished #i) #const_at(finished #j))
    ;; Braking rules
    (ite (or #const_at(finished #i) (not (or #const_at(will_need_brake #i) #const_at(acc #i) #const_at(brake #i))))
         (= #brake_dt -1)
         (and (=> #const_at(steady #i) (= #brake_dt #steady_brake_bound-dt))
              (=> #const_at(acc #i) (= #brake_dt #acc_brake_bound-dt))
              (=> #const_at(brake #i)
                  (ite #const_at(will_need_brake #i) (= #brake_dt #eval_brake-dt(#next_v_max))
                                                     (= #brake_dt #eval_brake-dt(0))
              ))
              ;; The need for braking will not change if ...
              (=> (xor #const_at(will_need_brake #i) #const_at(brake #i))
                  (= #const_at(will_need_brake #j) #const_at(will_need_brake #i))
              )
    ))

    (=> #const_at(idle #i) (= #dt #IDLE_DT))
    (=> #const_at(steady #i) (ite #const_at(will_need_brake #i)
        ;; For sake of the back of the train
        (= #dt (min #steady_x_bound-dt #brake_dt))
        (= #dt #steady_x_bound-dt)
    ))
    (=> #const_at(acc #i) (= #dt (min #acc_v_bound-dt(#const_at(v_max #i)) #acc_x_bound-dt #brake_dt)))
    (=> #const_at(brake #i) (= #dt (min #brake_x_bound-dt #brake_dt)))
    (=> #const_at(finished #i) (= #dt 0))
    (= #const_at(v #j) (+ #v0 (* #const_at(a #i) #dt)))
    (= #const_at(dx #i) (* 0.5 (+ #v0 #const_at(v #j)) #dt))
#endlet brake_dt
#endlet dt
#endlet fdx_max
#endlet dx_max
#endlet next_v_max
#endlet v0
#endlet b
#endlet a

;; Set offsets and segment positions
#for (seg #SEGMENTS)
#let id #SEGMENT.ID(#seg)
#let len #SEGMENT.LEN(#seg)
#for (pos (b f))
#let next_pos #next(#pos #POSITIONS)
#let new_offset( (+ #const_at(#pos##offset #i) #const_at(dx #i)) )
#let offset #const_at(#pos##offset #j)
#let adjs( #ADJ_SEGMENTS_ON_ROUTE(#id #route_id) )
    ;; Set offset
    (=> #seg-bool_at(#id #pos #i)
        (ite #<(#new_offset #len) (= #offset #new_offset) (= #offset 0)
    ))
    #for (seg2 #adjs)
    #let id2 #SEGMENT.ID(#seg2)
        ;; Possibly transfer to adjacent segment
        (=> (and #seg-bool_at(#id #pos #i) #seg-bool_at(#id2 #next_pos #i))
            (ite #<(#new_offset #len) #seg-bool_at(#id #pos #j)
                                      #seg-bool_at(#id2 #pos #j)
        ))
    #endlet id2
    #endfor
    #if #equal(#pos b)
    ;; Possibly stay within the same segment
    (=> (and #seg-bool_at(#id #pos #i) #seg-bool_at(#id #next_pos #i))
        #seg-bool_at(#id #pos #j)
    )
        ;; Possibly transfer the back into the start of the model
        #if #equal(#id #ROUTE-START_SEGMENT-ID(#route))
        (=> (and (not #seg-bool_at(#id #pos #i)) #seg-bool_at(#id #next_pos #i))
            (and (= #seg-bool_at(#id #pos #j) #>=(#new_offset 0))
                 (= #offset #new_offset)
        ))
        #endif
        #if #null(#adjs)
        ;; Stop condition for the back at the end
        (=> (and #seg-bool_at(#id #pos #i) (not #seg-bool_at(#id #next_pos #i)))
            (= #seg-bool_at(#id #pos #j) #<(#new_offset #len))
        )
        #endif
    #elif #null(#adjs)
    #let prev_pos( #prev(#pos #POSITIONS) )
        ;; Stop condition for the front before the end
        (=> #seg-bool_at(#id #pos #i)
            (= #seg-bool_at(#id #pos #j) #<(#new_offset #len))
        )
        ;; Stop condition for the front at the end
        (=> (and #seg-bool_at(#id #prev_pos #i) (not #seg-bool_at(#id #pos #i)))
            (and (not #seg-bool_at(#id #pos #j)) (= #offset #new_offset)
        ))
    #endlet prev_pos
    #endif
#endlet adjs
#endlet offset
#endlet new_offset
#endlet next_pos
#endfor
#endlet len
#endlet id
#endfor
;; Offsets stay the same
#for (pos (b f))
    (=> #const_at(finished #i) (= #const_at(#pos##offset #j) #const_at(#pos##offset #i)) )
#endfor

#undef seg-bool_at
#undef const_at
#endlet route
#endlet route_id
#endlet train
#endlet sched
#endfor
#endlet j
#endfor))

;;;; Goals

(assert (and 1 #for (idx 0 #LAST_SCHEDULE_IDX)
#let sched( #nth(#idx #SCHEDULES) )
#let route( #SCHEDULE-ROUTE(#sched) )
    #CONST_AT(acc #idx 0)
    #LAST(finished #idx)
#endlet route
#endlet sched
#endfor))

;;; Evaluation

(check-sat)

#GET_VALUES
