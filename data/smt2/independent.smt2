#ifndef N
#define N 10
#endif

#include data/expr/stdlib.expr

#ifdef ONLINE
(set-option :t-suggest-strategy init)
#endif

#ifndef M
#define M 2
#endif

#ifndef L
#define L 3
#endif
#define L-1 $d(- #L 1)

#ifndef UNSAT
#define UNSAT 0
#endif

#for (v 1 #M)
#for (i 1 #L)
(declare-const #AT(x#v #i) Real)
#endfor
#endfor

(assert (and  #for (v 1 #M)
                  #INTERVAL_COUNT(#AT(x#v 1) 0 #N $(+ #N 1))
              #endfor
              #for (i 1 #L-1)
              #for (v 1 #M)
                  (or (= #AT(x#v $d(+ #i 1)) (+ #AT(x#v #i) 1))
                      (= #AT(x#v $d(+ #i 1)) (+ #AT(x#v #i) 0))
                  )
              #endfor
              #endfor
              #if #UNSAT
              (< (+ #for (v 1 #M) #AT(x#v #L) #endfor) 0)
              #else
              (>= (+ #for (v 1 #M) #AT(x#v #L) #endfor) $d(* #N #M))
              #endif
))

(check-sat)
(get-model)
