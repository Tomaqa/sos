#ifndef N
#define N 2
#endif

#ifndef L
#define L 3
#endif

#define sat 0
#define unsat_first 1
#define unsat_last 2
#define unsat_all 3

#ifndef UNSAT
#define UNSAT #unsat_first
#endif

#for (i 1 #L)
(declare-const x#i Real)
#endfor

#define MAX_N_OF(i) $d(+ #N #i -1)

(assert (and  #for (i 1 #L) (or
              #for (n #i #MAX_N_OF(#i))
                  (= x#i #n)
              #endfor
                  #if (< #i #L)
                  (= x#i (- x$d(+ #i 1) 1))
                  #endif
              ) #endfor
              #if (= #UNSAT #unsat_last)
              (~ x#L 0)
              #elif (= #UNSAT #unsat_first)
              (~ x1 0)
              #elif (= #UNSAT #unsat_all)
              (~ (+ #for (i 1 #L) x#i #endfor) 0)
              #else
              #for (i 1 #L)
              (~ x#i $d(+ #N #i -1))
              #endfor
              #endif
))

(check-sat)
