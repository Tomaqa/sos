;;;; Railway example with ODEs and synchronized time for all trains
;;;; Inspired from Rolling & Transpotting [Luteberget]

;;; An event is if for any train:
;;; * max. velocity of the current or next segment is reached
;;; * new segment is entered by either front or back
;;; * braking curve intersects the position curve

#define INFRA twotrack

#include data/expr/rail/rail.expr

;;;; Main parameters

#ifndef STEPS
#define STEPS 1
#endif

;;;; Bools definition

#set Main_bools (#ALL_CONSTS((enter)) #ALL_SEGMENT-BOOLS_ALL_POS)
#DECL_BOOL_CONSTS

;;;; Initial and final conditions

#define const(var) #FIRST(#CONST(#var #idx))
#define last_const(var) #LAST(#CONST(#var #idx))

(assert (and #for (conn #connections)
#let ((idx #object-idx(#conn)))
    #const(enter)
#endlet
#endfor))

#undef const

;;;; Connection constraints

#let (idx train route dir
      adjs start
      )
#define const(var) #AT(#CONST(#var #idx) #i)
#define seg-bool(seg pos) #AT(#SEGMENT-BOOL(#seg #idx #pos) #i)
#define seg-bools(pos) #AT((#SEGMENT-BOOLS(#idx #pos)) #i)

;;; Step-local constraints
(assert (and 1 #for (i #FIRST_STEP #LAST_STEP)

;;; Connection-local constraints
#for (conn #connections)
#set idx #object-idx(#conn)
#set train #.!(#conn train)
#set route #.!(#conn route)
#set dir #.!(#conn dir)

;; Segment bool constraints
#for (pos #Positions)
    ;; The train cannot be in more places at once
    #PAIR_CONFLICT(#seg-bools(#pos))
    #for (seg #segments)
    #set adjs #ADJ_SEGMENTS_ON_ROUTE(#seg #route #dir)
    #if (not #equal(#pos next))
        ;; Next segment must differ from the current one
        #PAIR_CONFLICT(( #seg-bool(#seg #pos) #seg-bool(#seg next) ))
    #else
        ;; Choose one of next segments
        #if (not #null(#adjs))
        (=> #seg-bool(#seg f)
            (or #for (seg2 #adjs) #seg-bool(#seg2 next) #endfor) )
        #endif
        ;; All segment bools must follow the order of POSITIONS
        #for (next_pos #nextcdr(#pos #Positions))
        #for (seg2 #adjs)
            #PAIR_CONFLICT(( #seg-bool(#seg #next_pos) #seg-bool(#seg2 #pos) ))
        #endfor
        #endfor
    #endif
        (= #seg-bool(#seg b) #seg-bool(#seg f) )
    #endfor
#endfor

;; Entering the model
#set start #CONNECTION-START_NODE(#conn)
    (=> #const(enter) (or #for (seg #INC_SEGMENTS_ON_ROUTE(#start #route #dir))
        (and #seg-bool(#seg b) #seg-bool(#seg f))
    #endfor))
#endfor ;; conn

#endfor)) ;;; i

;;; Jump constraints
#define final_const(var) #AT(#CONST(#var #idx) #j)
#define final_seg-bool(seg pos) #AT(#SEGMENT-BOOL(#seg #idx #pos) #j)
(assert (and 1 #for (i #FIRST_STEP #LAST_STEP-1)
#let ( (j $d(+ #i 1)) )
    (=> #const(enter) (not #final_const(enter)) )

    #for (seg #segments)
        (=> #seg-bool(#seg next) #final_seg-bool(#seg f) )
    #endfor
#endlet
#endfor)) ;;; i

#undef seg-bools
#undef seg-bool
#undef final_seg-bool
#undef const
#undef final_const
#endlet


;;;; Evaluation

(check-sat)

; #GET_VALUES
(get-model)
