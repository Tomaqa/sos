#pragma once

#include "unsot.hpp"

namespace unsot::util {
    enum class Side { left, right, up, down };

    template <typename T> class Optional;
    class Flag;

    template <typename T, template<typename...> typename ConstraintT = Void>
        class Wrap;

    struct Priority;

    /// It does *not* include definitions of iterator types
    template <typename ValueT, typename SizeT = size_t, typename KeyT = void>
        struct Container_types;

    class C_str;
    struct String_view;

    template <typename T> struct Unique_ptr;
    template <typename T> struct Shared_ptr;

    template <typename... Args> class Union;

    template <typename T> class Ref;

    template <typename B> class Lock_mixin;
    using Lock = Lock_mixin<Empty>;

    template <typename B, typename ValueT> class Associative_mixin;
    template <typename B> class Map_mixin;

    template <typename KeyT> class Set;
    template <typename KeyT> class Multiset;
    template <typename KeyT, typename ValueT> class Map;

    template <typename KeyT, typename ValueT = void, typename HashT = std::hash<KeyT>>
        class Hash;
    //+ template <typename T> class Hash_object;

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T, template<typename...> typename Map2T = Map1T>
        class Bimap;

    /// Either container or pointer type can be used
    template <typename T, template<typename...> typename AccCont = Vector> class View;
    template <typename T, template<typename...> typename AccCont = Vector> class Unique_view;

    template <typename F> class Scope_guard;

    struct Path;
    class Run;

    using Mask = Vector<bool>;

    namespace aux {
        template <typename T> using Is_integral = If<is_integral_v<T>>;
    }

    using Idx = Wrap<int, aux::Is_integral>;

    namespace aux {
        template <typename ContT, typename T = typename Decay<ContT>::value_type,
                  typename = decltype(declval<ContT>().contains(declval<T>()))>
            using Has_contains = void;
        template <typename ContT,
                  typename = decltype(declval<ContT>().reserve(0))>
            using Has_reserve = void;
        template <typename ContT,
                  typename = decltype(declval<ContT>().resize(0))>
            using Has_resize = void;
        template <typename ContT, typename T = typename Decay<ContT>::value_type,
                  typename = decltype(declval<ContT>().push_back(declval<T>()))>
            using Has_push_back = void;
    }

    template <typename T>
        constexpr bool is_integral_v = unsot::is_integral_v<T> || is_same_v<T, Idx>;

    template <typename ContT>
        constexpr bool enabled_contains_v = enabled_v<aux::Has_contains, ContT>;
    template <typename ContT>
        constexpr bool enabled_reserve_v = enabled_v<aux::Has_reserve, ContT>;
    template <typename ContT>
        constexpr bool enabled_resize_v = enabled_v<aux::Has_resize, ContT>;
    template <typename ContT>
        constexpr bool enabled_push_back_v = enabled_v<aux::Has_push_back, ContT>;

    template <typename UnConvF, typename T>
        constexpr bool is_unary_conv_f_v = is_invocable_v<UnConvF, Add_lref<T>>
                                        || is_invocable_v<UnConvF, Add_rref<T>>;
    template <typename BinConvF, typename T, typename U = T>
        constexpr bool is_binary_conv_f_v = is_invocable_v<BinConvF, Add_lref<T>, Add_lref<U>>
                                         || is_invocable_v<BinConvF, Add_lref<T>, Add_rref<U>>
                                         || is_invocable_v<BinConvF, Add_rref<T>, Add_lref<U>>
                                         || is_invocable_v<BinConvF, Add_rref<T>, Add_rref<U>>;

    constexpr auto nop = [](auto&&...){};
    using Nop = decltype(nop);

    constexpr auto fwd = [](auto&& arg) -> auto&& { return FORWARD(arg); };
    using Fwd = decltype(fwd);

    static_assert(is_unary_conv_f_v<Fwd, int>);
    static_assert(is_unary_conv_f_v<Fwd, int&>);
    static_assert(is_unary_conv_f_v<Fwd, int&&>);

    template <typename RetT>
        constexpr auto ret_def = [](auto&&...) -> RetT { return {}; };
    template <typename RetT> using Ret_def = decltype(ret_def<RetT>);

    constexpr auto true_pred = [](auto&&...){ return true; };
    constexpr auto false_pred = [](auto&&...){ return false; };
    using True_pred = decltype(true_pred);
    using False_pred = decltype(false_pred);

    template <typename ItT> auto to_pointer(ItT) noexcept;

    template <typename ContT> typename ContT::iterator
        to_iterator(typename ContT::const_iterator) noexcept;

    void mask(Mask&, Idx);
    void unmask(Mask&, Idx);
    inline bool masked(const Mask&, Idx);
}

namespace unsot::util {
    template <typename T, template<typename...> typename ConstraintT>
    class Wrap : public Static<Wrap<T, ConstraintT>> {
    public:
        using Base = T;

        Wrap()                                                                            = default;
        ~Wrap()                                                                           = default;
        Wrap(const Wrap&)                                                                 = default;
        Wrap& operator =(const Wrap&)                                                     = default;
        Wrap(Wrap&&)                                                                      = default;
        Wrap& operator =(Wrap&&)                                                          = default;
        template <typename U = Base, typename = ConstraintT<U>>
            constexpr Wrap(Base val_)                                         : _val(move(val_)) { }

        constexpr const auto& cbase() const noexcept                                { return _val; }
        constexpr const auto& base() const& noexcept                             { return cbase(); }
        constexpr auto& base()& noexcept                                            { return _val; }
        constexpr auto base()&& noexcept                                    { return move(base()); }

        constexpr operator const Base&() const& noexcept                         { return cbase(); }
        constexpr operator Base&()& noexcept                                      { return base(); }
        constexpr operator Base()&& noexcept                          { return move(*this).base(); }

        String to_string() const                                  { return unsot::to_string(_val); }

        constexpr bool equals(const Wrap&) const noexcept;
        constexpr bool less(const Wrap&) const noexcept;
    private:
        Base _val{};
    };

    struct Priority : Inherit<Wrap<unsigned char, aux::Is_integral>> {
        static constexpr Base min = 0;
        static constexpr Base low = 10;
        static constexpr Base medium = 20;
        static constexpr Base high = 30;
        static constexpr Base max = Base(~0U);

        using Inherit::Inherit;
    };

    template <typename ValueT, typename SizeT, typename KeyT>
    struct Container_types : Container_types<ValueT, SizeT> {
        using key_type = KeyT;
    };

    template <typename ValueT, typename SizeT>
    struct Container_types<ValueT, SizeT, void> {
        using value_type = ValueT;
        using reference = value_type&;
        using const_reference = const value_type&;
        using pointer = value_type*;
        using const_pointer = const value_type*;
        using size_type = SizeT;
    };

    template <typename B>
    class Lock_mixin : public Inherit<B, Lock_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Lock_mixin<B>>;

        using Inherit::Inherit;

        bool locked() const noexcept                                             { return _locked; }
        bool unlocked() const noexcept                                         { return !locked(); }
        void lock() noexcept                                                     { _locked = true; }
        void unlock() noexcept                                                  { _locked = false; }
    private:
        bool _locked{};
    };

    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator +(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator -(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator *(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator /(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator %(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator &(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator |(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator ^(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator <<(Idx, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx operator >>(Idx, T);
    template <typename T, Req<unsot::is_integral_v<T>> = 0>
        constexpr Idx operator +(T, Idx);
    template <typename T, Req<unsot::is_integral_v<T>> = 0>
        constexpr Idx operator -(T, Idx);
    template <typename T, Req<unsot::is_integral_v<T>> = 0>
        constexpr Idx operator *(T, Idx);
    template <typename T, Req<unsot::is_integral_v<T>> = 0>
        constexpr Idx operator ^(T, Idx);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator +=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator -=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator *=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator /=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator %=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator &=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator |=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator ^=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator <<=(Idx&, T);
    template <typename T, Req<is_integral_v<T>> = 0>
        constexpr Idx& operator >>=(Idx&, T);
    constexpr Idx& operator ++(Idx&);
    constexpr Idx operator ++(Idx&, int);
    constexpr Idx& operator --(Idx&);
    constexpr Idx operator --(Idx&, int);
}

namespace std {
    template <typename T, template<typename...> typename ConstraintT>
    struct hash<unsot::util::Wrap<T, ConstraintT>> {
        std::size_t operator ()(const unsot::util::Wrap<T, ConstraintT>& in) const
                                                              { return invoke(std::hash<T>(), in); }
    };
}

#include "util/ptr.hpp"

#include "util.inl"

namespace unsot::util {
    constexpr Idx invalid_idx = -1;
}
