#pragma once

#include "smt/solver/bools.hpp"
#include "smt/solver/reals.hpp"
#include "ode/solver.hpp"

#include "util.hpp"
#include "expr.hpp"

namespace unsot::solver {
    using namespace util;
    using namespace smt::solver;
    namespace bools = smt::solver::bools;

    using expr::Key;
    using expr::Key_id;

    using expr::Real;
    using expr::Reals;

    using expr::Bool;
    using expr::Bools;

    using expr::List;
    using expr::Formula;

    using smt::Sat;

    using ode::Flow;

    template <typename B, typename OdeSolver> class Mixin;
}

//+ I believe it would be better if they were nested classes of the `Mixin`
//+ (i.e. wrt. accessing protected members)
#include "solver/flow.hpp"
#include "solver/var.hpp"

namespace unsot::solver {
    template <typename B, typename OdeSolver>
    class Mixin : public Inherit<B, Mixin<B, OdeSolver>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B, OdeSolver>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Unsot_t = This;

        class Check_sat;

        class Run;

        using Ode_solver = OdeSolver;

        using Flow_inst = flow::Inst<That>;
        using Flow_insts = flow::Insts<That>;
        using Flow_insts_ptr = flow::Insts_ptr<That>;

        static_assert(is_same_v<That, typename Flow_inst::Solver>);

        using typename Inherit::Reals;

        using Invariant = var::Invariant<Flow_inst>;
        using Ode = var::Ode<Flow_inst>;
        using Final_var = var::Final<Reals::Types::var_type>;

        static_assert(is_same_v<Flow_insts, typename Ode::Flow_insts>);
        static_assert(is_same_v<Flow_insts_ptr, typename Ode::Flow_insts_ptr>);

        using Inherit::Inherit;
        Mixin();
        virtual ~Mixin()                                                                  = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&)                                                                    = default;
        Mixin& operator =(Mixin&&)                                                        = default;

        const auto& code_solver() const noexcept                             { return _ode_solver; }
        const auto& ode_solver() const noexcept                            { return code_solver(); }
        auto& ode_solver() noexcept                                          { return _ode_solver; }

        //+ should be sufficient to be only in `Check_sat`
        bool required_flow_progress() const noexcept             { return _required_flow_progress; }
        void require_flow_progress() noexcept                    { _required_flow_progress = true; }
        void relax_flow_progress() noexcept                     { _required_flow_progress = false; }

        virtual void define_flow(Key, Formula, expr::Keys ode_keys,
                                 expr::Keys real_arg_keys = {}, expr::Keys bool_arg_keys = {});

        virtual Formula expand_def_flow(const Key&, const expr::Keys& init_vals,
                                        const expr::Keys& real_arg_vals = {},
                                        const expr::Keys& bool_arg_vals = {});
        virtual void assert_def_flow(const Key&, const expr::Keys& init_vals,
                                     const expr::Keys& real_arg_vals = {},
                                     const expr::Keys& bool_arg_vals = {});
    protected:
        friend flow::Def;
        friend Flow_inst;

        class Parser;

        using typename Inherit::Fun_def;
        using typename Inherit::Fun_inst;

        using Inherit::do_check;
        using Inherit::allow_dups;

        const auto& cflow_defs() const noexcept                               { return _flow_defs; }
        auto& flow_defs() noexcept                                            { return _flow_defs; }
        const auto& cflow_ids_map() const noexcept                         { return _flow_ids_map; }
        auto& flow_ids_map() noexcept                                      { return _flow_ids_map; }
        const auto& cflow_def(const flow::Id&) const noexcept;
        auto& flow_def(const flow::Id&) noexcept;
        const auto& cflow_def(const Key&) const;
        auto& flow_def(const Key&) noexcept;
        const auto& cflow_id(const Key&) const;
        auto& flow_id(const Key&) noexcept;

        const auto& cflow_insts_ptr() const noexcept                     { return _flow_insts_ptr; }
        auto& flow_insts_ptr() noexcept                                  { return _flow_insts_ptr; }
        const auto& cflow_insts() const noexcept                      { return *cflow_insts_ptr(); }
        auto& flow_insts() noexcept                                    { return *flow_insts_ptr(); }
        const auto& cflow_inst(const flow::Inst_id&) const;
        auto& flow_inst(const flow::Inst_id&);

        const auto& cfinal_var_ptr(const Key& ode_key, const Flow_inst&) const;

        bool is_aux_var(const Key&) const noexcept override;

        bool valid_option_value(const Option&, const Option_value&) const override;
        void apply_option(const Option&) override;

        virtual void declare_final_var(Key);
        virtual void declare_invariant(Key, Key ode_key, const Flow_inst&);

        virtual void define_ode(const Key& ode_key, Key fun_key, const flow::Id&);

        virtual flow::Id add_flow_def(flow::Def, Flow);

        virtual Key instantiate_ode_fun_inst(Fun_inst, Key ode_key,
                                             var::Id final_vid, flow::Inst_id);

        virtual Key instantiate_def_ode(const Key& fun_key, const Key& ode_key, const Flow_inst&);

        void init_solve() override;

        String var_stats_to_string() const override;
    private:
        Ode_solver _ode_solver{};

        flow::Defs _flow_defs{};
        flow::Ids_map _flow_ids_map{};
        Flow_insts_ptr _flow_insts_ptr{flow::new_insts<Flow_insts_ptr>()};

        bool _required_flow_progress{};
    };
}

#include "solver/check_sat.hpp"
#include "solver/parser.hpp"

#include "solver.inl"
