#pragma once

namespace unsot::ode::flow::aux {
    class Flow::Timer : public Static<Flow::Timer> {
    public:
        friend Parse;

        Timer()                                                     = default;
        ~Timer()                                                    = default;
        Timer(const Timer&)                                         = default;
        Timer& operator =(const Timer&)                             = default;
        Timer(Timer&&)                                              = default;
        Timer& operator =(Timer&&)                                  = default;
        explicit Timer(const Keys&);
        Timer(const Keys&, Formula tau_phi, Formula t_phi = {});
        Timer(const Keys&, Formulas tau_phis, Formulas t_phis = {});

        //+ Real ct_init() const                           { return _t_init; }
        //+ Real ct_end() const                             { return _t_end; }
        //+ Time time_len() const noexcept;

        bool valid_ids(const Id& tau_id, const Id& t_id) const noexcept;
        void check_ids(const Id& tau_id, const Id& t_id) const;

        bool operator ()(const Reals& state,
                         const Id& tau_id = def_id,
                         const Id& t_id = def_id) const;

        String to_string() const&;

        bool equals(const This&) const noexcept;
    protected:
        //+ static constexpr Real max_t_end = 1e9;

        const auto& ctau_invs() const noexcept           { return _tau_invs; }
        auto& tau_invs() noexcept                        { return _tau_invs; }
        const auto& ct_invs() const noexcept               { return _t_invs; }
        auto& t_invs() noexcept                            { return _t_invs; }

        //+ Real& t_init() const                           { return _t_init; }
        //+ Real& t_end() const                             { return _t_end; }

        //+ void set_t_init(const State& state) const;
        //+ void set_t_end(const State& state) const;
    private:
        Local_invariants _tau_invs{};
        Local_invariants _t_invs{};
        //+ mutable Real _t_init;
        //+ mutable Real _t_end{max_t_end};
    };
}
