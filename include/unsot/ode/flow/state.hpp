#pragma once

namespace unsot::ode::flow {
    /// Represents current (continuous) state of a flow
    /// - of all its arguments including non-ODE arguments
    /// and special arguments `_t` and `_tau`
    /// It should behave similarly to simple vector of reals
    /// Its structure must correspond with `Flow::Keys`
    /// Thus, only modification of particular elements are allowed
    class State : public Inherit<Reals, State> {
    public:
        State()                                                     = default;
        ~State()                                                    = default;
        State(const State&)                                         = default;
        State(State&&)                                              = default;
        State& operator =(State);
        void swap(State&);
        State(Real t_, Reals ode_vals, Reals arg_vals = {}, Real tau_ = 0);
        explicit State(Real t_, Reals vals, size_t ode_size_, Real tau_ = 0);

        static State parse(List);

        size_t ode_size() const noexcept                 { return _ode_size; }
        size_t arg_size() const noexcept                 { return _arg_size; }
        size_t t_ode_size() const noexcept            { return ode_size()+1; }

        inline const_iterator code_begin() const noexcept;
        inline const_iterator code_end() const noexcept;
        inline const_iterator ode_begin() const& noexcept
                                                      { return code_begin(); }
        inline const_iterator ode_end() const& noexcept { return code_end(); }
        inline iterator ode_begin()& noexcept;
        inline iterator ode_end()& noexcept;
        inline auto ode_begin()&& noexcept;
        inline auto ode_end()&& noexcept;
        inline const_iterator carg_begin() const noexcept;
        inline const_iterator carg_end() const noexcept;
        inline const_iterator arg_begin() const noexcept
                                                      { return carg_begin(); }
        inline const_iterator arg_end() const noexcept  { return carg_end(); }

        inline Real ct() const noexcept;
        inline Real ctau() const noexcept;

        inline Reals code_values_slice() const;
        inline Reals carg_values_slice() const;
        Reals ct_ode_values_slice() const;
        Reals ode_values_slice() const         { return code_values_slice(); }
        Reals arg_values_slice() const         { return carg_values_slice(); }
        Reals t_ode_values_slice() const     { return ct_ode_values_slice(); }
        inline Reals ode_values_slice();
        inline Reals arg_values_slice();
        Reals t_ode_values_slice();

        inline void advance_t(Real dt) noexcept;
        inline void advance_t(const State& in, Real dt) noexcept;
        inline void reset_tau() noexcept;

        String to_string() const&;
    protected:
        inline iterator arg_begin() noexcept;
        inline iterator arg_end() noexcept;

        inline Real& t() noexcept;
        inline Real& tau() noexcept;
    private:
        void init(Real t_, Real tau_, size_t ode_size_);
        void set_subsizes(size_t ode_size_);

        bool valid() const noexcept;
        void check() const;
        bool valid_subsizes() const noexcept;
        void check_subsizes() const;

        void parse_impl(List&&);

        size_t _ode_size{};
        size_t _arg_size{};
    };
}

#include "ode/flow/state.inl"
