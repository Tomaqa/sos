#pragma once

namespace unsot::ode::flow {
    State::const_iterator State::code_begin() const noexcept
    {
        return cbegin();
    }

    State::const_iterator State::code_end() const noexcept
    {
        return code_begin()+ode_size();
    }

    State::iterator State::ode_begin()& noexcept
    {
        return begin();
    }

    State::iterator State::ode_end()& noexcept
    {
        return begin()+ode_size();
    }

    auto State::ode_begin()&& noexcept
    {
        return make_move_iterator(ode_begin());
    }

    auto State::ode_end()&& noexcept
    {
        return make_move_iterator(ode_end());
    }

    State::const_iterator State::carg_begin() const noexcept
    {
        return code_end();
    }

    State::const_iterator State::carg_end() const noexcept
    {
        return carg_begin()+arg_size();
    }

    State::iterator State::arg_begin() noexcept
    {
        return ode_end();
    }

    State::iterator State::arg_end() noexcept
    {
        return arg_begin()+arg_size();
    }

    Real State::ct() const noexcept
    {
        return *carg_end();
    }

    Real& State::t() noexcept
    {
        return *arg_end();
    }

    Real State::ctau() const noexcept
    {
        return *(carg_end()+1);
    }

    Real& State::tau() noexcept
    {
        return *(arg_end()+1);
    }

    Reals State::code_values_slice() const
    {
        return {code_begin(), code_end()};
    }

    Reals State::carg_values_slice() const
    {
        return {carg_begin(), carg_end()};
    }

    Reals State::ode_values_slice()
    {
        return {make_move_iterator(ode_begin()),
                make_move_iterator(ode_end())};
    }

    Reals State::arg_values_slice()
    {
        return {make_move_iterator(arg_begin()),
                make_move_iterator(arg_end())};
    }

    void State::advance_t(Real dt) noexcept
    {
        t() += dt;
        tau() += dt;
    }

    void State::advance_t(const State& in, Real dt) noexcept
    {
        t() = in.ct() + dt;
        tau() = in.ctau() + dt;
    }

    void State::reset_tau() noexcept
    {
        tau() = 0;
    }
}
