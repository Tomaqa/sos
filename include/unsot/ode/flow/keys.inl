#pragma once

#include "util/alg.hpp"

namespace unsot::ode::flow {
    void Keys::set_ids_map()
    {
        ids_map_ptr() = expr::new_ids_map(ckeys());
    }

    const auto& Keys::ckey_id(const Key& key_) const
    {
        return expr::key_id(cids_map(), key_);
    }

    auto& Keys::key_id(const Key& key_) noexcept
    {
        return expr::key_id(ids_map(), key_);
    }

    Keys::const_iterator Keys::code_begin() const noexcept
    {
        return cbegin();
    }

    Keys::const_iterator Keys::code_end() const noexcept
    {
        return code_begin()+ode_size();
    }

    Keys::const_iterator Keys::carg_begin() const noexcept
    {
        return code_end();
    }

    Keys::const_iterator Keys::carg_end() const noexcept
    {
        return carg_begin()+arg_size();
    }

    expr::Keys Keys::code_keys_slice() const
    {
        return {code_begin(), code_end()};
    }

    expr::Keys Keys::carg_keys_slice() const
    {
        return {carg_begin(), carg_end()};
    }

    expr::Keys Keys::ode_keys_slice()
    {
        return {make_move_iterator(ode_begin()),
                make_move_iterator(ode_end())};
    }

    expr::Keys Keys::arg_keys_slice()
    {
        return {make_move_iterator(arg_begin()),
                make_move_iterator(arg_end())};
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::ode::flow {
    template <typename UnF>
    void for_each_ode_key(const Keys& keys, UnF f)
    {
        for_each(keys.code_begin(), keys.code_end(), move(f));
    }

    template <typename UnF>
    void for_each_inv_key(const Keys& keys, UnF f)
    {
        auto f2 = f;
        f2(tau_key);
        f2(t_key);
        for_each_ode_key(keys, move(f));
        invoke(move(f2), global_key);
    }

    template <typename UnF>
    void for_each_arg_key(const Keys& keys, UnF f)
    {
        for_each(keys.carg_begin(), keys.carg_end(), move(f));
    }
}
