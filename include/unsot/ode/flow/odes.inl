#pragma once

namespace unsot::ode::flow::aux {
    Flow::Odes::Odes(const Keys& keys_, Key key_)
        : Inherit(keys_, move(key_))
    { }
}
