#pragma once

namespace unsot::ode::flow::aux {
    template <typename B>
    Flow::Local_mixin<B>::Local_mixin(const Keys& keys_, Key key_)
        : Inherit(keys_), _key(move(key_))
    { }

    template <typename B>
    Flow::Local_mixin<B>::
        Local_mixin(const Keys& keys_, Key key_, Formula phi)
        : Inherit(keys_, move(phi)), _key(move(key_))
    { }

    template <typename B>
    Flow::Local_mixin<B>::
        Local_mixin(const Keys& keys_, Key key_, Formulas phis,
                    Formula def_phi)
        : Inherit(keys_, move(phis), move(def_phi)), _key(move(key_))
    { }

    template <typename B>
    bool Flow::Local_mixin<B>::lvalid_pre_init() const noexcept
    {
        return valid_key();
    }

    template <typename B>
    void Flow::Local_mixin<B>::lcheck_pre_init() const
    {
        check_key();
    }

    template <typename B>
    bool Flow::Local_mixin<B>::valid_key() const noexcept
    {
        return this->valid_fun_key(ckey());
    }

    template <typename B>
    void Flow::Local_mixin<B>::check_key() const
    {
        this->check_fun_key(ckey());
    }

    template <typename B>
    bool Flow::Local_mixin<B>::valid_fun_key(const Key& key_) const noexcept
    {
        return Inherit::valid_fun_key(key_) && !is_global_key(key_);
    }

    template <typename B>
    void Flow::Local_mixin<B>::check_fun_key(const Key& key_) const
    try {
        Inherit::check_fun_key(key_);
    }
    catch (const Error& err) {
        throw err + " (local one)";
    }

    template <typename B>
    String Flow::Local_mixin<B>::head_to_string() const
    {
        using unsot::to_string;
        return "["s + to_string(ckey()) + "]";
    }

    template <typename B>
    bool Flow::Local_mixin<B>::lequals(const This& rhs) const noexcept
    {
        return equals_key(rhs.ckey());
    }

    template <typename B>
    bool Flow::Local_mixin<B>::equals_key(const Key& key_) const noexcept
    {
        return ckey() == key_;
    }
}
