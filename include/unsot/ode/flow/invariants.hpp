#pragma once

#include "util/flag.hpp"

namespace unsot::ode::flow::aux {
    class Flow::Invariants : public Inherit<Functions<Pred>, Invariants> {
    public:
        friend Parse;

        static inline const Formula def_formula;

        Invariants();
        virtual ~Invariants()                                       = default;
        Invariants(const Invariants&)                               = default;
        Invariants& operator =(const Invariants&)                   = default;
        Invariants(Invariants&&)                                    = default;
        Invariants& operator =(Invariants&&)                        = default;
        explicit Invariants(const Keys&);
        Invariants(const Keys&, Formula);
        Invariants(const Keys&, Formulas, Formula def_phi_ = def_formula);

        bool empty() const noexcept               { return _empty.is_true(); }

        bool valid_id(const Id&) const noexcept override;

        bool operator ()(const Reals& state, const Id& = def_id) const;

        String body_to_string() const override;
    protected:
        void init() override;

        void init_funs() override;
        void init_fun(Fun&) override;

        void check_funs() const override;

        bool valid_formula_head_impl(const Formula&) const override;
        bool valid_formula_body_impl(const Formula&) const override;

        bool is_atom_formula(const Formula&) const noexcept override;
        virtual bool is_atom_equality(const Formula&) const noexcept;

        static bool valid_atom_oper(const Formula&);

        bool valid_fun_key(const Key&) const noexcept override;
        void check_fun_key(const Key&) const override;

        const auto& cmask() const noexcept                   { return _mask; }
        auto& mask() noexcept                                { return _mask; }
        inline bool masked(const Id&) const;

        String fun_to_string(const Id&) const override;

        bool equals_this(const This&) const noexcept;
        bool lequals(const This&) const noexcept;
    private:
        Mask _mask{};
        Flag _empty;
    };

    class Flow::Local_invariants
        : public Inherit<Local_mixin<Invariants>, Local_invariants> {
    public:
        friend Parse;

        using Inherit::Inherit;
        Local_invariants(const Keys&, Key, Formulas,
                         Formula def_phi = def_formula);
    protected:
        bool valid_formula_head_impl(const Formula&) const override;
    };
}

#include "ode/flow/invariants.inl"
