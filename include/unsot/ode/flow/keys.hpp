#pragma once

namespace unsot::ode::flow {
    template <typename UnF> void for_each_ode_key(const Keys&, UnF);
    template <typename UnF> void for_each_inv_key(const Keys&, UnF);
    template <typename UnF> void for_each_arg_key(const Keys&, UnF);
}

namespace unsot::ode::flow {
    /// Array of argument keys beginning with ODE keys,
    /// followed by constant argument keys
    /// and ending with special argument keys ('_t', '_tau')
    /// Modification of the content is not supported
    class Keys : public Static<Keys> {
    public:
        using iterator = expr::Keys::iterator;
        using const_iterator = expr::Keys::const_iterator;

        Keys()                                                      = default;
        ~Keys()                                                     = default;
        Keys(const Keys&)                                           = default;
        Keys& operator =(const Keys&)                               = default;
        Keys(Keys&&)                                                = default;
        Keys& operator =(Keys&&)                                    = default;
        Keys(expr::Keys ode_keys_, expr::Keys arg_keys_ = {});
        explicit Keys(expr::Keys, size_t ode_size_);

        static Keys parse(List);

        const auto& cptr() const noexcept                     { return _ptr; }
        const auto& ckeys() const noexcept                 { return *cptr(); }
        const auto& keys() const noexcept                  { return ckeys(); }

        const auto& cids_map_ptr() const noexcept     { return _ids_map_ptr; }
        const auto& cids_map() const noexcept      { return *cids_map_ptr(); }
        const auto& ids_map() const noexcept            { return cids_map(); }
        inline const auto& ckey_id(const Key&) const;
        Key_id t_ode_key_id(const Key&) const;

        size_t size() const noexcept            { return std::size(ckeys()); }
        size_t ode_size() const noexcept                 { return _ode_size; }
        size_t arg_size() const noexcept                 { return _arg_size; }
        size_t t_ode_size() const noexcept            { return ode_size()+1; }

        const_iterator cbegin() const noexcept{ return std::cbegin(ckeys()); }
        const_iterator cend() const noexcept    { return std::cend(ckeys()); }
        const_iterator begin() const noexcept  { return std::begin(ckeys()); }
        const_iterator end() const noexcept      { return std::end(ckeys()); }

        inline const_iterator code_begin() const noexcept;
        inline const_iterator code_end() const noexcept;
        inline const_iterator ode_begin() const noexcept
                                                      { return code_begin(); }
        inline const_iterator ode_end() const noexcept  { return code_end(); }
        inline const_iterator carg_begin() const noexcept;
        inline const_iterator carg_end() const noexcept;
        inline const_iterator arg_begin() const noexcept
                                                      { return carg_begin(); }
        inline const_iterator arg_end() const noexcept  { return carg_end(); }

        inline expr::Keys code_keys_slice() const;
        inline expr::Keys carg_keys_slice() const;
        expr::Keys ct_ode_keys_slice() const;
        expr::Keys ode_keys_slice() const        { return code_keys_slice(); }
        expr::Keys arg_keys_slice() const        { return carg_keys_slice(); }
        expr::Keys t_ode_keys_slice() const    { return ct_ode_keys_slice(); }
        inline expr::Keys ode_keys_slice();
        inline expr::Keys arg_keys_slice();
        expr::Keys t_ode_keys_slice();

        bool contains(const Key&) const noexcept;
        void check_contains(const Key&) const;
        bool contains_ode_key(const Key&) const noexcept;
        void check_contains_ode_key(const Key&) const;
        bool contains_arg_key(const Key&) const noexcept;
        void check_contains_arg_key(const Key&) const;

        void set_arg_keys(expr::Keys);

        bool valid_state(const State&) const noexcept;
        void check_state(const State&) const;

        String to_string() const&;

        bool equals(const This&) const noexcept;
    protected:
        auto& ptr() noexcept                                  { return _ptr; }
        auto& keys() noexcept                               { return *ptr(); }

        auto& ids_map_ptr() noexcept                  { return _ids_map_ptr; }
        auto& ids_map() noexcept                    { return *ids_map_ptr(); }
        inline auto& key_id(const Key&) noexcept;
    private:
        void init(size_t ode_size_);
        void set_subsizes(size_t ode_size_);
        inline void set_ids_map();

        bool valid() const;
        void check() const;
        bool valid_subsizes() const noexcept;
        void check_subsizes() const;
        bool valid_keys() const noexcept;
        void check_keys() const;
        static bool valid_key(const Key&) noexcept;
        static void check_key(const Key&);
        bool valid_ode_keys() const noexcept;
        void check_ode_keys() const;
        static bool valid_ode_key(const Key&) noexcept;
        static void check_ode_key(const Key&);
        bool valid_unique() const;
        void check_unique() const;
        bool valid_special() const noexcept;
        void check_special() const;

        size_t parse_impl(List&&);

        expr::Keys_ptr _ptr{};

        expr::Ids_map_ptr _ids_map_ptr{};

        size_t _ode_size{};
        size_t _arg_size{};
    };
}

#include "ode/flow/keys.inl"
