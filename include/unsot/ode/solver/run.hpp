#pragma once

#include "util/run.hpp"
#include "ode/solver.hpp"

namespace unsot::ode::solver {
    template <typename S>
    class Base::Run : public virtual Inherit<util::Run> {
    public:
        using Solver = S;

        using Inherit::Inherit;
        virtual ~Run()                                              = default;

        virtual Path plot_path(const flow::Id& = flow::def_id) const;

        virtual Ostream& plot_os(const flow::Id& = flow::def_id) const;

        void init() override;
        void do_stuff() override;
        void finish() override;

        String usage() const override;

        /// This should be executed *during* `do_stuff'
        virtual void set_trajects_ostreams();
        /// This is executed *after* `do_stuff' (in `finish')
        virtual void plot_trajects();
    protected:
        using Ostream_ptrs = Vector<Ostream_ptr>;

        using Paths = Vector<Path>;

        virtual const Solver& csolver() const noexcept     { return _solver; }
        virtual Solver& solver() noexcept                  { return _solver; }

        virtual Path plot_path_suffix(const flow::Id&) const;

        virtual const Ostream_ptr&
            cplot_os_ptr(const flow::Id& = flow::def_id) const;
        virtual Ostream_ptr& plot_os_ptr(const flow::Id& = flow::def_id);

        virtual ofstream& plot_ofs(const flow::Id& = flow::def_id);

        String lusage() const;

        void linit()                                                       { }
        void lfinish();

        String getopt_str() const noexcept override;
        String lgetopt_str() const noexcept;
        bool process_opt(char) override;
        bool lprocess_opt(char);

        /// This is executed from `set_trajects_ostreams`
        virtual void set_plot_ostream_ptrs();
        virtual void set_plot_ostream_ptr(const flow::Id&);

        virtual void plot_traject(const flow::Id&);

        bool _plot{};
        Path _plot_path{};
        Path _plot_script_path{};
        Ostream_ptrs _plot_os_ptrs{};
    private:
        Solver _solver{};

        Vector<ofstream> _plot_ofss{};
    };
}

#include "ode/solver/run.inl"
