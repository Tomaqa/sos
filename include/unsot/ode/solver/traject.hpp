#pragma once

#include "util/run.hpp"

namespace unsot::ode::solver {
    class Base::Traject : public Inherit<States, Traject> {
    public:
        using Ostream = util::Ostream;

        Traject()                                                    = delete;
        ~Traject()                                                  = default;
        Traject(const Traject&)                                     = default;
        Traject& operator =(const Traject&)                         = default;
        Traject(Traject&&)                                          = default;
        Traject& operator =(Traject&&)                              = default;
        Traject(const flow::Ptr&);

        const Ostream& cos() const noexcept             { return *cos_ptr(); }
        Ostream& os() const noexcept                    { return *cos_ptr(); }

        void reset(size_t = 0);

        void add_step(State);

        bool is_set_ostream() const noexcept;
        void set_ostream(Ostream&) noexcept;

        bool write_block(int id = 0) const;

        String to_string() const&;
    protected:
        using Ostream_ptr = util::Ostream_ptr;

        const auto& cflow() const noexcept              { return *_flow_ptr; }
        const auto& ckeys() const noexcept         { return cflow().ckeys(); }

        const Ostream_ptr& cos_ptr() const noexcept        { return _os_ptr; }
        Ostream_ptr& os_ptr() noexcept                     { return _os_ptr; }
    private:
        flow::Ptr _flow_ptr;

        Ostream_ptr _os_ptr{};
    };
}
