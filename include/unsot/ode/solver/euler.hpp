#pragma once

#include "ode/solver.hpp"

namespace unsot::ode::solver {
    struct Euler : public Inherit<Base> {
        using Inherit::Inherit;
        virtual ~Euler()                                            = default;

        void do_step(const flow::Id&, const State& in, State& out,
                     const Real dt_, const Config&) const override;
    };
}

