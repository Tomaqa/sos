#pragma once

#include "util.hpp"
#include "util/fun.hpp"
#include "expr.hpp"
#include "expr/fun_arith.hpp"

namespace unsot::ode {
    using namespace util;

    class Flow;

    using Flows = Vector<Flow>;

    using expr::Key;
    using expr::Key_id;

    using expr::Real;
    using expr::Reals;
    using Reals_ptr = expr::Values_ptr<Real>;

    using expr::List;
    using expr::Formula;
    using Fun = expr::Fun<Real>;
    using Pred = expr::Pred<Real>;
    using Formulas = Vector<Formula>;
    using Funs = Vector<Fun>;
    using Preds = Vector<Pred>;
}
