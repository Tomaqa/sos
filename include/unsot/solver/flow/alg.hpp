#pragma once

#include "solver/flow.hpp"

namespace unsot::solver::flow {
    expr::Keys make_arg_keys(expr::Keys init_keys, expr::Keys real_arg_keys);
    inline expr::Keys make_arg_keys(const Def&);

    expr::Keys make_masked_arg_keys(expr::Keys init_keys, expr::Keys real_arg_keys, const Mask&);
    expr::Keys make_masked_arg_keys(expr::Keys, const Mask&);
    inline expr::Keys make_masked_arg_keys(const Def&);

    expr::Keys make_masked_arg_keys_keep_init(expr::Keys init_keys, expr::Keys real_arg_keys,
                                              const Mask&);
    inline expr::Keys make_masked_arg_keys_keep_init(const Def&);

    expr::Keys make_masked_arg_keys_keep_n(expr::Keys, const Mask&, int keep_n);

    template <typename DefT, typename BinF, typename DefUnF = Nop>
        void for_each_ode(DefT&&, const Keys&, BinF, DefUnF = nop);
    template <typename DefT, typename BinF, typename DefUnF = Nop>
        void for_each_invariant(DefT&&, const Keys&, BinF, DefUnF = nop);
    template <typename DefT, typename BinF, typename DefUnF = Nop>
        void for_each_variant(DefT&&, const Keys&, BinF, DefUnF = nop);
}

#include "solver/flow/alg.inl"
