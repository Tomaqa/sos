#ifdef NO_EXPLICIT_TP_INST

#include "solver.tpp"

#include "solver/run.tpp"

#else

namespace TP_INST_SOLVER_NAMESPACE {
    extern template class aux::_S::Unsot_t::Mixin;
}

#endif  /// NO_EXPLICIT_TP_INST
