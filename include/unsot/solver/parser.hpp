#pragma once

#include "util/hash.hpp"

namespace unsot::solver {
    using expr::Pos;
}

namespace unsot::solver {
    template <typename B, typename OdeSolver>
    class Mixin<B, OdeSolver>::Parser : public unsot::Inherit<typename B::Parser> {
    public:
        using Inherit = unsot::Inherit<typename B::Parser>;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Parser()                                                                 = default;
    protected:
        using Flow_def_arg_sorts_mask = Mask;
        using Flow_def_arg_sort_masks = Hash<Key, Flow_def_arg_sorts_mask>;

        const auto& cflow_def_arg_sort_masks() const noexcept   { return _flow_def_arg_sort_masks; }
        auto& flow_def_arg_sort_masks() noexcept                { return _flow_def_arg_sort_masks; }
        const auto& cflow_def_arg_sorts_mask(const Key&) const;
        auto& flow_def_arg_sorts_mask(const Key&) noexcept;

        void post_preprocess_cmd(expr::Ptr&, Key& cmd, Pos&) override;
        virtual void post_preprocess_cmd_init(expr::Ptr&, Key& cmd, Pos&);
        virtual void post_preprocess_cmd_final(expr::Ptr&, Key& cmd, Pos&);
        virtual void post_preprocess_cmd_diff(expr::Ptr&, Key& cmd, Pos&);

        void parse_expand_cmd(expr::Ptr&, Key& cmd, Pos&) override;
        virtual void parse_expand_cmd_flow(expr::Ptr&, Key& cmd, Pos&);

        void parse_cmd(const Key& cmd, Pos&) override;
        virtual void parse_cmd_define_flow(Pos&&);
    private:
        template <typename ConvF>
            void post_preprocess_flow_key_cmd(expr::Ptr&, Key& cmd, Pos&, ConvF);

        Flow_def_arg_sort_masks _flow_def_arg_sort_masks{};
    };
}

#include "solver/parser.inl"
