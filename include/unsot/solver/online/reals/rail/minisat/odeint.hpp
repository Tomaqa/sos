#pragma once

#include "solver/online/reals/rail/minisat.hpp"
#include "ode/solver/odeint.hpp"

namespace unsot {
    namespace aux {
        using _S = solver::online::reals::rail::minisat::Solver<ode::solver::Odeint>;
    }
}
#include "smt/solver/tp.hpp"
#include "smt/solver/bools/tp.hpp"
#include "smt/solver/reals/tp.hpp"
#include "smt/solver/online/tp.hpp"
#include "smt/solver/online/bools/tp.hpp"
#include "smt/solver/online/reals/tp.hpp"
#include "smt/solver/online/reals/rail/tp.hpp"
#include "solver/tp.hpp"
#include "solver/online/reals/tp.hpp"

#include "smt/sat/solver/online/minisat/tp.hpp"
