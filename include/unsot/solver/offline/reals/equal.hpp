#pragma once

#include "solver.hpp"
#include "smt/solver/offline/reals/equal.hpp"

namespace unsot::solver::offline::reals::equal {
    template <typename B> class Mixin;

    template <typename S, typename SatSolver, typename OdeSolver,
              var::Type typeV = var::Type::assignee>
        using Crtp = Mixin<solver::Mixin<
            smt::solver::offline::reals::equal::Crtp<S, SatSolver, typeV>,
            OdeSolver
        >>;
}

namespace unsot::solver::offline::reals::equal {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Unsot_offline_t = This;

        class Check_sat;

        using Inherit::Inherit;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;
    };
}

#include "solver/offline/reals/equal/check_sat.hpp"
