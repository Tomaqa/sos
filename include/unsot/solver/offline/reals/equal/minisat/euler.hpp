#pragma once

#include "solver/offline/reals/equal/minisat.hpp"
#include "ode/solver/euler.hpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot {
    namespace aux {
        using _S = solver::offline::reals::equal::minisat::Solver<ode::solver::Euler>;
    }

    extern template class aux::_S::Base::Crtp;
    extern template class aux::_S::Bools_t::Mixin;
    extern template class aux::_S::Reals_t::Mixin;
    extern template class aux::_S::Offline_bools_t::Mixin;
    extern template class aux::_S::Offline_reals_t::Mixin;
    extern template class aux::_S::Offline_equal_t::Mixin;
    extern template class aux::_S::Unsot_t::Mixin;
    extern template class aux::_S::Unsot_offline_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
