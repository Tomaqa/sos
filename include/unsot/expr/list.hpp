#pragma once

#include "util/ref.hpp"

#include <list>

namespace unsot::expr {
    namespace aux {
        template <typename E = Elem> struct List_foo;
    }

    template <typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Elem_base, L> cast_as_elem_base(L&&) noexcept;
    template <typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Key, L> cast_as_key(L&&);
    template <typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Elem_base, L> cast_as_elem_base_check(L&&);
    template <typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Key, L> cast_as_key_check(L&&);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<E, L> cast_as_elem(L&&) noexcept;
    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Arg, L> cast_as_value(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<E, L> cast_as_elem_check(L&&);
    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Arg, L> cast_as_value_check(L&&);

    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        auto cast_as_value_l(L&&) noexcept;

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Int, L> cast_as_int(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Real, L> cast_as_real(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Int, L> cast_as_int_check(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Forward_as_ref<Real, L> cast_as_real_check(L&&);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        auto cast_as_key_l(L&&) noexcept;
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        auto cast_as_int_l(L&&) noexcept;
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        auto cast_as_real_l(L&&) noexcept;

    bool is_flat(const List&) noexcept;
    bool is_deep(const List&) noexcept;
    void check_is_flat(const List&);
    void check_is_deep(const List&);

    bool is_as_elem_base(const List&) noexcept;
    bool is_as_key(const List&) noexcept;
    void check_is_as_elem_base(const List&);
    void check_is_as_key(const List&);

    template <typename E = Elem> bool is_as_elem(const List&) noexcept;
    template <typename Arg, typename E = Elem> bool is_as_value(const List&) noexcept;
    template <typename E = Elem> void check_is_as_elem(const List&);
    template <typename Arg, typename E = Elem> void check_is_as_value(const List&);

    template <typename E = Elem> bool is_as_int(const List&) noexcept;
    template <typename E = Elem> bool is_as_real(const List&) noexcept;
    template <typename E = Elem> void check_is_as_int(const List&);
    template <typename E = Elem> void check_is_as_real(const List&);

    /// `has_elem_bases_only` would be equivalent to `is_flat`
    bool has_keys_only(const List&) noexcept;
    void check_has_keys_only(const List&);

    template <typename E = Elem> bool has_elems_only(const List&) noexcept;
    template <typename Arg, typename E = Elem> bool has_values_only(const List&) noexcept;
    template <typename E = Elem> void check_has_elems_only(const List&);
    template <typename Arg, typename E = Elem> void check_has_values_only(const List&);

    template <typename E = Elem> bool has_ints_only(const List&) noexcept;
    template <typename E = Elem> bool has_reals_only(const List&) noexcept;
    template <typename E = Elem> void check_has_ints_only(const List&);
    template <typename E = Elem> void check_has_reals_only(const List&);

    template <typename L, Req<is_list_v<L>> = 0> Keys cast_to_keys(L&&);
    template <typename L, Req<is_list_v<L>> = 0> Lists cast_to_lists(L&&) noexcept;
    template <typename L, Req<is_list_v<L>> = 0> Keys cast_to_keys_check(L&&);
    template <typename L, Req<is_list_v<L>> = 0> Lists cast_to_lists_check(L&&);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Values<E> cast_to_elems(L&&) noexcept;
    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Values<Arg> cast_to_values(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Values<E> cast_to_elems_check(L&&);
    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Values<Arg> cast_to_values_check(L&&);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0> Ints cast_to_ints(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0> Reals cast_to_reals(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0> Ints cast_to_ints_check(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0> Reals cast_to_reals_check(L&&);

    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Values<Optional<Arg>> to_values(L&&) noexcept;
    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Values<Arg> to_values_check(L&&);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Values<Optional<Int>> to_ints(L&&) noexcept;
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Values<Optional<Real>> to_reals(L&&) noexcept;
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Ints to_ints_check(L&&);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0>
        Reals to_reals_check(L&&);
}

namespace unsot::expr {
    class List : public Inherit<Place, List>, public std::list<Ptr> {
    public:
        using Container = list;

        template <typename L> struct To_string;

        using Container::Container;
        List()                                                                            = default;
        virtual ~List()                                                                   = default;
        /// Uses deep copy
        List(const List&);
        List& operator =(const List&);
        List(List&&)                                                                      = default;
        List& operator =(List&&)                                                          = default;
        List(const List&, Shallow_copy_tag);
        List(const List&, Deep_copy_tag);
        /// Uses deep copy
        template <typename ItT, Req<is_iterator_v<ItT>> = 0> List(ItT first, ItT last);
        template <typename ItT, Req<is_iterator_v<ItT>> = 0>
            List(ItT first, ItT last, Shallow_copy_tag);
        template <typename ItT, Req<is_iterator_v<ItT>> = 0>
            List(ItT first, ItT last, Deep_copy_tag);
        List(initializer_list<Ptr>);
        List(Ptr);
        template <typename E = Elem> List(const char* input, Tag<E> t = {})
                                                                        : List(String(input), t) { }
        template <typename E = Elem> List(String, Tag<E> = {});
        template <typename E = Elem> List(istream&, Tag<E> = {});
        template <typename E = Elem> List(istream&& is, Tag<E> t = {})             : List(is, t) { }

        template <typename T, typename E = Elem,
                  typename U = initializer_list<Ptr>, typename... Args>
            static T cons_tp(U&&, Args&&...);
        template <typename T> static T cons_tp();

        List shallow_copy() const;
        List deep_copy() const;
        template <typename ItT> static List shallow_copy(ItT first, ItT last);
        template <typename ItT> static List deep_copy(ItT first, ItT last);

        bool is_elem() const noexcept override                                     { return false; }
        bool is_list() const noexcept override                                      { return true; }
        static inline bool is_me(const Ptr&) noexcept;
        static void check_is_me(const Ptr&);

        using Container::front;
        using Container::back;
        const Ptr& cfront() const noexcept                                       { return front(); }
        const Ptr& cback() const noexcept                                         { return back(); }

        using Container::insert;
        /// do not use ref. qualifier for `insert` to properly override it
        //+ uses deep copy (with const iterators), which is not obvious though
        template <typename ItT> iterator insert(const_iterator pos_, ItT first, ItT last);

        List& nest()&;
        List to_nested() const&;
        List& emerge() noexcept;
        List& emerge_rec() noexcept;
        List to_nested()&&;
        List& flatten()&;
        List to_flat() const&;
        List to_flat()&&;

        template <typename L> static auto make_to_string(L&&);
        String to_string() const& override;
        String to_string()&& override;

        //+ size_t hash() const override;
    protected:
        template <typename T, typename L> class To_string_crtp;

        template <typename E = Elem> List& parse(istream&, streampos&, int depth);

        bool lequals(const This&) const noexcept;
    };
    extern template List::List(String, Tag<Elem>);
    extern template List::List(istream&, Tag<Elem>);
    extern template List& List::parse<Elem>(istream&, streampos&, int depth);

    namespace aux {
        template <typename E>
        struct List_foo final {
            static bool is_as_elem(const List&) noexcept;
            static void check_is_as_elem(const List&);

            static bool has_elems_only(const List&) noexcept;
            static void check_has_elems_only(const List&);
        };
        extern template struct List_foo<Elem>;
    }
}

namespace unsot::expr {
    template <typename T, typename L>
    class List::To_string_crtp : public Crtp<T> {
    public:
        using typename Crtp<T>::That;

        struct Conf;

        using Ref = util::Ref<L>;

        using List = L;

        static_assert(is_same_v<Rm_const<List>, typename Ref::Item>);

        static constexpr bool is_const_v = Ref::is_const_v;
        using const_iterator = typename List::const_iterator;
        using iterator = typename List::iterator;
        using Iterator = Cond_t<is_const_v, const_iterator, iterator>;

        virtual ~To_string_crtp()                                                         = default;
        To_string_crtp(Ref ref_)                                              : _ref(move(ref_)) { }

        virtual String perform(const Conf& conf = {})
                                                                      { return perform_impl(conf); }

        operator String()                                                      { return perform(); }
    protected:
        const auto& clist() const                                           { return _ref.citem(); }
        const auto& list() const&                                                { return clist(); }
        auto& list()&                                                        { return _ref.item(); }
        auto&& list()&&                                                     { return move(list()); }

        virtual String perform_impl(const Conf&, int depth = 0);

        virtual void perform_head_pre(String&, const Conf&, int depth);
        virtual void perform_head_post(String&, const Conf&, int /*depth*/)                      { }
        virtual void perform_body_pre(String&, const Conf&, int /*depth*/, Iterator)             { }
        virtual String ptr_to_string(const Ptr& ptr, int depth);
        virtual String ptr_to_string(Ptr&& ptr, int depth);
        virtual void perform_body_post(String&, const Conf&, int /*depth*/)                      { }
        virtual void perform_tail_pre(String&, const Conf&, int /*depth*/)                       { }
        virtual void perform_tail_post(String&, const Conf&, int /*depth*/)                      { }
    private:
        Ref _ref{};
    };
    extern template class List::To_string_crtp<List::To_string<const List>, const List>;
    extern template class List::To_string_crtp<List::To_string<List>, List>;

    template <typename T, typename L>
    struct List::To_string_crtp<T, L>::Conf {
        bool top_brackets{true};
        String delim{" "};
        bool rec{false};
        String ls_delim{"\n"};
    };

    template <typename L>
    struct List::To_string : Inherit<List::To_string_crtp<List::To_string<L>, L>> {
        using Inherit = unsot::Inherit<List::To_string_crtp<List::To_string<L>, L>>;

        using Inherit::Inherit;
    };
}

#include "expr/list.inl"
