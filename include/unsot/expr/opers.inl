#pragma once

namespace unsot::expr::fun {
    template <typename T, typename U, typename V>
    constexpr const char* Opers<T, U, V>::type() noexcept
    {
        if constexpr (is_integral_v<T>) return "integral";
        else if constexpr (is_floating_point_v<T>) return "floating point";
        else static_assert(false_tp<T>);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Un_fs_map& Opers<T, U, V>::unary_fs_map() noexcept
    {
        if constexpr (is_integral_v<T>) return integral_un_fs_map<T>;
        else if constexpr (is_floating_point_v<T>) return floating_point_un_fs_map<T>;
        else static_assert(false_tp<T>);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Bin_fs_map& Opers<T, U, V>::binary_fs_map() noexcept
    {
        if constexpr (is_integral_v<T>) return integral_bin_fs_map<T>;
        else if constexpr (is_floating_point_v<T>) return floating_point_bin_fs_map<T>;
        else static_assert(false_tp<T>);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Ter_fs_map& Opers<T, U, V>::ternary_fs_map() noexcept
    {
        if constexpr (is_integral_v<T>) return integral_ter_fs_map<T>;
        else if constexpr (is_floating_point_v<T>) return floating_point_ter_fs_map<T>;
        else static_assert(false_tp<T>);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Un_preds_map& Opers<T, U, V>::unary_preds_map() noexcept
    {
        if constexpr (is_integral_v<T>) return integral_un_preds_map<T>;
        else if constexpr (is_floating_point_v<T>) return floating_point_un_preds_map<T>;
        else static_assert(false_tp<T>);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Bin_preds_map& Opers<T, U, V>::binary_preds_map() noexcept
    {
        if constexpr (is_integral_v<T>) return integral_bin_preds_map<T>;
        else if constexpr (is_floating_point_v<T>) return floating_point_bin_preds_map<T>;
        else static_assert(false_tp<T>);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Ter_preds_map& Opers<T, U, V>::ternary_preds_map() noexcept
    {
        if constexpr (is_integral_v<T>) return integral_ter_preds_map<T>;
        else if constexpr (is_floating_point_v<T>) return floating_point_ter_preds_map<T>;
        else static_assert(false_tp<T>);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Un_f& Opers<T, U, V>::unary_f(const Key& key_)
    {
        return unary_fs_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Bin_f& Opers<T, U, V>::binary_f(const Key& key_)
    {
        return binary_fs_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Ter_f& Opers<T, U, V>::ternary_f(const Key& key_)
    {
        return ternary_fs_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Un_pred& Opers<T, U, V>::unary_pred(const Key& key_)
    {
        return unary_preds_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Bin_pred& Opers<T, U, V>::binary_pred(const Key& key_)
    {
        return binary_preds_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Ter_pred& Opers<T, U, V>::ternary_pred(const Key& key_)
    {
        return ternary_preds_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Un_bool& Opers<T, U, V>::unary_bool(const Key& key_)
    {
        return unary_bools_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Bin_bool& Opers<T, U, V>::binary_bool(const Key& key_)
    {
        return binary_bools_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Ter_bool& Opers<T, U, V>::ternary_bool(const Key& key_)
    {
        return ternary_bools_map().cat(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_f(const Formula& phi) noexcept
    {
        return valid_nary_f(phi.coper(), phi.size());
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_f(const Formula& phi)
    {
        check_nary_f(phi.coper(), phi.size());
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_pred(const Formula& phi) noexcept
    {
        return valid_nary_pred(phi.coper(), phi.size());
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_pred(const Formula& phi)
    {
        check_nary_pred(phi.coper(), phi.size());
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_bool(const Formula& phi) noexcept
    {
        return valid_nary_bool(phi.coper(), phi.size());
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_bool(const Formula& phi)
    {
        check_nary_bool(phi.coper(), phi.size());
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid(const Formula& phi) noexcept
    {
        return valid_nary(phi.coper(), phi.size());
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check(const Formula& phi)
    {
        check_nary(phi.coper(), phi.size());
    }
}
