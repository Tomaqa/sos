#pragma once

#include "expr/fun.hpp"

#include "util/fun.hpp"

namespace unsot::expr::fun {
    template <typename F, typename O>
        static constexpr bool is_unary_v = is_same_v<F, typename O::Un_f>
                                        || is_same_v<F, typename O::Un_pred>
                                        || is_same_v<F, typename O::Un_bool>;
    template <typename F, typename O>
        static constexpr bool is_binary_v =
            is_same_v<F, typename O::Bin_f>
            || is_same_v<F, typename O::Bin_pred>
            || is_same_v<F, typename O::Bin_bool>;
    template <typename F, typename O>
        static constexpr bool is_ternary_v =
            is_same_v<F, typename O::Ter_f>
            || is_same_v<F, typename O::Ter_pred>
            || is_same_v<F, typename O::Ter_bool>;
}

namespace unsot::expr::fun {
    template <typename T, typename U, typename V>
    struct Opers {
        using Un_f = util::Un_f<T, T>;
        using Bin_f = util::Bin_f<T, T, U>;
        using Ter_f = util::Ter_f<T, T, U, V>;
        using Un_pred = util::Un_f<Bool, T>;
        using Bin_pred = util::Bin_f<Bool, T, U>;
        using Ter_pred = util::Ter_f<Bool, T, U, V>;
        using Un_bool = util::Un_f<Bool, bool>;
        using Bin_bool = util::Bin_f<Bool, bool>;
        using Ter_bool = util::Ter_f<Bool, bool>;

        template <typename F> using Map = Hash<Key, F>;
        using Un_fs_map = Map<Un_f>;
        using Bin_fs_map = Map<Bin_f>;
        using Ter_fs_map = Map<Ter_f>;
        using Un_preds_map = Map<Un_pred>;
        using Bin_preds_map = Map<Bin_pred>;
        using Ter_preds_map = Map<Ter_pred>;
        using Un_bools_map = Map<Un_bool>;
        using Bin_bools_map = Map<Bin_bool>;
        using Ter_bools_map = Map<Ter_bool>;

        static constexpr const char* type() noexcept;

        template <typename T_ = T, Req<is_integral_v<T_>> = 0>
            static const Un_fs_map integral_un_fs_map;
        template <typename T_ = T, Req<is_integral_v<T_>> = 0>
            static const Bin_fs_map integral_bin_fs_map;
        template <typename T_ = T, Req<is_integral_v<T_>> = 0>
            static const Ter_fs_map integral_ter_fs_map;
        template <typename T_ = T, Req<is_integral_v<T_>> = 0>
            static const Un_preds_map integral_un_preds_map;
        template <typename T_ = T, Req<is_integral_v<T_>> = 0>
            static const Bin_preds_map integral_bin_preds_map;
        template <typename T_ = T, Req<is_integral_v<T_>> = 0>
            static const Ter_preds_map integral_ter_preds_map;

        template <typename T_ = T, Req<is_floating_point_v<T_>> = 0>
            static const Un_fs_map floating_point_un_fs_map;
        template <typename T_ = T, Req<is_floating_point_v<T_>> = 0>
            static const Bin_fs_map floating_point_bin_fs_map;
        template <typename T_ = T, Req<is_floating_point_v<T_>> = 0>
            static const Ter_fs_map floating_point_ter_fs_map;
        template <typename T_ = T, Req<is_floating_point_v<T_>> = 0>
            static const Un_preds_map floating_point_un_preds_map;
        template <typename T_ = T, Req<is_floating_point_v<T_>> = 0>
            static const Bin_preds_map floating_point_bin_preds_map;
        template <typename T_ = T, Req<is_floating_point_v<T_>> = 0>
            static const Ter_preds_map floating_point_ter_preds_map;

        static const Un_bools_map un_bools_map;
        static const Bin_bools_map bin_bools_map;
        static const Ter_bools_map ter_bools_map;

        static const Un_fs_map& unary_fs_map() noexcept;
        static const Bin_fs_map& binary_fs_map() noexcept;
        static const Ter_fs_map& ternary_fs_map() noexcept;
        static const Un_preds_map& unary_preds_map() noexcept;
        static const Bin_preds_map& binary_preds_map() noexcept;
        static const Ter_preds_map& ternary_preds_map() noexcept;
        static const Un_bools_map& unary_bools_map() noexcept               { return un_bools_map; }
        static const Bin_bools_map& binary_bools_map() noexcept            { return bin_bools_map; }
        static const Ter_bools_map& ternary_bools_map() noexcept           { return ter_bools_map; }

        static const Un_f& unary_f(const Key&);
        static const Bin_f& binary_f(const Key&);
        static const Ter_f& ternary_f(const Key&);
        static const Un_pred& unary_pred(const Key&);
        static const Bin_pred& binary_pred(const Key&);
        static const Ter_pred& ternary_pred(const Key&);
        static const Un_bool& unary_bool(const Key&);
        static const Bin_bool& binary_bool(const Key&);
        static const Ter_bool& ternary_bool(const Key&);

        static bool valid_unary_f(const Key&) noexcept;
        static bool valid_binary_f(const Key&) noexcept;
        static bool valid_ternary_f(const Key&) noexcept;
        static bool valid_nary_f(const Key&, int) noexcept;
        static bool valid_f(const Key&) noexcept;
        static void check_unary_f(const Key&);
        static void check_binary_f(const Key&);
        static void check_ternary_f(const Key&);
        static void check_nary_f(const Key&, int);
        static void check_f(const Key&);

        static bool valid_unary_pred(const Key&) noexcept;
        static bool valid_binary_pred(const Key&) noexcept;
        static bool valid_ternary_pred(const Key&) noexcept;
        static bool valid_nary_pred(const Key&, int) noexcept;
        static bool valid_pred(const Key&) noexcept;
        static void check_unary_pred(const Key&);
        static void check_binary_pred(const Key&);
        static void check_ternary_pred(const Key&);
        static void check_nary_pred(const Key&, int);
        static void check_pred(const Key&);

        static bool valid_unary_bool(const Key&) noexcept;
        static bool valid_binary_bool(const Key&) noexcept;
        static bool valid_ternary_bool(const Key&) noexcept;
        static bool valid_nary_bool(const Key&, int) noexcept;
        static bool valid_bool(const Key&) noexcept;
        static void check_unary_bool(const Key&);
        static void check_binary_bool(const Key&);
        static void check_ternary_bool(const Key&);
        static void check_nary_bool(const Key&, int);
        static void check_bool(const Key&);

        static bool valid_unary(const Key&) noexcept;
        static bool valid_binary(const Key&) noexcept;
        static bool valid_ternary(const Key&) noexcept;
        static bool valid_nary(const Key&, int) noexcept;
        static bool valid(const Key&) noexcept;
        static void check_unary(const Key&);
        static void check_binary(const Key&);
        static void check_ternary(const Key&);
        static void check_nary(const Key&, int);
        static void check(const Key&);

        static bool valid_f(const Formula&) noexcept;
        static void check_f(const Formula&);
        static bool valid_pred(const Formula&) noexcept;
        static void check_pred(const Formula&);
        static bool valid_bool(const Formula&) noexcept;
        static void check_bool(const Formula&);
        static bool valid(const Formula&) noexcept;
        static void check(const Formula&);
    };
}

#include "expr/opers.inl"
