#pragma once

#include "util/run.hpp"
#include "expr/fun.hpp"

namespace unsot::expr::fun {
    template <typename Arg, typename ConfT>
    class Fun_tp<Arg, ConfT>::Run : public virtual unsot::Inherit<util::Run> {
    public:
        using Inherit::Inherit;

        void do_stuff() override;
    };
}

#include "expr/fun/run.inl"
