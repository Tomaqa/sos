#pragma once

namespace unsot::expr::fun {
    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::Run::do_stuff()
    {
        //+ os() << std::setprecision(double_precision) << std::fixed;
        for (String line; getline(is(), line);) {
            List ls(line);
            if (ls.empty()) continue;
            if (is_as_elem(ls)) {
                os() << cast_as_elem(ls) << endl;
                continue;
            }

            auto fun = Fun_tp::cons(ls, new_keys());
            if (empty(fun.ckeys())) {
                os() << invoke(move(fun)) << endl;
                continue;
            }

            bool first = true;
            while (getline(is(), line)) {
                if (line.empty()) {
                    if (first) continue;
                    break;
                }
                first = false;

                ls = line;
                Values vals(to_values_check<Arg>(move(ls)));
                os() << invoke(fun, move(vals)) << endl;
            }
        }
    }
}
