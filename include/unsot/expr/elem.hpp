#pragma once

#include "util/optional.hpp"
#include "util/union.hpp"

namespace unsot::expr {
    template <typename Arg, typename E = Elem, typename... Args> Ptr new_value(Args&&...);

    template <typename E = Elem> Ptr new_key();
    template <typename E = Elem, typename T, typename... Args> Ptr new_key(T&&, Args&&...);
    template <typename E = Elem, typename... Args> Ptr new_int(Args&&...);
    template <typename E = Elem, typename... Args> Ptr new_real(Args&&...);

    template <typename E = Elem, typename T> Ptr new_elem(T&&);

    template <typename T, Req<is_ptr_v<T> || is_elem_base_v<T>> = 0>
        Forward_as_ref<Key, T> cast_key(T&&);
    template <typename P, Req<is_ptr_v<P>> = 0>
        Forward_as_ref<Key, P> cast_key_check(P&&);

    template <typename Arg, typename E = Elem, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>> = 0>
        Forward_as_ref<Arg, T> cast_value(T&&);
    template <typename Arg, typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        Forward_as_ref<Arg, P> cast_value_check(P&&);

    template <typename Arg, typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        auto cast_value_l(P&&) noexcept;

    template <typename E = Elem, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>> = 0>
        Forward_as_ref<Int, T> cast_int(T&&);
    template <typename E = Elem, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>> = 0>
        Forward_as_ref<Real, T> cast_real(T&&);
    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        Forward_as_ref<Int, P> cast_int_check(P&&);
    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        Forward_as_ref<Real, P> cast_real_check(P&&);

    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        auto cast_key_l(P&&) noexcept;
    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        auto cast_int_l(P&&) noexcept;
    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        auto cast_real_l(P&&) noexcept;

    bool is_key(const Ptr&) noexcept;
    void check_is_key(const Ptr&);
    bool is_valid_key(const Elem_base&) noexcept;
    void check_is_valid_key(const Elem_base&);

    template <typename Arg = void, typename E = Elem> bool is_value(const Ptr&) noexcept;
    template <typename Arg = void, typename E = Elem> void check_is_value(const Ptr&);

    template <typename E = Elem> bool is_int(const Ptr&) noexcept;
    template <typename E = Elem> bool is_real(const Ptr&) noexcept;
    template <typename E = Elem> void check_is_int(const Ptr&);
    template <typename E = Elem> void check_is_real(const Ptr&);

    template <typename Arg, typename E = Elem, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>> = 0>
        Optional<Arg> to_value(T&&) noexcept;
    template <typename Arg, typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        Arg to_value_check(P&&);

    template <typename E = Elem, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>> = 0>
        Optional<Int> to_int(T&&) noexcept;
    template <typename E = Elem, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>> = 0>
        Optional<Real> to_real(T&&) noexcept;
    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        Int to_int_check(P&&);
    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        Real to_real_check(P&&);
}

namespace unsot::expr {
    class Elem_base : public Inherit<Place, Elem_base> {
    public:
        Elem_base()                                                 = default;
        virtual ~Elem_base()                                        = default;
        constexpr Elem_base(const Key&);

        virtual const Key& cget_key() const                               = 0;
        virtual const Key& get_key() const&                               = 0;
        /// The assigned `Key` is not checked (`valid_key`)
        virtual Key& get_key()&                                           = 0;
        virtual Key&& get_key()&&                                         = 0;
        virtual const Key& cget_key_check() const;
        const Key& get_key_check() const&         { return cget_key_check(); }
        virtual Key& get_key_check()&;
        Key&& get_key_check()&&              { return move(get_key_check()); }

        bool is_elem() const noexcept override                { return true; }
        bool is_list() const noexcept override               { return false; }
        static inline bool is_me(const Ptr& ptr) noexcept;
        static void check_is_me(const Ptr& ptr)        { check_is_elem(ptr); }

        virtual bool is_key() const noexcept                              = 0;
        void check_is_key() const;
        void check_is_not_key() const;
    };

    //+ divide sep. into `Union<Key, Number>`, `Number = Union<Args...>` ?
    template <typename... Args>
    class Elem_tp : public Inherit<Elem_base, Elem_tp<Args...>>,
                    protected Union<Key, Args...> {
    public:
        using Inherit = unsot::Inherit<Elem_base, Elem_tp<Args...>>;
        using Union = util::Union<Key, Args...>;
        using typename Inherit::This;
        using typename Inherit::Base;

        Elem_tp()                                                   = default;
        virtual ~Elem_tp()                                          = default;
        Elem_tp(const Elem_tp&)                                     = default;
        Elem_tp& operator =(const Elem_tp&)                         = default;
        Elem_tp(Elem_tp&&)                                          = default;
        Elem_tp& operator =(Elem_tp&&)                              = default;
        template <typename K, Req<is_key_v<K>> = 0> constexpr Elem_tp(K&&);
        template <typename Arg, Req<!is_key_v<Arg>> = 0> constexpr Elem_tp(Arg&&);
        template <typename Arg> Elem_tp& operator =(Arg&&);

        template <typename Arg> constexpr const auto& cget_value() const;
        template <typename Arg> constexpr const auto& get_value() const&;
        template <typename Arg> constexpr auto& get_value()&;
        template <typename Arg> constexpr auto&& get_value()&&;
        template <typename Arg> constexpr auto cget_value_l() const noexcept;
        template <typename Arg> constexpr auto get_value_l() const noexcept;
        template <typename Arg> constexpr auto get_value_l() noexcept;

        const Key& cget_key() const override;
        const Key& get_key() const& override;
        /// The assigned `Key` is not checked (`valid_key`)
        Key& get_key()& override;
        Key&& get_key()&& override;
        constexpr auto cget_key_l() const noexcept;
        constexpr auto get_key_l() const noexcept;
        constexpr auto get_key_l() noexcept;

        using Union::valid;
        using Union::invalid;
        template <typename Arg> constexpr bool is_value() const noexcept;

        bool is_key() const noexcept override;

        template <typename Arg, typename... Ts> Arg& emplace(Ts&&...);

        template <typename Arg> Optional<Arg> to_value() const& noexcept;
        template <typename Arg> Optional<Arg> to_value()&& noexcept;
        template <typename Arg> Arg to_value_check() const&;
        template <typename Arg> Arg to_value_check()&&;

        String to_string() const& override;
        String to_string()&& override;

        bool equals(const This&) const noexcept;

        //+ size_t hash() const override;
    private:
        template <typename Arg, typename E> static Arg to_value_check_impl(E&&);
    };
}

#include "expr/elem.inl"
