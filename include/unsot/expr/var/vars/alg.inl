#pragma once

namespace unsot::expr::var {
    template <typename VarsT>
    bool all_set(const VarsT& vars) noexcept
    {
        return all_of(vars, mem_fn(&Base::is_set));
    }

    template <typename VarsT, typename ContT, typename ValT>
    bool all_set(const ContT& vars, const ValT& value) noexcept
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        return all_of(vars, [&value](auto& ptr){
            return Vars::Types::Regular_var::cast(ptr) == value;
        });
    }

    template <typename VarsT>
    bool any_set(const VarsT& vars) noexcept
    {
        return any_of(vars, mem_fn(&Base::is_set));
    }

    template <typename VarsT, typename ContT, typename ValT>
    bool any_set(const ContT& vars, const ValT& value) noexcept
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        return any_of(vars, [&value](auto& ptr){
            return Vars::Types::Regular_var::cast(ptr) == value;
        });
    }

    template <typename VarsT>
    bool all_unset(const VarsT& vars) noexcept
    {
        return all_of(vars, mem_fn(&Base::is_unset));
    }

    template <typename VarsT>
    bool any_unset(const VarsT& vars) noexcept
    {
        return any_of(vars, mem_fn(&Base::is_unset));
    }

    template <typename VarsT>
    bool all_unset_or_locked(const VarsT& vars) noexcept
    {
        return all_of(vars, [](auto& vptr){
            return vptr->locked() || vptr->is_unset();
        });
    }

    template <typename VarsT>
    bool all_computed(const VarsT& vars) noexcept
    {
        return all_of(vars, mem_fn(&Base::computed));
    }

    template <typename VarsT>
    bool any_computed(const VarsT& vars) noexcept
    {
        return any_of(vars, mem_fn(&Base::computed));
    }

    template <typename VarsT>
    bool all_evaluable(const VarsT& vars) noexcept
    {
        return all_of(vars, mem_fn(&Base::evaluable));
    }

    template <typename VarsT>
    bool any_evaluable(const VarsT& vars) noexcept
    {
        return any_of(vars, mem_fn(&Base::evaluable));
    }

    template <typename VarsT>
    bool all_computable(const VarsT& vars) noexcept
    {
        return all_of(vars, mem_fn(&Base::computable));
    }

    template <typename VarsT>
    bool any_computable(const VarsT& vars) noexcept
    {
        return any_of(vars, mem_fn(&Base::computable));
    }

    template <typename VarsT>
    bool all_consistent(VarsT& vars) noexcept
    {
        return all_of(vars, mem_fn(&Base::consistent));
    }

    template <typename VarsT>
    bool any_consistent(VarsT& vars) noexcept
    {
        return any_of(vars, mem_fn(&Base::consistent));
    }

    template <typename VarsT>
    bool all_surely_consistent(const VarsT& vars) noexcept
    {
        return all_of(vars, mem_fn(&Base::surely_consistent));
    }

    template <typename VarsT>
    bool any_surely_consistent(const VarsT& vars) noexcept
    {
        return any_of(vars, mem_fn(&Base::surely_consistent));
    }

    template <typename VarsT, typename ContT, typename ValT>
    void set_all(ContT& vars, const ValT& value) noexcept
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        for (auto& ptr : vars)
            Vars::Types::Regular_var::cast(ptr).set(value);
    }

    template <typename VarsT>
    void unset_all(VarsT& vars) noexcept
    {
        for (auto& ptr : vars) ptr->unset();
    }

    template <typename VarsT>
    void reset_all(VarsT& vars) noexcept
    {
        for (auto& ptr : vars) ptr->reset();
    }

    template <typename VarsT, typename ContT>
    void eval_all(ContT& vars)
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        for (auto& ptr : vars) Vars::Types::Regular_var::cast(ptr).eval();
    }

    template <typename VarsT, typename ContT>
    void compute_all(ContT& vars)
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        for (auto& ptr : vars) Vars::Types::Regular_var::cast(ptr).compute();
    }

    template <typename VarsT, typename ContT>
    auto try_get_all(const ContT& vars) noexcept
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        using Output = Values<Optional<typename Vars::Value>>;
        return to<Output>(vars, [](auto& ptr){
            return Vars::Types::Regular_var::cast(ptr).try_get();
        });
    }

    template <typename VarsT, typename ContT>
    bool try_eval_all(ContT& vars) noexcept
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        bool any = false;
        for (auto& ptr : vars) {
            any |= Vars::Types::Regular_var::cast(ptr).try_eval().second;
        }
        return any;
    }

    //+ return sth. more descriptive (ideally enum {all, any, none})
    //+ template <typename VarsT>
    //+ Flag try_eval_all(VarsT& vars) noexcept
    //+ {
    //+     bool any = false;
    //+     bool all = true;
    //+     for (auto& ptr : vars) {
    //+         const bool eval = ptr->try_eval().second;
    //+         any |= eval;
    //+         all &= eval;
    //+     }

    //+     if (all) return true;
    //+     if (any) return unknown;
    //+     return false;
    //+ }

    template <typename VarsT, typename ContT>
    auto try_compute_all(ContT& vars) noexcept
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        using Output = Values<Optional<typename Vars::Value>>;
        return to<Output>(vars, [](auto& ptr){
            return Vars::Types::Regular_var::cast(ptr).try_compute();
        });
    }

    template <typename VarsT>
    Keys to_keys(VarsT&& vars)
    {
        if constexpr (Decay<VarsT>::is_global_v)
            return FORWARD(vars).keys();
        else return to<Keys>(FORWARD(vars), [](auto&& ptr){
            return Forward_as_ref<Base, decltype(ptr)>(*ptr).key();
        });
    }

    template <typename VarsT, typename ContT>
    auto to_values(ContT&& vars)
    {
        using Vars = Cond_t<!is_void_v<VarsT>, VarsT, ContT>;
        if constexpr (Decay<VarsT>::is_global_v)
            return FORWARD(vars).values();
        else return to<typename Vars::Values>(vars, [](auto& ptr) -> typename Vars::Value {
            if (ptr->is_unset()) return {};
            return Vars::Types::Regular_var::cast(ptr).get();
        });
    }
}
