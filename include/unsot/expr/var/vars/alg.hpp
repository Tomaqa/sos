#pragma once

#include "expr/var/vars.hpp"

namespace unsot::expr::var {
    template <typename VarsT> bool all_set(const VarsT&) noexcept;
    template <typename VarsT = void, typename ContT, typename ValT>
        bool all_set(const ContT&, const ValT&) noexcept;
    template <typename VarsT> bool any_set(const VarsT&) noexcept;
    template <typename VarsT = void, typename ContT, typename ValT>
        bool any_set(const ContT&, const ValT&) noexcept;
    template <typename VarsT> bool all_unset(const VarsT&) noexcept;
    template <typename VarsT> bool any_unset(const VarsT&) noexcept;
    template <typename VarsT> bool all_unset_or_locked(const VarsT&) noexcept;
    template <typename VarsT> bool all_computed(const VarsT&) noexcept;
    template <typename VarsT> bool any_computed(const VarsT&) noexcept;
    template <typename VarsT> bool all_evaluable(const VarsT&) noexcept;
    template <typename VarsT> bool any_evaluable(const VarsT&) noexcept;
    template <typename VarsT> bool all_computable(const VarsT&) noexcept;
    template <typename VarsT> bool any_computable(const VarsT&) noexcept;

    template <typename VarsT> bool all_consistent(VarsT&) noexcept;
    template <typename VarsT> bool any_consistent(VarsT&) noexcept;
    template <typename VarsT>
        bool all_surely_consistent(const VarsT&) noexcept;
    template <typename VarsT>
        bool any_surely_consistent(const VarsT&) noexcept;

    template <typename VarsT = void, typename ContT, typename ValT>
        void set_all(ContT&, const ValT&) noexcept;
    template <typename VarsT> void unset_all(VarsT&) noexcept;
    template <typename VarsT> void reset_all(VarsT&) noexcept;
    template <typename VarsT = void, typename ContT> void eval_all(ContT&);
    template <typename VarsT = void, typename ContT>
        void compute_all(ContT&);
    template <typename VarsT = void, typename ContT>
        auto try_get_all(const ContT&) noexcept;
    template <typename VarsT = void, typename ContT>
        bool try_eval_all(ContT&) noexcept;
    //+ template <typename VarsT> Flag try_eval_all(VarsT&) noexcept;
    template <typename VarsT = void, typename ContT>
        auto try_compute_all(ContT&) noexcept;

    /// Returns local keys/values only, if `!is_global`
    template <typename VarsT> Keys to_keys(VarsT&&);
    template <typename VarsT = void, typename ContT> auto to_values(ContT&&);
}

#include "expr/var/vars/alg.inl"
