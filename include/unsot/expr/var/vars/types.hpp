#pragma once

namespace unsot::expr::var::vars {
    namespace aux::types {
        template <typename Sort, typename ArgSort, typename ConfT>
        struct Base {
            template <typename B> class Fun_mixin;
            template <typename B> class Auto_mixin;

            using Vars_base = vars::Base<Sort, ArgSort>;

            using Value = typename Vars_base::Value;
            using Arg_value = typename Vars_base::Arg_value;

            /// conf. must be shared by all connected vars
            using Conf = ConfT;

            static constexpr bool is_bools_v = is_bool_v<Value>;
            static constexpr bool has_funs_v = vars::has_funs_v<Value, Arg_value>;

            using Args_base = typename Vars_base::Args_base;
            using Args_base_link = typename Vars_base::Args_base_link;
            using Args_base_link_ptr = typename Vars_base::Args_base_link_ptr;
            using Preds_base = typename Vars_base::Preds_base;
            using Preds_base_link = Link_tp<Preds_base>;
            using Preds_base_link_ptr = Link_ptr_tp<Preds_base>;

            template <typename S = Value> using Regular_var_tp = expr::Var<S, Conf>;

            template <typename S = Value, typename AS = Arg_value, typename FunT = expr::Fun<AS>>
                using Fun_tp = Fun_mixin<Fun_base_tp<S, Conf, Regular_var_tp<AS>, FunT>>;
            template <typename AS = Arg_value>
                using Pred_tp = Fun_tp<Bool, AS, expr::Pred<AS>>;
            template <typename AS = Arg_value>
                using Assigner_tp = var::Assigner<Pred_tp<AS>, Conf>;
            template <typename AS = Arg_value>
                using Equality_tp = var::Equality<Pred_tp<AS>, Conf>;

            template <typename S = Value, typename AssignerT = Equality_tp<S>>
                using Auto_var_tp = Auto_mixin<Auto_assignee_base<S, AssignerT, Conf>>;

            using Regular_var = Regular_var_tp<>;

            static_assert(is_same_v<typename Regular_var::Value, Value>);
        };

        template <typename B, Type typeV>
        struct Type_mixin : Inherit<B> {
            using Inherit = unsot::Inherit<B>;

            using typename Inherit::Value;

            static constexpr Type var_type = typeV;

            using typename Inherit::Conf;

            template <typename S = Value, Type tV = var_type,
                      typename AutoVar = typename Inherit::template Auto_var_tp<S>>
                using Var_tp = var::Tp<S, tV, Conf, AutoVar>;

            using typename Inherit::Regular_var;
            using Var = Var_tp<>;

            static_assert(is_same_v<typename Var::Value, Value>);
            static_assert(is_base_of_v<Regular_var, Var>);
        };

        template <typename B>
        struct Funs_mixin : Inherit<B> {
            using Inherit = unsot::Inherit<B>;

            using typename Inherit::Value;
            using typename Inherit::Arg_value;

            using typename Inherit::Regular_var;
            using typename Inherit::Var;

            using Fun = typename Inherit::template Fun_tp<>;

            static_assert(is_same_v<typename Fun::Value, Value>);
            static_assert(is_same_v<typename Fun::Arg_value, Arg_value>);
            static_assert(is_derived_from_v<Fun, Regular_var>);
            static_assert(is_derived_from_v<typename Fun::Arg_var, Regular_var>);
            static_assert(is_base_of_v<typename Fun::Arg_var, Var>);
        };

        template <typename B>
        struct Preds_mixin : Inherit<B> {
            using Inherit = unsot::Inherit<B>;

            using typename Inherit::Arg_value;

            using Inherit::var_type;

            using typename Inherit::Regular_var;

            using Pred = typename Inherit::template Pred_tp<>;
            using Assigner = typename Inherit::template Assigner_tp<>;
            using Equality = typename Inherit::template Equality_tp<>;

            static_assert(is_same_v<typename Pred::Value, Bool>);
            static_assert(is_same_v<typename Pred::Arg_value, Arg_value>);
            static_assert(is_derived_from_v<Pred, Regular_var>);
            static_assert(!type::is_assign_v<var_type> || is_base_of_v<Pred, Assigner>);
            static_assert(is_base_of_v<Assigner, Equality>);
            static_assert(is_base_of_v<typename Pred::Arg_var, typename Assigner::Assignee>);
        };
    }

    namespace aux {
        template <typename Sort, typename ArgSort, Type typeV, typename ConfT>
            using Types = Cond_mixin<is_bool_v<Sort>, types::Preds_mixin,
                          Cond_mixin<has_funs_v<Sort, ArgSort>, types::Funs_mixin,
                          types::Type_mixin<types::Base<Sort, ArgSort, ConfT>, typeV>>>;
    }

    template <typename Sort, typename ArgSort, Type typeV, typename ConfT>
    struct Types : aux::Types<Sort, ArgSort, typeV, ConfT> {};
}

namespace unsot::expr::var::vars::aux::types {
    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    class Base<Sort, ArgSort, ConfT>::Fun_mixin : public unsot::Inherit<B, Fun_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Fun_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Types_base = types::Base<Sort, ArgSort, ConfT>;
        using Vars_fun_t = This;

        using typename Inherit::Value;
        using typename Inherit::Values_ptr;

        using Args_base = typename Types_base::Args_base;
        using Args_base_link_ptr = typename Types_base::Args_base_link_ptr;

        using typename Inherit::Fun;

        using Inherit::Parent::operator =;
        Fun_mixin()                                                 = default;
        virtual ~Fun_mixin()                                        = default;
        Fun_mixin(const Fun_mixin&)                                  = delete;
        Fun_mixin& operator =(const Fun_mixin&)                      = delete;
        Fun_mixin(Fun_mixin&&)                                      = default;
        Fun_mixin& operator =(Fun_mixin&&)                          = default;

        using Inherit::Fun_t::Inherit::cons_tp;
        template <typename T, typename V = Dummy>
            static T cons_tp(Keys_ptr&, Values_ptr&, Ids_map_ptr&, Fun,
                             Args_base_link_ptr, Key, V = {});

        const auto& cargs_base_l_ptr() const noexcept
                                                  { return _args_base_l_ptr; }
        const auto& cargs_base() const noexcept
                                              { return **cargs_base_l_ptr(); }
        auto& args_base() noexcept             { return **args_base_l_ptr(); }

        using Inherit::carg_ptr;
        using Inherit::arg_ptr;
        const Ptr& carg_ptr(const Id&) const override;
        Ptr& arg_ptr(const Id&) override;
    protected:
        explicit Fun_mixin(Keys_ptr, Values_ptr, Ids_map_ptr, Id, Fun,
                           Args_base_link_ptr, bool is_set_ = false);

        auto& args_base_l_ptr() noexcept          { return _args_base_l_ptr; }
    private:
        Args_base_link_ptr _args_base_l_ptr{};
    };

    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    class Base<Sort, ArgSort, ConfT>::Auto_mixin : public unsot::Inherit<B, Auto_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Auto_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Types_base = types::Base<Sort, ArgSort, ConfT>;
        using Vars_auto_assignee_t = This;

        using typename Inherit::Value;
        using typename Inherit::Values_ptr;

        using Preds_base = typename Types_base::Preds_base;
        using Preds_base_link_ptr = typename Types_base::Preds_base_link_ptr;

        using Inherit::Parent::operator =;
        Auto_mixin()                                                = default;
        virtual ~Auto_mixin()                                       = default;
        Auto_mixin(const Auto_mixin&)                                = delete;
        Auto_mixin& operator =(const Auto_mixin&)                    = delete;
        Auto_mixin(Auto_mixin&&)                                    = default;
        Auto_mixin& operator =(Auto_mixin&&)                        = default;

        using Inherit::cons_tp;
        template <typename T, typename V = Dummy>
            static T cons_tp(Keys_ptr&, Values_ptr&, Ids_map_ptr&,
                             Preds_base_link_ptr, Key, V = {});

        const auto& cpreds_base_l_ptr() const noexcept
                                                 { return _preds_base_l_ptr; }
        const auto& cpreds_base() const noexcept
                                             { return **cpreds_base_l_ptr(); }
        auto& preds_base() noexcept           { return **preds_base_l_ptr(); }

        const Ptr& cassigner_ptr(const Id&) const override;
        Ptr& assigner_ptr(const Id&) override;
    protected:
        explicit Auto_mixin(Keys_ptr, Values_ptr, Ids_map_ptr, Id,
                            Preds_base_link_ptr, bool is_set_ = false);

        auto& preds_base_l_ptr() noexcept        { return _preds_base_l_ptr; }
    private:
        Preds_base_link_ptr _preds_base_l_ptr{};
    };
}

#include "expr/var/vars/types.inl"
