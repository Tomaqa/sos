#pragma once

#include "expr/var.hpp"

#include "util/view.hpp"

namespace unsot::expr::var::vars {
    template <typename Sort, typename ArgSort = Sort> class Base;
    template <typename B, Type typeV = Type{}, typename ConfT = conf::Tp<typeV>>
        class Types_mixin;
    template <typename B> class Funs_mixin;
    template <typename B> class Preds_mixin;
    template <typename B> class Auto_assign_mixin;

    template <typename Sort, typename ArgSort = Sort, Type typeV = Type{},
              typename ConfT = conf::Tp<typeV>>
        struct Types;

    template <typename BaseT> using Link_tp = BaseT*;
    template <typename BaseT> using Link_ptr_tp = Shared_ptr<Link_tp<BaseT>>;

    template <typename Sort, typename ArgSort>
        constexpr bool has_funs_v = !is_bool_v<Sort> || is_bool_v<ArgSort>;
}

namespace unsot::expr::var::vars {
    namespace aux {
        template <typename Sort, typename ArgSort>
        class Base {
        public:
            using Aux_base_t = Base;

            using Value = Sort;
            using Arg_value = ArgSort;

            using Rets_base = vars::Base<Value>;
            using Args_base = vars::Base<Arg_value>;
            using Preds_base = vars::Base<Bool, Arg_value>;

            using Values = typename Var<Value>::Values;
            using Values_ptr = typename Var<Value>::Values_ptr;

            using Ptrs_view = Unique_view<Ptrs>;

            using Global_iterator = Ptrs::iterator;
            using Global_const_iterator = Ptrs::const_iterator;
            using Local_iterator = Ptrs_view::iterator;
            using Local_const_iterator = Ptrs_view::const_iterator;

            Base();
            ~Base()                                                 = default;
            Base(const Base&)                                        = delete;
            Base& operator =(const Base&)                            = delete;
            Base(Base&&)                                            = default;
            Base& operator =(Base&&)                                = default;

            template <typename T, typename... Args> Ptr new_var_tp(Args&&...);

            template <typename BaseT = Rets_base> void connect(BaseT&);

            const auto& cptrs() const noexcept        { return *cptrs_ptr(); }
            const auto& ptrs() const& noexcept             { return cptrs(); }
            auto& ptrs()& noexcept                     { return *ptrs_ptr(); }
            auto&& ptrs()&& noexcept                  { return move(ptrs()); }
            const auto& cptrs_view() const noexcept     { return _ptrs_view; }
            const auto& ptrs_view() const noexcept    { return cptrs_view(); }
            auto& ptrs_view() noexcept                  { return _ptrs_view; }

            const auto& ckeys_ptr() const noexcept       { return _keys_ptr; }
            const auto& cvalues_ptr() const noexcept   { return _values_ptr; }
            auto& values_ptr() noexcept                { return _values_ptr; }
            const auto& cids_map_ptr() const noexcept { return _ids_map_ptr; }
            const auto& ckeys() const noexcept        { return *ckeys_ptr(); }
            const auto& keys() const& noexcept             { return ckeys(); }
            auto&& keys()&& noexcept                  { return move(keys()); }
            const auto& cvalues() const noexcept    { return *cvalues_ptr(); }
            const auto& values() const& noexcept         { return cvalues(); }
            auto& values()& noexcept                 { return *values_ptr(); }
            auto&& values()&& noexcept              { return move(values()); }
            const auto& cids_map() const noexcept  { return *cids_map_ptr(); }
            const auto& ids_map() const& noexcept       { return cids_map(); }
            auto&& ids_map()&& noexcept            { return move(ids_map()); }

            size_t global_size() const noexcept      { return size(cptrs()); }
            bool global_empty() const noexcept      { return empty(cptrs()); }

            size_t local_size() const noexcept  { return size(cptrs_view()); }
            bool local_empty() const noexcept  { return empty(cptrs_view()); }

            Global_const_iterator cgbegin() const noexcept
                                                   { return cbegin(cptrs()); }
            Global_const_iterator cgend() const noexcept
                                                     { return cend(cptrs()); }
            Global_const_iterator gbegin() const noexcept{ return cgbegin(); }
            Global_const_iterator gend() const noexcept    { return cgend(); }
            Global_iterator gbegin() noexcept        { return begin(ptrs()); }
            Global_iterator gend() noexcept            { return end(ptrs()); }

            Local_const_iterator clbegin() const noexcept
                                             { return cptrs_view().cbegin(); }
            Local_const_iterator clend() const noexcept
                                               { return cptrs_view().cend(); }
            Local_const_iterator lbegin() const noexcept { return clbegin(); }
            Local_const_iterator lend() const noexcept     { return clend(); }
            Local_iterator lbegin() noexcept   { return ptrs_view().begin(); }
            Local_iterator lend() noexcept       { return ptrs_view().end(); }

            bool global_contains(const Key&) const noexcept;
            void check_global_contains(const Key&) const;
            void check_not_global_contains(const Key&) const;

            bool local_contains(const Key&) const noexcept;
            void check_local_contains(const Key&) const;
            void check_not_local_contains(const Key&) const;
            bool local_contains(const Key_id&) const noexcept;

            const auto& cptr(const Key_id&) const noexcept;
            const auto& ptr(const Key_id& kid) const& noexcept
                                                         { return cptr(kid); }
            auto& ptr(const Key_id&)& noexcept;
            auto&& ptr(const Key_id& kid)&& noexcept{ return move(ptr(kid)); }
            const auto& cptr(const Key&) const;
            const auto& ptr(const Key& key_) const&     { return cptr(key_); }
            auto& ptr(const Key&)&;
            auto&& ptr(const Key& key_)&&          { return move(ptr(key_)); }

            const auto& ckey(const Key_id&) const noexcept;
            const auto& cvalue(const Key_id&) const noexcept;
            const auto& value(const Key_id& kid) const& noexcept
                                                       { return cvalue(kid); }
            auto& value(const Key_id&)& noexcept;
            auto&& value(const Key_id& kid)&& noexcept
                                                  { return move(value(kid)); }
            const auto& cvalue(const Key&) const;
            const auto& value(const Key& key_) const& { return cvalue(key_); }
            auto& value(const Key&)&;
            auto&& value(const Key& key_)&&      { return move(value(key_)); }
            const auto& ckey_id(const Key&) const;

            /// Cannot use `Optional` with references
            Ptr* global_find(const Key&) noexcept;
            Ptr* local_find(const Key&) noexcept;
            Ptr* local_find(const Key_id&) noexcept;

            void reserve(size_t);

            void push_back(Ptr);
        protected:
            template <typename, typename> friend class Base;

            using Ptrs_ptr = Shared_ptr<Ptrs>;

            static Ptrs_ptr new_ptrs(Ptrs&& = {});

            const auto& cptrs_ptr() const noexcept       { return _ptrs_ptr; }
            auto& ptrs_ptr() noexcept                    { return _ptrs_ptr; }

            auto& keys_ptr() noexcept                    { return _keys_ptr; }
            auto& ids_map_ptr() noexcept              { return _ids_map_ptr; }
            auto& keys()& noexcept                     { return *keys_ptr(); }
            auto& ids_map()& noexcept               { return *ids_map_ptr(); }

            const auto& key(const Key_id& kid) const& noexcept
                                                         { return ckey(kid); }
            auto& key(const Key_id&)& noexcept;
            auto&& key(const Key_id& kid)&& noexcept{ return move(key(kid)); }
            auto& key_id(const Key&);
        private:
            Ptrs_ptr _ptrs_ptr{};
            Ptrs_view _ptrs_view{};

            Keys_ptr _keys_ptr{};
            Values_ptr _values_ptr{};
            Ids_map_ptr _ids_map_ptr{};
        };
    }

    template <typename Sort, typename ArgSort>
    class Base : public Inherit<aux::Base<Sort, ArgSort>, Base<Sort, ArgSort>>,
                 public Container_types<Ptr, Key_id, Key> {
    public:
        using Inherit = unsot::Inherit<aux::Base<Sort, ArgSort>, Base<Sort, ArgSort>>;
        using typename Inherit::This;

        using typename Inherit::Value;
        using typename Inherit::Arg_value;

        using typename Inherit::Rets_base;
        using typename Inherit::Args_base;

        using Args_base_link = Link_tp<Args_base>;
        using Args_base_link_ptr = Link_ptr_tp<Args_base>;

        static constexpr bool is_global_v = is_same_v<Value, Arg_value>;

        using typename Inherit::Global_iterator;
        using typename Inherit::Global_const_iterator;
        using typename Inherit::Local_iterator;
        using typename Inherit::Local_const_iterator;
        using iterator = Cond_t<is_global_v, Global_iterator, Local_iterator>;
        using const_iterator = Cond_t<is_global_v, Global_const_iterator, Local_const_iterator>;

        Base();
        ~Base()                                                     = default;
        Base(const Base&)                                            = delete;
        Base& operator =(const Base&)                                = delete;
        Base(Base&&);
        Base& operator =(Base&&);
        template <typename BaseT = Rets_base> Base(BaseT&, Args_base&);
        void swap(Base&) noexcept;

        template <typename T, typename FunT = typename T::Fun, typename... Args>
            Ptr new_fun_tp(Formula, Args&&...);

        void connect_args(Args_base&);

        const auto& cargs_base_l_ptr() const noexcept
                                                  { return _args_base_l_ptr; }
        const auto& cargs_base_l() const noexcept
                                               { return *cargs_base_l_ptr(); }
        const auto& cargs_base() const noexcept    { return *cargs_base_l(); }
        const auto& args_base() const noexcept        { return cargs_base(); }
        auto& args_base() noexcept                  { return *args_base_l(); }

        size_t size() const noexcept;
        bool empty() const noexcept;

        const_iterator cbegin() const noexcept;
        const_iterator cend() const noexcept;
        const_iterator begin() const noexcept             { return cbegin(); }
        const_iterator end() const noexcept                 { return cend(); }
        iterator begin() noexcept;
        iterator end() noexcept;
        const auto& cfront() const noexcept              { return *cbegin(); }
        const auto& cback() const noexcept               { return *--cend(); }
        const auto& front() const noexcept                { return cfront(); }
        const auto& back() const noexcept                  { return cback(); }
        auto& front() noexcept                            { return *begin(); }
        auto& back() noexcept                             { return *--end(); }

        const Ptr& operator [](Idx) const noexcept;
        Ptr& operator [](Idx) noexcept;
        const Ptr& operator [](const Key& key_) const
                                                  { return this->cptr(key_); }
        Ptr& operator [](const Key& key_)          { return this->ptr(key_); }
        /// Disambiguate 0 (which can be treated as NULL etc.)
        const Ptr& operator [](nullptr_t) const noexcept;
        Ptr& operator [](nullptr_t) noexcept;

        //+ also for ids
        bool contains(const Key&) const noexcept;
        void check_contains(const Key&) const;
        void check_not_contains(const Key&) const;

        String to_string() const&;
        String to_string()&&;

        bool equals(const This&) const;
    protected:
        template <typename, typename> friend class Base;

        Args_base_link_ptr new_args_base_l();

        auto& args_base_l_ptr() noexcept          { return _args_base_l_ptr; }
        auto& args_base_l() noexcept            { return *args_base_l_ptr(); }
    private:
        Args_base_link_ptr _args_base_l_ptr{};
    };

    template <typename B, Type typeV, typename ConfT>
    class Types_mixin : public Inherit<B, Types_mixin<B, typeV, ConfT>> {
    public:
        using Inherit = unsot::Inherit<B, Types_mixin<B, typeV, ConfT>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Types_t = This;

        using typename Inherit::Value;
        using typename Inherit::Arg_value;

        using Types = vars::Types<Value, Arg_value, typeV, ConfT>;

        static_assert(is_same_v<Base, typename Types::Vars_base>);
        static_assert(is_base_of_v<expr::Var<Value>, typename Types::Var>);

        using Inherit::Inherit;

        const auto& cvar(const Key_id&) const noexcept;
        const auto& var(const Key_id& kid) const& noexcept
                                                         { return cvar(kid); }
        auto& var(const Key_id&)& noexcept;
        auto&& var(const Key_id& kid)&& noexcept    { return move(var(kid)); }
        const auto& cvar(const Key&) const;
        const auto& var(const Key& key_) const&         { return cvar(key_); }
        auto& var(const Key&)&;
        auto&& var(const Key& key_)&&              { return move(var(key_)); }

        template <typename ValT = Dummy> void add_var(Key, ValT = {});
    };

    template <typename B>
    class Funs_mixin : public Inherit<B, Funs_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Funs_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Funs_t = This;

        using typename Inherit::Types;

        using Inherit::Inherit;

        const auto& cfun(const Key_id&) const noexcept;
        const auto& fun(const Key_id& kid) const& noexcept
                                                         { return cfun(kid); }
        auto& fun(const Key_id&)& noexcept;
        auto&& fun(const Key_id& kid)&& noexcept    { return move(fun(kid)); }
        const auto& cfun(const Key&) const;
        const auto& fun(const Key& key_) const&         { return cfun(key_); }
        auto& fun(const Key&)&;
        auto&& fun(const Key& key_)&&              { return move(fun(key_)); }

        template <typename ValT = Dummy> void add_fun(Formula, Key, ValT = {});
    };

    template <typename B>
    class Preds_mixin : public Inherit<B, Preds_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Preds_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Preds_t = This;

        using typename Inherit::Types;

        using Inherit::is_global_v;
        static_assert(!is_global_v || is_derived_from_v<typename Types::Var,
                                                        typename Types::Pred::Arg_var>);
        static_assert(!is_global_v || !type::is_assign_v<Types::var_type>
                      || is_derived_from_v<typename Types::Var,
                                           typename Types::Assigner::Assignee>);

        using Inherit::Inherit;

        const auto& cpred(const Key_id&) const noexcept;
        const auto& pred(const Key_id& kid) const& noexcept
                                                        { return cpred(kid); }
        auto& pred(const Key_id&)& noexcept;
        auto&& pred(const Key_id& kid)&& noexcept  { return move(pred(kid)); }
        const auto& cpred(const Key&) const;
        const auto& pred(const Key& key_) const&       { return cpred(key_); }
        auto& pred(const Key&)&;
        auto&& pred(const Key& key_)&&            { return move(pred(key_)); }

        const auto& cassigner(const Key_id&) const noexcept;
        const auto& assigner(const Key_id& kid) const& noexcept
                                                    { return cassigner(kid); }
        auto& assigner(const Key_id&)& noexcept;
        auto&& assigner(const Key_id& kid)&& noexcept
                                               { return move(assigner(kid)); }
        const auto& cassigner(const Key&) const;
        const auto& assigner(const Key& key_) const&
                                                   { return cassigner(key_); }
        auto& assigner(const Key&)&;
        auto&& assigner(const Key& key_)&&    { return move(assigner(key_)); }

        template <typename ValT = Dummy> void add_pred(Formula, Key, ValT = {});
    };

    template <typename B>
    class Auto_assign_mixin : public Inherit<B, Auto_assign_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Auto_assign_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Auto_assign_t = This;

        using typename Inherit::Types;

        using Preds_base = typename Types::Preds_base;
        using Preds_base_link = typename Types::Preds_base_link;
        using Preds_base_link_ptr = typename Types::Preds_base_link_ptr;

        using Inherit::Inherit;
        void swap(Auto_assign_mixin&) noexcept;

        void connect_preds(Preds_base&);

        const auto& cpreds_base_l_ptr() const noexcept
                                                 { return _preds_base_l_ptr; }
        const auto& cpreds_base_l() const noexcept
                                              { return *cpreds_base_l_ptr(); }

        template <typename ValT = Dummy> void add_var(Key, ValT = {});
    protected:
        static Preds_base_link_ptr new_preds_base_l();

        auto& preds_base_l_ptr() noexcept        { return _preds_base_l_ptr; }
        auto& preds_base_l() noexcept          { return *preds_base_l_ptr(); }
    private:
        Preds_base_link_ptr _preds_base_l_ptr{new_preds_base_l()};
    };
}

namespace unsot::expr::var {
    namespace aux {
        template <typename Sort, typename ArgSort, Type typeV, typename ConfT,
                  typename BaseT = vars::Types_mixin<vars::Base<Sort, ArgSort>, typeV, ConfT>>
            using Vars = Cond_mixin<typeV == Type::auto_assignee, vars::Auto_assign_mixin,
                         Cond_mixin<BaseT::Types::is_bools_v, vars::Preds_mixin,
                         Cond_mixin<BaseT::Types::has_funs_v, vars::Funs_mixin,
                         BaseT>>>;
    }

    template <typename Sort, typename ArgSort, Type typeV, typename ConfT>
    class Vars : public Inherit<aux::Vars<Sort, ArgSort, typeV, ConfT>,
                                Vars<Sort, ArgSort, typeV, ConfT>> {
    public:
        using Inherit = unsot::Inherit<aux::Vars<Sort, ArgSort, typeV, ConfT>,
                                       Vars<Sort, ArgSort, typeV, ConfT>>;

        using Inherit::Inherit;
    };
}

#include "expr/var/vars/types.hpp"

#include "expr/var/vars.inl"
