#pragma once

namespace unsot::expr::fun {
    namespace aux {
        const Args_base::Key_ids_ptr&
        Args_base::cpart_key_ids_ptr(Idx idx) const noexcept
        {
            return cpart_key_ids_ptrs()[idx];
        }

        Args_base::Key_ids_ptr& Args_base::part_key_ids_ptr(Idx idx) noexcept
        {
             return part_key_ids_ptrs()[idx];
        }

        const Key_id& Args_base::ckey_id(const Key& key_) const
        {
            return key_id(cids_map(), key_);
        }

    ////////////////////////////////////////////////////////////////

        template <typename Arg>
        Values_base<Arg>::Values_base(Values_ptr vptr)
            : _values_ptr(move(vptr))
        { }

        template <typename Arg>
        Values_base<Arg>::Values_base(const Keys& keys, Values_ptr vptr)
            : Values_base(vptr ? move(vptr) : new_values<Value>(keys))
        { }
    }

    ////////////////////////////////////////////////////////////////

    template <typename L, Req<is_list_v<L>>>
    Fun_base::Fun_base(L&& ls, Keys_ptr kptr, Ids_map_ptr imap_ptr)
        : Inherit(FORWARD(ls)), Args_base(move(kptr), move(imap_ptr))
    { }

    template <typename L, Req<is_list_v<L>>>
    Fun_base::Fun_base(Key op, L&& ls, Keys_ptr kptr, Ids_map_ptr imap_ptr)
        : Inherit(move(op), FORWARD(ls)),
          Args_base(move(kptr), move(imap_ptr))
    { }

    template <typename L, Req<is_list_v<L>>>
    Fun_base::Fun_base(L&& ls)
        : Inherit(FORWARD(ls))
    { }

    template <typename L, Req<is_list_v<L>>>
    Fun_base::Fun_base(Key op, L&& ls)
        : Inherit(move(op), FORWARD(ls))
    { }

    ////////////////////////////////////////////////////////////////

    template <typename Arg, typename ConfT>
    template <typename L, Req<is_list_v<L>>>
    Fun_tp<Arg, ConfT>::Fun_tp(L&& ls, Keys_ptr kptr, Values_ptr vptr,
                               Ids_map_ptr imap_ptr)
        : Inherit(FORWARD(ls), move(kptr), move(imap_ptr)),
          Values_base(this->ckeys(), move(vptr))
    { }

    template <typename Arg, typename ConfT>
    template <typename L, Req<is_list_v<L>>>
    Fun_tp<Arg, ConfT>::Fun_tp(Key op, L&& ls, Keys_ptr kptr, Values_ptr vptr,
                               Ids_map_ptr imap_ptr)
        : Inherit(move(op), FORWARD(ls), move(kptr), move(imap_ptr)),
          Values_base(this->ckeys(), move(vptr))
    { }

    template <typename Arg, typename ConfT>
    template <typename L, Req<is_list_v<L>>>
    Fun_tp<Arg, ConfT>::Fun_tp(L&& ls)
        : Inherit(FORWARD(ls))
    { }

    template <typename Arg, typename ConfT>
    template <typename L, Req<is_list_v<L>>>
    Fun_tp<Arg, ConfT>::Fun_tp(Key op, L&& ls)
        : Inherit(move(op), FORWARD(ls))
    { }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::virtual_init_with_args(Keys_ptr kptr, Ids_map_ptr imap_ptr)
    {
        auto vptr = new_values<Value>(as_const(*kptr));
        virtual_init_with_args(move(kptr), move(vptr), move(imap_ptr));
    }

    template <typename Arg, typename ConfT>
    void Fun_tp<Arg, ConfT>::virtual_init_with_args(Keys_ptr kptr, Values_ptr vptr,
                                                    Ids_map_ptr imap_ptr)
    {
        this->values_ptr() = move(vptr);
        Inherit::virtual_init_with_args(move(kptr), move(imap_ptr));
    }

    template <typename Arg, typename ConfT>
    template <typename Vs, Req<is_values_v<Vs>>>
    Optional<typename Fun_tp<Arg, ConfT>::Ret>
    Fun_tp<Arg, ConfT>::eval(Vs&& vals) const noexcept
    {
        this->values() = FORWARD(vals);
        return eval();
    }

    template <typename Arg, typename ConfT>
    template <typename Vs, Req<is_values_v<Vs>>>
    typename Fun_tp<Arg, ConfT>::Ret
    Fun_tp<Arg, ConfT>::eval_check(Vs&& vals) const
    {
        this->values() = FORWARD(vals);
        return eval_check();
    }

    template <typename Arg, typename ConfT>
    template <typename V>
    V Fun_tp<Arg, ConfT>::ret_to_value(Ret ret) noexcept
    {
        return fun::ret_to_value<V, ConfT>(move(ret));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr::fun {
    template <typename Arg, typename ConfT>
    Arg ret_to_value(Ret<Arg, ConfT> ret) noexcept
    {
        if constexpr (!ret_is_union_v<Arg, ConfT>) return ret;
        else return to<Arg>(move(ret)).value();
    }

    template <typename Arg, typename ConfT>
    Arg ret_to_value_check(Ret<Arg, ConfT> ret)
    {
        if constexpr (!ret_is_union_v<Arg, ConfT>) return ret;
        else return to_check<Arg>(move(ret));
    }

    template <typename Arg, typename E>
    Flag evaluable(const Ptr& ptr) noexcept
    {
        if (is_value<Arg, E>(ptr) || Base::is_me(ptr)) return true;
        if (is_key(ptr)) return false;
        return unknown;
    }

    template <typename RetT, typename Arg, typename E, typename P, Req<is_ptr_v<P>>>
    Optional<RetT> eval(P&& ptr) noexcept
    {
        if (Elem_base::is_me(ptr)) return to_value<RetT, E>(FORWARD(ptr));
        assert(ptr->is_list());
        if (!Base::is_me(ptr)) {
            if (auto ls_l = List::dcast_l(FORWARD(ptr)); !ls_l) return {};
            else return ret_to_value<Arg>(Fun_tp<Arg>::cons(
                FORWARD(*ls_l), new_keys()
            ).eval());
        }

        if constexpr (is_same_v<RetT, Bool>) {
            if (Pred<Arg>::is_me(ptr))
                return Pred<Arg>::cast(FORWARD(ptr)).eval();
        }
        else if (Fun<Arg>::is_me(ptr))
            return Fun<Arg>::cast(FORWARD(ptr)).eval();

        if (auto fun_l = Fun_tp<Arg>::dcast_l(FORWARD(ptr)); !fun_l) return {};
        else return ret_to_value<Arg>(fun_l->eval());
    }

    template <typename RetT, typename Arg, typename E, typename P, Req<is_ptr_v<P>>>
    RetT eval_check(P&& ptr)
    {
        if (Elem_base::is_me(ptr)) return to_value_check<RetT, E>(FORWARD(ptr));
        assert(ptr->is_list());
        if (!Base::is_me(ptr)) {
            auto&& ls = List::cast_check(FORWARD(ptr));
            return ret_to_value_check<Arg>(Fun_tp<Arg>::cons(
                FORWARD(ls), new_keys()
            ).eval_check());
        }

        if constexpr (is_same_v<RetT, Bool>) {
            if (Pred<Arg>::is_me(ptr))
                return Pred<Arg>::cast(FORWARD(ptr)).eval_check();
        }
        else if (Fun<Arg>::is_me(ptr))
            return Fun<Arg>::cast(FORWARD(ptr)).eval_check();

        auto&& fun = Fun_tp<Arg>::cast_check(FORWARD(ptr));
        return ret_to_value_check<Arg>(fun.eval_check());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename Arg, typename L, Req<is_list_v<L>>>
    Ptr new_fun(L&& ls, Keys_ptr kptr, Values_ptr<Arg> vptr, Ids_map_ptr imap_ptr)
    {
        using Opers = fun::Opers<Arg>;
        bool is_pred;
        if constexpr (is_formula_v<L>)
            is_pred = Opers::valid_pred(ls) || Opers::valid_bool(ls);
        else {
            auto& op = cast_key(ls.cfront());
            is_pred = Opers::valid_pred(op) || Opers::valid_bool(op);
        }
        if (is_pred) return Pred<Arg>::new_me(FORWARD(ls), move(kptr), move(vptr), move(imap_ptr));
        return Fun<Arg>::new_me(FORWARD(ls), move(kptr), move(vptr), move(imap_ptr));
    }

    template <typename Arg, typename L, Req<is_list_v<L>>>
    Ptr new_fun(Key op, L&& ls, Keys_ptr kptr, Values_ptr<Arg> vptr, Ids_map_ptr imap_ptr)
    {
        using Opers = fun::Opers<Arg>;
        if (Opers::valid_pred(op) || Opers::valid_bool(op))
            return Pred<Arg>::new_me(move(op), FORWARD(ls), move(kptr), move(vptr), move(imap_ptr));
        return Fun<Arg>::new_me(move(op), FORWARD(ls), move(kptr), move(vptr), move(imap_ptr));
    }
}
