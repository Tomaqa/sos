#pragma once

namespace unsot::expr {
    template <typename F>
    auto as_ckeys(F f)
    {
        return wrap(move(f), cast_key<const Ptr&>);
    }

    template <typename F>
    auto as_clists(F f)
    {
        return wrap(move(f), List::cast<const Ptr&>);
    }

    template <typename F>
    auto as_cformulas(F f)
    {
        return wrap(move(f), Formula::cast<const Ptr&>);
    }

    template <typename K, typename F>
    auto as_keys(F f)
    {
        return wrap(move(f), cast_key<Forward_as<Ptr, K>>);
    }

    template <typename L, typename F>
    auto as_lists(F f)
    {
        return wrap(move(f), List::cast<Forward_as_ref<Ptr, L>>);
    }

    template <typename L, typename F>
    auto as_formulas(F f)
    {
        return wrap(move(f), Formula::cast<Forward_as_ref<Ptr, L>>);
    }

    template <typename F>
    auto as_ckeys_check(F f)
    {
        return wrap(move(f), cast_key_check<const Ptr&>);
    }

    template <typename F>
    auto as_clists_check(F f)
    {
        return wrap(move(f), List::cast_check<const Ptr&>);
    }

    template <typename F>
    auto as_cformulas_check(F f)
    {
        return wrap(move(f), Formula::cast_check<const Ptr&>);
    }

    template <typename K, typename F>
    auto as_keys_check(F f)
    {
        return wrap(move(f), cast_key_check<Forward_as<Ptr, K>>);
    }

    template <typename L, typename F>
    auto as_lists_check(F f)
    {
        return wrap(move(f), List::cast_check<Forward_as_ref<Ptr, L>>);
    }

    template <typename L, typename F>
    auto as_formulas_check(F f)
    {
        return wrap(move(f), Formula::cast_check<Forward_as_ref<Ptr, L>>);
    }

    template <typename E, typename F>
    auto as_celems(F f)
    {
        return wrap(move(f), E::template cast<const Ptr&>);
    }

    template <typename Arg, typename E, typename F>
    auto as_cvalues(F f)
    {
        return wrap(move(f), cast_value<Arg, E, const Ptr&>);
    }

    template <typename E, typename F>
    auto as_elems(F f)
    {
        return wrap(move(f), Decay<E>::template cast<Forward_as_ref<Ptr, E>>);
    }

    template <typename Arg, typename E, typename F>
    auto as_values(F f)
    {
        return wrap(move(f), cast_value<Arg, E, Forward_as<Ptr, Arg>>);
    }

    template <typename E, typename F>
    auto as_celems_check(F f)
    {
        return wrap(move(f), E::template cast_check<const E&>);
    }

    template <typename Arg, typename E, typename F>
    auto as_cvalues_check(F f)
    {
        return wrap(move(f), cast_value_check<Arg, E, const Ptr&>);
    }

    template <typename E, typename F>
    auto as_elems_check(F f)
    {
        return wrap(move(f), Decay<E>::template cast_check<Forward_as_ref<Ptr, E>>);
    }

    template <typename Arg, typename E, typename F>
    auto as_values_check(F f)
    {
        return wrap(move(f), cast_value_check<Arg, E, Forward_as<Ptr, Arg>>);
    }

    template <typename E, typename F>
    auto as_cints(F f)
    {
        return wrap(move(f), cast_int<E, const Ptr&>);
    }

    template <typename E, typename F>
    auto as_creals(F f)
    {
        return wrap(move(f), cast_real<E, const Ptr&>);
    }

    template <typename I, typename E, typename F>
    auto as_ints(F f)
    {
        return wrap(move(f), cast_int<E, Forward_as<Ptr, I>>);
    }

    template <typename R, typename E, typename F>
    auto as_reals(F f)
    {
        return wrap(move(f), cast_real<E, Forward_as<Ptr, R>>);
    }

    template <typename E, typename F>
    auto as_cints_check(F f)
    {
        return wrap(move(f), cast_int_check<E, const Ptr&>);
    }

    template <typename E, typename F>
    auto as_creals_check(F f)
    {
        return wrap(move(f), cast_real_check<E, const Ptr&>);
    }

    template <typename I, typename E, typename F>
    auto as_ints_check(F f)
    {
        return wrap(move(f), cast_int_check<E, Forward_as<Ptr, I>>);
    }

    template <typename R, typename E, typename F>
    auto as_reals_check(F f)
    {
        return wrap(move(f), cast_real_check<E, Forward_as<Ptr, R>>);
    }

    //+ template <typename UnF, typename LoopF>
    //+ void as_clist_or_csublists(const List& list, UnF f, LoopF loop)
    //+ {
    //+     if (!list.is_deep()) return f(list);
    //+     return loop(list, as_clist(move(f)));
    //+ }

    //+ template <typename UnF, typename LoopF>
    //+ void as_list_or_sublists(List& list, UnF f, LoopF loop)
    //+ {
    //+     if (!list.is_deep()) return f(list);
    //+     return loop(list, as_list(move(f)));
    //+ }

    //+ template <typename UnF, typename LoopF>
    //+ void as_clist_or_csublists_check(const List& list, UnF f, LoopF loop)
    //+ {
    //+     if (!list.is_deep()) return f(list);
    //+     return loop(list, as_clist_check(move(f)));
    //+ }

    //+ template <typename UnF, typename LoopF>
    //+ void as_list_or_sublists_check(List& list, UnF f, LoopF loop)
    //+ {
    //+     if (!list.is_deep()) return f(list);
    //+     return loop(list, as_list_check(move(f)));
    //+ }

    template <typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_elem_base(L&& ls, UnF f)
    {
        for_each_if_elem<Elem_base>(FORWARD(ls), move(f));
    }

    template <typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_key(L&& ls, UnF f)
    {
        for_each_if(FORWARD(ls), is_key, move(f));
    }

    template <typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_list(L&& ls, UnF f)
    {
        for_each_if(FORWARD(ls), List::is_me, move(f));
    }

    template <typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_formula(L&& ls, UnF f)
    {
        for_each_if(FORWARD(ls), Formula::is_me, move(f));
    }

    template <typename E, typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_elem(L&& ls, UnF f)
    {
        for_each_if(FORWARD(ls), E::is_me, move(f));
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_value(L&& ls, UnF f)
    {
        for_each_if(FORWARD(ls), is_value<Arg, E>, move(f));
    }

    template <typename E, typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_int(L&& ls, UnF f)
    {
        for_each_if_value<Int, E>(FORWARD(ls), move(f));
    }

    template <typename E, typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_real(L&& ls, UnF f)
    {
        for_each_if_value<Real, E>(FORWARD(ls), move(f));
    }

    namespace aux {
        template <typename L, Req<is_list_v<L>> = 0, typename UnElsePred, typename UnElseF>
        void for_each_else_rec(L&& ls, UnElsePred pred, UnElseF f)
        {
            for_each_else_rec_pre(FORWARD(ls), List::is_me, List::cast<Forward_as_ref<Ptr, L>>,
                                  [&pred, &f](auto&& ptr){
                if (!pred(ptr)) return;
                f(FORWARD(ptr));
            });
        }
    }

    template <typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_elem_base_rec(L&& ls, UnF f)
    {
        return for_each_if_elem_rec<Elem_base>(FORWARD(ls), move(f));
    }

    template <typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_key_rec(L&& ls, UnF f)
    {
        aux::for_each_else_rec(FORWARD(ls), is_key, move(f));
    }

    template <typename L, Req<is_list_v<L>>, typename UnF, typename BreakPred>
    void for_each_if_list_rec_pre(L&& ls, UnF f, BreakPred break_pred)
    {
        for_each_if_rec_pre(ls, List::is_me, move(f), List::cast<Forward_as_ref<Ptr, L>>,
                            as_clists(move(break_pred)));
    }

    template <typename L, Req<is_list_v<L>>, typename UnF, typename BreakPred>
    void for_each_if_list_rec_post(L&& ls, UnF f, BreakPred break_pred)
    {
        for_each_if_rec_post(ls, List::is_me, move(f), List::cast<Forward_as_ref<Ptr, L>>,
                             as_clists(move(break_pred)));
    }

    template <typename L, Req<is_list_v<L>>, typename UnF, typename BreakPred>
    void for_each_if_formula_rec_pre(L&& ls, UnF f, BreakPred break_pred)
    {
        for_each_if_rec_pre(ls, Formula::is_me, move(f), Formula::cast<Forward_as_ref<Ptr, L>>,
                            as_cformulas(move(break_pred)));
    }

    template <typename L, Req<is_list_v<L>>, typename UnF, typename BreakPred>
    void for_each_if_formula_rec_post(L&& ls, UnF f, BreakPred break_pred)
    {
        for_each_if_rec_post(ls, Formula::is_me, move(f), Formula::cast<Forward_as_ref<Ptr, L>>,
                             as_cformulas(move(break_pred)));
    }

    template <typename E, typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_elem_rec(L&& ls, UnF f)
    {
        aux::for_each_else_rec(FORWARD(ls), E::is_me, move(f));
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_value_rec(L&& ls, UnF f)
    {
        aux::for_each_else_rec(FORWARD(ls), is_value<Arg, E>, move(f));
    }

    template <typename E, typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_int_rec(L&& ls, UnF f)
    {
        for_each_if_value_rec<Int, E>(FORWARD(ls), move(f));
    }

    template <typename E, typename L, Req<is_list_v<L>>, typename UnF>
    void for_each_if_real_rec(L&& ls, UnF f)
    {
        for_each_if_value_rec<Real, E>(FORWARD(ls), move(f));
    }

    template <typename L, Req<is_list_v<L> && !is_cref_v<L>>>
    L replace(L&& ls, const Key& from, const Key& to)
    {
        for_each_if_key_rec(ls, as_keys([&from, &to](Key& key){
            if (key == from) key = to;
        }));
        return FORWARD(ls);
    }

    template <typename L, Req<is_list_v<L> && !is_cref_v<L>>>
    L replace(L&& ls, const Keys& from, const Keys& to)
    {
        for_each(from, cbegin(to), [&ls](const Key& from1, const Key& to1){
            replace(ls, from1, to1);
        });
        return FORWARD(ls);
    }

    template <typename L, Req<is_list_v<L>>>
    L replace_copy(const L& ls, const Key& from, const Key& to)
    {
        //+ not optimal
        return replace(L::cons(ls), from, to);
    }

    template <typename L, Req<is_list_v<L>>>
    L replace_copy(const L& ls, const Keys& from, const Keys& to)
    {
        //+ not optimal
        return replace(L::cons(ls), from, to);
    }

    template <typename L, Req<is_list_v<L>>>
    String to_lines(L&& ls)
    {
        using L_ = Rm_ref<L>;
        using To_string = typename Decay<L>::template To_string<L_>;
        typename To_string::Conf conf;
        conf.top_brackets = false;
        conf.rec = true;
        return To_string(Ref<L_>(FORWARD(ls))).perform(conf) + "\n";
    }
}
