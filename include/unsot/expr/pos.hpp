#pragma once

#include "expr.hpp"

#include "util/optional.hpp"
#include "util/ref.hpp"

namespace unsot::expr {
    namespace aux::pos {
        template <typename T, typename L> class Const_crtp;
        template <typename T, typename L> class Crtp;

        template <typename L, template<typename...> typename P> struct Tp;

        template <typename P, typename E = Elem> struct Const_foo;
        template <typename E = Elem> struct Foo;
    }

    using Const_pos = aux::pos::Tp<const List, aux::pos::Const_crtp>;
    using Pos = aux::pos::Tp<List, aux::pos::Crtp>;

    template <typename L, Req<is_list_v<L>> = 0> auto make_pos(L&&);

    template <typename P> bool is_elem_base_at(const P&) noexcept;
    template <typename P> bool is_key_at(const P&) noexcept;
    template <typename P> bool is_list_at(const P&) noexcept;
    template <typename P> bool is_seq_at(const P&) noexcept;
    template <typename P> void check_is_elem_base_at(const P&);
    template <typename P> void check_is_key_at(const P&);
    template <typename P> void check_is_list_at(const P&);
    template <typename P> void check_is_seq_at(const P&);

    template <typename E = Elem, typename P> bool is_elem_at(const P&) noexcept;
    template <typename Arg, typename E = Elem, typename P> bool is_value_at(const P&) noexcept;
    template <typename E = Elem, typename P> void check_is_elem_at(const P&);
    template <typename Arg, typename E = Elem, typename P> void check_is_value_at(const P&);

    template <typename E = Elem, typename P> bool is_int_at(const P&) noexcept;
    template <typename E = Elem, typename P> bool is_real_at(const P&) noexcept;
    template <typename E = Elem, typename P> void check_is_int_at(const P&);
    template <typename E = Elem, typename P> void check_is_real_at(const P&);

    template <typename P> bool is_flat_list_at(const P&) noexcept;
    template <typename P> bool is_deep_list_at(const P&) noexcept;
    template <typename P> void check_is_flat_list_at(const P&);
    template <typename P> void check_is_deep_list_at(const P&);

    template <typename P> Forward_as_ref<Elem_base, P> peek_elem_base(P&&) noexcept;
    template <typename P> Forward_as_ref<Key, P> peek_key(P&&);
    template <typename P> Forward_as_ref<List, P> peek_list(P&&) noexcept;
    template <typename P> auto get_elem_base(P&&) noexcept
        -> Cond_t<Decay<P>::is_const_v, const Elem_base&, Forward_as_ref<Elem_base, P>>;
    template <typename P> auto get_key(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Key&, Forward_as_ref<Key, P>>;
    template <typename P> auto get_list(P&&) noexcept
        -> Cond_t<Decay<P>::is_const_v, const List&, Forward_as_ref<List, P>>;
    Key extract_key(Pos&);
    List extract_list(Pos&);
    Key extract_copy_key(Pos&);
    List extract_shallow_copy_list(Pos&);
    List extract_deep_copy_list(Pos&);
    Key extract_maybe_copy_key(Pos&);
    List extract_maybe_shallow_copy_list(Pos&);
    List extract_maybe_deep_copy_list(Pos&);
    template <typename P> Forward_as_ref<Elem_base, P> peek_elem_base_check(P&&);
    template <typename P> Forward_as_ref<Key, P> peek_key_check(P&&);
    template <typename P> Forward_as_ref<List, P> peek_list_check(P&&);
    template <typename P> auto get_elem_base_check(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Elem_base&, Forward_as_ref<Elem_base, P>>;
    template <typename P> auto get_key_check(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Key&, Forward_as_ref<Key, P>>;
    template <typename P> auto get_list_check(P&&)
        -> Cond_t<Decay<P>::is_const_v, const List&, Forward_as_ref<List, P>>;
    Key extract_key_check(Pos&);
    List extract_list_check(Pos&);
    Key extract_copy_key_check(Pos&);
    List extract_shallow_copy_list_check(Pos&);
    List extract_deep_copy_list_check(Pos&);
    Key extract_maybe_copy_key_check(Pos&);
    List extract_maybe_shallow_copy_list_check(Pos&);
    List extract_maybe_deep_copy_list_check(Pos&);

    template <typename E = Elem, typename P> Forward_as_ref<E, P> peek_elem(P&&) noexcept;
    template <typename Arg, typename E = Elem, typename P>
        Forward_as_ref<Arg, P> peek_value(P&&);
    template <typename E = Elem, typename P> auto get_elem(P&&) noexcept
        -> Cond_t<Decay<P>::is_const_v, const E&, Forward_as_ref<E, P>>;
    template <typename Arg, typename E = Elem, typename P> auto get_value(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Arg&, Forward_as_ref<Arg, P>>;
    template <typename E = Elem> E extract_elem(Pos&);
    template <typename Arg, typename E = Elem> Arg extract_value(Pos&);
    template <typename E = Elem> E extract_copy_elem(Pos&);
    template <typename Arg, typename E = Elem> Arg extract_copy_value(Pos&);
    template <typename E = Elem> E extract_maybe_copy_elem(Pos&);
    template <typename Arg, typename E = Elem> Arg extract_maybe_copy_value(Pos&);
    template <typename E = Elem, typename P> Forward_as_ref<E, P> peek_elem_check(P&&);
    template <typename Arg, typename E = Elem, typename P>
        Forward_as_ref<Arg, P> peek_value_check(P&&);
    template <typename E = Elem, typename P> auto get_elem_check(P&&)
        -> Cond_t<Decay<P>::is_const_v, const E&, Forward_as_ref<E, P>>;
    template <typename Arg, typename E = Elem, typename P> auto get_value_check(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Arg&, Forward_as_ref<Arg, P>>;
    template <typename E = Elem> E extract_elem_check(Pos&);
    template <typename Arg, typename E = Elem> Arg extract_value_check(Pos&);
    template <typename E = Elem> E extract_copy_elem_check(Pos&);
    template <typename Arg, typename E = Elem> Arg extract_copy_value_check(Pos&);
    template <typename E = Elem> E extract_maybe_copy_elem_check(Pos&);
    template <typename Arg, typename E = Elem> Arg extract_maybe_copy_value_check(Pos&);

    template <typename E = Elem, typename P> Forward_as_ref<Int, P> peek_int(P&&);
    template <typename E = Elem, typename P> Forward_as_ref<Real, P> peek_real(P&&);
    template <typename E = Elem, typename P> auto get_int(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Int&, Forward_as_ref<Int, P>>;
    template <typename E = Elem, typename P> auto get_real(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Real&, Forward_as_ref<Real, P>>;
    template <typename E = Elem> Int extract_int(Pos&);
    template <typename E = Elem> Real extract_real(Pos&);
    template <typename E = Elem> Int extract_copy_int(Pos&);
    template <typename E = Elem> Real extract_copy_real(Pos&);
    template <typename E = Elem> Int extract_maybe_copy_int(Pos&);
    template <typename E = Elem> Real extract_maybe_copy_real(Pos&);
    template <typename E = Elem, typename P> Forward_as_ref<Int, P> peek_int_check(P&&);
    template <typename E = Elem, typename P> Forward_as_ref<Real, P> peek_real_check(P&&);
    template <typename E = Elem, typename P> auto get_int_check(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Int&, Forward_as_ref<Int, P>>;
    template <typename E = Elem, typename P> auto get_real_check(P&&)
        -> Cond_t<Decay<P>::is_const_v, const Real&, Forward_as_ref<Real, P>>;
    template <typename E = Elem> Int extract_int_check(Pos&);
    template <typename E = Elem> Real extract_real_check(Pos&);
    template <typename E = Elem> Int extract_copy_int_check(Pos&);
    template <typename E = Elem> Real extract_copy_real_check(Pos&);
    template <typename E = Elem> Int extract_maybe_copy_int_check(Pos&);
    template <typename E = Elem> Real extract_maybe_copy_real_check(Pos&);

    void maybe_shallow_copy_at(Pos&);
    void maybe_deep_copy_at(Pos&);
}

namespace unsot::expr::aux::pos {
    template <typename T, typename L>
    class Const_crtp : public Static<Const_crtp<T, L>>,
                       public unsot::Crtp<T> {
    public:
        using typename unsot::Crtp<T>::That;

        template <typename L_ = L> struct To_string;

        using Ref = util::Ref<L>;

        using List = typename Ref::Item;

        static constexpr bool is_const_v = Ref::is_const_v;

        using const_iterator = typename List::const_iterator;
        using iterator = typename List::iterator;
        using Iterator = Cond_t<is_const_v, const_iterator, iterator>;

        Const_crtp()                                                                      = default;
        ~Const_crtp()                                                                     = default;
        Const_crtp(const Const_crtp&)                                                     = default;
        Const_crtp& operator =(const Const_crtp&)                                         = default;
        Const_crtp(Const_crtp&&)                                                          = default;
        Const_crtp& operator =(Const_crtp&&)                                              = default;
        Const_crtp(Ref);
        template <typename L_, Req<is_list_v<L_> && is_const_v> = 0>
            explicit Const_crtp(L_& ls)                          : Const_crtp(Ref(as_const(ls))) { }
        explicit Const_crtp(List&& ls)                               : Const_crtp(Ref(move(ls))) { }
        template <typename L_, Req<is_list_v<L_>> = 0> Const_crtp& operator =(L_&&);

        const auto& clist() const                                           { return _ref.citem(); }
        const auto& list() const&                                                { return clist(); }

        constexpr bool is_owner() const noexcept                         { return _ref.is_owner(); }

        const_iterator cit() const noexcept;
        const_iterator it() const noexcept                                         { return cit(); }

        const_iterator cbegin() const noexcept                          { return clist().cbegin(); }
        const_iterator cend() const noexcept                              { return clist().cend(); }
        const_iterator begin() const noexcept                                   { return cbegin(); }
        const_iterator end() const noexcept                                       { return cend(); }

        bool at_begin() const noexcept;
        bool at_end() const noexcept;
        /// valid only if `front` contains valid value
        bool at_front() const noexcept;
        bool at_back() const noexcept;
        void check_at_begin() const;
        void check_not_at_begin() const;
        void check_at_end() const;
        void check_not_at_end() const;
        void check_at_front() const;
        void check_at_back() const;
        operator bool() const noexcept                                         { return !at_end(); }

        That& set_it(Iterator) noexcept;
        That& set_it() noexcept;
        That& unset_it() noexcept;
        That& set_back_it() noexcept;
        That& next() noexcept;
        That& prev() noexcept;

        const Ptr& cpeek() const noexcept;
        const Ptr& peek() const& noexcept                                        { return cpeek(); }
        const Ptr& cget() noexcept;
        const Ptr& get() noexcept                                                 { return cget(); }
        const Ptr& cpeek_check() const;
        const Ptr& peek_check() const&                                     { return cpeek_check(); }
        const Ptr& cget_check();
        const Ptr& get_check()                                              { return cget_check(); }

        static auto make_to_string(const Const_crtp&);
        String to_string() const&;
    protected:
        template <typename B> class To_string_mixin;

        auto& list()&                                                        { return _ref.item(); }
        auto&& list()&&                                                     { return move(list()); }

        Iterator it() noexcept;

        Iterator begin() noexcept                                         { return list().begin(); }
        Iterator end() noexcept                                             { return list().end(); }

        /// more efficient in cases where end is not possible or permitted
        const Iterator& crit() const noexcept                               { return _it.cvalue(); }
        Iterator& rit() noexcept                                             { return _it.value(); }
    private:
        Ref _ref{};

        Optional<Iterator> _it{};
    };
    extern template class Const_crtp<Const_pos, const List>;
    extern template class Const_crtp<Pos, List>;

    template <typename T, typename L>
    class Crtp : public Inherit<Const_crtp<T, L>> {
    public:
        using Inherit = unsot::Inherit<Const_crtp<T, L>>;
        using typename Inherit::That;

        template <typename L_ = L>
            using To_string = typename Inherit::template To_string<L_>;

        using typename Inherit::Ref;

        using typename Inherit::List;

        using typename Inherit::const_iterator;
        using typename Inherit::iterator;
        using typename Inherit::Iterator;

        using Inherit::Inherit;
        explicit Crtp(const List& ls)                                           : Crtp(List(ls)) { }
        explicit Crtp(List& ls)                                               : Inherit(Ref(ls)) { }
        explicit Crtp(List&& ls)                                        : Inherit(Ref(move(ls))) { }
        template <typename L_, Req<is_list_v<L_>> = 0> Crtp& operator =(L_&&);

        using Inherit::list;

        using Inherit::it;

        using Inherit::begin;
        using Inherit::end;

        using Inherit::peek;
        using Inherit::peek_check;
        Ptr& peek()& noexcept;
        Ptr&& peek()&& noexcept                                             { return move(peek()); }
        Ptr& get()& noexcept;
        Ptr&& get()&& noexcept                                               { return move(get()); }
        Ptr extract();
        Ptr& peek_check()&;
        Ptr&& peek_check()&&                                          { return move(peek_check()); }
        Ptr& get_check()&;
        Ptr&& get_check()&&                                            { return move(get_check()); }
        Ptr extract_check();

        /// (linear complexity)
        void clear();

        That& insert(Ptr ptr);
        That& insert(const_iterator first, const_iterator last);
        template <typename... Args> That& emplace(Args&&...);
        That& erase();
        That& erase_until(const_iterator);
        /// (due to the need of calling destructors, it is always linear)
        That& erase_until();
        That& splice(List&&);
        That& splice(List&&, const_iterator);
        That& splice(List&&, const_iterator first, const_iterator last);

        using Inherit::make_to_string;
        using Inherit::to_string;
        static auto make_to_string(Crtp&&);
        String to_string()&&;
    };
    extern template class Crtp<Pos, List>;

    template <typename L, template<typename...> typename P>
    struct Tp : Inherit<P<Tp<L, P>, L>> {
        using Inherit = unsot::Inherit<P<Tp<L, P>, L>>;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;
    };

    template <typename P, typename E>
    struct Const_foo final {
        static bool is_elem_base_at(const P&) noexcept;
        static bool is_key_at(const P&) noexcept;
        static bool is_list_at(const P&) noexcept;
        static bool is_seq_at(const P&) noexcept;
        static void check_is_elem_base_at(const P&);
        static void check_is_key_at(const P&);
        static void check_is_list_at(const P&);
        static void check_is_seq_at(const P&);

        static bool is_elem_at(const P&) noexcept;
        static void check_is_elem_at(const P&);

        static bool is_int_at(const P&) noexcept;
        static bool is_real_at(const P&) noexcept;
        static void check_is_int_at(const P&);
        static void check_is_real_at(const P&);

        static bool is_flat_list_at(const P&) noexcept;
        static bool is_deep_list_at(const P&) noexcept;
        static void check_is_flat_list_at(const P&);
        static void check_is_deep_list_at(const P&);
    };
    extern template struct Const_foo<Const_pos, Elem>;
    extern template struct Const_foo<Pos, Elem>;

    template <typename E>
    struct Foo final {
        static E extract_elem(Pos&);
        static E extract_elem_check(Pos&);
        static E extract_copy_elem(Pos&);
        static E extract_copy_elem_check(Pos&);
        static E extract_maybe_copy_elem(Pos&);
        static E extract_maybe_copy_elem_check(Pos&);

        static Int extract_int(Pos&);
        static Real extract_real(Pos&);
        static Int extract_copy_int(Pos&);
        static Real extract_copy_real(Pos&);
        static Int extract_maybe_copy_int(Pos&);
        static Real extract_maybe_copy_real(Pos&);
        static Int extract_int_check(Pos&);
        static Real extract_real_check(Pos&);
        static Int extract_copy_int_check(Pos&);
        static Real extract_copy_real_check(Pos&);
        static Int extract_maybe_copy_int_check(Pos&);
        static Real extract_maybe_copy_real_check(Pos&);
    };
    extern template struct Foo<Elem>;
}

namespace unsot::expr::aux::pos {
    template <typename T, typename L>
    template <typename B>
    class Const_crtp<T, L>::To_string_mixin : public Inherit<B> {
    public:
        using Inherit = unsot::Inherit<B>;

        using typename Inherit::Conf;

        using typename Inherit::Ref;

        using typename Inherit::const_iterator;
        using typename Inherit::Iterator;

        template <typename P> To_string_mixin(P&&);
    protected:
        void perform_head_post(String&, const Conf&, int depth) override;
        void perform_body_pre(String&, const Conf&, int depth, Iterator) override;
        void perform_tail_pre(String&, const Conf&, int depth) override;

        const Const_crtp& _pos;
        bool _mark_pos{};
        const_iterator _pos_it{};
    };

    template <typename T, typename L>
    template <typename L_>
    struct Const_crtp<T, L>::To_string
        : Inherit<To_string_mixin<typename Decay<L>::template To_string<L_>>> {
        using Inherit = unsot::Inherit<To_string_mixin<typename Decay<L>::template To_string<L_>>>;

        using Inherit::Inherit;
    };
}

#include "expr/pos.inl"
