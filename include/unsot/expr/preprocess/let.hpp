#pragma once

#include "expr.hpp"

#include <stack>

namespace unsot::expr {
    class Let : public Inherit<Place, Let> {
    public:
        Let()                                                       = default;
        virtual ~Let()                                              = default;
        Let(const Let&)                                              = delete;
        Let& operator =(const Let&)                                  = delete;
        Let(Let&&)                                                  = default;
        Let& operator =(Let&&)                                      = default;
        Let(Key key_)                                   : _key(move(key_)) { }

        /// Deep copies *only* the underlying end pointer
        Ptr to_ptr() const& override;
        Ptr to_ptr()&& override          { return mthis().Inherit::to_ptr(); }

        const auto& ckey() const noexcept                     { return _key; }
        const auto& key() const noexcept                    { return ckey(); }

        inline const Ptr& cptr() const noexcept;
        inline const Ptr& ptr() const& noexcept             { return cptr(); }
        inline Ptr& ptr()& noexcept;
        inline Ptr&& ptr()&& noexcept                  { return move(ptr()); }
        const Ptr& cptr_check() const;
        const Ptr& ptr_check() const&                 { return cptr_check(); }
        Ptr& ptr_check()&;
        Ptr&& ptr_check()&&                      { return move(ptr_check()); }

        bool is_elem() const noexcept override               { return false; }
        bool is_list() const noexcept override               { return false; }

        inline bool undeclared() const noexcept;
        inline bool undefined() const noexcept;
        void check_not_undeclared() const;
        void check_not_undefined() const;
        String undeclared_msg() const;
        String undefined_msg() const;

        inline bool is_self_ref(const Ptr&) const noexcept;
        void check_is_not_self_ref(const Ptr&) const;
        String is_self_ref_msg() const;
        bool is_self_ref() const noexcept      { return is_self_ref(cptr()); }
        void check_is_not_self_ref() const  { check_is_not_self_ref(cptr()); }

        /// Traverses all lets until an "actual expr place" is reached
        Ptr expand() const;
        /// Also recursively expands all underlying lets (if it is a list)
        Ptr expand_all() const;

        template <bool groupedV = false> void push(Ptr = {});
        void pop(bool grouped = false);

        void set(Ptr);

        String to_string() const& override;
        String to_string()&& override;
        String key_to_string() const;
    protected:
        struct Stack_entry;
        //! struct Meta_stack_entry;

        using Stack = std::stack<Stack_entry>;
        //! using Meta_stack = std::stack<Meta_stack_entry>;

        const auto& cstack() const noexcept                 { return _stack; }
        auto& stack() noexcept                              { return _stack; }
        const auto& cstack_entry() const noexcept   { return cstack().top(); }
        auto& stack_entry() noexcept                 { return stack().top(); }
        //! const auto& cmeta_stack() const noexcept       { return _meta_stack; }
        //! auto& meta_stack() noexcept                    { return _meta_stack; }
        //! const auto& cmeta_stack_entry() const noexcept
        //!                                        { return cmeta_stack().top(); }
        //! auto& meta_stack_entry() noexcept       { return meta_stack().top(); }

        bool lequals(const This&) const noexcept;
        bool equals_base_anyway(const Base&) const noexcept override;
    private:
        Key _key{};

        Stack _stack{};
        //! Meta_stack _meta_stack{};
    };
    extern template void Let::push<false>(Ptr);
    extern template void Let::push<true>(Ptr);
}

namespace unsot::expr {
    struct Let::Stack_entry {
        Ptr ptr{};
        const bool grouped{};
        //! int skipped_cnt{};
    };

    //! struct Let::Meta_stack_entry {
    //!     const bool grouped{};
    //! };
}

#include "expr/preprocess/let.inl"
