#pragma once

namespace unsot::expr {
    const Ptr& Let::cptr() const noexcept
    {
        assert(!undeclared());
        auto& entry = cstack_entry();
        /// With arg to avoid infinite loop
        assert(!is_self_ref(entry.ptr));
        //! it would not return the self-reference if `skipped_cnt > 0`
        //! the problem is that the let object (and its stack) is shared
        //! but we need a standalone instance where it is clear
        //! how many times it must be dereferenced to the end pointer (indep. on the other insts!)
        return entry.ptr;
    }

    Ptr& Let::ptr()& noexcept
    {
        assert(!undeclared());
        auto& entry = stack_entry();
        /// With arg to avoid infinite loop
        assert(!is_self_ref(entry.ptr));
        return entry.ptr;
    }

    bool Let::undeclared() const noexcept
    {
        return empty(cstack());
    }

    bool Let::undefined() const noexcept
    {
        return !cptr();
    }

    bool Let::is_self_ref(const Ptr& ptr_) const noexcept
    {
        return this == ptr_.get();
    }
}
