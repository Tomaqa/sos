#pragma once

namespace unsot::expr::bools {
    template <typename E, typename E_, Req<is_elem_v<E_, E>>>
    Lit::Lit(E_&& elem, bool sign_, Tag<E>)
    {
        if (elem.is_key()) {
            *this = Lit(FORWARD(elem).get_key(), sign_);
            return;
        }

        Bool val = FORWARD(elem).template get_value<Bool>();
        if (!sign_) val = bools::neg(move(val));
        *this = to_string(move(val));
    }

    ////////////////////////////////////////////////////////////////

    template <typename L, Req<is_list_v<L>>>
    Cnf::Cnf(L&& ls, Idx init_idx_, Var_id x)
        : _init_idx(init_idx_), _x(move(x))
    {
        parse_top(FORWARD(ls));
    }

    template <typename L, Req<is_list_v<L>>>
    Cnf::Cnf(L&& ls, Var_id x)
        : Cnf(FORWARD(ls), gcounter, move(x))
    { }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr::bools {
    template <typename E, typename E_, Req<is_elem_v<E_, E>>>
    Decay<E_> neg(E_&& elem)
    {
        if (elem.is_key()) return Lit(FORWARD(elem).get_key(), false);
        return neg(FORWARD(elem).template get_value<Bool>());
    }

    template <typename L, Req<is_lit_v<L>>>
    Decay<L> neg(L&& lit)
    {
        return FORWARD(lit).to_neg();
    }

    template <typename C, Req<is_clause_v<C>>>
    Decay<C> neg(C&& clause)
    {
        return FORWARD(clause).to_neg();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename E, typename... Args>
    Ptr new_bool(Args&&... args)
    {
        return new_value<Bool, E>(FORWARD(args)...);
    }

    template <typename E, typename T>
    Forward_as<Bool, T> cast_bool(T&& t)
    {
        return cast_value<Bool, E>(FORWARD(t));
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    Forward_as<Bool, P> cast_bool_check(P&& ptr)
    {
        return cast_value_check<Bool, E>(FORWARD(ptr));
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    auto cast_bool_l(P&& ptr) noexcept
    {
        return cast_value_l<Bool, E>(FORWARD(ptr));
    }

    template <typename E>
    bool is_bool(const Ptr& ptr) noexcept
    {
        return is_value<Bool, E>(ptr);
    }

    template <typename E>
    void check_is_bool(const Ptr& ptr)
    {
        check_is_value<Bool, E>(ptr);
    }

    template <typename E, typename T>
    Optional<Bool> to_bool(T&& t) noexcept
    {
        return to_value<Bool, E>(FORWARD(t));
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    Bool to_bool_check(P&& ptr)
    {
        return to_value_check<Bool, E>(FORWARD(ptr));
    }
}
