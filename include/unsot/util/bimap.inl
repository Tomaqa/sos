#pragma once

namespace unsot::util::aux::bimap {
    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    const auto& Base<Key1T, Key2T, Map1T, Map2T>::ckey2_l(const Key1& key1_) const
    {
        return cmap1()[key1_];
    }

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    auto& Base<Key1T, Key2T, Map1T, Map2T>::key2_l(const Key1& key1_) noexcept
    {
        return map1()[key1_];
    }

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    const auto& Base<Key1T, Key2T, Map1T, Map2T>::ckey1_l(const Key2& key2_) const
    {
        return cmap2()[key2_];
    }

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    auto& Base<Key1T, Key2T, Map1T, Map2T>::key1_l(const Key2& key2_) noexcept
    {
        return map2()[key2_];
    }

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    typename Base<Key1T, Key2T, Map1T, Map2T>::Key2_link
    Base<Key1T, Key2T, Map1T, Map2T>::find_key2(const Key1& key1_) const noexcept
    {
        auto it = cmap1().find(key1_);
        if (it == cmap1().end()) return nullptr;
        return it->second;
    }

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    typename Base<Key1T, Key2T, Map1T, Map2T>::Key1_link
    Base<Key1T, Key2T, Map1T, Map2T>::find_key1(const Key2& key2_) const noexcept
    {
        auto it = cmap2().find(key2_);
        if (it == cmap2().end()) return nullptr;
        return it->second;
    }

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    bool Base<Key1T, Key2T, Map1T, Map2T>::insert(const Key1& key1_, const Key2& key2_)
    {
        auto [it1, inserted1] = this->map1().insert(make_pair(key1_, Key2_link{}));
        if (!inserted1) return false;
        Key1_link key1_l = &it1->first;

        auto [it2, inserted2] = this->map2().insert(make_pair(key2_, key1_l));
        assert(inserted2);
        it1->second = &it2->first;
        return true;
    }

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    String Base<Key1T, Key2T, Map1T, Map2T>::to_string() const&
    {
        using unsot::to_string;
        String str;
        for (auto& [k1, kl2] : cmap1()) {
            str += to_string(k1) + " " + to_string(*kl2) + ", ";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    Operator_mixin<B>& Operator_mixin<B>::operator [](const Key1& key1_)
    {
        auto [it, _] = this->map1().insert({key1_, {}});
        _key1_l = &it->first;
        _key2_ll = &it->second;
        _ref1 = true;
        return *this;
    }

    template <typename B>
    Operator_mixin<B>& Operator_mixin<B>::operator [](const Key2& key2_)
    {
        auto [it, _] = this->map2().insert({key2_, {}});
        _key2_l = &it->first;
        _key1_ll = &it->second;
        _ref1 = false;
        return *this;
    }

    template <typename B>
    Operator_mixin<B>& Operator_mixin<B>::operator =(const Key2& key2_)
    {
        assign_key2(key2_);
        return *this;
    }

    template <typename B>
    Operator_mixin<B>& Operator_mixin<B>::operator =(const Key1& key1_)
    {
        assign_key1(key1_);
        return *this;
    }

    template <typename B>
    void Operator_mixin<B>::assign_key2(const Key2& key2_)
    {
        assert(_ref1.is_true());
        auto [it, _] = this->map2().insert({key2_, _key1_l});
        *_key2_ll = &it->first;
        _ref1.forget();
    }

    template <typename B>
    void Operator_mixin<B>::assign_key1(const Key1& key1_)
    {
        assert(_ref1.is_false());
        auto [it, _] = this->map1().insert({key1_, _key2_l});
        *_key1_ll = &it->first;
        _ref1.forget();
    }
}
