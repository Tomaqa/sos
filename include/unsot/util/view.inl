#pragma once

#include "util/alg.hpp"

namespace unsot::util::view {
    namespace aux {
        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::swap(That& rhs) noexcept
        {
            this->that().swap_impl(rhs);
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::swap_impl(That& rhs) noexcept
        {
            accessors().swap(rhs.accessors());
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
        Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::
            make_const_iterator(Const_access_iterator it) const noexcept
        {
            return this->cthat().make_const_iterator_impl(it);
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
        Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::
            make_iterator(Access_iterator it) noexcept
        {
            return this->that().make_iterator_impl(it);
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
        Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::cbegin() const noexcept
        {
            return make_const_iterator(cacc_begin());
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
        Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::cend() const noexcept
        {
            return make_const_iterator(cacc_end());
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
        Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::begin() noexcept
        {
            return make_iterator(acc_begin());
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
        Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::end() noexcept
        {
            return make_iterator(acc_end());
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        const auto& Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::
            caccess(const Accessor& acc) const noexcept
        {
            return this->cthat().caccess_impl(acc);
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        auto& Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::
            access(const Accessor& acc) noexcept
        {
            return this->that().access_impl(acc);
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        const auto&
        Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::operator [](Idx idx) const noexcept
        {
            return caccess(caccessor(idx));
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        auto&
        Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::operator [](Idx idx) noexcept
        {
            return access(accessor(idx));
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::push_back(Accessor acc)
        {
            this->that().push_back_impl(std::move(acc));
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::push_back_impl(Accessor acc)
        {
            accessors().push_back(std::move(acc));
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::pop_back()
        {
            this->that().pop_back_impl();
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::pop_back_impl()
        {
            accessors().pop_back();
        }

        template <typename V, typename PtrT, template<typename...> typename AccCont,
                  typename CPtrT, typename AccT, bool randomAccess>
        String Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::to_string() const&
        {
            using unsot::to_string;
            String str;
            for (auto& e : this->cthat()) {
                str += to_string(e) + " ";
            }
            return str;
        }

    ////////////////////////////////////////////////////////////////

        template <typename B, typename ContT>
        template <typename ItT, Req<is_iterator_v<ItT> && !is_ptr_v<Decay<ItT>>>>
        void Cont_mixin<B, ContT>::push_back(ItT it)
        {
            this->that().push_back_impl(it);
        }

        template <typename B, typename ContT>
        template <typename ItT>
        void Cont_mixin<B, ContT>::push_back_impl(ItT it)
        {
            this->push_back(to_pointer(it));
        }

        template <typename B, typename ContT>
        template <typename ItT>
        void Cont_mixin<B, ContT>::assign(ItT first, ItT last, size_t size_)
        {
            this->clear();
            this->reserve(size_);
            for (auto it = first; it != last; ++it) push_back(it);
        }

        template <typename B, typename ContT>
        template <typename ItT>
        void Cont_mixin<B, ContT>::assign(ItT first, ItT last)
        {
            assign(first, last, last - first);
        }

        template <typename B, typename ContT>
        template <typename C, Req<is_container_v<C>>>
        void Cont_mixin<B, ContT>::assign(C& cont_)
        {
            assign(std::begin(cont_), std::end(cont_), std::size(cont_));
        }

        template <typename B, typename ContT>
        template <typename C, Req<is_container_v<C>>>
        void Cont_mixin<B, ContT>::assign(C&& cont_)
        {
            assign(make_move_iterator(std::begin(cont_)),
                   make_move_iterator(std::end(cont_)), std::size(cont_));
        }

    ////////////////////////////////////////////////////////////////

        template <typename B>
        Mixin<B, true>::Mixin(Container& cont_)
            : _cont_l(&cont_)
        { }

        template <typename B>
        void Mixin<B, true>::swap_impl(That& rhs) noexcept
        {
            Inherit::swap_impl(rhs);
            std::swap(_cont_l, rhs._cont_l);
        }

        template <typename B>
        void Mixin<B, true>::connect(Container& cont_)
        {
            cont_l() = &cont_;
        }

        template <typename B>
        typename Mixin<B, true>::const_iterator
        Mixin<B, true>::make_const_iterator_impl(Const_access_iterator it) const noexcept
        {
            return {it, ccont_l()};
        }

        template <typename B>
        typename Mixin<B, true>::iterator
        Mixin<B, true>::make_iterator_impl(Access_iterator it) noexcept
        {
            return {it, cont_l()};
        }

        template <typename B>
        const auto& Mixin<B, true>::caccess_impl(const Accessor& acc) const noexcept
        {
            return ccont()[acc];
        }

        template <typename B>
        auto& Mixin<B, true>::access_impl(const Accessor& acc) noexcept
        {
            return cont()[acc];
        }

        template <typename B>
        typename Mixin<B, true>::Accessor Mixin<B, true>::to_accessor(Ptr ptr) const noexcept
        {
            /// Unreliable in general compared to the impl. with `distance`
            return ptr - &this->caccess(0);
        }

        template <typename B>
        void Mixin<B, true>::push_back(Ptr ptr)
        {
            this->that().push_back(to_accessor(ptr));
        }

        template <typename B>
        template <typename ItT>
        void Mixin<B, true>::push_back_impl(ItT it)
        {
            Accessor acc = std::distance(std::begin(cont()), it);
            this->push_back(std::move(acc));
        }

    ////////////////////////////////////////////////////////////////

        template <typename B>
        void Front_mixin<B>::push_front(Accessor acc)
        {
            this->that().push_front_impl(std::move(acc));
        }

        template <typename B>
        void Front_mixin<B>::push_front_impl(Accessor acc)
        {
            this->accessors().push_front(std::move(acc));
        }

        template <typename B>
        template <typename PtrT, typename AccT,
                  Req<is_ptr_v<Decay<PtrT>> && !is_iterator_v<PtrT> && !is_same_v<Decay<PtrT>, AccT>>>
        void Front_mixin<B>::push_front(PtrT ptr)
        {
            this->that().push_front(to_accessor(ptr));
        }

        template <typename B>
        template <typename ItT, Req<is_iterator_v<ItT> && !is_ptr_v<Decay<ItT>>>>
        void Front_mixin<B>::push_front(ItT it)
        {
            this->that().push_front_impl(it);
        }

        template <typename B>
        template <typename ItT>
        void Front_mixin<B>::push_front_impl(ItT it)
        {
            if constexpr (Inherit::random_access_v) {
                Accessor acc = std::distance(std::begin(this->cont()), it);
                this->push_front(std::move(acc));
            }
            else this->push_front(to_pointer(it));
        }

        template <typename B>
        void Front_mixin<B>::pop_front()
        {
            this->that().pop_front_impl();
        }

        template <typename B>
        void Front_mixin<B>::pop_front_impl()
        {
            this->accessors().pop_front();
        }
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    void Unique_mixin<B>::swap_impl(That& rhs) noexcept
    {
        Inherit::swap_impl(rhs);
        accessors_set().swap(rhs.accessors_set());
    }

    template <typename B>
    bool Unique_mixin<B>::contains(const Accessor& acc) const noexcept
    {
        return caccessors_set().contains(acc);
    }

    template <typename B>
    template <typename T, typename P, Req<is_same_v<P, typename T::Ptr>>>
    bool Unique_mixin<B>::contains(P ptr) const noexcept
    {
        if constexpr (T::random_access_v) {
            return contains(this->to_accessor(ptr));
        }
        else static_assert(false_tp<T>);
    }

    template <typename B>
    void Unique_mixin<B>::reserve(size_t size_)
    {
        Inherit::reserve(size_);
        accessors_set().reserve(size_);
    }

    template <typename B>
    void Unique_mixin<B>::clear()
    {
        Inherit::clear();
        accessors_set().clear();
    }

    template <typename B>
    void Unique_mixin<B>::push_back_impl(Accessor acc)
    {
        accessors_set().insert(acc);
        Inherit::push_back_impl(std::move(acc));
    }

    template <typename B>
    void Unique_mixin<B>::pop_back_impl()
    {
        //+ use multiset?
        accessors_set().erase(this->caccessors().back());
        Inherit::pop_back_impl();
    }

    template <typename B>
    template <typename T, typename Arg>
    void Unique_mixin<B>::push_front_impl(Arg&& arg)
    {
        if constexpr (enabled_v<aux::Enabled_front, T>) {
            if constexpr (is_same_v<DECAY(arg), Accessor>) {
                accessors_set().insert(arg);
            }
            Inherit::push_front_impl(FORWARD(arg));
        }
        else static_assert(false_tp<T>);
    }

    template <typename B>
    template <typename T>
    void Unique_mixin<B>::pop_front_impl()
    {
        if constexpr (enabled_v<aux::Enabled_front, T>) {
            //+ use multiset?
            accessors_set().erase(this->caccessors().front());
            Inherit::pop_front_impl();
        }
        else static_assert(false_tp<T>);
    }

    template <typename B>
    bool Unique_mixin<B>::insert(Accessor acc)
    {
        auto [_, inserted] = accessors_set().insert(acc);
        if (inserted) Inherit::push_back_impl(std::move(acc));
        return inserted;
    }

    template <typename B>
    template <typename T>
    void Unique_mixin<B>::merge(T&& rhs)
    {
        using T_ = DECAY(rhs);
        if constexpr (is_same_v<T_, This>) {
            accessors_set().merge(FORWARD(rhs.accessors_set()));
            append(this->accessors(), FORWARD(rhs.accessors()));
        }
        else if constexpr (is_same_v<T_, Accessors_set>) {
            append(this->accessors(), rhs);
            accessors_set().merge(FORWARD(rhs));
        }
        else if constexpr (is_same_v<T_, Accessors>) {
            std::copy(rhs.begin(), rhs.end(), inserter(accessors_set()));
            append(this->accessors(), FORWARD(rhs));
        }
        else static_assert(false_tp<T_>);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::util::view::aux {
    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    template <typename AccIt>
    const auto&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::Iterator_crtp<AccIt>::cderef() const noexcept
    {
        return this->cthat().cderef();
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    template <typename AccIt>
    auto&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::Iterator_crtp<AccIt>::deref() noexcept
    {
        return this->that().deref();
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    template <typename AccIt>
    String
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::Iterator_crtp<AccIt>::to_string() const&
    {
        using unsot::to_string;
        return to_string(cderef());
    }

    ////////////////////////////////////////////////////////////////

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::operator ++() noexcept
    {
        Inherit::operator ++();
        return *this;
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::operator ++(int) noexcept
    {
        auto it = Inherit::operator ++(0);
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, this->ccont_l()};
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::
        operator +(int n) const noexcept
    {
        /// It may not be implemented as a member function
        auto it = static_cast<const Inherit&>(*this) + n;
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, this->ccont_l()};
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::operator --() noexcept
    {
        Inherit::operator --();
        return *this;
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::operator --(int) noexcept
    {
        auto it = Inherit::operator --(0);
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, this->ccont_l()};
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::
        operator -(int n) const noexcept
    {
        /// It may not be implemented as a member function
        auto it = static_cast<const Inherit&>(*this) - n;
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, this->ccont_l()};
    }

    ////////////////////////////////////////////////////////////////

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator ++() noexcept
    {
        Inherit::operator ++();
        return *this;
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator ++(int) noexcept
    {
        auto it = Inherit::operator ++(0);
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, this->ccont_l()};
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator +(int n) const noexcept
    {
        /// It may not be implemented as a member function
        auto it = static_cast<const Inherit&>(*this) + n;
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, this->ccont_l()};
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator --() noexcept
    {
        Inherit::operator --();
        return *this;
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator --(int) noexcept
    {
        auto it = Inherit::operator --(0);
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, this->ccont_l()};
    }

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator -(int n) const noexcept
    {
        /// It may not be implemented as a member function
        auto it = static_cast<const Inherit&>(*this) - n;
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, this->ccont_l()};
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, bool randomAccess>
    template <typename AccIt>
    const auto& Mixin<B, randomAccess>::Iterator_base<AccIt>::cderef() const noexcept
    {
        return *Inherit::operator *();
    }

    template <typename B, bool randomAccess>
    template <typename AccIt>
    auto& Mixin<B, randomAccess>::Iterator_base<AccIt>::deref() noexcept
    {
        return *Inherit::operator *();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    template <typename AccIt>
    Mixin<B, true>::Iterator_base<AccIt>::
        Iterator_base(Parent par, Container_link cont_l_)
        : Inherit(par), _cont_l(cont_l_)
    { }

    template <typename B>
    template <typename AccIt>
    const auto& Mixin<B, true>::Iterator_base<AccIt>::
        cderef() const noexcept
    {
        const Accessor& acc = Inherit::operator *();
        return (*ccont_l())[acc];
    }

    template <typename B>
    template <typename AccIt>
    auto& Mixin<B, true>::Iterator_base<AccIt>::deref() noexcept
    {
        const Accessor& acc = Inherit::operator *();
        return (*cont_l())[acc];
    }
}
