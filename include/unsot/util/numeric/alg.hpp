#pragma once

#include "util/numeric.hpp"

#include <numeric>

namespace unsot::util::numeric {
    template <typename T>
        Vector<T>& operator +=(Vector<T>& lhs, const Vector<T>& rhs);
    template <typename T>
        Vector<T> operator +(Vector<T> lhs, const Vector<T>& rhs);
    template <typename T>
        Vector<T>& operator *=(Vector<T>& lhs, const T& rhs);
    template <typename T> Vector<T> operator *(Vector<T> lhs, const T& rhs);
    template <typename T> Vector<T> operator *(const T& lhs, Vector<T> rhs);

    template <typename T>
        bool apx_equal(const T&, const T&,
                       double rel_eps = def_rel_eps,
                       double abs_eps = def_abs_eps);
    template <typename T>
        bool apx_less_equal(const T&, const T&,
                            double rel_eps = def_rel_eps,
                            double abs_eps = def_abs_eps);
    template <typename T>
        bool apx_greater_equal(const T&, const T&,
                               double rel_eps = def_rel_eps,
                               double abs_eps = def_abs_eps);
    template <typename T>
        bool apx_less(const T&, const T&,
                      double rel_eps = def_rel_eps,
                      double abs_eps = def_abs_eps);
    template <typename T>
        bool apx_greater(const T&, const T&,
                         double rel_eps = def_rel_eps,
                         double abs_eps = def_abs_eps);

    template <typename ItT, typename T = typename ItT::value_type,
              typename BinPred = less<T>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        constexpr auto&& min(ItT first, ItT last, BinPred = {});
    template <typename ItT, typename T = typename ItT::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        auto min(ItT first, ItT last, UnConvF, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename BinPred = less<T>,
              typename = If<is_container_v<ContT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        constexpr auto&& min(ContT&&, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        auto min(ContT&&, UnConvF, BinPred = {});
    template <typename ItT, typename T = typename ItT::value_type,
              typename BinPred = less<T>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        constexpr auto&& max(ItT first, ItT last, BinPred = {});
    template <typename ItT, typename T = typename ItT::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        auto max(ItT first, ItT last, UnConvF, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename BinPred = less<T>,
              typename = If<is_container_v<ContT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        constexpr auto&& max(ContT&&, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        auto max(ContT&&, UnConvF, BinPred = {});

    template <typename T, typename ContT,
              typename = If<is_container_v<ContT>>>
        T accumulate(const ContT&, T init = {});
    template <typename T, typename ContT, typename BinF,
              typename = If<is_container_v<ContT>>>
        T accumulate(const ContT&, T init, BinF);

    template <typename T, typename ContT,
              typename = If<is_container_v<ContT>>>
        T reduce(const ContT&, T init = {});
    template <typename T, typename ContT, typename BinF,
              typename = If<is_container_v<ContT>>>
        T reduce(const ContT&, T init, BinF);
}

#include "util/numeric/alg.inl"
