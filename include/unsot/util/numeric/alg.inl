#pragma once

#include "util/optional.hpp"
#include "util/alg.hpp"

namespace unsot::util::numeric {
    template <typename T>
    Vector<T>& operator +=(Vector<T>& lhs, const Vector<T>& rhs)
    {
        for_each(lhs, begin(rhs), [](T& l, const T& r){ l += r; });
        return lhs;
    }

    template <typename T>
    Vector<T> operator +(Vector<T> lhs, const Vector<T>& rhs)
    {
        return (lhs += rhs);
    }

    template <typename T>
    Vector<T>& operator *=(Vector<T>& lhs, const T& rhs)
    {
        for_each(lhs, [&rhs](T& l){ l *= rhs; });
        return lhs;
    }

    template <typename T>
    Vector<T> operator *(Vector<T> lhs, const T& rhs)
    {
        return (lhs *= rhs);
    }

    template <typename T>
    Vector<T> operator *(const T& lhs, Vector<T> rhs)
    {
        return (rhs *= lhs);
    }

    namespace aux {
        template <typename T, Req<is_container_v<T>> = 0>
        bool apx_equal(const T& a, const T& b, double rel_eps, double abs_eps)
        {
            return util::equal(a, b,
                [reps{rel_eps}, aeps{abs_eps}](auto& l, auto& r){
                    return numeric::apx_equal(l, r, reps, aeps);
            });
        }

        template <typename T, Req<is_integral_v<T>> = 0>
        constexpr bool apx_equal_arith(const T& a, const T& b, double, double)
        {
            return a == b;
        }

        template <typename T, Req<is_floating_point_v<T>> = 0>
        constexpr bool apx_equal_arith(const T& a, const T& b, double rel_eps, double abs_eps)
        {
            if (a == b) return true;
            if (!std::isfinite(a) || !std::isfinite(b)) return false;
            const double diff = abs(a - b);
            if (diff <= abs_eps) return true;
            return diff <= rel_eps*std::max(abs(a), abs(b));
        }

        template <typename T, Req<is_arithmetic_v<T>> = 0>
        constexpr bool apx_equal(const T& a, const T& b, double rel_eps, double abs_eps)
        {
            return apx_equal_arith(a, b, rel_eps, abs_eps);
        }

        template <typename T>
        constexpr bool apx_equal(const Optional<T>& a, const Optional<T>& b,
                                 double rel_eps, double abs_eps)
        {
            if (a.invalid() || b.invalid()) return a.invalid() && b.invalid();
            return numeric::apx_equal(a.cvalue(), b.cvalue(), rel_eps, abs_eps);
        }

        template <typename T, Req<is_integral_v<T>> = 0>
        constexpr bool apx_less_equal(const T& a, const T& b, double, double)
        {
            return a <= b;
        }

        template <typename T, Req<is_floating_point_v<T>> = 0>
        constexpr bool apx_less_equal(const T& a, const T& b, double rel_eps, double abs_eps)
        {
            if (a <= b) return true;
            return aux::apx_equal_arith(a, b, rel_eps, abs_eps);
        }

        template <typename T, Req<is_integral_v<T>> = 0>
        constexpr bool apx_greater_equal(const T& a, const T& b, double, double)
        {
            return a >= b;
        }

        template <typename T, Req<is_floating_point_v<T>> = 0>
        constexpr bool apx_greater_equal(const T& a, const T& b, double rel_eps, double abs_eps)
        {
            if (a >= b) return true;
            return aux::apx_equal_arith(a, b, rel_eps, abs_eps);
        }

        template <typename T, Req<is_integral_v<T>> = 0>
        constexpr bool apx_less(const T& a, const T& b, double, double)
        {
            return a < b;
        }

        template <typename T, Req<is_floating_point_v<T>> = 0>
        constexpr bool apx_less(const T& a, const T& b, double rel_eps, double abs_eps)
        {
            //+ returns true for NaN, in contrary to '<'
            return !aux::apx_greater_equal(a, b, rel_eps, abs_eps);
        }

        template <typename T, Req<is_integral_v<T>> = 0>
        constexpr bool apx_greater(const T& a, const T& b, double, double)
        {
            return a > b;
        }

        template <typename T, Req<is_floating_point_v<T>> = 0>
        constexpr bool apx_greater(const T& a, const T& b, double rel_eps, double abs_eps)
        {
            //+ returns true for NaN, in contrary to '>'
            return !aux::apx_less_equal(a, b, rel_eps, abs_eps);
        }
    }

    template <typename T>
    bool apx_equal(const T& a, const T& b, double rel_eps, double abs_eps)
    {
        return aux::apx_equal(a, b, rel_eps, abs_eps);
    }

    template <typename T>
    bool apx_less_equal(const T& a, const T& b, double rel_eps, double abs_eps)
    {
        return aux::apx_less_equal(a, b, rel_eps, abs_eps);
    }

    template <typename T>
    bool apx_greater_equal(const T& a, const T& b, double rel_eps, double abs_eps)
    {
        return aux::apx_greater_equal(a, b, rel_eps, abs_eps);
    }

    template <typename T>
    bool apx_less(const T& a, const T& b, double rel_eps, double abs_eps)
    {
        return aux::apx_less(a, b, rel_eps, abs_eps);
    }

    template <typename T>
    bool apx_greater(const T& a, const T& b, double rel_eps, double abs_eps)
    {
        return aux::apx_greater(a, b, rel_eps, abs_eps);
    }

    template <typename ItT, typename T, typename BinPred, typename, typename>
    constexpr auto&& min(ItT first, ItT last, BinPred pred)
    {
        return FORWARD(*std::min_element(first, last, std::move(pred)));
    }

    template <typename ItT, typename T, typename UnConvF, typename BinPred,
              typename, typename>
    auto min(ItT first, ItT last, UnConvF conv, BinPred pred)
    {
        assert(first != last);
        ItT min_it = first;
        auto min_v = conv(FORWARD(*min_it));
        ++first;
        for (; first != last; ++first) {
            auto v = conv(FORWARD(*first));
            if (pred(v, min_v)) {
                min_it = first;
                min_v = std::move(v);
            }
        }
        return min_v;
    }

    template <typename ContT, typename T, typename BinPred, typename, typename>
    constexpr auto&& min(ContT&& cont, BinPred pred)
    {
        return FORWARD(numeric::min(begin(cont), end(cont), std::move(pred)));
    }

    template <typename ContT, typename T, typename UnConvF, typename BinPred,
              typename, typename>
    auto min(ContT&& cont, UnConvF conv, BinPred pred)
    {
        return FORWARD(min(begin(cont), end(cont), std::move(conv), std::move(pred)));
    }

    template <typename ItT, typename T, typename BinPred, typename, typename>
    constexpr auto&& max(ItT first, ItT last, BinPred pred)
    {
        return FORWARD(*std::max_element(first, last, std::move(pred)));
    }

    template <typename ItT, typename T, typename UnConvF, typename BinPred,
              typename, typename>
    auto max(ItT first, ItT last, UnConvF conv, BinPred pred)
    {
        assert(first != last);
        ItT max_it = first;
        auto max_v = conv(FORWARD(*max_it));
        ++first;
        for (; first != last; ++first) {
            auto v = conv(FORWARD(*first));
            if (pred(max_v, v)) {
                max_it = first;
                max_v = std::move(v);
            }
        }
        return max_v;
    }

    template <typename ContT, typename T, typename BinPred, typename, typename>
    constexpr auto&& max(ContT&& cont, BinPred pred)
    {
        return FORWARD(numeric::max(begin(cont), end(cont), std::move(pred)));
    }

    template <typename ContT, typename T, typename UnConvF, typename BinPred,
              typename, typename>
    auto max(ContT&& cont, UnConvF conv, BinPred pred)
    {
        return FORWARD(max(begin(cont), end(cont), std::move(conv), std::move(pred)));
    }

    template <typename T, typename ContT, typename>
    T accumulate(const ContT& cont, T init)
    {
        return std::accumulate(cbegin(cont), cend(cont), std::move(init));
    }

    template <typename T, typename ContT, typename BinF, typename>
    T accumulate(const ContT& cont, T init, BinF f)
    {
        return std::accumulate(cbegin(cont), cend(cont), std::move(init), std::move(f));
    }

    template <typename T, typename ContT, typename>
    T reduce(const ContT& cont, T init)
    {
        return std::reduce(cbegin(cont), cend(cont), std::move(init));
    }

    template <typename T, typename ContT, typename BinF, typename>
    T reduce(const ContT& cont, T init, BinF f)
    {
        return std::reduce(cbegin(cont), cend(cont), std::move(init), std::move(f));
    }
}
