#pragma once

#include "util.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <filesystem>

extern "C" {
#include <unistd.h>
}

extern char *optarg;
extern int optind, optopt;

namespace unsot::util {
    using std::cout;
    using std::cerr;
    using std::cin;
    using std::endl;

    using Istream = istream;
    using Ostream = ostream;
    template <typename T> using Stream_ptr = T*;
    using Istream_ptr = Stream_ptr<Istream>;
    using Ostream_ptr = Stream_ptr<Ostream>;

    namespace filesystem = std::filesystem;

    using filesystem::absolute;
    using filesystem::relative;
    using filesystem::canonical;

    static constexpr const char* project_name = "unsot";

    Path pwd();
    Path root();
}

namespace unsot::util {
    struct Path : Inherit<filesystem::path, Path> {
        using Inherit::Inherit;

        using Inherit::is_absolute;
        using Inherit::is_relative;

        using Inherit::has_filename;
        using Inherit::has_extension;
        using Inherit::filename;
        using Inherit::extension;
        bool has_dirname() const;
        Path dirname() const;

        String to_string() const&                         { return string(); }
    };

    class Run : public Object<Run> {
    public:
        using Base = Run;

        Run()                                                       = default;
        virtual ~Run()                                              = default;
        Run(const Run&)                                              = delete;
        Run& operator =(const Run&)                                  = delete;
        Run(Run&&)                                                  = default;
        Run& operator =(Run&&)                                      = default;
        Run(int argc, const char* argv[]);

        const auto& cipath() const noexcept                 { return _ipath; }
        auto& ipath() noexcept                              { return _ipath; }
        const auto& copath() const noexcept                 { return _opath; }
        auto& opath() noexcept                              { return _opath; }
        auto& is() const noexcept                         { return *_is_ptr; }
        auto& os() const noexcept                         { return *_os_ptr; }

        virtual int run();

        virtual void init();
        virtual void do_stuff();
        virtual void finish()                                              { }

        virtual String usage() const;

        virtual void set_istream_ptr();
        virtual void set_ostream_ptr();

        static Path project_dirname();
        Path common_dirname() const;
        [[nodiscard]] ifstream open_ifstream(const Path&) const;
    protected:
        using Argv = char* const*;

        static String usage_row(char opt, String desc);

        virtual void getopts();
        virtual bool require_args() const noexcept           { return false; }
        virtual String getopt_str() const noexcept;
        virtual bool process_opt(char);
        /// Use in case of option with unused "::" argument
        virtual bool process_optarg_opts();

        virtual void process_additional_args();

        template <typename StreamT, typename FstreamT>
            static void set_stream_ptr(Stream_ptr<StreamT>&, FstreamT&, const Path&,
                                       Stream_ptr<StreamT> std_s_ptr, const String& err_msg);
        template <typename StreamT, typename FstreamT>
            static void set_stream_ptr(Stream_ptr<StreamT>&, FstreamT&, const Path&,
                                       const String& err_msg);
        template <typename StreamT>
            static void check_stream_ptr(const Stream_ptr<StreamT>&, const String& err_msg);

        int _argc{};
        Argv _argv{};
    private:
        Path _ipath{};
        Path _opath{};
        ifstream _ifs{};
        ofstream _ofs{};
        Istream_ptr _is_ptr{};
        Ostream_ptr _os_ptr{};
    };
}

#include "util/run.inl"
