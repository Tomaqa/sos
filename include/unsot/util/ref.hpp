#pragma once

#include "util/union.hpp"

namespace unsot::util {
    template <typename T>
    class Ref : protected Inherit<Union<Decay<T>, Add_ptr<Rm_ref<T>>>, Ref<T>> {
    public:
        using Inherit = unsot::Inherit<Union<Decay<T>, Add_ptr<Rm_ref<T>>>, Ref<T>>;
        using typename Inherit::This;

        using Item = Decay<T>;

        static constexpr bool is_const_v = unsot::is_const_v<Rm_ref<T>>;

        Ref()                                                                             = default;
        ~Ref()                                                                            = default;
        Ref(const Ref&)                                                                   = default;
        Ref& operator =(const Ref&)                                                       = default;
        Ref(Ref&&)                                                                        = default;
        Ref& operator =(Ref&&)                                                            = default;
        template <typename T_> explicit constexpr Ref(T_&&);
        explicit constexpr Ref(Item&&);
        explicit constexpr Ref(const Item&&);
        Ref(Tag<Item>);
        template <typename T_, typename = If<!is_derived_from_v<T_, This>>>
            constexpr Ref& operator =(T_&&);

        constexpr const T& citem() const                                { return item_impl(*this); }
        constexpr const T& item() const&                                         { return citem(); }
        constexpr T& item()&                                            { return item_impl(*this); }
        constexpr T&& item()&&                                              { return move(item()); }

        using Inherit::valid;
        using Inherit::invalid;
        constexpr bool is_owner() const noexcept;

        String to_string() const&;
        String to_string()&&;

        using Inherit::equals;
    protected:
        using Ptr = Add_ptr<Item>;
    private:
        template <typename R> static constexpr auto& item_impl(R&&);

        template <typename R> static String to_string_impl(R&&);
    };
}

#include "util/ref.inl"
