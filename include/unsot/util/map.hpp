#pragma once

#include "util/associative.hpp"

#include <map>

namespace unsot::util {
    template <typename KeyT, typename ValueT>
    class Map : public Inherit<Map_mixin<std::map<KeyT, ValueT>>,
                               Map<KeyT, ValueT>> {
    //+ class Map : public Inherit<Map_mixin<std::map<KeyT, ValueT, less<>>>,
    //+                            Map<KeyT, ValueT>> {
    public:
        using Inherit = unsot::Inherit<Map_mixin<std::map<KeyT, ValueT>>,
                                       Map<KeyT, ValueT>>;
        //+ using Inherit =
        //+     unsot::Inherit<Map_mixin<std::map<KeyT, ValueT, less<>>>,
        //+                    Map<KeyT, ValueT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };
}

#include "util/map.inl"
