#pragma once

#include "util.hpp"

#include "util/fun.hpp"

#include <algorithm>

namespace unsot::util {
    using std::size;
    using std::empty;
    using std::begin;
    using std::end;
    using std::cbegin;
    using std::cend;
    using std::data;

    using std::make_move_iterator;
    using std::back_inserter;
    using std::inserter;

    using std::min;
    using std::max;

    using std::clamp;

    template <typename F, typename WrapF> auto wrap(F, WrapF);
    template <typename BinF, typename InputIt2>
        auto as_unary(BinF, InputIt2 first2);

    template <typename ContT, typename UnPred,
              typename = If<is_container_v<ContT>>>
        bool all_of(const ContT&, UnPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>,
              typename = If<is_iterator_v<InputIt2>>>
        bool all_of(const Cont1&, InputIt2 first2, BinPred);
    template <typename ContT, typename UnPred,
              typename = If<is_container_v<ContT>>>
        bool any_of(const ContT&, UnPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>,
              typename = If<is_iterator_v<InputIt2>>>
        bool any_of(const Cont1&, InputIt2 first2, BinPred);
    template <typename ContT, typename UnPred,
              typename = If<is_container_v<ContT>>>
        bool none_of(const ContT&, UnPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>,
              typename = If<is_iterator_v<InputIt2>>>
        bool none_of(const Cont1&, InputIt2 first2, BinPred);
    template <typename InputIt, typename BinPred = equal_to<void>,
              typename = If<is_iterator_v<InputIt>>>
        bool all_equal(InputIt first, InputIt last, BinPred = {});
    template <typename ContT, typename BinPred = equal_to<void>,
              typename = If<is_container_v<ContT>>>
        bool all_equal(const ContT&, BinPred = {});
    template <typename InputIt, typename BinPred = equal_to<void>,
              typename = If<is_iterator_v<InputIt>>>
        bool any_equal(InputIt first, InputIt last, BinPred = {});
    template <typename ContT, typename BinPred = equal_to<void>,
              typename = If<is_container_v<ContT>>>
        bool any_equal(const ContT&, BinPred = {});
    template <typename InputIt, typename BinPred = equal_to<void>,
              typename = If<is_iterator_v<InputIt>>>
        bool none_equal(InputIt first, InputIt last, BinPred = {});
    template <typename ContT, typename BinPred = equal_to<void>,
              typename = If<is_container_v<ContT>>>
        bool none_equal(const ContT&, BinPred = {});
    template <typename Cont1, typename T2,
              typename BinPred = equal_to<void>,
              typename = If<is_container_v<Cont1>>,
              typename = If<is_iterator_v<T2> || is_container_v<T2>>>
        bool equal(const Cont1&, const T2&, BinPred = {});

    template <typename ContT, typename T,
              typename = If<is_container_v<ContT>>>
        auto find(const ContT&, const T& value);
    template <typename ContT, typename T,
              typename = If<is_container_v<ContT>>>
        auto find(ContT&, const T& value);
    template <typename ContT, typename UnPred>
        auto find_if(const ContT&, UnPred);
    template <typename ContT, typename UnPred> auto find_if(ContT&, UnPred);
    template <typename ContT, typename UnPred>
        auto find_if_not(const ContT&, UnPred);
    template <typename ContT, typename UnPred>
        auto find_if_not(ContT&, UnPred);
    template <typename InputIt, typename T,
              typename = If<is_iterator_v<InputIt>>>
        bool contains(InputIt first, InputIt last, const T& value);
    template <typename ContT, typename T,
              typename = If<is_container_v<ContT> && !is_string_v<ContT>>>
        bool contains(const ContT&, const T& value);
    template <typename T, typename U = T> bool contains(initializer_list<T>, const U& value);

    template <typename ContT, typename T,
              typename = If<is_container_v<ContT>>>
        auto count(const ContT&, const T& value);
    template <typename ContT, typename UnPred,
              typename = If<is_container_v<ContT>>>
        auto count_if(const ContT&, UnPred);

    template <typename ContT, typename OutputIt>
        OutputIt copy(const ContT&, OutputIt d_first);
    template <typename ContT, typename OutputIt>
        OutputIt move(ContT&&, OutputIt d_first);
    template <typename ContT, typename OutputIt>
        OutputIt move(ContT&, OutputIt d_first);
    template <typename InputIt, typename SizeT, typename OutputIt>
        OutputIt move_n(InputIt first1, SizeT, OutputIt d_first);

    template <typename ContT, typename UnF> UnF for_each(const ContT&, UnF);
    template <typename ContT, typename UnF> UnF for_each(ContT&&, UnF);
    template <typename ContT, typename UnF> UnF for_each(ContT&, UnF);
    template <typename InputIt1, typename InputIt2, typename BinF>
        BinF for_each(InputIt1 first1, InputIt1 last1, InputIt2 first2, BinF);
    template <typename Cont1, typename InputIt2, typename BinF,
              typename = If<is_container_v<Cont1>>>
        BinF for_each(const Cont1&, InputIt2 first2, BinF);
    template <typename Cont1, typename InputIt2, typename BinF,
              typename = If<is_container_v<Cont1>>>
        BinF for_each(Cont1&&, InputIt2 first2, BinF);
    template <typename Cont1, typename InputIt2, typename BinF,
              typename = If<is_container_v<Cont1>>>
        BinF for_each(Cont1&, InputIt2 first2, BinF);
    template <typename InputIt1, typename SizeT, typename InputIt2, typename BinF>
        InputIt1 for_each_n(InputIt1 first1, SizeT, InputIt2 first2, BinF);
    template <typename ContT, typename OutputIt, typename UnF>
        OutputIt transform(const ContT&, OutputIt d_first, UnF);
    template <typename ContT, typename OutputIt, typename UnF>
        OutputIt transform(ContT&&, OutputIt d_first, UnF);
    template <typename ContT, typename OutputIt, typename UnF>
        OutputIt transform(ContT&, OutputIt d_first, UnF);
    template <typename Cont1, typename InputIt2,
              typename OutputIt, typename BinF,
              typename = If<is_container_v<Cont1>>>
        OutputIt transform(const Cont1&, InputIt2 first2,
                           OutputIt d_first, BinF);
    template <typename Cont1, typename InputIt2,
              typename OutputIt, typename BinF,
              typename = If<is_container_v<Cont1>>>
        OutputIt transform(Cont1&&, InputIt2 first2, OutputIt d_first, BinF);
    template <typename Cont1, typename InputIt2,
              typename OutputIt, typename BinF,
              typename = If<is_container_v<Cont1>>>
        OutputIt transform(Cont1&, InputIt2 first2, OutputIt d_first, BinF);

    template <typename ContT, typename UnF>
        UnF for_each_cdr(const ContT&, UnF);
    template <typename ContT, typename UnF> UnF for_each_cdr(ContT&&, UnF);
    template <typename ContT, typename UnF> UnF for_each_cdr(ContT&, UnF);

    template <typename InputIt, typename UnPred>
        UnPred for_each_while(InputIt first, InputIt last, UnPred);
    template <typename ContT, typename UnPred>
        UnPred for_each_while(const ContT&, UnPred);
    template <typename ContT, typename UnPred>
        UnPred for_each_while(ContT&&, UnPred);
    template <typename ContT, typename UnPred>
        UnPred for_each_while(ContT&, UnPred);
    template <typename InputIt1, typename InputIt2, typename BinPred>
        BinPred for_each_while(InputIt1 first1, InputIt1 last1,
                               InputIt2 first2, BinPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>>
        BinPred for_each_while(const Cont1&, InputIt2 first2, BinPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>>
        BinPred for_each_while(Cont1&&, InputIt2 first2, BinPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>>
        BinPred for_each_while(Cont1&, InputIt2 first2, BinPred);

    template <typename InputIt, typename UnPred, typename UnF>
        UnF for_each_if(InputIt first, InputIt last, UnPred, UnF);
    template <typename ContT, typename UnPred, typename UnF>
        UnF for_each_if(const ContT&, UnPred, UnF);
    template <typename ContT, typename UnPred, typename UnF>
        UnF for_each_if(ContT&&, UnPred, UnF);
    template <typename ContT, typename UnPred, typename UnF>
        UnF for_each_if(ContT&, UnPred, UnF);
    template <typename InputIt1, typename InputIt2,
              typename BinPred, typename BinF>
        BinF for_each_if(InputIt1 first1, InputIt1 last1, InputIt2 first2,
                         BinPred, BinF);
    template <typename Cont1, typename InputIt2,
              typename BinPred, typename BinF>
        BinF for_each_if(const Cont1&, InputIt2 first2, BinPred, BinF);
    template <typename Cont1, typename InputIt2,
              typename BinPred, typename BinF>
        BinF for_each_if(Cont1&&, InputIt2 first2, BinPred, BinF);
    template <typename Cont1, typename InputIt2,
              typename BinPred, typename BinF>
        BinF for_each_if(Cont1&, InputIt2 first2, BinPred, BinF);
    template <typename InputIt, typename UnPred,
              typename UnThenF, typename UnElseF>
        UnThenF for_each_ite(InputIt first, InputIt last, UnPred,
                             UnThenF, UnElseF);
    template <typename ContT, typename UnPred,
              typename UnThenF, typename UnElseF>
        UnThenF for_each_ite(const ContT&, UnPred, UnThenF, UnElseF);
    template <typename ContT, typename UnPred,
              typename UnThenF, typename UnElseF>
        UnThenF for_each_ite(ContT&&, UnPred, UnThenF, UnElseF);
    template <typename ContT, typename UnPred,
              typename UnThenF, typename UnElseF>
        UnThenF for_each_ite(ContT&, UnPred, UnThenF, UnElseF);
    template <typename InputIt1, typename InputIt2,
              typename BinPred, typename BinThenF, typename BinElseF>
        BinThenF for_each_ite(InputIt1 first1, InputIt1 last1,
                              InputIt2 first2, BinPred, BinThenF, BinElseF);
    template <typename Cont1, typename InputIt2,
              typename BinPred, typename BinThenF, typename BinElseF>
        BinThenF for_each_ite(const Cont1&, InputIt2 first2,
                              BinPred, BinThenF, BinElseF);
    template <typename Cont1, typename InputIt2,
              typename BinPred, typename BinThenF, typename BinElseF>
        BinThenF for_each_ite(Cont1&&, InputIt2 first2,
                              BinPred, BinThenF, BinElseF);
    template <typename Cont1, typename InputIt2,
              typename BinPred, typename BinThenF, typename BinElseF>
        BinThenF for_each_ite(Cont1&, InputIt2 first2,
                              BinPred, BinThenF, BinElseF);

    /// Do not require iterators, user defined types may not be recognized
    //! isn't it necessary to also use some `Forward_as_ref` like with `From` below?
    template <typename T, typename FromIt, typename SizeT, typename UnConvF = Fwd,
              Req<is_integral_v<SizeT>> = 0,
              typename E = decltype(*declval<FromIt>()), Req<is_unary_conv_f_v<UnConvF, E>> = 0>
        [[nodiscard]] T to(FromIt first, FromIt last, SizeT, UnConvF = fwd);
    template <typename T, typename FromIt, typename UnConvF = Fwd,
              typename E = decltype(*declval<FromIt>()), Req<is_unary_conv_f_v<UnConvF, E>> = 0>
        [[nodiscard]] T to(FromIt first, FromIt last, UnConvF = fwd);
    template <typename T, typename From, typename SizeT, typename UnConvF = Fwd,
              Req<is_container_v<From>> = 0, Req<is_integral_v<SizeT>> = 0,
              typename E = Forward_as_ref<typename Decay<From>::value_type, From>,
              Req<is_unary_conv_f_v<UnConvF, E>> = 0>
        [[nodiscard]] T to(From&& from, SizeT, UnConvF = fwd);
    template <typename T, typename From, typename UnConvF = Fwd,
              Req<is_container_v<From>> = 0,
              typename E = Forward_as_ref<typename Decay<From>::value_type, From>,
              Req<is_unary_conv_f_v<UnConvF, E>> = 0>
        [[nodiscard]] T to(From&& from, UnConvF = fwd);
    template <typename T, typename FromIt, typename SizeT, typename UnConvF = Fwd,
              Req<is_integral_v<SizeT>> = 0,
              typename E = decltype(*declval<FromIt>()), Req<is_unary_conv_f_v<UnConvF, E>> = 0>
        void to(T&, FromIt first, FromIt last, SizeT, UnConvF = fwd);
    template <typename T, typename FromIt, typename UnConvF = Fwd,
              typename E = decltype(*declval<FromIt>()), Req<is_unary_conv_f_v<UnConvF, E>> = 0>
        void to(T&, FromIt first, FromIt last, UnConvF = fwd);
    template <typename T, typename From, typename SizeT, typename UnConvF = Fwd,
              Req<is_container_v<From>> = 0, Req<is_integral_v<SizeT>> = 0,
              typename E = Forward_as_ref<typename Decay<From>::value_type, From>,
              Req<is_unary_conv_f_v<UnConvF, E>> = 0>
        void to(T&, From&&, SizeT, UnConvF = fwd);
    template <typename T, typename From, typename UnConvF = Fwd,
              Req<is_container_v<From>> = 0,
              typename E = Forward_as_ref<typename Decay<From>::value_type, From>,
              Req<is_unary_conv_f_v<UnConvF, E>> = 0>
        void to(T&, From&&, UnConvF = fwd);
    template <typename T, typename FromIt1, typename FromIt2, typename SizeT, typename BinConvF,
              Req<is_integral_v<SizeT>> = 0,
              typename E1 = decltype(*declval<FromIt1>()),
              typename E2 = decltype(*declval<FromIt2>()),
              Req<is_binary_conv_f_v<BinConvF, E1, E2>> = 0>
        [[nodiscard]] T to(FromIt1 first1, FromIt1 last1, FromIt2 first2, SizeT, BinConvF);
    template <typename T, typename FromIt1, typename FromIt2, typename BinConvF,
              typename E1 = decltype(*declval<FromIt1>()),
              typename E2 = decltype(*declval<FromIt2>()),
              Req<is_binary_conv_f_v<BinConvF, E1, E2>> = 0>
        [[nodiscard]] T to(FromIt1 first1, FromIt1 last1, FromIt2 first2, BinConvF);
    template <typename T, typename From1, typename FromIt2, typename SizeT, typename BinConvF,
              Req<is_container_v<From1>> = 0, Req<is_integral_v<SizeT>> = 0,
              typename E1 = Forward_as_ref<typename Decay<From1>::value_type, From1>,
              typename E2 = decltype(*declval<FromIt2>()),
              Req<is_binary_conv_f_v<BinConvF, E1, E2>> = 0>
        [[nodiscard]] T to(From1&&, FromIt2 first2, SizeT, BinConvF);
    template <typename T, typename From1, typename FromIt2, typename BinConvF,
              Req<is_container_v<From1>> = 0,
              typename E1 = Forward_as_ref<typename Decay<From1>::value_type, From1>,
              typename E2 = decltype(*declval<FromIt2>()),
              Req<is_binary_conv_f_v<BinConvF, E1, E2>> = 0>
        [[nodiscard]] T to(From1&&, FromIt2 first2, BinConvF);
    template <typename T, typename FromIt1, typename FromIt2, typename SizeT, typename BinConvF,
              Req<is_integral_v<SizeT>> = 0,
              typename E1 = decltype(*declval<FromIt1>()),
              typename E2 = decltype(*declval<FromIt2>()),
              Req<is_binary_conv_f_v<BinConvF, E1, E2>> = 0>
        void to(T&, FromIt1 first1, FromIt1 last1, FromIt2 first2, SizeT, BinConvF);
    template <typename T, typename FromIt1, typename FromIt2, typename BinConvF,
              typename E1 = decltype(*declval<FromIt1>()),
              typename E2 = decltype(*declval<FromIt2>()),
              Req<is_binary_conv_f_v<BinConvF, E1, E2>> = 0>
        void to(T&, FromIt1 first1, FromIt1 last1, FromIt2 first2, BinConvF);
    template <typename T, typename From1, typename FromIt2, typename SizeT, typename BinConvF,
              Req<is_container_v<From1>> = 0, Req<is_integral_v<SizeT>> = 0,
              typename E1 = Forward_as_ref<typename Decay<From1>::value_type, From1>,
              typename E2 = decltype(*declval<FromIt2>()),
              Req<is_binary_conv_f_v<BinConvF, E1, E2>> = 0>
        void to(T&, From1&&, FromIt2 first2, SizeT, BinConvF);
    template <typename T, typename From1, typename FromIt2, typename BinConvF,
              Req<is_container_v<From1>> = 0,
              typename E1 = Forward_as_ref<typename Decay<From1>::value_type, From1>,
              typename E2 = decltype(*declval<FromIt2>()),
              Req<is_binary_conv_f_v<BinConvF, E1, E2>> = 0>
        void to(T&, From1&&, FromIt2 first2, BinConvF);

    template <typename OutputCont, typename InputIt, typename SizeT>
        OutputCont& append(OutputCont&, InputIt first2, InputIt last2, SizeT);
    template <typename OutputCont, typename InputIt>
        OutputCont& append(OutputCont&, InputIt first2, InputIt last2);
    template <typename OutputCont, typename InputCont>
        OutputCont& append(OutputCont&, const InputCont&);
    template <typename OutputCont, typename InputCont>
        OutputCont& append(OutputCont&, InputCont&&);
    template <typename OutputCont, typename InputIt, typename SizeT>
        [[nodiscard]] OutputCont cat(OutputCont, InputIt first2, InputIt last2, SizeT);
    template <typename OutputCont, typename InputIt>
        [[nodiscard]] OutputCont cat(OutputCont, InputIt first2,
                                     InputIt last2);
    template <typename OutputCont, typename InputCont>
        [[nodiscard]] OutputCont cat(OutputCont, InputCont&&);
    template <typename InputIt, typename InputCont, typename SizeT>
        [[nodiscard]] auto cat(InputIt first1, InputIt last1, SizeT, InputCont&&);
    template <typename InputIt, typename InputCont>
        [[nodiscard]] auto cat(InputIt first1, InputIt last1, InputCont&&);

    template <typename OutputCont, typename InputIt, typename UnPred>
        [[nodiscard]] OutputCont split_if(InputIt first, InputIt last,
                                          UnPred);
    template <typename OutputCont, typename InputCont, typename UnPred>
        [[nodiscard]] OutputCont split_if(InputCont&&, UnPred);
    template <typename OutputCont, typename InputCont, typename UnPred>
        OutputCont inplace_split_if(InputCont&, UnPred);

    template <typename ContT, typename = If<!is_cref_v<ContT>>>
        ContT reverse(ContT&&);
    template <typename ContT> ContT reverse_copy(const ContT&);

    template <typename ItT, typename T = typename ItT::value_type,
              typename BinPred = less<void>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        bool is_sorted(ItT first, ItT last, BinPred = {});
    template <typename ItT, typename T = typename ItT::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        bool is_sorted(ItT first, ItT last, UnConvF, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        bool is_sorted(const ContT&, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        bool is_sorted(const ContT&, UnConvF, BinPred = {});
    template <typename ItT, typename T = typename ItT::value_type,
              typename BinPred = less<void>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        void sort(ItT first, ItT last, BinPred = {});
    template <typename ItT, typename T = typename ItT::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        void sort(ItT first, ItT last, UnConvF, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<!is_cref_v<ContT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        ContT sort(ContT&&, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<!is_cref_v<ContT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        ContT sort(ContT&&, UnConvF, BinPred = {});
    template <typename ItT, typename T = typename ItT::value_type,
              typename BinPred = less<void>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        void stable_sort(ItT first, ItT last, BinPred = {});
    template <typename ItT, typename T = typename ItT::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_iterator_v<ItT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        void stable_sort(ItT first, ItT last, UnConvF, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<!is_cref_v<ContT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        ContT stable_sort(ContT&&, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<!is_cref_v<ContT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        ContT stable_sort(ContT&&, UnConvF, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<!is_unary_conv_f_v<BinPred, T>>>
        ContT sort_copy(const ContT&, BinPred = {});
    template <typename ContT, typename T = typename Decay<ContT>::value_type,
              typename UnConvF, typename BinPred = less<void>,
              typename = If<is_container_v<ContT>>,
              typename = If<is_unary_conv_f_v<UnConvF, T>>>
        ContT sort_copy(const ContT&, UnConvF, BinPred = {});
}

#include "util/alg.inl"
