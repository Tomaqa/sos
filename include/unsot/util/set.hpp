#pragma once

#include "util/associative.hpp"

#include <set>

namespace unsot::util {
    template <typename KeyT>
    class Set : public Inherit<Associative_mixin<std::set<KeyT>>, Set<KeyT>> {
    //+ uses heterogenous lookup, but is it of any use here
    //+ since templated lookup member functions are available only in C++20?
    //+ class Set : public Inherit<Associative_mixin<std::set<KeyT, less<>>>,
    //+                            Set<KeyT>> {
    public:
        using Inherit =
            unsot::Inherit<Associative_mixin<std::set<KeyT>>, Set<KeyT>>;
        //+ using Inherit =
        //+     unsot::Inherit<Associative_mixin<std::set<KeyT, less<>>>,
        //+                    Set<KeyT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };

    //+ not tested etc. ...
    template <typename KeyT>
    class Multiset : public Inherit<Associative_mixin<std::multiset<KeyT>>, Multiset<KeyT>> {
    public:
        using Inherit = unsot::Inherit<Associative_mixin<std::multiset<KeyT>>, Multiset<KeyT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };
}

#include "util/set.inl"
