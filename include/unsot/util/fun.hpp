#pragma once

#include "util.hpp"

#include <functional>

#define BIND_THIS(fn) bind_this(&This::fn, this)
#define MEM_FN(obj, fn) mem_fn(&DECAY(obj)::fn)

#define CAPTURE_RVAL(arg) arg{FORWARD(arg)}
#define CAPTURE_RVAL2(arg1, arg2) CAPTURE_RVAL(arg1), CAPTURE_RVAL(arg2)
#define CAPTURE_RVAL3(arg1, arg2, arg3) CAPTURE_RVAL2(arg1, arg2), CAPTURE_RVAL(arg3)
#define CAPTURE_RVAL4(arg1, arg2, arg3, arg4) CAPTURE_RVAL2(arg1, arg2), CAPTURE_RVAL2(arg3, arg4)

namespace unsot {
    using namespace std::placeholders;
    using std::function;
    using std::bind;
    using std::ref;
    using std::cref;
    using std::mem_fn;
    using std::invoke;
    using std::apply;

    using std::not_fn;
    using std::equal_to;
    using std::less;
    using std::less_equal;
    using std::logical_not;
    using std::logical_and;
    using std::logical_or;
    using std::negate;
    using std::plus;
    using std::minus;
    using std::multiplies;
    using std::divides;
}

namespace unsot::util {
    template <typename T> using Lazy = function<T()>;

    template <typename R, typename T = R> using Un_f = function<R(T)>;
    template <typename R, typename T = R, typename U = T>
        using Bin_f = function<R(T, U)>;
    template <typename R, typename T, typename U = T, typename V = U>
        using Ter_f = function<R(T, U, V)>;

    constexpr auto lazy = [](auto&& f){ return invoke(FORWARD(f)); };

    template <typename P> constexpr auto deref = [](P&& p) -> auto&& {
        return Forward_as_ref<Decay<decltype(*p)>, P>(*p);
    };

    template <typename F, typename... Args>
        decltype(auto) bind_front(F&&, Args&&...);
    template <typename RetT, typename U, typename T, typename... Args>
        decltype(auto) bind_this(RetT U::*, T&&, Args&&...);
}

#include "util/fun.inl"
