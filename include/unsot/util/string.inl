#pragma once

#include "util/optional.hpp"

namespace unsot::util {
    template <typename Arg, typename Str, Req<is_string_v<Str>>>
    Optional<Arg> to_value(Str&& str) noexcept
    {
        Arg value;
        if (get_value<Arg>(value, FORWARD(str))) return value;
        return {};
    }

    template <typename Arg, typename Str, Req<is_string_v<Str>>>
    Arg to_value_check(Str&& str)
    {
        auto opt = to_value<Arg>(FORWARD(str));
        expect(opt.valid(),
               "Conversion from string to <"s + typeid(Arg).name()
               + "> value failed: " + str);
        return move(opt).value();
    }
}
