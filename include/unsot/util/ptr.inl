#pragma once

namespace unsot::util {
    template <typename B>
    String Smart_ptr_mixin<B>::to_string() const
    {
        using unsot::to_string;
        return to_string(**this);
    }

    template <typename B>
    bool Smart_ptr_mixin<B>::equals(const This& rhs) const noexcept
    {
        return this->get() == rhs.get();
    }

    ////////////////////////////////////////////////////////////////

    template <typename T>
    bool Shared_ptr<T>::unique() const noexcept
    {
        /// Only approximation in multithreaded environment !
        return this->use_count() == 1;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::util {
    template <typename P>
    void maybe_shallow_copy(P& ptr)
    {
        if (ptr.unique()) return;
        ptr = ptr->shallow_copy_to_ptr();
    }

    template <typename P>
    void maybe_deep_copy(P& ptr)
    {
        if (ptr.unique()) return;
        ptr = ptr->deep_copy_to_ptr();
    }
}
