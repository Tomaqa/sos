#pragma once

namespace unsot::util {
    /// 000 -> 0
    /// 111 -> 1
    /// 010 -> 0
    /// ??? -> ?
    /// 0?0 -> 0
    /// 1?1 -> ?
    /// 0?1 -> 0
    template <typename InputIt, typename UnPred>
    Flag all_true(InputIt first, InputIt last, UnPred pred)
    {
        bool was_unknown = false;
        if (!std::all_of(first, last, [&pred, &was_unknown](const auto& arg){
            const Flag flag = pred(arg);
            if (flag.unknown()) {
                was_unknown = true;
                return true;
            }
            return flag.cvalue();
        })) return false;

        return was_unknown ? unknown : true_flag;
    }

    template <typename ContT, typename UnPred>
    Flag all_true(const ContT& cont, UnPred pred)
    {
        return all_true(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename>
    Flag all_true(const Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        return all_true(cbegin(cont1), cend(cont1), as_unary(std::move(pred), first2));
    }

    /// 000 -> 1
    /// 111 -> 0
    /// 010 -> 0
    /// ??? -> ?
    /// 0?0 -> ?
    /// 1?1 -> 0
    /// 0?1 -> 0
    template <typename InputIt, typename UnPred>
    Flag all_false(InputIt first, InputIt last, UnPred pred)
    {
        /// \forall . \not x
        return all_true(first, last, [&pred](const auto& arg){
            return pred(arg).flip();
        });
    }

    template <typename ContT, typename UnPred>
    Flag all_false(const ContT& cont, UnPred pred)
    {
        /// \forall . \not x
        return all_true(cont, [&pred](const auto& arg){
            return pred(arg).flip();
        });
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename>
    Flag all_false(const Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        /// \forall . \not x
        return all_true(cont1, first2, [&pred](const auto& arg1, const auto& arg2){
            return pred(arg1, arg2).flip();
        });
    }

    /// 000 -> 0
    /// 111 -> 1
    /// 010 -> 1
    /// ??? -> ?
    /// 0?0 -> ?
    /// 1?1 -> 1
    /// 0?1 -> 1
    template <typename InputIt, typename UnPred>
    Flag any_true(InputIt first, InputIt last, UnPred pred)
    {
        /// \exists . x <=> \not \forall . \not x
        return all_false(first, last, std::move(pred)).flip();
    }

    template <typename ContT, typename UnPred>
    Flag any_true(const ContT& cont, UnPred pred)
    {
        /// \exists . x <=> \not \forall . \not x
        return all_false(cont, std::move(pred)).flip();
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename>
    Flag any_true(const Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        /// \exists . x <=> \not \forall . \not x
        return all_false(cont1, first2, std::move(pred)).flip();
    }

    /// 000 -> 1
    /// 111 -> 0
    /// 010 -> 1
    /// ??? -> ?
    /// 0?0 -> 1
    /// 1?1 -> ?
    /// 0?1 -> 1
    template <typename InputIt, typename UnPred>
    Flag any_false(InputIt first, InputIt last, UnPred pred)
    {
        /// \exists . \not x <=> \not \forall . x
        return all_true(first, last, std::move(pred)).flip();
    }

    template <typename ContT, typename UnPred>
    Flag any_false(const ContT& cont, UnPred pred)
    {
        /// \exists . \not x <=> \not \forall . x
        return all_true(cont, std::move(pred)).flip();
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename>
    Flag any_false(const Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        /// \exists . \not x <=> \not \forall . x
        return all_true(cont1, first2, std::move(pred)).flip();
    }
}
