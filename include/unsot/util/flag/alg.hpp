#pragma once

#include "util/flag.hpp"
#include "util/alg.hpp"

namespace unsot::util {
    template <typename InputIt, typename UnPred>
        Flag all_true(InputIt first, InputIt last, UnPred);
    template <typename ContT, typename UnPred>
        Flag all_true(const ContT&, UnPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>>
        Flag all_true(const Cont1&, InputIt2 first2, BinPred);
    template <typename InputIt, typename UnPred>
        Flag all_false(InputIt first, InputIt last, UnPred);
    template <typename ContT, typename UnPred>
        Flag all_false(const ContT&, UnPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>>
        Flag all_false(const Cont1&, InputIt2 first2, BinPred);
    template <typename InputIt, typename UnPred>
        Flag any_true(InputIt first, InputIt last, UnPred);
    template <typename ContT, typename UnPred>
        Flag any_true(const ContT&, UnPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>>
        Flag any_true(const Cont1&, InputIt2 first2, BinPred);
    template <typename InputIt, typename UnPred>
        Flag any_false(InputIt first, InputIt last, UnPred);
    template <typename ContT, typename UnPred>
        Flag any_false(const ContT&, UnPred);
    template <typename Cont1, typename InputIt2, typename BinPred,
              typename = If<is_container_v<Cont1>>>
        Flag any_false(const Cont1&, InputIt2 first2, BinPred);
}

#include "util/flag/alg.inl"
