#pragma once

#include <cstring>

#define _CSTREAM(cstream, x) cstream << x
#define _CSTREAMLN(cstream, x) _CSTREAM(cstream, x) << std::endl

#ifndef QUIET
#define CSTREAM(cstream, x) _CSTREAM(cstream, x)
#define CSTREAMLN(cstream, x) _CSTREAMLN(cstream, x)
#else
#define CSTREAM(cstream, x) UNUSED(_CSTREAM(ignore, x))
#define CSTREAMLN(cstream, x) CSTREAM(cstream, x)
#endif

#ifdef DEBUG
#include <iostream>
#include <iomanip>

using std::cout;
using std::cerr;
using std::clog;
using std::endl;

#define CDBG(x) CSTREAM(cerr, x)
#define CDBGLN(x) CSTREAMLN(cerr, x)
#else
#define CDBG(x) EMPTY
#define CDBGLN(x) EMPTY
#endif

#ifndef VERBOSE
#include <iosfwd>
#else
#include <iostream>

#define _CVERB_BODY(x, l, r) l << " " << x << " " << r
#define _CVERB(x, l, r) CSTREAM(std::cerr, _CVERB_BODY(x, l, r))
#define _CVERBLN(x, l, r) CSTREAMLN(std::cerr, _CVERB_BODY(x, l, r))

constexpr int _verbose_ = VERBOSE;
#endif

#if VERBOSE >= 1
#define CVERB(x) _CVERB(x, "<<<<", ">>>>")
#define CVERBLN(x) _CVERBLN(x, "<<<<", ">>>>")
#else
#define CVERB(x) EMPTY
#define CVERBLN(x) EMPTY
#endif

#if VERBOSE >= 2
#define CVERB2(x) _CVERB(x, " <<<", ">>> ")
#define CVERB2LN(x) _CVERBLN(x, " <<<", ">>> ")
#else
#define CVERB2(x)
#define CVERB2LN(x)
#endif

#if VERBOSE >= 3
#define CVERB3(x) _CVERB(x, "  <<", ">>  ")
#define CVERB3LN(x) _CVERBLN(x, "  <<", ">>  ")
#else
#define CVERB3(x)
#define CVERB3LN(x)
#endif

namespace unsot {
    using std::ostream;
    using std::istream;
    using std::ofstream;
    using std::ifstream;
    using std::stringstream;
    using std::istringstream;
    using std::ostringstream;
    using std::streampos;

    using namespace std::string_literals;

    using std::to_string;

    //+ String to_string(float arg);
    //+ String to_string(double arg);
    String to_string(char);
    inline String to_string(const char* str)                                         { return str; }
    inline const String& to_string(const String& str)                                { return str; }
    template <typename ItT, Req<is_iterator_v<ItT> && !is_ptr_v<Decay<ItT>>
                                && !enabled_member_to_string_v<ItT>> = 0>
        String to_string(ItT it)                                          { return to_string(*it); }
    String to_string(istream&);
    template <typename T1, typename T2> String to_string(const Pair<T1, T2>&);
    template <typename... Args> String to_string(const tuple<Args...>&);
    template <typename T, Req<enabled_member_to_string_v<T>> = 0>
        String to_string(const T& rhs)                                   { return rhs.to_string(); }

    template <typename T, Req<!enabled_to_string_v<T>> = 0,
              typename = decltype(declval<ostream>() << declval<T>())>
        String to_string(const T&);

    String to_string(const Ignore&);

    /// For non-`Object` types with defined `to_string` free function
    template <typename T, Req<enabled_to_string_v<T> && !is_string_v<T>> = 0>
        ostream& operator <<(ostream& os, const T& rhs)           { return (os << to_string(rhs)); }

    /// `Ignore` absorbs any arguments
    template <typename T> constexpr auto& operator <<(const Ignore& i, T&&) noexcept   { return i; }
    constexpr auto& operator <<(const Ignore& i, ostream&(*)(ostream&)) noexcept       { return i; }
    constexpr auto& operator <<(const Ignore& i, std::ios&(*)(std::ios&)) noexcept     { return i; }
    constexpr auto& operator <<(const Ignore& i, std::ios_base&(*)(std::ios_base&)) noexcept
                                                                                       { return i; }

    template <typename Arg> bool is_value(const String&) noexcept;
    template <typename Arg, typename Str, Req<is_string_v<Str>> = 0>
        bool get_value(Arg&, Str&&) noexcept;
    template <typename Arg> bool get_value(Arg&, istream&) noexcept;

    int istream_remain_size(istream&);
}

#include "unsot/string.inl"
