#pragma once

#include "unsot/error.hpp"

#include <sstream>

namespace unsot {
    template <typename T1, typename T2>
    String to_string(const Pair<T1, T2>& rhs)
    {
        return to_string(rhs.first) + " " + to_string(rhs.second);
    }

    namespace aux {
        template <size_t...> struct Seq {};

        template <size_t n, size_t... is>
        struct Gen_seq : Gen_seq<n-1, n-1, is...> {};

        template <size_t... is>
        struct Gen_seq<0, is...> : Seq<is...> {};

        /// Can be simplified with `std::apply', but it would introduce
        /// the dependecy of '<functional>' to all files
        template <typename Tuple, size_t... is>
        static void tuple_to_string(String& str, const Tuple& t, Seq<is...>)
        {
            using A = int[];
            A{0, (void(str += ((is == 0) ? "" : " ") + to_string(get<is>(t)) ), 0)...};
        }
    }

    template <typename... Args>
    String to_string(const tuple<Args...>& rhs)
    {
      String str;
      aux::tuple_to_string(str, rhs, aux::Gen_seq<sizeof...(Args)>());
      return str;
    }

    template <typename T, Req<!enabled_to_string_v<T>>, typename>
    String to_string(const T& rhs)
    {
        ostringstream oss;
        oss << rhs;
        return oss.str();
    }

    template <typename Arg>
    bool is_value(const String& str) noexcept
    {
        Arg val;
        return get_value(val, str);
    }

    template <typename Arg, typename Str, Req<is_string_v<Str>>>
    bool get_value(Arg& val, Str&& str) noexcept
    {
        istringstream iss(FORWARD(str));
        return get_value(val, iss);
    }

    template <typename Arg>
    bool get_value(Arg& val, istream& is) noexcept
    {
        is >> val;
        return is && is.eof();
    }
}
