#pragma once

#include "smt/solver/bools.hpp"

#include "util/view.hpp"

#include <deque>

namespace unsot::smt::solver {
    template <var::Type typeV = var::Type::assignee>
        using Reals = var::Vars<Real, Real, typeV>;
}

namespace unsot::smt::solver::reals {
    template <typename B, var::Type typeV = var::Type::assignee>
        class Mixin;

    template <typename S, typename SatSolver, var::Type typeV = var::Type::assignee>
        using Crtp = Mixin<bools::Crtp<S, SatSolver>, typeV>;

    using Preds = var::Vars<Bool, Real>;

    using Pred = Preds::Types::Pred;
    template <var::Type typeV = var::Type::assignee>
        using Var = typename Reals<typeV>::Types::Var;
    template <var::Type typeV = var::Type::assignee>
        using Fun = typename Reals<typeV>::Types::Fun;

    template <var::Type typeV = var::Type::assignee>
        using View = util::View<Reals<typeV>>;
    template <var::Type typeV = var::Type::assignee>
        using Uniq_view = util::Unique_view<Reals<typeV>>;
    template <var::Type typeV = var::Type::assignee>
        using Deque_view = util::View<Reals<typeV>, std::deque>;
    template <var::Type typeV = var::Type::assignee>
        using Deque_uniq_view = util::Unique_view<Reals<typeV>, std::deque>;
    /// Using `Bools` because of global var. ids
    using Preds_view = bools::View;
    using Preds_uniq_view = bools::Uniq_view;
    using Preds_deque_view = bools::Deque_view;
    using Preds_deque_uniq_view = bools::Deque_uniq_view;

    using bools::Views_map;

    constexpr const char* sort_key = "Real";

    using bools::view;

    using bools::make_default_views_map;
}

namespace unsot::smt::solver {
    template <typename Sort = Real, typename ArgSort = Sort, typename SolverT,
              Req<is_real_v<Sort> || is_real_v<ArgSort>> = 0>
        auto&& vars(SolverT&&) noexcept;

    template <typename Sort = Real, typename SolverT, Req<is_real_v<Sort>> = 0>
        bool contains(const SolverT&, const Key&) noexcept;
    template <typename Sort = Real, typename SolverT, Req<is_real_v<Sort>> = 0>
        void check_contains(const SolverT&, const Key&);
}

namespace unsot::smt::solver::reals {
    template <typename B, var::Type typeV>
    class Mixin : public Inherit<B, Mixin<B, typeV>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B, typeV>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Reals_t = This;

        class Check_sat;

        using Reals = solver::Reals<typeV>;

        using Real_var = reals::Var<Reals::Types::var_type>;
        using Real_fun = reals::Fun<Reals::Types::var_type>;

        using typename Inherit::Sat_solver;

        using Inherit::Inherit;
        Mixin();
        virtual ~Mixin()                                                                  = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&);
        Mixin& operator =(Mixin&&);

        //+ can also be somehow handled automatically by `Dynamic` ?
        void static_init();

        const auto& creals() const noexcept                                       { return _reals; }
        const auto& reals() const& noexcept                                     { return creals(); }
        auto& reals()& noexcept                                                   { return _reals; }
        auto&& reals()&& noexcept                                          { return move(reals()); }
        const auto& creal_preds() const noexcept                             { return _real_preds; }
        const auto& real_preds() const& noexcept                           { return creal_preds(); }
        auto& real_preds()& noexcept                                         { return _real_preds; }
        auto&& real_preds()&& noexcept                                { return move(real_preds()); }

        bool valid_sort(const Key&) const noexcept override;

        virtual bool is_t_key(const Key&) const noexcept;

        bool contains(const Key&) const noexcept override;
        bool contains_var(const Key&) const noexcept override;
        bool contains_real(const Key&) const noexcept;
        void check_contains_real(const Key&) const;
        bool contains_bool_var(const Key&) const noexcept override;
        virtual bool contains_real_var(const Key&) const noexcept;
        virtual bool contains_real_fun(const Key&) const noexcept;
        virtual bool contains_real_pred(const Key&) const noexcept;

        virtual bool contains_def_real_fun(const Key&) const noexcept;
        virtual bool contains_def_real_pred(const Key&) const noexcept;

        void reserve_reals(size_t);

        virtual void declare_real_var(Key);
        virtual void define_real_var(Key, Real);

        virtual Key instantiate_real(Formula);
        /// Assert expression with solely real arguments
        /// (thus it must be a predicate)
        virtual void assert_real(Formula);

        virtual void define_real_fun(Key, Formula, Keys = {});
        virtual void define_real_pred(Key, Formula, Keys = {});

        virtual Key instantiate_def_real_fun(const Key&, const Keys& = {});
        virtual Key instantiate_def_real_pred(const Key&, const Keys& = {});
        virtual Formula expand_def_real_fun(const Key&, const Keys& = {});
        virtual Formula expand_def_real_pred(const Key&, const Keys& = {});
        virtual void assert_def_real_pred(const Key&, const Keys& = {});

        virtual Idx compute_bmc_step_of(const Key&) const;
        virtual void replace_bmc_step_of(Key&, Idx) const;
    protected:
        using typename Inherit::Fun_def;
        using typename Inherit::Fun_inst;

        using Reals_view = View<Reals::Types::var_type>;
        using Reals_uniq_view = Uniq_view<Reals::Types::var_type>;
        using Reals_deque_view = Deque_view<Reals::Types::var_type>;
        using Reals_deque_uniq_view = Deque_uniq_view<Reals::Types::var_type>;

        using Inherit::aux_var_key_prefix;
        static inline const String aux_real_fun_key_prefix = aux_var_key_prefix + "f";
        static inline const String aux_real_pred_key_prefix = aux_var_key_prefix + "p";

        void declare_var_impl(Key&, const Key& sort) override;
        void define_var_impl(Key&, const Key& sort, Elem&) override;
        template <typename ValT = Dummy> void define_real_var_tp(Key&&, ValT&& = {});

        void pre_add_real(const Key&)                                                            { }

        Key instantiate_expr_impl(Formula&) override;
        void assert_expr_impl(Formula&) override;

        bool try_conv_to_bool_impl(expr::Ptr&) override;

        virtual Key instantiate_real_fun(Formula);
        virtual Key instantiate_real_pred(Formula);
        virtual void assert_real_pred(Formula);

        Fun_inst make_real_auto_fun_inst(Formula);
        Fun_inst make_real_auto_pred_inst(Formula);

        virtual Key instantiate_real_fun_inst(Fun_inst);
        virtual Key instantiate_real_pred_inst(Fun_inst);
        virtual Formula expand_real_fun_inst(Fun_inst);
        virtual Formula expand_real_pred_inst(Fun_inst);
        virtual void assert_real_pred_inst(Fun_inst);

        void define_fun_impl(Key&, const Key& sort, Formula&, Keys&, const Key& args_sort) override;

        void finish_solve(const Sat&) override;

        String var_to_string_impl(const Key&) const override;
        void print_vars_impl() const noexcept override;

        String var_stats_to_string() const override;
    private:
        Reals _reals{};
        Preds _real_preds{this->bools(), reals()};
    };
}

#include "smt/solver/reals/check_sat.hpp"

#include "smt/solver/reals.inl"
