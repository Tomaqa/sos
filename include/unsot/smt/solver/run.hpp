#pragma once

#include "expr/preprocess/run.hpp"
#include "smt/solver.hpp"

inline double preprocess_duration{};

namespace unsot::smt::solver {
    template <typename S>
    class Crtp<S>::Run : public Inherit<Preprocess::Run>,
                         public unsot::Crtp<typename Crtp<S>::That::Run> {
    public:
        using Solver = Crtp::That;
        using typename unsot::Crtp<typename Crtp<S>::That::Run>::That;

        using Inherit::Inherit;
        virtual ~Run()                                                                    = default;

        void init() override;
        void do_stuff() override;

        String usage() const override;

        List preprocess() override;
        virtual void parse();
    protected:
        virtual const Solver& csolver() const noexcept                           { return _solver; }
        virtual Solver& solver() noexcept                                        { return _solver; }

        String lusage() const;

        void linit();

        String getopt_str() const noexcept override;
        String lgetopt_str() const noexcept;
        bool process_opt(char) override;
        bool lprocess_opt(char);

        virtual void print_profiling(Duration total) const;

        unsigned char _preprocess_only{};
        bool _bool_formula_only{};
        bool _dimacs_only{};
        bool _print_t_inconsistent_count{};
    private:
        Solver _solver{};
    };
}
