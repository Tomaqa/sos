#pragma once

#include "util/hash.hpp"

namespace unsot::smt::solver::reals {
    template <typename B, var::Type typeV>
    class Mixin<B, typeV>::Check_sat
        : public unsot::Inherit<typename B::Check_sat, Mixin::Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Mixin::Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Check_sat()                                                              = default;
        Check_sat(Check_sat&&)                                                            = default;

        int ct_inconsistent_count() const noexcept                 { return _t_inconsistent_count; }
        int t_inconsistent_count() const noexcept                { return ct_inconsistent_count(); }

        bool all_sat() const noexcept override;

        void restart() override;
    protected:
        template <typename IdSort = Bool> struct Make_depend_preds_clause;

        int& t_inconsistent_count() noexcept                       { return _t_inconsistent_count; }

        virtual typename Sat_solver::Clause
            make_depend_real_preds_clause_of(const var::Id&,
                                             var::Distance max_len = var::inf_dist);
        virtual typename Sat_solver::Clause
            make_depend_real_preds_clause_impl_of(const var::Id&,
                                                  var::Distance max_len = var::inf_dist);
        virtual typename Sat_solver::Clause
            make_depend_real_preds_conflict_of(const var::Id&,
                                               var::Distance max_len = var::inf_dist);

        bool lall_sat() const noexcept;
    private:
        int _t_inconsistent_count{};
    };
}

namespace unsot::smt::solver::reals {
    template <typename B, var::Type typeV>
    template <typename IdSort>
    struct Mixin<B, typeV>::Check_sat::Make_depend_preds_clause {
        Make_depend_preds_clause(Solver&);

        typename Sat_solver::Clause
            perform(const var::Id&, var::Distance max_len_ = var::inf_dist);

        template <typename Sort> bool visit(const var::Id&);

        Solver& solver;
        Preds_uniq_view view;
        static inline Hash<var::Id> visited_ids{};
        var::Distance max_len{};
    };
}

#include "smt/solver/reals/check_sat.inl"