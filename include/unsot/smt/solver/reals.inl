#pragma once

namespace unsot::smt::solver {
    template <typename Sort, typename ArgSort, typename SolverT,
              Req<is_real_v<Sort> || is_real_v<ArgSort>>>
    auto&& vars(SolverT&& solver) noexcept
    {
        if constexpr (is_real_v<Sort>) return FORWARD(solver).reals();
        else {
            static_assert(is_bool_v<Sort>);
            return FORWARD(solver).real_preds();
        }
    }

    template <typename Sort, typename SolverT, Req<is_real_v<Sort>>>
    bool contains(const SolverT& solver, const Key& key) noexcept
    {
        return solver.contains_real(key);
    }

    template <typename Sort, typename SolverT, Req<is_real_v<Sort>>>
    void check_contains(const SolverT& solver, const Key& key)
    {
        solver.check_contains_real(key);
    }
}
