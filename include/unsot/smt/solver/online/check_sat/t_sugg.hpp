#pragma once

namespace unsot::smt::solver::online {
    template <typename B>
    class Mixin<B>::Check_sat::T_suggest_decision
        : public unsot::Inherit<Strategy_mixin<typename Mixin::template Strategy<T_suggest_decision_conf>>,
                                T_suggest_decision> {
    public:
        using Inherit =
            unsot::Inherit<Strategy_mixin<typename Mixin::template Strategy<T_suggest_decision_conf>>,
                           T_suggest_decision>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        static_assert(is_same_v<That, typename Check_sat::That::T_suggest_decision>);

        using typename Inherit::Strategy_conf;
        using Ret = typename Online_conf::T_decision_suggestion;

        static_assert(is_same_v<typename Strategy_conf::Ret, Optional<Ret>>);

        using Inherit::Inherit;
        virtual ~T_suggest_decision()                                                     = default;
        T_suggest_decision(T_suggest_decision&&)                                          = default;

        bool cenabled() const noexcept                                          { return _enabled; }

        Optional<Ret> perform() override;

        void post_check_sat_restart() override;
    protected:
        template <typename B2, typename StateT> class Stack_mixin;

        bool& enabled() noexcept                                                { return _enabled; }

        virtual bool invalid_current_state() const noexcept                         { return true; }
        bool valid_current_state() const noexcept               { return !invalid_current_state(); }

        void perform_init() override;
        Optional<Ret> perform_body() override;
        void perform_finish() override;

        virtual void backtrack();
        virtual void finish();

        static String name_impl()                                   { return "t-suggest-decision"; }
    private:
        /// Concrete strategy must enable this explicitly (based on the input)
        bool _enabled{};
    };
}

namespace unsot::smt::solver::online {
    template <typename B>
    template <typename B2, typename StateT>
    class Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin
        : public unsot::Inherit<B2, Stack_mixin<B2, StateT>> {
    public:
        using Inherit = unsot::Inherit<B2, Stack_mixin<B2, StateT>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Stack_mixin()                                                            = default;
        Stack_mixin(Stack_mixin&&)                                                        = default;

        void restart() override;
    protected:
        using State = StateT;
        using State_stack = Vector<State>;

        const auto& cstate_stack() const noexcept                           { return _state_stack; }
        auto& state_stack() noexcept                                        { return _state_stack; }
        const auto& ccurrent_state() const noexcept;
        auto& current_state() noexcept;

        bool invalid_current_state() const noexcept override;

        virtual void add_state(State) noexcept;
        virtual void rm_current_state() noexcept;

        void perform_init() override;

        void backtrack() override;

        virtual bool is_ok(const State&) const noexcept                             { return true; }

        virtual String state_to_string(const State&) const                            { return ""; }
        virtual String current_state_to_string() const;
        virtual String current_state_to_string_extra() const                          { return ""; }
    private:
        State_stack _state_stack{};
    };
}

#include "smt/solver/online/check_sat/t_sugg.inl"
