#pragma once

namespace unsot::smt::solver::online {
    template <typename B>
    template <typename T>
    void Mixin<B>::Check_sat::set_t_propagate_strategy_tp(bool force)
    {
        T_propagate::template set_tp<T>(t_propagate_ptr(), force);
    }

    template <typename B>
    template <typename T>
    void Mixin<B>::Check_sat::set_t_suggest_decision_strategy_tp(bool force)
    {
        T_suggest_decision::template set_tp<T>(t_suggest_decision_ptr(), force);
    }

    template <typename B>
    const auto& Mixin<B>::Check_sat::ct_propagate_ref() const noexcept
    {
        return T_propagate::cref(ct_propagate_ptr());
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::t_propagate_ref() noexcept
    {
        return T_propagate::ref(t_propagate_ptr());
    }

    template <typename B>
    const auto& Mixin<B>::Check_sat::ct_suggest_decision_ref() const noexcept
    {
        return T_suggest_decision::cref(ct_suggest_decision_ptr());
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::t_suggest_decision_ref() noexcept
    {
        return T_suggest_decision::ref(t_suggest_decision_ptr());
    }

    template <typename B>
    typename Mixin<B>::Check_sat::Online_conf::T_propagate_result
    Mixin<B>::Check_sat::t_propagate_exec()
    {
        return t_propagate_ref().perform();
    }

    template <typename B>
    Optional<typename Mixin<B>::Check_sat::Online_conf::T_decision_suggestion>
    Mixin<B>::Check_sat::t_suggest_decision()
    {
        return t_suggest_decision_ref().perform();
    }
}
