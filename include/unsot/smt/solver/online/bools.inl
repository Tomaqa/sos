#pragma once

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <typename T>
    void Mixin<B>::set_check_sat_strategy_tp(bool force)
    {
        Inherit::template set_check_sat_strategy_tp<T>(force);
        lset_check_sat_strategy();
    }

    template <typename B>
    void Mixin<B>::set_t_learn_strategy(const String& name, bool force)
    try {
        set_t_learn_strategy_impl(name, force);
    }
    catch (Dummy) {
        // Not exactly the base class, but here it does not matter (it just uses `T_learn::name`)
        using T_learn = typename Check_sat::template T_learn<typename Check_sat::T_explain_conf>;
        THROW(T_learn::unknown_strategy_msg(name));
    }

    template <typename B>
    void Mixin<B>::set_t_explain_strategy(const String& name, bool force)
    {
        Check_sat::T_explain::set_tp(BIND_THIS(set_t_explain_strategy_impl), name, force);
    }

    template <typename B>
    void Mixin<B>::set_analyze_t_inconsistency_strategy(const String& name, bool force)
    {
        Check_sat::Analyze_t_inconsistency::set_tp(BIND_THIS(set_analyze_t_inconsistency_strategy_impl), name, force);
    }
}
