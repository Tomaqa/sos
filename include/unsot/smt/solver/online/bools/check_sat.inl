#pragma once

#include "util/numeric/alg.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <typename T>
    void Mixin<B>::Check_sat::set_t_explain_strategy_tp(bool force)
    {
        T_explain::template set_tp<T>(t_explain_ptr(), force);
    }

    template <typename B>
    template <typename T>
    void Mixin<B>::Check_sat::set_analyze_t_inconsistency_strategy_tp(bool force)
    {
        Analyze_t_inconsistency::template set_tp<T>(analyze_t_inconsistency_ptr(), force);
    }

    template <typename B>
    const auto& Mixin<B>::Check_sat::ct_explain_ref() const noexcept
    {
        return T_explain::cref(ct_explain_ptr());
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::t_explain_ref() noexcept
    {
        return T_explain::ref(t_explain_ptr());
    }

    template <typename B>
    const auto& Mixin<B>::Check_sat::canalyze_t_inconsistency_ref() const noexcept
    {
        return Analyze_t_inconsistency::cref(canalyze_t_inconsistency_ptr());
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::analyze_t_inconsistency_ref() noexcept
    {
        return Analyze_t_inconsistency::ref(analyze_t_inconsistency_ptr());
    }

    template <typename B>
    const auto& Mixin<B>::Check_sat::ctracked_bools_view(int level) const noexcept
    {
        return ctracked_bool_views()[level];
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::tracked_bools_view(int level) noexcept
    {
        return tracked_bool_views()[level];
    }

    template <typename B>
    const auto& Mixin<B>::Check_sat::ccurrent_tracked_bools_view() const noexcept
    {
        return ctracked_bools_view(decision_level());
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::current_tracked_bools_view() noexcept
    {
        return tracked_bools_view(decision_level());
    }

    template <typename B>
    int Mixin<B>::Check_sat::decision_level() const noexcept
    {
        assert(!ctracked_bool_views().empty());
        return ctracked_bool_views().size()-1;
    }

    template <typename B>
    int Mixin<B>::Check_sat::cdecision_level_of(const var::Id& vid) const
    {
        assert(tracked(this->csolver().cbools().cptr(vid)));
        return cdecision_levels_map()[vid];
    }

    template <typename B>
    int& Mixin<B>::Check_sat::decision_level_of(const var::Id& vid)
    {
        return decision_levels_map()[vid];
    }

    template <typename B>
    int Mixin<B>::Check_sat::cdecision_level_of(const typename Sat_solver::Lit& lit) const
    {
        const int level = this->csolver().csat_solver().decision_level_of(lit.var_id());
        assert(level == cdecision_level_of(this->csolver().cbool_id(lit.var_id())));
        return level;
    }

    template <typename B>
    const var::Id& Mixin<B>::Check_sat::decision_bool_id_at(int level) const
    {
        return ctracked_bools_view(level).caccessors().front();
    }

    template <typename B>
    const var::Ptr& Mixin<B>::Check_sat::cdecision_bool_ptr_at(int level) const
    {
        return ctracked_bools_view(level).cfront();
    }

    template <typename B>
    var::Ptr& Mixin<B>::Check_sat::decision_bool_ptr_at(int level)
    {
        return tracked_bools_view(level).front();
    }

    template <typename B>
    const var::Id& Mixin<B>::Check_sat::current_decision_bool_id() const
    {
        return decision_bool_id_at(decision_level());
    }

    template <typename B>
    const var::Ptr& Mixin<B>::Check_sat::ccurrent_decision_bool_ptr() const
    {
        return cdecision_bool_ptr_at(decision_level());
    }

    template <typename B>
    var::Ptr& Mixin<B>::Check_sat::current_decision_bool_ptr()
    {
        return decision_bool_ptr_at(decision_level());
    }

    template <typename B>
    template <typename ItT>
    int Mixin<B>::Check_sat::max_decision_level_of(ItT first, ItT last) const
    {
        using Tl_ = T_learn_base;

        assert(first != last);
        [[maybe_unused]] const int max_level =
            numeric::max(first, last, [this](auto& lit){ return cdecision_level_of(lit); });
        if constexpr (!Tl_::sorting_decision_levels()) return max_level;
        else {
            const int flevel = cdecision_level_of(*first);
            if (first+1 == last) return flevel;
            const auto aux_it = invoke([first, last]{
                if constexpr (Tl_::order_decision_levels_v == Tl_::Order_decision_levels::asc)
                    return last-1;
                else return first+1;
            });
            const int level = max(flevel, cdecision_level_of(*aux_it));
            assert(level == max_level);
            return level;
        }
    }

    template <typename B>
    typename Mixin<B>::Check_sat::Online_conf::T_explain_result
    Mixin<B>::Check_sat::t_explain(typename Sat_solver::Lit p)
    {
        auto vptr_l = this->solver().find_bool_ptr(p);
        assert(vptr_l);
        return t_explain_ref().perform(*vptr_l);
    }
}
