#ifdef NO_EXPLICIT_TP_INST

#include "smt/solver/online/bools.tpp"

#include "smt/solver/online/bools/run.tpp"

#else

namespace TP_INST_SOLVER_NAMESPACE {
    extern template class aux::_S::Online_bools_t::Mixin;
}

#endif  /// NO_EXPLICIT_TP_INST
