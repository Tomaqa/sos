#pragma once

namespace unsot::smt::solver::online::bools {
    template <typename B>
    class Mixin<B>::Check_sat::T_learn_base {
    public:
        // Basic options how to order literals in learnt clauses,
        // but in general, it does not seem beneficial
        //+ rather move there features to a mixin
        enum class Order_decision_levels { no, asc, desc, random };

        static constexpr Order_decision_levels order_decision_levels_v = Order_decision_levels::no;

        static constexpr bool sorting_decision_levels() noexcept;

        T_learn_base()                                                                    = default;
        ~T_learn_base()                                                                   = default;
        T_learn_base(T_learn_base&&)                                                      = default;

        const auto& cliterals_count() const noexcept                     { return _literals_count; }
        const auto& ctotal_literals_count() const noexcept         { return _total_literals_count; }
    protected:
        auto& literals_count() noexcept                                  { return _literals_count; }
        auto& total_literals_count() noexcept                      { return _total_literals_count; }

        static String name_impl()                                              { return "t-learn"; }
    private:
        int _literals_count{};
        long _total_literals_count{};
    };

    template <typename B>
    template <typename ConfT>
    class Mixin<B>::Check_sat::T_learn
        : public T_learn_base,
          public unsot::Inherit<Strategy_mixin<typename Mixin::Online_t::Check_sat::template Strategy_mixin<typename Mixin::template Strategy<ConfT, var::Ptr&>, var::Ptr&>>, T_learn<ConfT>> {
    public:
        using Inherit = unsot::Inherit<Strategy_mixin<typename Mixin::Online_t::Check_sat::template Strategy_mixin<typename Mixin::template Strategy<ConfT, var::Ptr&>, var::Ptr&>>, T_learn<ConfT>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using typename Inherit::Strategy_conf;

        static_assert(is_same_v<typename Strategy_conf::Ret, void>);

        using Inherit::Inherit;
        virtual ~T_learn()                                                                = default;
        T_learn(T_learn&&)                                                                = default;

        virtual bool perform_limited(var::Ptr&, var::Distance max_len);

        virtual typename Sat_solver::Clause
            explain(const var::Ptr&, var::Distance max_len = var::inf_dist);

        virtual void perform_with(typename Sat_solver::Clause);

        String profiling_to_string(Duration total) const override;
    protected:
        void perform_body(var::Ptr&) override;
        virtual void perform_body_head(var::Ptr&, typename Sat_solver::Clause&);
        virtual void perform_body_impl(var::Ptr&, typename Sat_solver::Clause&&);
        virtual void perform_body_tail(var::Ptr&);
        void perform_finish() override;

        virtual bool perform_body_limited(var::Ptr&, var::Distance max_len);

        virtual void order_clause(var::Ptr&, typename Sat_solver::Clause&);
    private:
        virtual typename Sat_solver::Clause explain_impl(const var::Ptr&, var::Distance max_len)= 0;
    };

    template <typename B>
    class Mixin<B>::Check_sat::T_explain : public unsot::Inherit<T_learn<T_explain_conf>, T_explain> {
    public:
        using Inherit = unsot::Inherit<T_learn<T_explain_conf>, T_explain>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        static_assert(is_same_v<That, typename Check_sat::That::T_explain>);

        using Inherit::Inherit;
        virtual ~T_explain()                                                              = default;
        T_explain(T_explain&&)                                                            = default;
    protected:
        static String name_impl()                                            { return "t-explain"; }
    };

    template <typename B>
    class Mixin<B>::Check_sat::Analyze_t_inconsistency
        : public unsot::Inherit<T_learn<Analyze_t_inconsistency_conf>, Analyze_t_inconsistency> {
    public:
        using Inherit = unsot::Inherit<T_learn<Analyze_t_inconsistency_conf>, Analyze_t_inconsistency>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        static_assert(is_same_v<That, typename Check_sat::That::Analyze_t_inconsistency>);

        using Inherit::Inherit;
        virtual ~Analyze_t_inconsistency()                                                = default;
        Analyze_t_inconsistency(Analyze_t_inconsistency&&)                                = default;
    protected:
        void perform_body_head(var::Ptr&, typename Sat_solver::Clause&) override;

        virtual void backtrack(var::Ptr&, typename Sat_solver::Clause&);

        static String name_impl()                                      { return "t-inconsistency"; }

        static String profiling_total_name_impl()         { return Check_sat::T_propagate::name(); }
    };
}

#include "smt/solver/online/bools/check_sat/t_learn.inl"
