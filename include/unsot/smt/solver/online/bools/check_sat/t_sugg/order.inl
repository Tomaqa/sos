#pragma once

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <typename B2>
    bool Mixin<B>::Check_sat::T_suggest_decision::Order_mixin<B2>::invalid_current_state() const noexcept
    {
        return ccurrent_bool_id() == var::invalid_id;
    }
}
