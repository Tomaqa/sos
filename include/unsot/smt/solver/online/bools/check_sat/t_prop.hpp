#pragma once

#include "util/hash.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    class Mixin<B>::Check_sat::T_propagate
        : public unsot::Inherit<Strategy_mixin<typename B::Check_sat::T_propagate>, T_propagate> {
    public:
        using Inherit = unsot::Inherit<Strategy_mixin<typename B::Check_sat::T_propagate>, T_propagate>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~T_propagate()                                                            = default;
        T_propagate(T_propagate&&)                                                        = default;

        bool maybe_performed_with(const var::Ptr&) const noexcept;
        bool performed_with(const var::Ptr&) const noexcept;

        void post_check_sat_t_backtrack_bool(var::Ptr&) override;

        void restart() override;
    protected:
        using Performed_bools = Hash<var::Id>;

        const auto& cperformed_bools() const noexcept                   { return _performed_bools; }
        auto& performed_bools() noexcept                                { return _performed_bools; }

        template <bool doAssert> bool maybe_performed_with_tp(const var::Ptr&) const noexcept;
        template <bool doAssert> bool performed_with_tp(const var::Ptr&) const noexcept;
        virtual bool maybe_performed_with_impl(const var::Ptr&) const noexcept;
        virtual bool performed_with_impl(const var::Ptr&) const noexcept;

        virtual void perform_with(var::Ptr&, const var::Ptr& from);
        virtual void perform_print_with(const var::Ptr&, const var::Ptr& from) const;
        virtual void perform_with_impl(var::Ptr&, const var::Ptr& from);
        virtual void perform_not_learn_with(var::Ptr&);

        virtual bool maybe_learn_from(var::Ptr&, const var::Ptr& from);
        virtual bool should_not_learn_from(const var::Ptr&, const var::Ptr& from) const;
        virtual bool should_learn_from(const var::Ptr&, const var::Ptr& from) const;
        virtual void learn_from(var::Ptr&, const var::Ptr& from);
        virtual bool maybe_learn_from_anyway(var::Ptr&, const var::Ptr& from);

        virtual bool maybe_learn_short_clause_from(var::Ptr&, const var::Ptr& from);
        virtual var::Distance short_clause_threshold() const;
    private:
        Performed_bools _performed_bools{};
    };
}

#include "smt/solver/online/bools/check_sat/t_prop.inl"
