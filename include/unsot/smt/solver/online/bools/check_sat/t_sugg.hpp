#pragma once

namespace unsot::smt::solver::online::bools {
    template <typename B>
    class Mixin<B>::Check_sat::T_suggest_decision
        : public unsot::Inherit<Strategy_mixin<typename B::Check_sat::T_suggest_decision>, T_suggest_decision> {
    public:
        using Inherit = unsot::Inherit<Strategy_mixin<typename B::Check_sat::T_suggest_decision>, T_suggest_decision>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        using typename Inherit::Ret;

        template <typename B2> class Order_mixin;

        using Order = Order_mixin<That>;

        //+ a decision heuristic which suggests the last assigned value of a variable,
        //+ instead of always 1/0

        //+ sometimes, let the SAT solver choose a dec. variable, but suggest the value

        using Inherit::Inherit;
        virtual ~T_suggest_decision()                                                     = default;
        T_suggest_decision(T_suggest_decision&&)                                          = default;

        void pre_check_sat_t_backtrack_bool(var::Ptr&) override;
    protected:
        template <typename B2> class Stack_mixin;

        virtual bool invalid_current_bool() const noexcept;
        bool valid_current_bool() const noexcept                 { return !invalid_current_bool(); }
        /// At least one of them must be reimplemented
        virtual const var::Ptr& ccurrent_bool_ptr() const;
        virtual const var::Id& ccurrent_bool_id() const;

        /// To be used inside `perform_body`
        virtual Ret perform_with(const var::Ptr&);
        virtual void perform_print_with(const var::Ptr&, const Flag& val) const;
        virtual typename Sat_solver::Var_id perform_var_id_with(const var::Ptr&) const;
        virtual Flag perform_value_with(const var::Ptr&) const;

        void backtrack() override;
        virtual void backtrack_impl();

        virtual void maybe_backtrack_from_bool(const var::Ptr&);
        virtual bool maybe_backtrack_from_bool_head(const var::Ptr&) const;
        virtual bool maybe_backtrack_from_bool_body(const var::Ptr&);
        virtual void backtrack_from_bool_finish();
    };
}

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin
        : public unsot::Inherit<B2, Stack_mixin<B2>> {
    public:
        using Inherit = unsot::Inherit<B2, Stack_mixin<B2>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Stack_mixin()                                                            = default;
        Stack_mixin(Stack_mixin&&)                                                        = default;
    protected:
        using typename Inherit::State;

        void perform_print_with(const var::Ptr&, const Flag& val) const override;
    };
}

#include "smt/solver/online/bools/check_sat/t_sugg/order.hpp"

#include "smt/solver/online/bools/check_sat/t_sugg.inl"
