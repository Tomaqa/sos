#pragma once

namespace unsot::smt::solver::online::bools {
    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::maybe_performed_with(const var::Ptr& vptr) const noexcept
    {
        return maybe_performed_with_tp<do_assert>(vptr);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::performed_with(const var::Ptr& vptr) const noexcept
    {
        return performed_with_tp<do_assert>(vptr);
    }
}
