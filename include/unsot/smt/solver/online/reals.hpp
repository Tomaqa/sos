#pragma once

#include "smt/solver/reals.hpp"
#include "smt/solver/online/bools.hpp"

namespace unsot::smt::solver::online::reals {
    using namespace solver::reals;

    template <typename B> class Mixin;

    template <typename S, typename SatSolver, var::Type typeV = var::Type::assignee>
        using Crtp = Mixin<solver::reals::Mixin<bools::Crtp<S, SatSolver>, typeV>>;
}

namespace unsot::smt::solver::online::reals {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Online_reals_t = This;

        class Check_sat;

        class Run;

        using typename Inherit::Sat_solver;

        using Inherit::Inherit;
        Mixin()                                                                           = default;
        virtual ~Mixin()                                                                  = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&)                                                                    = default;
        Mixin& operator =(Mixin&&)                                                        = default;
    protected:
        void set_check_sat_strategy_impl(const String& name, bool force = no_force) override;

        void set_t_suggest_decision_strategy_impl(const String& name, bool force = no_force) override;
        void set_analyze_t_inconsistency_strategy_impl(const String& name, bool force = no_force) override;
    };
}

#include "smt/solver/online/reals/check_sat.hpp"
