#pragma once

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename T>
    void Mixin<B>::set_check_sat_strategy_tp(bool force)
    {
        using T_ = Cond_t<is_void_v<T>, typename Check_sat::Rail, T>;
        Inherit::template set_check_sat_strategy_tp<T_>(force);
    }
}
