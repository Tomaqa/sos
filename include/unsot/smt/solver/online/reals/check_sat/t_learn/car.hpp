#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::Analyze_t_inconsistency::Car_mixin
        : public unsot::Inherit<B2, Car_mixin<B2>> {
    public:
        using Inherit = unsot::Inherit<B2, Car_mixin<B2>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Car_mixin()                                                              = default;
        Car_mixin(Car_mixin&&)                                                            = default;

        static String svariant_name()                                              { return "car"; }
        String variant_name() const override                             { return svariant_name(); }
    };
}

#include "smt/solver/online/reals/check_sat/t_learn/car.inl"
