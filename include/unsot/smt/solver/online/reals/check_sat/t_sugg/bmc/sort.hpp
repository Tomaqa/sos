#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    template <typename B3>
    class Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::Sort_mixin
        : public unsot::Inherit<B3, Sort_mixin<B3>> {
    public:
        using Inherit = unsot::Inherit<B3, Sort_mixin<B3>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Sort_mixin()                                          = default;
        Sort_mixin(Sort_mixin&&)                                       = default;
    protected:
        Flag sort_bool_view_compare_rest(const var::Ptr&, const var::Ptr&) const override;
        virtual Flag sort_bool_view_compare_initial(const Preds::Types::Assigner&, const Preds::Types::Assigner&) const;
    };
}
