#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    class Mixin<B>::Check_sat::T_suggest_decision
        : public unsot::Inherit<typename B::Check_sat::T_suggest_decision, T_suggest_decision> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat::T_suggest_decision, T_suggest_decision>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        using typename Inherit::Ret;

        template <typename B2> class Bmc_mixin;

        using Bmc = Bmc_mixin<This>;

        using Inherit::Inherit;
        virtual ~T_suggest_decision()                                                     = default;
        T_suggest_decision(T_suggest_decision&&)                                          = default;
    protected:
        struct Bmc_state;
    };
}

#include "smt/solver/online/reals/check_sat/t_sugg/bmc.hpp"
