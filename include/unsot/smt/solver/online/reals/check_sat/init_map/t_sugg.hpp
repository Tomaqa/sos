#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision
        : public unsot::Inherit<typename B2::T_suggest_decision, T_suggest_decision> {
    public:
        using Inherit = unsot::Inherit<typename B2::T_suggest_decision, T_suggest_decision>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        template <typename B3> class Initial_mixin;
        /// Includes `Check_sat::T_suggest_decision::Bmc::Sort_mixin`
        template <typename B3> class Bmc_sort_mixin;

        using Initial = Initial_mixin<This>;
        using Bmc = typename Inherit::template Bmc_mixin<This>;
        using Sorted_bmc = Bmc_sort_mixin<Bmc>;

        using Inherit::Inherit;
        virtual ~T_suggest_decision()                                                     = default;
        T_suggest_decision(T_suggest_decision&&)                                          = default;

        const auto& ccheck_sat_ref() const noexcept;
    protected:
        struct Initial_state;

        auto& check_sat_ref() noexcept;
    };
}

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    template <typename B3>
    class Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Bmc_sort_mixin
        : public unsot::Inherit<typename Check_sat::T_suggest_decision::Bmc::template Sort_mixin<B3>, Bmc_sort_mixin<B3>> {
    public:
        using Inherit = unsot::Inherit<typename Check_sat::T_suggest_decision::Bmc::template Sort_mixin<B3>, Bmc_sort_mixin<B3>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Bmc_sort_mixin()                                                         = default;
        Bmc_sort_mixin(Bmc_sort_mixin&&)                                                  = default;

        static String svariant_name();
        String variant_name() const override                             { return svariant_name(); }
    protected:
        Flag sort_bool_view_compare_initial(const Preds::Types::Assigner&, const Preds::Types::Assigner&) const override;
    };
}

#include "smt/solver/online/reals/check_sat/init_map/t_sugg/init.hpp"

#include "smt/solver/online/reals/check_sat/init_map/t_sugg.inl"
