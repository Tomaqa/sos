#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    class Mixin<B>::Check_sat::T_propagate
        : public unsot::Inherit<typename B::Check_sat::T_propagate, T_propagate> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat::T_propagate, T_propagate>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using typename Inherit::Strategy_conf;

        using Inherit::Inherit;
        virtual ~T_propagate()                                                            = default;
        T_propagate(T_propagate&&)                                                        = default;

        /// Whether applying an inference rule is possible right now
        bool inferial(const var::Ptr&) const noexcept;
        virtual bool inferial(const Preds::Types::Assigner&) const noexcept;
        /// Whether an inference rule has been applied
        bool has_inferenced(const var::Ptr&) const noexcept;
        virtual bool has_inferenced(const Preds::Types::Assigner&) const noexcept;

        bool empty() const noexcept override;

        void post_check_sat_notice(var::Ptr&) override;

        void restart() override;
    protected:
        struct Perform_with_result;

        /// It does not seem to make any significant difference
        //+ not configurable, but it won't probably work with the cascade inheritance
        static constexpr bool postpone_already_pending_v = false;

        using Pending_real_preds = Cond_t<postpone_already_pending_v,
                                          Preds_deque_uniq_view, Preds_deque_view>;

        const auto& cpending_real_preds() const noexcept             { return _pending_real_preds; }
        auto& pending_real_preds() noexcept                          { return _pending_real_preds; }

        virtual void notice_real_pred(var::Ptr&);
        virtual void notice_real_pred_impl(var::Ptr&);

        void perform_init() override;
        typename Strategy_conf::Ret perform_body() override;

        virtual Optional<Perform_with_result> try_perform_with(const var::Ptr&);
        virtual Optional<Perform_with_result> try_perform_with_not_inferenced(const var::Ptr&);
        virtual Perform_with_result perform_with_inferenced(const var::Ptr&);
        virtual Optional<Perform_with_result>
            try_perform_from_inferenced_with(var::Ptr&, const var::Ptr& from);
        virtual Optional<Perform_with_result>
            try_perform_from_inferenced_with_inferial(var::Ptr&, const var::Ptr& from);
        void perform_with_impl(var::Ptr&, const var::Ptr& from) override;

        bool should_learn_from(const var::Ptr&, const var::Ptr& from) const override;
    private:
        Pending_real_preds _pending_real_preds{this->solver().bools()};
    };
}

namespace unsot::smt::solver::online::reals {
    template <typename B>
    struct Mixin<B>::Check_sat::T_propagate::Perform_with_result {
        constexpr Perform_with_result& operator +=(Perform_with_result) noexcept;

        short inconsistent_count{};
        short propagation_count{};
    };
}

#include "smt/solver/online/reals/check_sat/t_prop.inl"
