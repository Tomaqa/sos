#pragma once

#include "smt/solver/online/reals/rail.hpp"
#include "smt/sat/solver/online/expr/minisat.hpp"

#define TP_INST_SOLVER_NAMESPACE unsot::smt

namespace unsot::smt::solver::online::reals::rail::minisat {
    template <typename S>
        using Crtp = rail::Crtp<S, sat::solver::online::expr::Minisat<>>;

    struct Solver final : Inherit<Crtp<Solver>> {
        using Inherit::Inherit;
    };
}

namespace unsot::smt {
    namespace aux {
        using _S = solver::online::reals::rail::minisat::Solver;
    }
}
#include "smt/solver/tp.hpp"
#include "smt/solver/bools/tp.hpp"
#include "smt/solver/reals/tp.hpp"
#include "smt/solver/online/tp.hpp"
#include "smt/solver/online/bools/tp.hpp"
#include "smt/solver/online/reals/tp.hpp"
#include "smt/solver/online/reals/rail/tp.hpp"

namespace unsot::smt::sat::solver {
    namespace aux {
        using _Minisat = online::expr::Minisat<>;
    }
}
#include "smt/sat/solver/online/minisat/tp.hpp"
