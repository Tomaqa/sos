#pragma once

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    template <typename B3>
    class Mixin<B>::Check_sat::Rail_mixin<B2>::T_suggest_decision_bmc_mixin
        : public unsot::Inherit<Strategy_mixin<B3>, T_suggest_decision_bmc_mixin<B3>> {
    public:
        using Inherit = unsot::Inherit<Strategy_mixin<B3>, T_suggest_decision_bmc_mixin<B3>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~T_suggest_decision_bmc_mixin()                                           = default;
        T_suggest_decision_bmc_mixin(T_suggest_decision_bmc_mixin&&)                      = default;
    protected:
        using typename Inherit::Var_info;

        Var_info compute_var_info(const var::Ptr&) override;

        Flag sort_bool_view_compare_rest(const var::Ptr&, const var::Ptr&) const override;
    };

    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::Rail_mixin<B2>::T_suggest_decision
        : public unsot::Inherit<typename Rail_mixin::Init_map_mixin::T_suggest_decision::template Bmc_sort_mixin<T_suggest_decision_bmc_mixin<typename B2::T_suggest_decision::Bmc>>, T_suggest_decision> {
    public:
        using Inherit = unsot::Inherit<typename Rail_mixin::Init_map_mixin::T_suggest_decision::template Bmc_sort_mixin<T_suggest_decision_bmc_mixin<typename B2::T_suggest_decision::Bmc>>, T_suggest_decision>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~T_suggest_decision()                                                     = default;
        T_suggest_decision(T_suggest_decision&&)                                          = default;
    };
}

#include "smt/solver/online/reals/rail/check_sat/t_sugg.inl"
