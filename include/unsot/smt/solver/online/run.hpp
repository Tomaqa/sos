#pragma once

#include "smt/solver/run.hpp"
#include "smt/solver/online.hpp"

namespace unsot::smt::solver::online {
    template <typename B>
    class Mixin<B>::Run : public unsot::Inherit<typename B::Run> {
    public:
        using Inherit = unsot::Inherit<typename B::Run>;
        using typename Inherit::Solver;
        using typename Inherit::That;

        using Inherit::Inherit;
        virtual ~Run()                                                                    = default;

        String usage() const override;

        List preprocess() override;
    protected:
        String lusage() const;

        String getopt_str() const noexcept override;
        String lgetopt_str() const noexcept;
        bool process_opt(char) override;
        bool lprocess_opt(char);
    };
}
