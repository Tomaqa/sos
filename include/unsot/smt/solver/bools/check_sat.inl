#pragma once

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    template <typename ItT>
    typename Mixin<B, SatSolver>::Sat_solver::Clause
    Mixin<B, SatSolver>::Check_sat::make_sat_clause(ItT first, ItT last, size_t reserve_size) const
    {
        typename Sat_solver::Clause clause;
        maybe_add_sat_lits(clause, first, last, reserve_size);
        return clause;
    }

    template <typename B, typename SatSolver>
    template <typename VarsT>
    typename Mixin<B, SatSolver>::Sat_solver::Clause
    Mixin<B, SatSolver>::Check_sat::make_sat_clause(const VarsT& vars_, size_t reserve_size) const
    {
        return make_sat_clause(vars_.cbegin(), vars_.cend(), reserve_size);
    }

    template <typename B, typename SatSolver>
    template <typename ItT>
    void Mixin<B, SatSolver>::Check_sat::add_sat_lits(typename Sat_solver::Clause& clause, ItT first, ItT last, size_t reserve_size) const
    {
        add_sat_lits_impl<ItT>(clause, first, last, reserve_size);
    }

    template <typename B, typename SatSolver>
    template <typename ItT>
    void Mixin<B, SatSolver>::Check_sat::maybe_add_sat_lits(typename Sat_solver::Clause& clause, ItT first, ItT last, size_t reserve_size) const
    {
        add_sat_lits_impl<ItT, true>(clause, first, last, reserve_size);
    }

    template <typename B, typename SatSolver>
    template <typename ItT, bool maybeV>
    void Mixin<B, SatSolver>::Check_sat::add_sat_lits_impl(typename Sat_solver::Clause& clause, ItT first, ItT last, size_t reserve_size) const
    {
        if (reserve_size > 0) clause.reserve(size(clause)+reserve_size);
        std::for_each(first, last, [this, &clause](auto& vptr){
            auto& var = Var::cast(vptr);
            if constexpr (!maybeV) add_sat_lit(clause, var);
            else maybe_add_sat_lit(clause, var);
        });
    }
}