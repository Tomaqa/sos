#pragma once

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    class Mixin<B, SatSolver>::Parser : public unsot::Inherit<typename B::Parser> {
    public:
        using Inherit = unsot::Inherit<typename B::Parser>;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Parser()                                           = default;
    protected:
        void post_preprocess_cmd(expr::Ptr&, Key& cmd, Pos&) override;
        virtual void post_preprocess_cmd_ite(expr::Ptr&, Key& cmd, Pos&);
        virtual void post_preprocess_cmd_distinct(expr::Ptr&, Key& cmd, Pos&);
        virtual void post_preprocess_cmd_declare_const(expr::Ptr&, Key& cmd, Pos&);
        void post_preprocess_elem(expr::Ptr&) override;

        void parse_cmd(const Key& cmd, Pos&) override;
        virtual void parse_cmd_declare_fun(Pos&&);
        virtual void parse_cmd_define_fun(Pos&&);
    };
}
