#pragma once

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    class Mixin<B, SatSolver>::Check_sat
        : public unsot::Inherit<typename B::Check_sat, Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;

        virtual bool all_sat() const noexcept;

        virtual void restart();
    protected:
        typename Sat_solver::Lit make_sat_lit(const Var&) const;
        template <typename ItT> typename Sat_solver::Clause
            make_sat_clause(ItT first, ItT last, size_t reserve_size = 0) const;
        template <typename VarsT> typename Sat_solver::Clause
            make_sat_clause(const VarsT&, size_t reserve_size = 0) const;

        void add_sat_lit(typename Sat_solver::Clause&, const Var&) const;
        void maybe_add_sat_lit(typename Sat_solver::Clause&, const Var&) const;
        template <typename ItT>
            void add_sat_lits(typename Sat_solver::Clause&, ItT first, ItT last, size_t reserve_size = 0) const;
        template <typename ItT>
            void maybe_add_sat_lits(typename Sat_solver::Clause&, ItT first, ItT last, size_t reserve_size = 0) const;

        bool lall_sat() const noexcept;

        virtual void print_sat_clause(const typename Sat_solver::Clause&, const String& msg) const;
        virtual void print_sat_clause_impl(const typename Sat_solver::Clause&, const String& msg) const;
    private:
        template <bool maybeV = false>
            void add_sat_lit_impl(typename Sat_solver::Clause&, const Var&) const;
        template <typename ItT, bool maybeV = false>
            void add_sat_lits_impl(typename Sat_solver::Clause&, ItT first, ItT last, size_t reserve_size) const;
    };
}

#include "smt/solver/bools/check_sat.inl"