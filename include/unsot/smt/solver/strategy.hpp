#pragma once

#include "util.hpp"

namespace unsot::smt::solver {
    template <typename S>
    class Crtp<S>::Strategy_base : public Dynamic<Strategy_base, Strategy_ptr>,
                                   public Lock {
    public:
        using typename Dynamic<Strategy_base, Strategy_ptr>::This;
        using typename Dynamic<Strategy_base, Strategy_ptr>::Ptr;
        using Solver = Crtp::That;
        using Base = This;

        static_assert(is_same_v<Strategy_ptr, Ptr>);

        Strategy_base()                                                                    = delete;
        virtual ~Strategy_base()                                                          = default;
        Strategy_base(const Strategy_base&)                                                = delete;
        Strategy_base& operator =(const Strategy_base&)                                    = delete;
        Strategy_base(Strategy_base&&)                                                    = default;
        Strategy_base& operator =(Strategy_base&&)                                         = delete;
        Strategy_base(Solver&);

        template <typename T> static Ptr make_ptr(T&&);

        const auto& csolver() const noexcept                                     { return _solver; }

        static String svariant_name()                                                 { return ""; }
        virtual String variant_name() const                              { return svariant_name(); }
        template <typename T> bool is_variant_tp() const;
        bool is_variant(const String& name) const;
    protected:
        auto& solver() noexcept                                                  { return _solver; }
    private:
        Solver& _solver;
    };

    namespace aux {
        template <typename RetT>
        class Ret_base {
        protected:
            const auto& cret() const noexcept                                       { return _ret; }
            auto& ret() noexcept                                                    { return _ret; }
        private:
            RetT _ret{};
        };

        template <>
        class Ret_base<void> {};
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    class Crtp<S>::Strategy : public Inherit<Strategy_base, Strategy<ConfT, Args...>>,
                              public unsot::Crtp<typename ConfT::template Type_tp<typename Strategy_base::Solver>>,
                              public aux::Ret_base<typename ConfT::Ret> {
    public:
        using Inherit = unsot::Inherit<Strategy_base, Strategy<ConfT, Args...>>;
        using typename Inherit::This;
        using typename Inherit::Ptr;
        using typename Inherit::Solver;
        using typename unsot::Crtp<typename ConfT::template Type_tp<typename Strategy_base::Solver>>::That;

        using Strategy_conf = ConfT;

        using Inherit::Inherit;
        virtual ~Strategy()                                                               = default;
        Strategy(Strategy&&)                                                              = default;

        template <typename T = void> static void set_tp(Ptr&, bool force = no_force);
        template <typename SetF>
            static void set_tp(SetF, const String& concrete_name, bool force = no_force);
        static String unknown_strategy_msg(const String& concrete_name);

        static const That& cref(const Ptr&) noexcept;
        static That& ref(Ptr&) noexcept;

        bool cprofiling() const noexcept                                      { return _profiling; }
        bool& profiling() noexcept                                            { return _profiling; }
        const auto& cduration() const noexcept                                 { return _duration; }
        const auto& ccount() const noexcept                                       { return _count; }

        virtual typename Strategy_conf::Ret perform(Args...);

        static String name();
        virtual String full_name() const;

        virtual String profiling_to_string(Duration total) const;
    protected:
        static constexpr bool is_ret_void_v = is_void_v<typename Strategy_conf::Ret>;

        template <typename T> using Print_perform_parts = Fwd_t<typename T::Print_perform_parts>;
        static constexpr bool print_perform_parts_v = enabled_bool_v<Print_perform_parts, Strategy_conf>;

        auto& duration() noexcept                                              { return _duration; }
        auto& count() noexcept                                                    { return _count; }

        virtual typename Strategy_conf::Ret perform_profiled(Args...);
        virtual void perform_init(Args&...)                                                      { }
        virtual void perform_finish()                                                            { }

        static String profiling_total_name();
        static String profiling_total_name_impl()                                { return "total"; }
    private:
        struct Access;

        virtual typename Strategy_conf::Ret perform_body(Args...)                               = 0;

        bool _profiling{};
        Duration _duration{};
        long _count{};
    };
}

namespace unsot::smt::solver {
    template <typename S>
    template <typename ConfT, typename... Args>
    struct Crtp<S>::Strategy<ConfT, Args...>::Access : That {
        static String name();
        static String profiling_total_name();
    };
}

#include "smt/solver/strategy.inl"
