#pragma once

namespace unsot::smt::solver {
    template <typename S>
    class Crtp<S>::Check_sat : public Inherit<Strategy<Check_sat_conf>, Check_sat> {
    public:
        using Inherit = unsot::Inherit<Strategy<Check_sat_conf>, Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        static_assert(is_same_v<That, typename Solver::Check_sat>);

        using typename Inherit::Strategy_conf;

        static_assert(is_same_v<typename Strategy_conf::Ret, Sat>);

        using Inherit::Inherit;
        virtual ~Check_sat()                                                              = default;
        Check_sat(Check_sat&&)                                                            = default;

        const auto& cinit_duration() const noexcept                       { return _init_duration; }
        const auto& cfinish_duration() const noexcept                   { return _finish_duration; }

        String profiling_to_string(Duration total) const override;
    protected:
        auto& init_duration() noexcept                                    { return _init_duration; }
        auto& finish_duration() noexcept                                { return _finish_duration; }

        void perform_init() override;
        virtual void perform_init_profiled();
        virtual void perform_init_impl();
        Sat perform_body() override                                              { return unknown; }
        void perform_finish() override;
        virtual void perform_finish_profiled();
        virtual void perform_finish_impl();

        static String name_impl()                                            { return "check-sat"; }
    private:
        Duration _init_duration{};
        Duration _finish_duration{};
    };
}

#include "smt/solver/check_sat.inl"
