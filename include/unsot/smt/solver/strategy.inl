#pragma once

namespace unsot::smt::solver {
    template <typename S>
    Crtp<S>::Strategy_base::Strategy_base(Solver& solver_)
        : _solver(solver_)
    { }

    template <typename S>
    template <typename T>
    typename Crtp<S>::Strategy_base::Ptr Crtp<S>::Strategy_base::make_ptr(T&& t)
    {
        return MAKE_UNIQUE(FORWARD(t));
    }

    template <typename S>
    template <typename T>
    bool Crtp<S>::Strategy_base::is_variant_tp() const
    {
        return is_variant(T::svariant_name());
    }

    template <typename S>
    bool Crtp<S>::Strategy_base::is_variant(const String& name) const
    {
        assert(empty(svariant_name()));
        if (empty(name)) return false;
        return variant_name() == name;
    }

    ////////////////////////////////////////////////////////////////

    template <typename S>
    template <typename ConfT, typename... Args>
    template <typename T>
    void Crtp<S>::Strategy<ConfT, Args...>::set_tp(Ptr& ptr, bool force)
    {
        using T_ = Cond_t<is_void_v<T>, That, T>;
        static_assert(is_base_of_v<This, T_>);
        if (!force && ptr->locked()) return;
        ptr = T_::new_me(move(ref(ptr)));
        if (force) ptr->lock();
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    template <typename SetF>
    void Crtp<S>::Strategy<ConfT, Args...>::set_tp(SetF set_f, const String& concrete_name, bool force)
    try {
        invoke(move(set_f), concrete_name, force);
    }
    catch (Dummy) {
        THROW(unknown_strategy_msg(concrete_name));
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    String Crtp<S>::Strategy<ConfT, Args...>::unknown_strategy_msg(const String& concrete_name)
    {
        return "Unknown "s + name() + " strategy: " + concrete_name;
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    const typename Crtp<S>::Strategy<ConfT, Args...>::That&
    Crtp<S>::Strategy<ConfT, Args...>::cref(const Ptr& ptr) noexcept
    {
        static_assert(is_base_of_v<This, That>);
        static_assert(is_same_v<That, typename This::That>);
        static_assert(is_same_v<const This&, decltype(This::cast(ptr))>);
        return This::cast(ptr).cthat();
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    typename Crtp<S>::Strategy<ConfT, Args...>::That&
    Crtp<S>::Strategy<ConfT, Args...>::ref(Ptr& ptr) noexcept
    {
        return const_cast<That&>(cref(ptr));
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    String Crtp<S>::Strategy<ConfT, Args...>::name()
    {
        return Access::name();
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    String Crtp<S>::Strategy<ConfT, Args...>::full_name() const
    {
        String str = name();
        String var_str = this->variant_name();
        if (!empty(var_str)) str += "(" + move(var_str) + ")";
        return str;
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    String Crtp<S>::Strategy<ConfT, Args...>::profiling_total_name()
    {
        return Access::profiling_total_name();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver {
    template <typename S>
    template <typename ConfT, typename... Args>
    String Crtp<S>::Strategy<ConfT, Args...>::Access::name()
    {
        return Access::name_impl();
    }

    template <typename S>
    template <typename ConfT, typename... Args>
    String Crtp<S>::Strategy<ConfT, Args...>::Access::profiling_total_name()
    {
        return Access::profiling_total_name_impl();
    }
}
