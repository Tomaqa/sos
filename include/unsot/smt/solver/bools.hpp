#pragma once

#include "smt/solver.hpp"

#include "util/hash.hpp"
#include "util/view.hpp"
#include "expr/bools.hpp"
#include "expr/var/vars_arith.hpp"
#include "smt/sat/solver/expr.hpp"

#include <deque>

namespace unsot::smt::solver {
    using Bools = var::Vars<Bool>;
}

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver> class Mixin;

    template <typename S, typename SatSolver> using Crtp = Mixin<solver::Crtp<S>, SatSolver>;

    using Var = Bools::Types::Var;

    using View = util::View<Bools>;
    using Uniq_view = util::Unique_view<Bools>;
    using Deque_view = util::View<Bools, std::deque>;
    using Deque_uniq_view = util::Unique_view<Bools, std::deque>;

    template <typename V> using Views_map = Hash<var::Id, V>;

    constexpr const char* sort_key = "Bool";

    template <typename M> auto&& view(M&&, const var::Id&);

    template <typename V, typename SrcSort, typename SolverT>
        Views_map<V> make_default_views_map(SolverT&);
    template <typename V> void shrink_views_map(Views_map<V>&);
}

namespace unsot::smt::solver {
    template <typename Sort = Bool, typename ArgSort = Sort, typename SolverT,
              Req<is_bool_v<Sort> && is_bool_v<ArgSort>> = 0>
        auto&& vars(SolverT&&) noexcept;

    template <typename Sort = void, typename SolverT, Req<is_void_v<Sort> || is_bool_v<Sort>> = 0>
        bool contains(const SolverT&, const Key&) noexcept;
    template <typename Sort = void, typename SolverT, Req<is_void_v<Sort> || is_bool_v<Sort>> = 0>
        void check_contains(const SolverT&, const Key&);

    bool valid_arg_pairs(const Lists&) noexcept;
    void check_arg_pairs(const Lists&);

    //+ it should be `Elems` not `Keys` -> allow using direct values
    template <typename Phi, typename UnF>
        void for_each_free_arg_key(Phi&&, const Keys&, UnF);
    template <typename Sort = void, typename SolverT>
        void check_contains_free_arg_keys(const SolverT&, const Formula&, const Keys& = {});

    String var_to_string(const var::Base&);
}

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    class Mixin : public Inherit<B, Mixin<B, SatSolver>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B, SatSolver>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Bools_t = This;

        class Check_sat;

        using Sat_solver = SatSolver;

        using Inherit::Inherit;
        Mixin();
        virtual ~Mixin()                                                                  = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&)                                                                    = default;
        Mixin& operator =(Mixin&&)                                                        = default;

        void static_init();

        const auto& cbool_formula() const noexcept                         { return _bool_formula; }
        const auto& bool_formula() const& noexcept                       { return cbool_formula(); }
        auto&& bool_formula()&& noexcept                            { return move(bool_formula()); }

        const auto& cbools() const noexcept                                       { return _bools; }
        const auto& bools() const& noexcept                                     { return cbools(); }
        auto& bools()& noexcept                                                   { return _bools; }
        auto&& bools()&& noexcept                                          { return move(bools()); }

        const auto& csat_solver() const noexcept                             { return _sat_solver; }
        const auto& sat_solver() const noexcept                            { return csat_solver(); }

        virtual bool valid_sort(const Key&) const noexcept;
        virtual void check_sort(const Key&) const;

        bool contains(const Key&) const noexcept override;
        bool contains_var(const Key&) const noexcept override;
        bool contains_bool(const Key&) const noexcept;
        void check_contains_bool(const Key&) const;
        virtual bool contains_bool_var(const Key&) const noexcept;
        virtual bool contains_bool_fun(const Key&) const noexcept;

        virtual bool contains_def_bool_fun(const Key&) const noexcept;

        void reserve_bools(size_t);

        void declare_var(Key key_, const Key& sort)                { declare_var_impl(key_, sort); }
        void define_var(Key key_, const Key& sort, Elem elem) { define_var_impl(key_, sort, elem); }
        virtual void declare_bool_var(Key);
        virtual void define_bool_var(Key, Bool);

        using Inherit::assert_expr;
        virtual Key instantiate_expr(Formula);
        void assert_elem(Elem) override;
        void assert_expr(Formula) override;

        virtual Key instantiate_bool(Formula);
        void assert_bool(Formula) override;

        void define_fun(Key key_, const Key& sort, Formula phi, Keys arg_keys, const Key& args_sort)
                                          { define_fun_impl(key_, sort, phi, arg_keys, args_sort); }
        void define_fun(Key key_, const Key& sort, Formula phi, Keys arg_keys = {})
                                               { define_fun_impl(key_, sort, phi, arg_keys, sort); }
        virtual void define_bool_fun(Key, Formula, Keys = {});

        virtual Key instantiate_def_bool_fun(const Key&, const Keys& = {});
        virtual Formula expand_def_bool_fun(const Key&, const Keys& = {});
        virtual void assert_def_bool_fun(const Key&, const Keys& = {});
    protected:
        class Fun_def;
        struct Fun_inst;

        struct Formula_hash;

        class Parser;

        //? is using an expr-SAT solver (which uses string var ids) still worth?
        using Bool_ids_map = Hash<typename Sat_solver::Var_id, var::Id>;
        using Sat_var_ids_map = Vector<typename Sat_solver::Var_id>;

        using Fun_defs_map = Hash<Key, Fun_def>;

        using Formula_keys_map = Hash<Formula, Key, Formula_hash>;

        using Inherit::aux_var_key_prefix;
        static inline const String aux_bool_var_key_prefix = aux_var_key_prefix + "F";

        static constexpr bool do_check = true;
        static constexpr bool no_check = false;

        static constexpr bool allow_dups = true;
        static constexpr bool no_dups = false;

        auto& bool_formula()& noexcept                                     { return _bool_formula; }

        auto& sat_solver() noexcept                                          { return _sat_solver; }

        const auto& cbool_ids_map() const noexcept                         { return _bool_ids_map; }
        auto& bool_ids_map() noexcept                                      { return _bool_ids_map; }
        const auto& csat_var_ids_map() const noexcept                   { return _sat_var_ids_map; }
        auto& sat_var_ids_map() noexcept                                { return _sat_var_ids_map; }
        const var::Id& cbool_id(const typename Sat_solver::Var_id&) const;
        const typename Sat_solver::Var_id& csat_var_id(const var::Id&) const;

        const auto& cfun_defs_map() const noexcept                         { return _fun_defs_map; }
        auto& fun_defs_map() noexcept                                      { return _fun_defs_map; }
        const auto& cfun_def(const Key&) const;
        auto& fun_def(const Key&);

        const auto& cformula_keys_map() const noexcept                 { return _formula_keys_map; }
        auto& formula_keys_map() noexcept                              { return _formula_keys_map; }
        const Key& cformula_key(const Formula&) const;
        Key& formula_key(const Formula&);

        /// Auxiliary SAT variables are not tracked -> can be `invalid_id`
        const var::Id find_bool_id(const typename Sat_solver::Var_id&) const noexcept;
        const var::Ptr* cfind_bool_ptr(const typename Sat_solver::Var_id&) const noexcept;
        var::Ptr* find_bool_ptr(const typename Sat_solver::Var_id&) noexcept;
        const var::Ptr* cfind_bool_ptr(const typename Sat_solver::Lit&) const noexcept;
        var::Ptr* find_bool_ptr(const typename Sat_solver::Lit&) noexcept;

        bool contains_fun_def(const Key&) const noexcept;
        void check_contains_fun_def(const Key&) const;
        void check_not_contains_fun_def(const Key&) const;

        bool contains_formula(const Formula&) const noexcept;

        virtual void declare_var_impl(Key&, const Key& sort);
        virtual void define_var_impl(Key&, const Key& sort, Elem&);
        template <typename Sort, typename ValT = Sort> void define_var_tp(Key&&, ValT&& = dummy);
        template <typename ValT = Dummy> void define_bool_var_tp(Key&&, ValT&& = {});

        [[nodiscard]] typename Sat_solver::Var_id pre_add_bool(const Key&);
        void post_add_bool(typename Sat_solver::Var_id);
        typename Sat_solver::Var_id add_new_sat_var(const Key&);

        virtual Key instantiate_expr_impl(Formula&);
        virtual void assert_expr_impl(Formula&);

        bool try_conv_to_bool(Formula&);
        virtual bool try_conv_to_bool(expr::Ptr&);
        virtual void conv_to_bool(Formula&);
        virtual String conv_to_bool_msg(const Formula&) const;

        virtual bool try_conv_to_bool_impl(expr::Ptr&);

        Fun_inst make_fun_inst(const Key&, const Keys&);
        Fun_inst make_auto_fun_inst(Formula, Key = aux_var_key_prefix);
        Fun_inst make_bool_auto_fun_inst(Formula);

        template <typename ArgSort, bool checkArgs = true, bool allowDups = false, typename UnF>
            Key instantiate_fun_inst_tp(Fun_inst&&, UnF = nop);
        virtual Formula expand_fun_inst(Fun_inst);
        virtual void assert_fun_inst(Fun_inst);
        virtual Key instantiate_bool_fun_inst(Fun_inst);
        virtual Formula expand_bool_fun_inst(Fun_inst);
        virtual void assert_bool_fun_inst(Fun_inst);

        void assert_cnf(expr::bools::Cnf);

        virtual void define_fun_impl(Key&, const Key& sort, Formula&, Keys&, const Key& args_sort);
        template <typename Sort, typename ArgSort = Sort>
            void define_fun_tp(Key&&, Formula&&, Keys&&);

        template <typename InstF> Key instantiate_def_fun_tp(const Key&, const Keys&, InstF);
        virtual Formula expand_def_fun(const Key&, const Keys& = {});
        virtual void assert_def_fun(const Key&, const Keys& = {});

        void init_solve() override;
        void finish_solve(const Sat&) override;

        String var_to_string_impl(const Key&) const override;
        void print_vars_impl() const noexcept override;

        String var_stats_to_string() const override;
    private:
        Formula _bool_formula;

        Bools _bools{};

        Sat_solver _sat_solver{Sat_solver::cons()};

        Bool_ids_map _bool_ids_map{};
        Sat_var_ids_map _sat_var_ids_map{};

        Fun_defs_map _fun_defs_map{};
        Formula_keys_map _formula_keys_map{};
    };
}

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    class Mixin<B, SatSolver>::Fun_def : public Static<Fun_def> {
    public:
        using typename Static<Fun_def>::This;
        using Solver_link = Mixin::That*;

        Fun_def()                                                   = default;
        ~Fun_def()                                                  = default;
        Fun_def(const Fun_def&)                                     = default;
        Fun_def& operator =(const Fun_def&)                         = default;
        Fun_def(Fun_def&&)                                          = default;
        Fun_def& operator =(Fun_def&&)                              = default;
        Fun_def(Solver_link, Key, Formula, Keys = {});

        const auto& ckey() const noexcept                     { return _key; }
        const auto& key() const& noexcept                   { return ckey(); }
        auto&& key()&& noexcept                        { return move(key()); }
        const auto& cformula() const noexcept             { return _formula; }
        const auto& formula() const& noexcept           { return cformula(); }
        auto&& formula()&& noexcept                { return move(formula()); }
        const Keys& carg_keys() const noexcept           { return _arg_keys; }
        const Keys& arg_keys() const& noexcept         { return carg_keys(); }
        Keys&& arg_keys()&& noexcept              { return move(arg_keys()); }

        Fun_inst instantiate(const Keys& = {});

        String to_string() const&;
    protected:
        const auto& csolver_l() const noexcept           { return _solver_l; }
        auto& solver_l() noexcept                        { return _solver_l; }
        const auto& csolver() const noexcept          { return *csolver_l(); }
        auto& solver() noexcept                        { return *solver_l(); }

        auto& key()& noexcept                                 { return _key; }
        auto& formula()& noexcept                         { return _formula; }
        auto& arg_keys()& noexcept                       { return _arg_keys; }
    private:
        Solver_link _solver_l{};

        Key _key{};
        Formula _formula{};
        Keys _arg_keys{};
    };

    template <typename B, typename SatSolver>
    struct Mixin<B, SatSolver>::Fun_inst : public Static<Fun_inst> {
        using typename Static<Fun_inst>::This;

        Fun_inst()                                                  = default;
        ~Fun_inst()                                                 = default;
        Fun_inst(const Fun_inst&)                                   = default;
        Fun_inst& operator =(const Fun_inst&)                       = default;
        Fun_inst(Fun_inst&&)                                        = default;
        Fun_inst& operator =(Fun_inst&&)                            = default;
        Fun_inst(Key key_, Formula phi)
                                     : key(move(key_)), formula(move(phi)) { }

        Formula to_pred() const&;
        Formula to_pred()&&;

        String to_string() const&;

        Key key;
        Formula formula;
    };

    template <typename B, typename SatSolver>
    struct Mixin<B, SatSolver>::Formula_hash {
        size_t operator ()(const Formula&) const;
    };
}

#include "smt/solver/bools/check_sat.hpp"
#include "smt/solver/bools/parser.hpp"

#include "smt/solver/bools.inl"
