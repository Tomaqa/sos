#pragma once

#include "smt/solver/bools.hpp"
#include "smt/solver/offline.hpp"

namespace unsot::smt::solver::offline::bools {
    using namespace solver::bools;

    template <typename B> class Mixin;

    template <typename S, typename SatSolver>
        using Crtp = Mixin<solver::bools::Crtp<S, SatSolver>>;
}

namespace unsot::smt::solver::offline::bools {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Offline_bools_t = This;

        class Check_sat;

        class Run;

        using Inherit::Inherit;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;
    protected:
        /// Do not use e.g. `using Inherit::bools`, as it would be considered
        /// as protected function of this class and could cause
        /// access restrictions from derived classes due to CRTP
    };
}

#include "smt/solver/offline/bools/check_sat.hpp"
