#pragma once

namespace unsot::smt::solver::offline::reals::equal {
    template <typename B>
    class Mixin<B>::Check_sat : public unsot::Inherit<typename B::Check_sat, Mixin::Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Mixin::Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
    protected:
        void try_eval_reals() override;
        Flag update_reals() override;
        virtual void sort_real_preds_view();
    };
}
