#pragma once

#include "smt/sat/solver/online.hpp"
#include "smt/sat/solver/minisat.hpp"

namespace unsot::smt::sat::solver::online::minisat {
    using namespace solver::minisat;

    template <typename B> class Mixin;

    template <typename S, typename BaseConf = minisat::Conf,
              template<typename> typename OnlineConf = online::Conf,
              typename SmtCheckSat = Smt_check_sat_base<OnlineConf<BaseConf>>>
        using Crtp = Mixin<online::Mixin<solver::minisat::Crtp<S, BaseConf>,
                                         OnlineConf, SmtCheckSat>>;

    using Orig_clause_ref = ::Minisat::CRef;

    constexpr auto Orig_undef_clause_ref = ::Minisat::CRef_Undef;
}

namespace unsot::smt::sat::solver::online {
    template <template<typename> typename ConfT = Conf,
              typename SmtCheckSat = Smt_check_sat_base<ConfT<minisat::Conf>>>
    struct Minisat
        : Inherit<minisat::Crtp<Minisat<ConfT, SmtCheckSat>, minisat::Conf, ConfT, SmtCheckSat>,
                  Minisat<ConfT, SmtCheckSat>> {
        using Inherit =
            unsot::Inherit<minisat::Crtp<Minisat<ConfT, SmtCheckSat>, minisat::Conf,
                                         ConfT, SmtCheckSat>,
                           Minisat<ConfT, SmtCheckSat>>;

        using Inherit::Inherit;
    };
}

namespace unsot::smt::sat::solver::online::minisat {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Online_minisat_t = This;

        using typename Inherit::Lit;

        using typename Inherit::Clause;

        using Inherit::Inherit;
        virtual ~Mixin()                                            = default;
        Mixin(Mixin&&);
        Mixin& operator =(Mixin&&);

        int decision_level() const override;
        int decision_level_of(const Var_id&) const override;

        using Inherit::sat_propagate;
        void sat_propagate(Lit) override;

        void sat_learn(Clause) override;

        void sat_backtrack() override;
        void sat_backtrack_to(int level) override;
    protected:
        class Solver;

        void linit();
    };
}

namespace unsot::smt::sat::solver::online::minisat {
    template <typename B>
    class Mixin<B>::Solver : public unsot::Inherit<typename B::Solver> {
    public:
        using Inherit = unsot::Inherit<typename B::Solver>;

        using Online_solver = Mixin;
        using Online_solver_link = Online_solver*;

        using Inherit::Inherit;

        void connect(Online_solver&);
    protected:
        friend Online_solver;

        const auto& conline_solver() const noexcept                    { return *_online_solver_l; }
        const auto& online_solver() const noexcept                      { return conline_solver(); }
        auto& online_solver() noexcept                                 { return *_online_solver_l; }

        Orig_lbool search(int nof_conflicts) override;

        void newDecisionLevel() override;
        Orig_lit pickBranchLit() override;

        Orig_clause_ref propagate() override;
        void uncheckedEnqueue(Orig_lit, Orig_clause_ref from) override;

        virtual void learn(typename Online_solver::Clause);
        void cancelUntil(int level) override;
        void cancelAll() override;

        void externReason(Orig_lit, Orig_clause_ref&) override;

        void reduceDB() override;
    private:
        Online_solver_link _online_solver_l{};

        Orig_clause_ref _learnt_cref{Orig_undef_clause_ref};

        bool _searching{};
    };
}

#include "smt/sat/solver/online/minisat.inl"
