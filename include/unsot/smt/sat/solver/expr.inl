#pragma once

namespace unsot::smt::sat::solver::expr {
    template <typename B>
    const auto& Mixin<B>::cvar_id(const bools::Var_id& ex) const
    try {
        return cvar_ids_bimap()[ex];
    }
    catch (const Error& err) {
        THROW("Lookup of SAT literal corresponding to expr literal failed: ") + err;
    }

    template <typename B>
    const auto& Mixin<B>::cevar_id(const Var_id& x) const
    try {
        return cvar_ids_bimap()[x];
    }
    catch (const Error& err) {
        THROW("Lookup of expr literal corresponding to SAT literal failed: ") + err;
    }

    template <typename B>
    bool Mixin<B>::add_eclause(bools::Clause eclause)
    {
        return this->add_clause(to_clause(move(eclause)));
    }

    template <typename B>
    bool Mixin<B>::add_eclause(bools::Lit ep)
    {
        return this->add_clause(to_lit(move(ep)));
    }

    template <typename B>
    bool Mixin<B>::add_eclause(bools::Lit ep, bools::Lit eq)
    {
        return this->add_clause(to_lit(move(ep)), to_lit(move(eq)));
    }

    template <typename B>
    bool Mixin<B>::add_eclause(bools::Lit ep, bools::Lit eq, bools::Lit er)
    {
        return this->add_clause(to_lit(move(ep)), to_lit(move(eq)), to_lit(move(er)));
    }
}
