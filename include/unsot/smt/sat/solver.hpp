#pragma once

#include "smt/sat.hpp"

namespace unsot::smt::sat::solver {
    template <typename S, typename ConfT> class Crtp;
    template <typename B, typename SolverT> class Solver_mixin;
}

namespace unsot::smt::sat::solver {
    /// General SAT solver interface (according to MiniSAT)
    /// `Lit` stands for whole boolean variable including its value
    template <typename S, typename ConfT>
    class Crtp : public Dynamic<Crtp<S, ConfT>>, public unsot::Crtp<S> {
    public:
        using typename unsot::Dynamic<Crtp<S, ConfT>>::This;
        using typename unsot::Crtp<S>::That;
        using Base = This;

        class Run;

        using Base_conf = ConfT;
        using Var_id = typename Base_conf::Var_id;
        using Lit = typename Base_conf::Lit;

        static_assert(is_constructible_v<Lit, Var_id, bool>);
        /*+
        /// To avoid confusion, `sign` and `is_neg` are explicitly distinguished
        static_assert(is_invocable_r_v<bool, decltype(&Lit::is_neg)>);
        /// `true` means positive and `false` negative polarity (sign)
        static_assert(is_invocable_r_v<bool, decltype(&Lit::sign)>);
        static_assert(is_invocable_r_v<Var_id, decltype(&Lit::var_id)>);
        */

        using Clause = Vector<Lit>;
        using Cnf = Vector<Clause>;

        Crtp()                                                      = default;
        virtual ~Crtp()                                             = default;

        virtual Var_id new_var()                                          = 0;

        virtual size_t vars_size() const noexcept                         = 0;
        virtual size_t clauses_size() const noexcept                      = 0;

        /// Define at least one of these for `Lit`
        virtual void neg(Lit&) const noexcept;
        virtual Lit to_neg(Lit) const noexcept;
        virtual void neg(Clause&) const noexcept;

        virtual bool contains(const Var_id&) const noexcept               = 0;
        virtual void check_contains(const Var_id&) const;

        virtual bool add_cnf(Cnf);
        virtual bool add_clause(Clause)                                   = 0;
        virtual bool add_clause()                                         = 0;
        virtual bool add_clause(Lit)                                      = 0;
        virtual bool add_clause(Lit, Lit)                                 = 0;
        virtual bool add_clause(Lit, Lit, Lit)                            = 0;

        virtual Cnf to_cnf() const                                        = 0;

        virtual ostream& to_dimacs(ostream&, Cnf) const;
        virtual ostream& to_dimacs(ostream&) const;

        /// Removes already satisfied clauses.
        virtual bool simplify()                                           = 0;

        virtual bool solve(const Clause& assumps)                         = 0;
        /// Solve with resource constraints.
        virtual Sat solve_limited(const Clause& assumps)                  = 0;
        virtual bool solve()                                              = 0;
        virtual bool solve(Lit)                                           = 0;
        virtual bool solve(Lit, Lit)                                      = 0;
        virtual bool solve(Lit, Lit, Lit)                                 = 0;

        /// Is solver NOT in a conflicting state?
        virtual bool okay() const                                         = 0;

        virtual Value value(const Var_id&) const                          = 0;
        /// Same as `value` but also considers literal sign
        virtual Value lit_value(const Lit&) const;
        /// Don't care vars are assigned somehow in model
        /// i.e. they are never `unknown` ?
        virtual Value model_value(const Var_id&) const                    = 0;
        virtual Value lit_model_value(const Lit&) const;

        /// Get the model after formula is sat
        virtual const Values& cmodel() const                              = 0;
        const Values& model() const                       { return cmodel(); }
        /// Get conflicting clause after formula is unsat
        virtual const Clause& cconflict() const                           = 0;
        const Clause& conflict() const                 { return cconflict(); }
    };

    template <typename B, typename SolverT>
    class Solver_mixin : public Inherit<B, Solver_mixin<B, SolverT>> {
    public:
        using Inherit = unsot::Inherit<B, Solver_mixin<B, SolverT>>;
        using typename Inherit::This;
        using typename Inherit::That;

        using typename Inherit::Var_id;
        using typename Inherit::Lit;

        using typename Inherit::Clause;
        using typename Inherit::Cnf;

        using Inherit::Inherit;
        virtual ~Solver_mixin()                                     = default;
        Solver_mixin(const Solver_mixin&)                            = delete;
        Solver_mixin& operator =(const Solver_mixin&)                = delete;
        Solver_mixin(Solver_mixin&&)                                = default;
        Solver_mixin& operator =(Solver_mixin&&)                    = default;
    protected:
        struct Access;

        using Solver = SolverT;
        using Solver_ptr = Unique_ptr<Solver>;

        static Solver_ptr new_solver();

        const auto& csolver_ptr() const noexcept       { return _solver_ptr; }
        auto& solver_ptr() noexcept                    { return _solver_ptr; }
        const auto& csolver() const noexcept;
        const auto& solver() const noexcept              { return csolver(); }
        auto& solver() noexcept;
    private:
        Solver_ptr _solver_ptr{new_solver()};
    };
}

namespace unsot::smt::sat::solver {
    template <typename B, typename SolverT>
    /// avoid name clash with a possible final class also named `Solver`
    struct Solver_mixin<B, SolverT>::Access : That::Inherit {
        static Solver_ptr new_solver();

        static const auto& csolver(const That&) noexcept;
        static auto& solver(That&) noexcept;
    };
}

#include "smt/sat/solver.inl"
