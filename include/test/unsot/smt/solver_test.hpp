#pragma once

#include "test.hpp"

#include "smt/solver/bools.hpp"
#include "smt/sat/solver/offline/expr/minisat.hpp"

#include "util/alg.hpp"
#include "util/numeric/alg.hpp"
#include "expr.hpp"
#include "expr/formula.hpp"
#include "expr/var/vars/alg.hpp"

namespace unsot::smt::solver::test {
    using namespace unsot::test;
    using namespace smt;
    using namespace smt::solver;
    using unsot::to_string;

    using sat::solver::offline::expr::Minisat;

    template <typename S>
    struct Test_solver_crtp : Inherit<solver::bools::Crtp<S, Minisat>, Test_solver_crtp<S>> {
        using Inherit = unsot::Inherit<solver::bools::Crtp<S, Minisat>, Test_solver_crtp<S>>;
        using typename Inherit::This;

        using Inherit::Inherit;

        bool eval_bools()
        {
            this->init_solve();
            auto& sat_s = this->sat_solver();
            bool sat = sat_s.solve();

            if (sat) for (auto& vptr : this->cbools()) {
                auto& k = vptr->ckey();
                assert(sat_s.contains(k));
                this->bools().var(k) = sat_s.model_value(sat_s.cvar_id(k)).is_true();
            }

            return sat;
        }

        virtual String to_string() const&
        {
            return this->cbools().to_string();
        }

        bool equals(const This& rhs) const noexcept
        {
            return this->cbools() == rhs.cbools()
                && this->csat_solver().okay() == rhs.csat_solver().okay();
        }
    };

    struct Test_solver : Test_solver_crtp<Test_solver> {};

    ////////////////////////////////////////////////////////////////

    template <typename T>
    struct Vars_input {
        using Value = T;

        Key key;
        bool has_value{false};
        T value{};
        bool has_formula{false};
        Formula formula{};
        bool has_args{false};
        Keys arg_keys{};
        Keys arg_values{};
    };

    template <typename T>
    struct Vars_output {
        using Value = T;

        Key key;
        T value{};
        Formula formula{};
    };

    template <typename T>
    String to_string(const Vars_input<T>& i)
    {
        using unsot::to_string;
        String str(i.key);
        if (i.has_value) str += ":" + to_string(i.value);
        if (i.has_formula) str += i.formula.to_string();
        if (i.has_args) {
            str += "[ " + to_string(i.arg_keys) + "]";
            str += "<-[ " + to_string(i.arg_values) + "]";
        }
        return str;
    }

    template <typename T>
    String to_string(const Vars_output<T>& o)
    {
        using unsot::to_string;
        String str = o.key + ":" + to_string(o.value);
        if (!o.formula.empty()) str += o.formula.to_string();
        return str;
    }

    template <typename T>
    bool operator ==(const Vars_output<T>& rhs, const Vars_output<T>& lhs)
    {
        return rhs.key == lhs.key
            && rhs.value == lhs.value
            && rhs.formula == lhs.formula;
    }

    using Bools_input = Vars_input<Bool>;
    using Bools_output = Vars_output<Bool>;

    using Bools_inputs = Vector<Bools_input>;
    using Bools_outputs = Vector<Bools_output>;

    using Reals_input = Vars_input<Real>;
    using Reals_output = Vars_output<Real>;

    using Reals_inputs = Vector<Reals_input>;
    using Reals_outputs = Vector<Reals_output>;

    ////////////////////////////////////////////////////////////////

    template <typename S = Test_solver, typename InputsT = Bools_inputs,
              typename OutputsT = Bools_outputs>
    struct Vars_case : Inherit<Case<InputsT, OutputsT>> {
        using Inherit = unsot::Inherit<Case<InputsT, OutputsT>>;

        using typename Inherit::Input;
        using typename Inherit::Output;

        using Single_input = typename Input::value_type;
        using Value = typename Single_input::Value;

        static_assert(is_same_v<Value, typename Output::value_type::Value>);

        using Inherit::Inherit;

        Shared_ptr<S> sptr{make_shared<S>()};

        bool contains_bool_condition(const Key& key) const noexcept
        {
            auto& s = *sptr;
            return s.contains(key) && s.contains_bool(key) && s.cbools().contains(key);
        }

        virtual bool contains_condition(const Single_input&, const Key& key) const noexcept
        {
            return contains_bool_condition(key);
        }

        void check_contains_condition(const Single_input& in, const Key& key) const
        {
            using unsot::to_string;
            expect(contains_condition(in, key),
                   "At '"s + to_string(in) + "': 'contains_condition' failed.");
        }

        bool formula_contains_bool_condition(const Single_input& in, const Key& key) const noexcept
        {
            auto& s = *sptr;
            if (!s.contains_bool_fun(key)) return false;
            if (!s.contains_def_bool_fun(in.key))
                return false;
            if (s.contains(in.key) || s.contains_bool(in.key) || s.cbools().contains(in.key)
                || s.contains_var(in.key) || s.contains_bool_var(in.key) || s.contains_bool_fun(in.key))
                return false;

            return true;
        }

        virtual bool formula_contains_condition(const Single_input& in, const Key& key) const noexcept
        {
            return formula_contains_bool_condition(in, key);
        }

        void check_formula_contains_condition(const Single_input& in, const Key& key) const
        {
            using unsot::to_string;
            expect(formula_contains_condition(in, key),
                   "At '"s + to_string(in) + "': 'formula_contains_condition' failed.");
        }

        bool var_contains_bool_condition(const Single_input& in, const Key& key) const noexcept
        {
            auto& s = *sptr;
            if (!s.contains_var(key) || !s.contains_bool_var(key))
                return false;
            if (!s.contains(in.key) || !s.contains_bool(in.key) || !s.cbools().contains(in.key)
                || !s.contains_var(in.key) || !s.contains_bool_var(in.key))
                return false;

            return true;
        }

        virtual bool var_contains_condition(const Single_input& in, const Key& key) const noexcept
        {
            return var_contains_bool_condition(in, key);
        }

        void check_var_contains_condition(const Single_input& in, const Key& key) const
        {
            using unsot::to_string;
            expect(var_contains_condition(in, key),
                   "At '"s + to_string(in) + "': 'var_contains_condition' failed.");
        }

        virtual const var::Ptr& get_ptr(const Single_input&, const Key& key) const
        {
            auto& s = *sptr;
            s.check_contains(key);
            s.check_contains_bool(key);
            solver::check_contains_free_arg_keys<Bool>(s, Formula::cons("and", List(key)));

            return s.cbools().cptr(key);
        }

        bool condition(const Output& expected_, const Output& result_) const override
        {
            int i = 0;
            return all_of(result_, cbegin(expected_),
                          [this, &i](auto& res, auto& exp){
                const auto& in = this->cinput()[i++];
                auto& key = res.key;
                if (key != exp.key) return false;

                check_contains_condition(in, key);
                if (in.has_formula) check_formula_contains_condition(in, key);
                else check_var_contains_condition(in, key);

                auto& vptr = get_ptr(in, key);

                if (in.has_value) {
                    if (res.value != exp.value) return false;
                    if (!in.has_formula) {
                        if (vptr->is_unset()) return false;
                        if (vptr->unlocked()) return false;
                    }
                }
                else {
                    if (vptr->is_set()) return false;
                    if (vptr->locked()) return false;
                }

                if (in.has_formula) {
                    if (res.formula != exp.formula) return false;
                }

                return true;
            });
        }

        virtual void add_var(const Single_input& in)
        {
            auto& s = *sptr;
            if (in.has_value) s.define_bool_var(in.key, in.value);
            else s.declare_bool_var(in.key);
            s.assert_elem(in.key);
        }

        virtual void add_fun(Key& key, Formula& phi, const Single_input& in)
        {
            auto& s = *sptr;
            s.define_bool_fun(in.key, in.formula, in.arg_keys);
            key = s.instantiate_def_bool_fun(in.key, in.arg_values);
            phi = s.expand_def_bool_fun(in.key, in.arg_values);
            expect(phi.initialized(),
                   "Expanded formula is not initialized: "s + phi.to_string());
            s.assert_def_bool_fun(in.key, in.arg_values);
            s.assert_bool("not "s + key);
            s.assert_elem(key);
        }

        Output result() override
        {
            auto& s = *sptr;

            Output out;

            for (auto& i : this->cinput()) {
                /// To also possibly check move constructor and assignment
                auto tmp(move(s));
                s = move(tmp);

                Key k = i.key;
                Value v = i.value;
                Formula f = i.formula;
                assert(!f.initialized());

                if (!i.has_formula) add_var(i);
                else add_fun(k, f, i);

                out.push_back({move(k), v, move(f)});
            }

            if (!this->ignored_result()) for (auto& e : this->expected()) {
                if (!e.formula.empty()) e.formula.virtual_init();
            }

            return out;
        }
    };

    template <typename S = Test_solver> using Bools_case = Vars_case<S>;

    ////////////////////////////////////////////////////////////////

    struct Expr_bools_input {
        Keys bkeys;
        Formula formula;

        Expr_bools_input(Keys bkeys_, Formula phi)
                                 : bkeys(move(bkeys_)), formula(move(phi)) { }

        template <typename S = Test_solver>
        void declare(S& s) const
        {
            for (auto& k : bkeys) {
                s.declare_bool_var(k);
            }
        }

        virtual String to_string() const&
        {
            return keys_to_string() + " " + formula.to_string();
        }

        virtual String keys_to_string() const&
        {
            using unsot::to_string;
            return "[ " + to_string(bkeys) + "]";
        }
    };

    template <typename S = Test_solver,
              typename InputT = Expr_bools_input>
    struct Inst_expr_case : Inherit<Case<InputT, S>> {
        using Inherit = unsot::Inherit<Case<InputT, S>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        virtual void check(const Output& s, const Key& k) const
        {
            expect(s.contains(k),
                   "Solver does not contain the instantiated expression: "s + k);
            expect(valid_sort(s, k), sort_msg(k));
            expect(valid_fun(s, k), fun_msg(k));
            expect(valid_def_fun(s, k), def_fun_msg(k));
        }

        virtual bool valid_sort(const Output& s, const Key& k) const noexcept
        {
            return s.contains_bool(k);
        }

        virtual String sort_msg(const Key& k) const
        {
            return "The instantiated expression is not bool: "s + k;
        }

        virtual bool valid_fun(const Output& s, const Key& k) const noexcept
        {
            return s.contains_bool_fun(k);
        }

        virtual String fun_msg(const Key& k) const
        {
            return "The instantiated expression is not bool function: "s + k;
        }

        virtual bool valid_def_fun(const Output& s, const Key& k) const noexcept
        {
            return !s.contains_def_bool_fun(k);
        }

        virtual String def_fun_msg(const Key& k) const
        {
            return "The instantiated expression collides with its definition: "s + k;
        }

        virtual void eval(Output& s)
        {
            try_eval_all(s.bools());
            try_eval_all(this->expected().bools());
        }

        Output result() override
        {
            Output s;

            this->cinput().declare(s);

            Key k = s.instantiate_expr(this->cinput().formula);

            check(s, k);

            if (!this->ignored_result()) eval(s);

            auto s_ = move(s);
            decltype(s_) s2_;
            s2_ = move(s_);
            return s2_;
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename S = Test_solver, typename InputT = Expr_bools_input>
    struct Assert_expr_case : Inherit<Case<InputT, S>> {
        using Inherit = unsot::Inherit<Case<InputT, S>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        virtual void eval(Output& s)
        {
            s.eval_bools();
        }

        Output result() override
        {
            Output s;

            this->cinput().declare(s);

            s.assert_expr(this->cinput().formula);

            eval(s);
            if (!this->ignored_result()) eval(this->expected());

            auto s_ = move(s);
            decltype(s_) s2_;
            s2_ = move(s_);
            return s2_;
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename S = Test_solver>
    struct Parse_case : Inherit<Case<String, S>> {
        using Inherit = unsot::Inherit<Case<String, S>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        virtual void eval(Output& s)
        {
            s.eval_bools();
        }

        Output result() override
        {
            Output s;

            s.parse(this->cinput());

            eval(s);
            if (!this->ignored_result()) eval(this->expected());

            auto s_ = move(s);
            decltype(s_) s2_;
            s2_ = move(s_);
            return s2_;
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Bounce {
        static inline const String name = "BOUNCE";

        static inline const String input =
            "(echo \"<" + name + ">\")"
            "(set-option :verbosity 3) "
            "#define N 6\n"
            "#define N-1 $d(- #N 1)\n"
            "#for (i 0 #N)"
            "(declare-fun up#i () Bool)"
            "(declare-fun t#i () Real)"
            "(declare-const x#i Real)"
            "#endfor "
            "#for (i 0 #N-1)"
            "(declare-fun x#i##* () Real)"
            "#endfor"
            "(define-fun tau () Real 5)"
            //+ "(define-fun ftau () Real (* tau 0.35))"
            "#define ftau (* tau 0.35)\n"
            "(assert (and "
                "up0"
                "(= t0 0)"
                "(= x0 0)"
                "#for (i 0 #N-1) "
                "#let j $d(+ #i 1) "
                "(= t#j (+ t#i tau))"
                //+ "(=>      up#i  (= x#i##* (+ x#i ftau)))"
                //+ "(=> (not up#i) (= x#i##* (- x#i ftau)))"
                "(=>      up#i  (= x#i##* (+ x#i #ftau)))"
                "(=> (not up#i) (= x#i##* (- x#i #ftau)))"
                "(=> (and      up#i       (< x#i##* tau)  ) (and      up#j  (= x#j x#i##*) ))"
                "(=> (and      up#i  (not (< x#i##* tau)) ) (and (not up#j) (= x#j tau) ))"
                "(=> (and (not up#i)      (> x#i##* 0)    ) (and (not up#j) (= x#j x#i##*) ))"
                "(=> (and (not up#i) (not (> x#i##* 0)  ) ) (and      up#j  (= x#j 0)   ))"
                "#endlet j "
                "#endfor "
            "))"
            ;

        static inline const Keys expected_bkeys = {
            "up0",  "up1",  "up2",  "up3",  "up4",  "up5",  "up6",
        };
        static inline const expr::Bools expected_bvals = {
             true,   true,   true,  false,  false,  false,   true,
        };

        static inline const Keys expected_rkeys = {
            "t0",  "t1",  "t2",  "t3",  "t4",  "t5",  "t6",
            "x0",  "x1",  "x2",  "x3",  "x4",  "x5",  "x6",
            "x0*", "x1*", "x2*", "x3*", "x4*", "x5*",
        };
        static inline const expr::Reals expected_rvals = {
              0.,    5.,   10.,   15.,   20.,   25.,   30.,
              0.,    1.75,  3.5,   5.,    3.25,  1.5,   0.,
              1.75,  3.5,   5.25,  3.25,  1.5,  -0.25,
        };
    };

    struct Branch {
        static inline const String name = "BRANCH";

        static constexpr int N = 10;

        static inline const String input =
            "(echo \"<" + name + ">\")"
            "(set-option :verbosity 3) "
            "(declare-const x0 Real)"
            "(declare-const x1 Real)"
            "#define N " + to_string(N) + "\n"
            "(assert (and "
                "(or #for (i 1 #N) (= x0 #i) #endfor)"
                "(= x0 x1)"
                "(or #for (i 1 #N) (= x1 #i) #endfor)"
                "(>= x1 #N)"
            "))"
            ;

        static inline const Keys expected_bkeys = {};
        static inline const expr::Bools expected_bvals = {};

        static inline const Keys expected_rkeys = {
            "x0",  "x1",
        };
        static inline const expr::Reals expected_rvals = {
              N ,    N ,
        };
    };

    template <typename Input = Bounce, typename S>
    void check_smt(S& s, const Sat& sat)
    {
        const auto& exp_bkeys = Input::expected_bkeys;
        const auto& exp_bvals = Input::expected_bvals;
        const auto& exp_rkeys = Input::expected_rkeys;
        const auto& exp_rvals = Input::expected_rvals;

        cout << endl << "</" << Input::name << ">" << endl;

        expect(sat, "Unexpected unsatisfiable result: "s + to_string(sat));
        expect(s.cmode() == Mode::sat,
               "Solver internal mode does not match the result.");

        for (auto& k : s.cbools().ckeys()) {
            auto& var = s.cbools().cvar(k);
            expect(var.get() ==
                   s.csat_solver().model_value(s.csat_solver().cvar_id(k)).is_true(),
                   "Boolean value does not correspond to SAT model value: "s
                   + var.to_string());
        }

        for_each(exp_bkeys, cbegin(exp_bvals), [&s](auto& k, auto& v){
            auto& var = s.cbools().cvar(k);
            expect(var.get() == v,
                   "Bool value does not correspond to the expected: "s
                   + var.to_string() + " != " + to_string(v));
        });

        for_each(exp_rkeys, cbegin(exp_rvals), [&s](auto& k, auto& v){
            auto& var = s.creals().cvar(k);
            expect(numeric::apx_equal(var.get(), v),
                   "Real value does not correspond to the expected: "s
                   + var.to_string() + " !~ " + to_string(v));
        });
    }

    Sat mode_to_sat(const Mode& mode)
    {
        return mode == Mode::sat ? Sat::sat
             : mode == Mode::unsat ? Sat::unsat
             : Sat::unknown;
    }
}
